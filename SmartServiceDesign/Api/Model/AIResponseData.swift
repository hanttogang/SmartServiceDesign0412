//
//  AiResponseData.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/27/24.
//

import Foundation


struct BrandAI: Decodable {
    let brandName: String

    enum CodingKeys: String, CodingKey {
        case brandName = "brand_name"
    }
}

struct ContractAI: Decodable {
    let contractMonth: String
    let brandIdx: Int
    let contractName: String
    let totalPrice: String
    let brand: BrandAI

    enum CodingKeys: String, CodingKey {
        case contractMonth = "contract_month"
        case brandIdx = "brand_idx"
        case contractName = "contract_name"
        case totalPrice = "total_price"
        case brand
    }
}

struct CorporationAI: Decodable {
    let bizNumber: String
    let corporationName: String
    let total: Int

    enum CodingKeys: String, CodingKey {
        case bizNumber = "biz_number"
        case corporationName = "corporation_name"
        case total
    }
}

struct IdxAI: Decodable {
    let topBrandContracts: [ContractAI]
    let topMarketingContracts: [ContractAI]
    let topTotalContracts: [ContractAI]
    let payingCredit: [CorporationAI]
    let taxPurchase: [CorporationAI]
    let taxSales: [CorporationAI]
    let bankWithdrawal: [CorporationAI]
    let bankDeposit: [CorporationAI]

    enum CodingKeys: String, CodingKey {
        case topBrandContracts = "top_brand_contracts"
        case topMarketingContracts = "top_marketing_contracts"
        case topTotalContracts = "top_total_contracts"
        case payingCredit = "paying_credit"
        case taxPurchase = "tax_purchase"
        case taxSales = "tax_sales"
        case bankWithdrawal = "bank_withdrawal"
        case bankDeposit = "bank_deposit"
    }
}

struct AIResponseData: Decodable {
    let idx: IdxAI
    let title: String
}

