//
//  BrandProject.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/16/24.
//

import Foundation

struct BrandProject {
    let brandProjectIdx: Int
    let brandName: String
    let brandImage: String
    let projectName: String
    let humanCount: Int
    let brandIdx: Int
}
