//
//  ProjectDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/16/24.
//

import Foundation

struct ProjectDataResponse: Codable {
    let result: Bool
    let data: [AdvertisingProjectData]
    let pagination: AdvertisingProjectPagination
}

struct AdvertisingProjectData: Codable {
    let projectIdx: Int
    let brandIdx: Int
    let userIdx: Int
    let projectName: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?
    let humanCount: Int

    enum CodingKeys: String, CodingKey {
        case projectIdx = "project_idx"
        case brandIdx = "brand_idx"
        case userIdx = "user_idx"
        case projectName = "project_name"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
        case humanCount = "human_count"
    }
}

struct AdvertisingProjectPagination: Codable {
    let total: Int
    let currentPage: Int
    let totalPage: Int
    let block: Int
    let currentBlock: Int
    let totalBlock: Int

    enum CodingKeys: String, CodingKey {
        case total
        case currentPage = "current_page"
        case totalPage = "total_page"
        case block
        case currentBlock = "current_block"
        case totalBlock = "total_block"
    }
}
