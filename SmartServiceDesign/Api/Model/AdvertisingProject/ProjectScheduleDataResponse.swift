//
//  ProjectScheduleDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/16/24.
//

import Foundation

struct ProjectScheduleDataResponse: Codable {
    let result: Bool
    let data: [ProjectDetailScheduleData]
    let pagination: SchedulPagination
    
    enum CodingKeys: String, CodingKey {
        case result
        case data
        case pagination
    }
}



struct ProjectDetailScheduleData: Codable {
    let scheduleIdx: Int
    let projectIdx: Int
    let scheduleName: String
    let oneLineExplain: String
    let scheduleStartDt: String
    let scheduleEndDt: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?
    let project: ProjectDetailSchedul
    let taskCount: Int
    let humanCount: Int
    
    enum CodingKeys: String, CodingKey {
        case scheduleIdx = "schedule_idx"
        case projectIdx = "project_idx"
        case scheduleName = "schedule_name"
        case oneLineExplain = "one_line_explain"
        case scheduleStartDt = "schedule_start_dt"
        case scheduleEndDt = "schedule_end_dt"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
        case project
        case taskCount = "task_count"
        case humanCount = "human_count"
    }
}

struct ProjectDetailSchedul: Codable {
    let projectIdx: Int
    let brandIdx: Int
    let userIdx: Int
    let projectName: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?
    
    enum CodingKeys: String, CodingKey {
        case projectIdx = "project_idx"
        case brandIdx = "brand_idx"
        case userIdx = "user_idx"
        case projectName = "project_name"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
    }
}

struct SchedulPagination: Codable {
    let total: Int
    let currentPage: Int
    let totalPage: Int
    let block: Int
    let currentBlock: Int
    let totalBlock: Int
    
    enum CodingKeys: String, CodingKey {
        case total
        case currentPage = "current_page"
        case totalPage = "total_page"
        case block
        case currentBlock = "current_block"
        case totalBlock = "total_block"
    }
}
