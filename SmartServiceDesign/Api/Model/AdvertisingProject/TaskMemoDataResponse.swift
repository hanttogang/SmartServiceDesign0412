//
//  TaskMemoDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/24/24.
//

import Foundation
import Foundation

struct TaskMemoDataResponse: Codable {
    let result: Bool
    let data: [TaskMemoData]
    let pagination: PaginationDataInMemo
}

struct TaskMemoData: Codable {
    let taskMemoIdx: Int
    let taskIdx: Int
    let userIdx: Int
    let content: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?
    let task: TaskInMemo
    let user: UserInMemo
    
    enum CodingKeys: String, CodingKey {
        case taskMemoIdx = "task_memo_idx"
        case taskIdx = "task_idx"
        case userIdx = "user_idx"
        case content
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
        case task
        case user
    }
}

struct TaskInMemo: Codable {
    let taskIdx: Int
    let scheduleIdx: Int
    let taskName: String
    let oneLineExplain: String
    let taskStartDt: String
    let taskEndDt: String
    let taskStatus: String
    let hideOrNot: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?
    
    enum CodingKeys: String, CodingKey {
        case taskIdx = "task_idx"
        case scheduleIdx = "schedule_idx"
        case taskName = "task_name"
        case oneLineExplain = "one_line_explain"
        case taskStartDt = "task_start_dt"
        case taskEndDt = "task_end_dt"
        case taskStatus = "task_status"
        case hideOrNot = "hide_or_not"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
    }
}

struct UserInMemo: Codable {
    let userIdx: Int
    let userEmail: String
    let userName: String
    let userType: String
    let userPhone: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?
    let humanResource: HumanResourceInMemo?
    
    enum CodingKeys: String, CodingKey {
        case userIdx = "user_idx"
        case userEmail = "user_email"
        case userName = "user_name"
        case userType = "user_type"
        case userPhone = "user_phone"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
        case humanResource = "human_resource"
    }
}

struct HumanResourceInMemo: Codable {
    let humanResourceIdx: Int
    let userIdx: Int
    let masterUserIdx: Int
    let position: String
    let department: String
    let userName: String
    let userType: String?
    let userEmail: String
    let userImage: String
    let userPhone: String
    let oneLineExplain: String?
    let manageType: String?
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?
    
    enum CodingKeys: String, CodingKey {
        case humanResourceIdx = "human_resource_idx"
        case userIdx = "user_idx"
        case masterUserIdx = "master_user_idx"
        case position
        case department
        case userName = "user_name"
        case userType = "user_type"
        case userEmail = "user_email"
        case userImage = "user_image"
        case userPhone = "user_phone"
        case oneLineExplain = "one_line_explain"
        case manageType = "manage_type"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
    }
}

struct PaginationDataInMemo: Codable {
    let total: Int
    let currentPage: Int
    let totalPage: Int
    let block: Int
    let currentBlock: Int
    let totalBlock: Int
    
    enum CodingKeys: String, CodingKey {
        case total
        case currentPage = "current_page"
        case totalPage = "total_page"
        case block
        case currentBlock = "current_block"
        case totalBlock = "total_block"
    }
}
