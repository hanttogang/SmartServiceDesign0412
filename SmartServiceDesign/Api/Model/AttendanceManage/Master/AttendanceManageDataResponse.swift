//
//  AttendanceManageDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/27/24.
//

import Foundation
// Main Response
struct AttendanceManageDataResponse: Codable {
    let result: Bool
    let data: [WorkDataForAdmin]
}

// Work Data
struct WorkDataForAdmin: Codable {
    let workIdx: Int
    let userIdx: Int
    let workType: String
    let startWorkDt, firstCreateDt, lastUpdateDt: String
    let endWorkDt: String?
    let deleteDt: String?
    let user: UserForAdmin
    let workingTime: String?

    enum CodingKeys: String, CodingKey {
        case workIdx = "work_idx"
        case userIdx = "user_idx"
        case workType = "work_type"
        case startWorkDt = "start_work_dt"
        case endWorkDt = "end_work_dt"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
        case user
        case workingTime = "working_time"
    }
}

// User Data
struct UserForAdmin: Codable {
    let userIdx: Int
    let userEmail: String
    let userName: String
    let userType: String
    let userPhone: String
    let firstCreateDt, lastUpdateDt: String
    let deleteDt: String?

    enum CodingKeys: String, CodingKey {
        case userIdx = "user_idx"
        case userEmail = "user_email"
        case userName = "user_name"
        case userType = "user_type"
        case userPhone = "user_phone"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
    }
}

