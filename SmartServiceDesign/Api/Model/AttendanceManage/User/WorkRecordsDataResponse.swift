//
//  WorkRecordsDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/27/24.
//

import Foundation

enum RecordsUserWorkType: String, Codable {
    case 출근 = "출근"
    case 퇴근 = "퇴근"
    // Add more cases if there are other work types
}

struct RecordsWorkData: Codable {
    let workIdx: Int
    let userIdx: Int
    let workType: RecordsUserWorkType
    let startWorkDt: String?
    let endWorkDt: String?
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?
    let workingTime: String

    enum CodingKeys: String, CodingKey {
        case workIdx = "work_idx"
        case userIdx = "user_idx"
        case workType = "work_type"
        case startWorkDt = "start_work_dt"
        case endWorkDt = "end_work_dt"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
        case workingTime = "working_time"
    }
}


struct WorkRecordsDataResponse: Codable {
    let result: Bool
    let data: RecordsWorkData
}
