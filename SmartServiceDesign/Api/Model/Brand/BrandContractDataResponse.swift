//
//  BrandContractResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/5/24.
//

import Foundation

struct BrandContractDataResponse: Codable {
    let result: Bool
    let data: [BrandContractData]
    let pagination: BrandContractPagination
}

struct BrandContractData: Codable {
    let brandContractIdx: Int
    let brandIdx: Int
    let contractName: String
    let oneLineExplan: String
    let contractStartDt: String
    let contractEndDt: String
    let contractPrice: Int
    let contractStatus: String
    let contractImage: String
    let brandImage: String?
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?
    let brand: Brand
    let brandCost: [BrandCost]  // 추가된 필드

    enum CodingKeys: String, CodingKey {
        case brandContractIdx = "brand_contract_idx"
        case brandIdx = "brand_idx"
        case contractName = "contract_name"
        case oneLineExplan = "one_line_explan"
        case contractStartDt = "contract_start_dt"
        case contractEndDt = "contract_end_dt"
        case contractPrice = "contract_price"
        case contractStatus = "contract_status"
        case contractImage = "contract_image"
        case brandImage = "brand_image"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
        case brand
        case brandCost = "brand_cost"  // 추가된 필드
    }
}

// 추가된 구조체
struct BrandCost: Codable {
    let brandCostIdx: Int
    let brandContractIdx: Int
    let costType: String
    let costName: String
    let costAmount: Int
    let baseMonth: String
    let memo: String?
    let processingStatus: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?

    enum CodingKeys: String, CodingKey {
        case brandCostIdx = "brand_cost_idx"
        case brandContractIdx = "brand_contract_idx"
        case costType = "cost_type"
        case costName = "cost_name"
        case costAmount = "cost_amount"
        case baseMonth = "base_month"
        case memo
        case processingStatus = "processing_status"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
    }
}

struct Brand: Codable {
    let brandIdx: Int
    let userIdx: Int
    let brandName: String
    let registrationNumber: String
    let buisnessType: String
    let address: String
    let addressDetail: String
    let representativeName: String
    let representativePhone: String
    let representativeEmail: String
    let managerName: String
    let managerPhone: String
    let managerEmail: String
    let manageType: String
    let brandImage: String
    let registrationImage: String
    let bankbookCopy: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?

    enum CodingKeys: String, CodingKey {
        case brandIdx = "brand_idx"
        case userIdx = "user_idx"
        case brandName = "brand_name"
        case registrationNumber = "registration_number"
        case buisnessType = "buisness_type"
        case address
        case addressDetail = "address_detail"
        case representativeName = "representative_name"
        case representativePhone = "representative_phone"
        case representativeEmail = "representative_email"
        case managerName = "manager_name"
        case managerPhone = "manager_phone"
        case managerEmail = "manager_email"
        case manageType = "manage_type"
        case brandImage = "brand_image"
        case registrationImage = "registration_image"
        case bankbookCopy = "bankbook_copy"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
    }
}

struct BrandContractPagination: Codable {
    let total: Int
    let currentPage: Int
    let totalPage: Int
    let block: Int
    let currentBlock: Int
    let totalBlock: Int

    enum CodingKeys: String, CodingKey {
        case total
        case currentPage = "current_page"
        case totalPage = "total_page"
        case block
        case currentBlock = "current_block"
        case totalBlock = "total_block"
    }
}
