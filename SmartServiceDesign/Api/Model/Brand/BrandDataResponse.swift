//
//  BrandModel.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/2/24.
//

import Foundation

struct BrandDataResponse: Codable {
    let data: [BrandData]
    let pagination: BrandPagination
    let result: Bool}

struct BrandData: Codable {
    let address, addressDetail, bankbookCopy: String
    let brandIdx: Int
    let brandImage, brandName, buisnessType: String
    let deleteDt: String?
    let firstCreateDt, lastUpdateDt, manageType: String
    let managerName: String?
    let managerEmail, managerPhone: String
    let registrationImage, registrationNumber: String
    let representativeEmail, representativeName, representativePhone: String
    let userIdx: Int

    enum CodingKeys: String, CodingKey {
        case address
        case addressDetail = "address_detail"
        case bankbookCopy = "bankbook_copy"
        case brandIdx = "brand_idx"
        case brandImage = "brand_image"
        case brandName = "brand_name"
        case buisnessType = "buisness_type"
        case deleteDt = "delete_dt"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case manageType = "manage_type"
        case managerEmail = "manager_email"
        case managerName = "manager_name"
        case managerPhone = "manager_phone"
        case registrationImage = "registration_image"
        case registrationNumber = "registration_number"
        case representativeEmail = "representative_email"
        case representativeName = "representative_name"
        case representativePhone = "representative_phone"
        case userIdx = "user_idx"
    }
}

struct BrandPagination: Codable {
    let block, currentBlock, currentPage, total: Int
    let totalBlock, totalPage: Int

    enum CodingKeys: String, CodingKey {
        case block
        case currentBlock = "current_block"
        case currentPage = "current_page"
        case total
        case totalBlock = "total_block"
        case totalPage = "total_page"
    }
}
