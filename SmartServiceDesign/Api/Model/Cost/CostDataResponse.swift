//
//  CostDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 3/5/24.
//

import Foundation

struct CostDataResponse: Codable {
    let contractPrice: Int
    let totalCost: Int
    let costsByType: [String: Int]

    enum CodingKeys: String, CodingKey {
        case contractPrice = "contract_price"
        case totalCost = "total_cost"
        case costsByType = "costs_by_type"
    }
}
