//
//  MarketingCostDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 3/5/24.
//

import Foundation

struct MarketingCostDataResponse: Codable {
    let result: Bool
    let data: [MarketingCostData]
    let pagination: MarketingCostPagination

    enum CodingKeys: String, CodingKey {
        case result = "result"
        case data = "data"
        case pagination = "pagination"
    }
}

struct MarketingCostData: Codable {
    let marketingCostIdx: Int
    let marketingContractIdx: Int
    let costType: String
    let costName: String
    let costAmount: Int
    let baseMonth: String
    let memo: String?
    let processingStatus: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?

    enum CodingKeys: String, CodingKey {
        case marketingCostIdx = "marketing_cost_idx"
        case marketingContractIdx = "marketing_contract_idx"
        case costType = "cost_type"
        case costName = "cost_name"
        case costAmount = "cost_amount"
        case baseMonth = "base_month"
        case memo = "memo"
        case processingStatus = "processing_status"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
    }
}

struct MarketingCostPagination: Codable {
    let total: Int
    let currentPage: Int
    let totalPage: Int
    let block: Int
    let currentBlock: Int
    let totalBlock: Int

    enum CodingKeys: String, CodingKey {
        case total = "total"
        case currentPage = "current_page"
        case totalPage = "total_page"
        case block = "block"
        case currentBlock = "current_block"
        case totalBlock = "total_block"
    }
}
