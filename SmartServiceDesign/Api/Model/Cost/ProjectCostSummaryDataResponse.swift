//
//  ProjectCostDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 3/5/24.
//

import Foundation

struct ProjectCostSummaryDataResponse: Codable {
    
    let totalCost: Int
    let costsByType: [String: Int]

    enum CodingKeys: String, CodingKey {
        
        case totalCost = "total_cost"
        case costsByType = "costs_by_type"
    }
}
