//
//  AllEmployeeCreatorDataREsponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/17/24.
//

import Foundation


import Foundation

struct AllEmployeeCreatorDataResponse: Codable {
    let result: Bool
    let data: [AllEmployeeCreatorData]
    let pagination: AllEmployeeCreatorPagination
}

struct AllEmployeeCreatorData: Codable {
    let humanResourceIdx: Int
    let userIdx: Int
    let masterUserIdx: Int
    let position: String?
    let department: String?
    let userName: String
    let userType: String?
    let userEmail: String
    let userImage: String
    let userPhone: String
    let oneLineExplain: String?
    let manageType: String?
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?
    let user: AllEmployeeCreator
    
    enum CodingKeys: String, CodingKey {
        case humanResourceIdx = "human_resource_idx"
        case userIdx = "user_idx"
        case masterUserIdx = "master_user_idx"
        case position
        case department
        case userName = "user_name"
        case userType = "user_type"
        case userEmail = "user_email"
        case userImage = "user_image"
        case userPhone = "user_phone"
        case oneLineExplain = "one_line_explain"
        case manageType = "manage_type"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
        case user
    }
}


struct AllEmployeeCreator: Codable {
    let userIdx: Int
    let userEmail: String
    let userName: String
    let userType: String
    let userPhone: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?

    enum CodingKeys: String, CodingKey {
        case userIdx = "user_idx"
        case userEmail = "user_email"
        case userName = "user_name"
        case userType = "user_type"
        case userPhone = "user_phone"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
    }
}


struct AllEmployeeCreatorPagination: Codable {
    let total: Int
    let currentPage: Int
    let totalPage: Int
    let block: Int
    let currentBlock: Int
    let totalBlock: Int

    enum CodingKeys: String, CodingKey {
        case total
        case currentPage = "current_page"
        case totalPage = "total_page"
        case block
        case currentBlock = "current_block"
        case totalBlock = "total_block"
    }
}
