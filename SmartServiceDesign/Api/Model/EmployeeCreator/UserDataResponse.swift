//
//  UserDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/11/24.
//

//      직원/크리에이터 등록시 회원가입 되어있지만 직원/크리에이터로 등록되어있지 않은 유저 정보를 조회하기 위한 데이터입니다.
import Foundation

struct UserDataResponse: Codable {
    let result: Bool
    let data: [User]
    let pagination: UserPagination
}

struct User: Codable {
    let userIdx: Int
    let userEmail: String
    let userName: String
    let userType: String
    let userPhone: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?

    enum CodingKeys: String, CodingKey {
        case userIdx = "user_idx"
        case userEmail = "user_email"
        case userName = "user_name"
        case userType = "user_type"
        case userPhone = "user_phone"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
    }
}


struct UserPagination: Codable {
    let total: Int
    let currentPage: Int
    let totalPage: Int
    let block: Int
    let currentBlock: Int
    let totalBlock: Int

    enum CodingKeys: String, CodingKey {
        case total
        case currentPage = "current_page"
        case totalPage = "total_page"
        case block
        case currentBlock = "current_block"
        case totalBlock = "total_block"
    }
}

