//
//  LoginDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/7/24.
//

import Foundation

struct LoginDataResponse: Codable {
    let result: Bool
    let token: String
    let data: UserData
}

struct UserData: Codable {
    
    let userEmail: String
    let userIdx: Int
    let userName: String
    let userPhone: String
    let userType: String

    enum CodingKeys: String, CodingKey {
        
        case userEmail = "user_email"
        case userIdx = "user_idx"
        case userName = "user_name"
        case userPhone = "user_phone"
        case userType = "user_type"
    }
}

struct SuccessResult: Codable {
    let data: UserData
    let result: Int
    let token: String
}
