//
//  ChannelContractResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/10/24.
//
import Foundation

struct ChannelContractDataResponse: Codable {
    let result: Bool
    let data: [ChannelContractData]
    let pagination: ChannelPagination
}

struct ChannelContractData: Codable {
    let marketingContractIdx: Int
    let marketingIdx: Int
    let contractName: String
    let oneLineExplan: String
    let contractStartDt: String
    let contractEndDt: String
    let contractPrice: Int
    let contractStatus: String
    let contractImage: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?
    let marketing: MarketingChannel
    let marketingCost: [MarketingCost]

    enum CodingKeys: String, CodingKey {
        case marketingContractIdx = "marketing_contract_idx"
        case marketingIdx = "marketing_idx"
        case contractName = "contract_name"
        case oneLineExplan = "one_line_explan"
        case contractStartDt = "contract_start_dt"
        case contractEndDt = "contract_end_dt"
        case contractPrice = "contract_price"
        case contractStatus = "contract_status"
        case contractImage = "contract_image"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
        case marketing = "marketing"
        case marketingCost = "marketing_cost"
    }
}

struct MarketingChannel: Codable {
    let marketingIdx: Int
    let userIdx: Int
    let channelName: String
    let oneLineExplain: String
    let managerName: String
    let managerPhone: String
    let managerEmail: String
    let manageType: String
    let representativeImage: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?

    enum CodingKeys: String, CodingKey {
        case marketingIdx = "marketing_idx"
        case userIdx = "user_idx"
        case channelName = "channel_name"
        case oneLineExplain = "one_line_explain"
        case managerName = "manager_name"
        case managerPhone = "manager_phone"
        case managerEmail = "manager_email"
        case manageType = "manage_type"
        case representativeImage = "representative_image"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
    }
}

struct MarketingCost: Codable {
    let marketingCostIdx: Int
    let marketingContractIdx: Int
    let costType: String
    let costName: String
    let costAmount: Int
    let baseMonth: String
    let memo: String?
    let processingStatus: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?

    enum CodingKeys: String, CodingKey {
        case marketingCostIdx = "marketing_cost_idx"
        case marketingContractIdx = "marketing_contract_idx"
        case costType = "cost_type"
        case costName = "cost_name"
        case costAmount = "cost_amount"
        case baseMonth = "base_month"
        case memo = "memo"
        case processingStatus = "processing_status"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
    }
}

struct ChannelPagination: Codable {
    let total: Int
    let currentPage: Int
    let totalPage: Int
    let block: Int
    let currentBlock: Int
    let totalBlock: Int

    enum CodingKeys: String, CodingKey {
        case total = "total"
        case currentPage = "current_page"
        case totalPage = "total_page"
        case block = "block"
        case currentBlock = "current_block"
        case totalBlock = "total_block"
    }
}
