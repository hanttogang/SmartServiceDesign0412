//
//  MarketingDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/10/24.
//

import Foundation

import Foundation

struct MarketingDataResponse: Codable {
    let data: [MarketingData]
    let pagination: MarketingPagination
    let result: Bool
}

struct MarketingData: Codable {
    let marketingIdx, userIdx: Int
    let channelName, oneLineExplain, managerName: String
    let managerPhone, managerEmail, manageType: String
    let representativeImage: String
    let firstCreateDt, lastUpdateDt: String
    let deleteDt: String?

    enum CodingKeys: String, CodingKey {
        case marketingIdx = "marketing_idx"
        case userIdx = "user_idx"
        case channelName = "channel_name"
        case oneLineExplain = "one_line_explain"
        case managerName = "manager_name"
        case managerPhone = "manager_phone"
        case managerEmail = "manager_email"
        case manageType = "manage_type"
        case representativeImage = "representative_image"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
    }
}

struct MarketingPagination: Codable {
    let total, currentPage, totalPage, block: Int
    let currentBlock, totalBlock: Int

    enum CodingKeys: String, CodingKey {
        case total
        case currentPage = "current_page"
        case totalPage = "total_page"
        case block
        case currentBlock = "current_block"
        case totalBlock = "total_block"
    }
}
