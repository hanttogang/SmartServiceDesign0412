//
//  BankListDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 3/4/24.
//

import Foundation

struct BankListDataResponse: Codable {
    let result: Bool
    let data: [BankListData]
}

struct BankListData: Codable {
    let code: String
    let name: String
}
