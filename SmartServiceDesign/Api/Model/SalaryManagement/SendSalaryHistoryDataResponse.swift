//
//  SendSalaryHistoryDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 3/5/24.
//

import Foundation

struct SendSalaryHistoryDataResponse: Codable {
    enum CodingKeys: String, CodingKey {
        case result
        case data
        case pagination
    }
    
    let result: Bool
    let data: [SendSalaryHistoryData]
    let pagination: SendSalaryHistoryPagination
}

struct SendSalaryHistoryData: Codable {
    enum CodingKeys: String, CodingKey {
        case salaryTransferIdx = "salary_transfer_idx"
        case salaryIdx = "salary_idx"
        case salaryTransferName = "salary_transfer_name"
        case transferDate = "transfer_date"
        case transferAmount = "transfer_amount"
        case memo
        case transferStatus = "transfer_status"
        case proofImage = "proof_image"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
    }
    
    let salaryTransferIdx: Int
    let salaryIdx: Int
    let salaryTransferName: String
    let transferDate: String
    let transferAmount: Int
    let memo: String
    let transferStatus: String
    let proofImage: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?
}

struct SendSalaryHistoryPagination: Codable {
    enum CodingKeys: String, CodingKey {
        case total
        case currentPage = "current_page"
        case totalPage = "total_page"
        case block
        case currentBlock = "current_block"
        case totalBlock = "total_block"
    }
    
    let total: Int
    let currentPage: Int
    let totalPage: Int
    let block: Int
    let currentBlock: Int
    let totalBlock: Int
}

//enum SendSalaryHistoryTransferStatus: String, Codable {
//    case waiting = "대기"
//    case completed = "완료"
//}

