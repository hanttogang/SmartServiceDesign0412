//
//  UserSalaryDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/29/24.
//

import Foundation

// 모든 필드를 포함하는 DataResponse 구조체
struct UserSalaryDataResponse: Codable {
    let result: Bool
    let data: [UserSalaryDataItem]
    let pagination: UserSalaryPagination
}

// data 배열의 각 아이템을 표현하는 DataItem 구조체
struct UserSalaryDataItem: Codable {
    let salaryIdx: Int
    let userIdx: Int
    let amount: Int
    let perMonth: Int
    let bankName: String
    let accountNumber: String
    let accountCopy: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?

    enum CodingKeys: String, CodingKey {
        case salaryIdx = "salary_idx"
        case userIdx = "user_idx"
        case amount
        case perMonth = "per_month"
        case bankName = "bank_name"
        case accountNumber = "account_number"
        case accountCopy = "account_copy"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
    }
}

// pagination을 표현하는 구조체
struct UserSalaryPagination: Codable {
    let total: Int
    let currentPage: Int
    let totalPage: Int
    let block: Int
    let currentBlock: Int
    let totalBlock: Int

    enum CodingKeys: String, CodingKey {
        case total
        case currentPage = "current_page"
        case totalPage = "total_page"
        case block
        case currentBlock = "current_block"
        case totalBlock = "total_block"
    }
}
