//
//  UserSalaryDetailDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 3/4/24.
//

import Foundation

struct UserSalaryDetailDataResponse: Codable {
    let result: Bool
    let data: [DetailUserSalaryData]
    let pagination: DetailUserPaginationData
}

struct DetailUserSalaryData: Codable {
    let salary_idx: Int
    let user_idx: Int
    let amount: Int
    let per_month: Int
    let bank_name: String
    let account_number: String
    let account_copy: String
    let first_create_dt: String
    let last_update_dt: String
    let delete_dt: String?
}

struct DetailUserPaginationData: Codable {
    let total: Int
    let current_page: Int
    let total_page: Int
    let block: Int
    let current_block: Int
    let total_block: Int
}
