//
//  BankPaymentHistoryDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/8/24.
//

import Foundation

struct BankPaymentHistoryDataResponse: Codable {
    let result: Bool
    let data: [BankData]
}

struct BankData: Codable, Hashable {
    let accountNumber: String
    let transactionType: String
    let deposit: Int
    let withdraw: Int
    let transactionDate: String
    let transactionTime: String

    enum CodingKeys: String, CodingKey {
        case accountNumber = "account_number"
        case transactionType = "transaction_type"
        case deposit
        case withdraw
        case transactionDate = "transaction_date"
        case transactionTime = "transaction_time"
    }
}
