//
//  BillResponseData.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/6/24.
//

import Foundation

struct BillDataResponse: Codable {
    let result: Bool
    let data: [BillData]
}

struct BillData: Codable, Hashable {
    let producerName: String
    let reciverName: String
    let amountTotal: Int
    let amountSupply: Int
    let amountTax: Int
    let producerBuisnessNumber: String
    let reciverBuisnessNumber: String
    let issueDate: String

    enum CodingKeys: String, CodingKey {
        case producerName = "producer_name"
        case reciverName = "reciver_name"
        case amountTotal = "amount_total"
        case amountSupply = "amount_supply"
        case amountTax = "amount_tax"
        case producerBuisnessNumber = "producer_buisness_number"
        case reciverBuisnessNumber = "reciver_buisness_number"
        case issueDate = "issue_date"
    }
}

