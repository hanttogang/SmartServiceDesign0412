//
//  CardDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/6/24.
//

import Foundation

struct CardDataResponse: Codable {
    let result: Bool
    let data: [CardData]
}

struct CardData: Codable, Hashable {
    let cardName: String
    let cardNumber: String
    let approvalNumber: String
    let status: String
    let approvalDate: String
    let approvalTime: String
    let approvalAmountTotal: Int
    let approvalAmount: Int
    let vat: Int
    let storeName: String
    let storeAddress: String

    enum CodingKeys: String, CodingKey {
        case cardName = "card_name"
        case cardNumber = "card_number"
        case approvalNumber = "approval_number"
        case status
        case approvalDate = "approval_date"
        case approvalTime = "approval_time"
        case approvalAmountTotal = "approval_amount_total"
        case approvalAmount = "approval_amount"
        case vat
        case storeName = "store_name"
        case storeAddress = "store_address"
    }
}
