//
//  ContentView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/11/23.
//

import SwiftUI
import UIKit
import Alamofire
import CoreData
import WebKit

struct ContentView: View {
    
    @State var isContentReady: Bool = false
    
    @EnvironmentObject var loginData: LoginData
    @EnvironmentObject var menuViewModel: MenuViewModel
    @EnvironmentObject var myWebVM: WebViewModel
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    
    @State private var viewCaptureMode: Bool = false
    @State private var viewCaptureImage: UIImage? = nil
    @State private var viewCaptureImageString: String = ""
    
    private let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    private let defaultUrl = "\(ApiClient.BASE_URL)"
    
    @State private var isApiLoading: Bool = false
    
    let webView = WKWebView()
    
    
    var body: some View{
        
        ZStack{
            
            if (isApiLoading){
                
                ProgressView()
                    .zIndex(3)
                
            }
            
            VStack{
//                Text("ContentView")
                if loginData.isLogin{
                    
//                    if loginData.userType == "master"{
                        MainContainer()
//                    }else{
//                        
//                        ChatMainView()
//                    }
                    
                }else{
                    
                    LoginView()
                    
                }
                
                
                
                
            }
            
            if viewCaptureMode{
                
            
                MainMarketingProjectManagementView()
                
                VStack(spacing: 0){

                    Spacer()
                    
                    ZStack{
                        
                        HStack{
                            Spacer()
                            
                            Circle()
                                .frame(width: 50, height: 50)
                                .foregroundColor(.blue)
                                .overlay{
                                    Button(action: {
//                                        withAnimation {
//                                            
//                                        }
                                        viewCaptureImage = viewCapture()
                                        loadCaptureImage()
                                        print("CaptureButtonClick: \(myWebVM.chatImage)")
                                        
                                        
                                        
                                        
                                        
                                    }) {
                                        
                                        HStack(spacing: 0){
                                            Image(systemName: "camera.viewfinder")
                                                .foregroundColor(.white)
                                        }
                                        .padding()
                                        
                                    }
                                }
                                .padding()
                                .padding(.bottom, screenHeight/16.916)//48
                        }
                        
                    }
                }
            }
            
            
            
            if !isContentReady {
                SplashView()
                    .background(.white)
                    .edgesIgnoringSafeArea(.all)
                
            }
            
            
            
            
        }
        .onReceive(myWebVM.captureMode, perform: { myWebVMcaptureMode in
            
            viewCaptureMode = myWebVMcaptureMode
            
        })
        .onAppear{
            // 현재로부터 1.5초 후에 execute 의 코드 실행
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                
                isContentReady = true
            })
        }
        
        
    }

    
    func viewCapture() -> UIImage? {
        // 현재 윈도우 참조 생성
        guard let window = UIApplication.shared.keyWindow else { return nil }

        // 렌더링 콘텍스트 생성
        UIGraphicsBeginImageContextWithOptions(window.bounds.size, false, 0.0)

        // 현재 윈도우의 레이어를 콘텍스트에 그림
        window.drawHierarchy(in: window.bounds, afterScreenUpdates: false)

        // 캡쳐된 이미지 가져오기
        let image = UIGraphicsGetImageFromCurrentImageContext()

        // 렌더링 콘텍스트 종료
        UIGraphicsEndImageContext()

        return image
    }
    
    func loadCaptureImage() {
        
        if viewCaptureImage != nil{
            
            print("옵셔널 벗긴 값: \(viewCaptureImage!)")
            
            isApiLoading = true
//            
            uploadImage(image: viewCaptureImage!, imageType: "chat")
//            print(registrationCustomerBrandData.bankbookCopy)
        }
        
    }
    
    //Api 를통해 필요한 값 호출 (url, key 값)
    private func getApiCommonChatImageURL(imageType: String, completion: @escaping (String?, String?) -> Void) {
        
        let headers: HTTPHeaders = [
            "accept": "application/json",
            "Authorization": "Bearer \(loginData.token)"
        ]
        
        let parameters: Parameters = [
            "chat_room_idx": myWebVM.chatRoomIdx,
            "mimetype": "image/png",
            "extension": "png"
        ]
        
        AF.request("\(defaultUrl)/api/common/chat/image/url", method: .get, parameters: parameters, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    let url = JSON["data"] as? String
                    let key = JSON["key"] as? String
                    completion(url, key)
                }
            case .failure(let error):
                print(error)
                completion(nil, nil)
            }
        }
    }
    
    //사진 선택시 uploadImag 함수를 호출
    private func uploadImage(image: UIImage, imageType: String) {
        
        getApiCommonChatImageURL(imageType: imageType) { (url, key) in
            
            guard let preSignedUrl = url else {
                print("Failed to get pre-signed URL.")
                return
            }
            
            guard let imageData = image.pngData() else {
                    print("Could not convert image to data.")
                    return
                }
                
                AF.upload(imageData, to: url!, method: .put).response { response in
                    switch response.result {
                    case .success:
                        print("Image uploaded successfully.")
                        
                        isApiLoading = false
                        
                    case .failure(let error):
                        print("Failed to upload image: \(error)")
                    }
                }
            
            
            guard let imageData = image.jpegData(compressionQuality: 1.0) else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: preSignedUrl).response { response in
                if let error = response.error {
                    print("Image upload failed with error: \(error)")
                } else {
                    print("Image uploaded successfully.")
                    switch imageType {
                    case "chat":
                        viewCaptureImageString = key!
                        
                        print("\(viewCaptureImageString)")
                        
                        
                        sendImage()
                        
                        
                        
                    default:
                        print("Unknown image type: \(imageType)")
                    }
                }
            }
        }
    }
    
    private func sendImage(){
        
        self.myWebVM.captureMode.send(false)
        myWebVM.captureModeBoolean = false
        
        myWebVM.captureImageString.send(viewCaptureImageString)

        print("called func sendImage")
        
        
    }
    
    
}

#Preview {
    ContentView().environmentObject(LoginData()).environmentObject(WebViewModel())
}

