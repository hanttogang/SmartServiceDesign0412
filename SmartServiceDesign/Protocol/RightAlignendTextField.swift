//
//  RightAlignendTextField.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/21/23.
//


import Foundation
import SwiftUI


struct RightAlignedTextField: UIViewRepresentable {
    @Binding var text: String

    func makeUIView(context: Context) -> UITextField {
        let textField = UITextField()
        textField.textAlignment = .right
        textField.autocapitalizationType = .none
        textField.textColor = UIColor(named: "color00000040")
        
        return textField
    }

    func updateUIView(_ uiView: UITextField, context: Context) {
        uiView.text = text
        uiView.addTarget(context.coordinator, action: #selector(Coordinator.textFieldDidChange(_:)), for: .editingChanged)
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    class Coordinator: NSObject, UITextFieldDelegate {
        var parent: RightAlignedTextField

        init(_ textField: RightAlignedTextField) {
            self.parent = textField
        }

        @objc func textFieldDidChange(_ textField: UITextField) {
            parent.text = textField.text ?? ""
        }
    }
}

