//
//  MyWebView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/31/24.
//


import SwiftUI
import WebKit
import Combine

//uikit 의 uiView 를 사용할 수 있게 함
// UIViewControllerRepresentable
struct MyWebView: UIViewRepresentable {
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(self)
    }
    
    
    @EnvironmentObject var viewModel: WebViewModel
    
    var urlToLoad: String
    
    //리프레시 컨트롤 헬퍼 클래스 만들기
//    let refreshHelper = WebViewRefreshControlHelper()
    
    
    // 뷰를 만들기
    func makeUIView(context: Context) -> WKWebView {
        
        guard let url = URL(string: self.urlToLoad) else {
            return WKWebView()
        }
        
        //웹뷰 인스턴스 생성
        let webview = WKWebView(frame: .zero, configuration: createWKWebConfig())
        
//        //wkWebView 의 델리게이트 연결을 위한 코디네이터 설정
//        webview.uiDelegate = context.coordinator
        webview.navigationDelegate = context.coordinator
        webview.allowsBackForwardNavigationGestures = false // 가로 스와이프로 뒤로가기 설정
        
        
//        //리프레시 컨트롤 달아주기
//        let myRefreshControl = UIRefreshControl()
//        myRefreshControl.tintColor = UIColor.gray
//        
//        webview.scrollView.refreshControl = myRefreshControl
//        webview.scrollView.bounces = true
//        
//        refreshHelper.viewModel = viewModel
//        refreshHelper.refreshControl = myRefreshControl //myRefreshControl 를 refreshHelper 클래스에서 사용하기 위해 전달
//        
//        //타겟: refreshHelper 클래스, action: 새로고침(didRefresh), for(조건): .valueChanged == 값이 변했을 때(UI가 땡겨졌을 때)
//        myRefreshControl.addTarget(refreshHelper, action: #selector(WebViewRefreshControlHelper.didRefresh), for: .valueChanged)
        
        //웹뷰 로드함
        webview.load(URLRequest(url: url))
        
        
        
        return webview
    }
    
    //업데이트 ui view
    func updateUIView(_ uiView: WKWebView, context: UIViewRepresentableContext<MyWebView>) {
        
    }
    
    //웹뷰 설정
    func createWKWebConfig() -> WKWebViewConfiguration {
        let preferences = WKPreferences()
        preferences.javaScriptCanOpenWindowsAutomatically = true//자바스크립도 사용할 수 있도록 함
        preferences.javaScriptEnabled = true // 자바스크립스 허용
        
        let wkWebConfig = WKWebViewConfiguration()
        
        //웹뷰 유저 컨트롤러
        let userContentController = WKUserContentController()
        userContentController.add(self.makeCoordinator(), name: "IOSBridge")
        wkWebConfig.userContentController = userContentController
        wkWebConfig.preferences = preferences
        
        
        let userChatViewController = WKUserContentController()
        
        
        
        return wkWebConfig
        
        
    }
    
    
    class Coordinator: NSObject {
        var myWebView: MyWebView //SwiftUi View
        var subscriptions = Set<AnyCancellable>()
        
        init(_ myWebView: MyWebView) {
            self.myWebView = myWebView
            
        }
        
        
    }
}




struct MyWebview_Previews: PreviewProvider {
    var urlToLoad: String
    
    static var previews: some View{
        
        
        MyWebView(urlToLoad: "https://www.naver.com/")
    }
}

//
////UIDelegate 담당
//extension MyWebView.Coordinator: WKUIDelegate {
//    
//    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
//        print("webView runJavaScriptAlertPanelWithMessage")
//        
//        self.myWebView.viewModel.jsBridgeEvent.send(JsValue(message, .JS_BRIDGE))
//        completionHandler()
//    }
//}


// 링크 이동 관련 담당
extension MyWebView.Coordinator: WKNavigationDelegate {
    
    //네비게이션 액션이 들어왔을 때,
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        //리퀘스트 url 이 없으면 리턴. 즉, 액션을 실행하지 않음
        guard let url = navigationAction.request.url else {
            print("url 이 없다")
            decisionHandler(.cancel) //링크를 열지 않음
            return
            
        }
        
        //case 에 따라 외부 링크 열기
        switch url.scheme {
        case "tel", "mailto" :
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            decisionHandler(.cancel)
        
        default:
            //특정 도메인일 때 이동하지 못하게 하기
            
            switch url.host {
            case "www.youtube.com":
                print("유튜브 이동 불가")
//                myWebView.viewModel.jsAlertEvent.send(JsValue(url.host, .BLOCKED_SITE))
                decisionHandler(.cancel)
            default:
                decisionHandler(.allow)
            }
            
        
        }
        
    }
    
    
    //didStart 웹뷰 검색 시작시
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("webView didStartProvisionalNavigation")
        
        //로딩중이라고 true 보내줌
        myWebView.viewModel.shouldShowIndicator.send(true)
        
        
        myWebView.viewModel.webNavigationSubject.sink{
            (action: WEB_NAVIGATION) in print("실행된 네비게이션 액션: \(action)")
            
            switch action {
            case .Back:
                if webView.canGoBack {
                    webView.goBack()
                }
            case .Forward:
                if webView.canGoForward{
                    webView.goForward()
                }
            case .Refresh:
                webView.reload()
            }
            
        }.store(in: &subscriptions) //Combine 이기 때문에 store 으로 메모리 관리
        
        
        
    }
    
    
//    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
//        print("webView = didCommit")
//        // 아직 로딩중
//        myWebView.viewModel.shouldShowIndicator.send(true)
//    }
    
    
    
    //didFinish 검색이 완료되었을 때 사용
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
//        print("webView didFinish")
        
        //로딩 완료이기 때문에 끝났다고 알리기. (false 보내기)
//        myWebView.viewModel.shouldShowIndicator.send(false)
//        
//        webView.evaluateJavaScript("document.title"){ (response, error) in
//            if let error = error {
//                print("타이틀 에러로 인해 가져오지 못함")
//            }
//            if let title = response as? String {
//                self.myWebView.viewModel.webSiteTitleSubject.send(title)
//            }
//            
//        }
        
        let testTest: String = "hello, world!!!"
        myWebView
            .viewModel
            .nativeToJsEvent
            .sink { message in
                
                print("didFinish() called /. ativeToJsEvent 이벤트 들어옴 / message: \(message)")
                
                //대부분의 자바스크립트 호출은 .evaluateJavaScript 로 이루어지며, 아래 코드에서는 함수 "receiveImageUrl(message)"을 호출
                webView.evaluateJavaScript("receiveImageUrl('\(testTest)');", completionHandler: { result, error in
                    
                    if let result = result {
                        print("nativeToJs result 성공: \(result)")
                        
                    }
                    
                    if let error = error {
                        print("보내질 이미지 url: \(message)")
                        print("nativeToJs result 실패: \(error)      @error end")
                        
                    }
                    
                })
                
                
//                webView.evaluateJavaScript("alert('\(message)')", completionHandler: nil)

            }.store(in: &subscriptions) //Combine 이기 때문에 store 으로 메모리 관리
        
        
        
        myWebView.viewModel.changedUrlSubject.compactMap { $0.url} //compactMap 사용시 옵셔널 형태의 값들은 넘어오지 않음
            .sink{
                changedUrl in print("변경된 url: \(changedUrl)")
                webView.load(URLRequest(url: changedUrl))
            }.store(in: &subscriptions) //Combine 이기 때문에 store 으로 메모리 관리
        
    }
//    
//    //웹컨텐트 처리가 끝났을 때.
//    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
//        //처리가 끝났기 때문에 로딩 끝났다고 알려줌
//        myWebView.viewModel.shouldShowIndicator.send(false)
//    }
//    
//    //웹 로딩 실패
//    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
//        //처리가 실패되었기 때문에 로딩 끝남
//        myWebView.viewModel.shouldShowIndicator.send(false)
//    }
//    
//    //네비게이션 프로세스중 에러가 발생
//    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
//        //프로세스 에러가 발생했기 때문에 로딩 끝났다고 알려줌
//        myWebView.viewModel.shouldShowIndicator.send(false)
//    }
}


//브릿지 코드
extension MyWebView.Coordinator: WKScriptMessageHandler {
    
    //웹뷰 js 에서 iOS Native 호출하는 메소드 들이 이 코드를 통함
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print("WKWebViewCoordinator - userContentController / message: \(message)")
        
        
        if message.name == "IOSBridge" {
            print("JSON 데이터가 웹으로부터 옴: \(message.body)")
            
            if let receivedData = message.body as? String{
                print("receiveData: \(receivedData)")
                
                myWebView.viewModel.jsBridgeEvent.send(JsValue(receivedData,
                                                              .JS_BRIDGE))
            }
            
        }
        
        
    }
    
    
    
}

