//
//  WebViewModel.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/31/24.
//


import Combine
import Foundation
import UIKit

typealias WEB_NAVIGATION = WebViewModel.NAVIGATION // 별칭 (편리함을 위해)


class WebViewModel: ObservableObject {
    
    
    enum NAVIGATION{
        case Back, Forward, Refresh
    }
    
    enum URL_TYPE {
        case NAVER
        case GOOGLE
        case DEV_HAN
        case TEST_IT
        
        var url: URL? {
            switch self {
            case .NAVER: return URL(string: "https://www.naver.com/")
            case .GOOGLE: return URL(string: "https://www.google.com")
            case .DEV_HAN: return URL(string: "https://smart-service-web.team-everywhere.com/")
            case .TEST_IT: return URL(string: "https://smart-service-web.team-everywhere.com/")
//            default:
//                return URL(string: "https://hanttogang.github.io/simple_js_alert/")
                
            }
        }
    }
    
//    //웹뷰 네비게이션 액션에 대한 이벤트 코드
//    var webNavigationSubject = PassthroughSubject<WEB_NAVIGATION, Never>() // WEB_NAVIGATION = WebViewModel.NAVIGATION
    
    // iOS -> JS 함수 이벤트 호출
    var nativeToJsEvent = PassthroughSubject<String, Never>()
    
    //Js 캘린더 상세 Briedge 들어오면 이벤트 호출
    var jsBridgeEvent = PassthroughSubject<JsValue, Never>()

//    //Js Alert 이 들어오면 이벤트 호출
//    var jsAlertEvent = PassthroughSubject<JsAlert, Never>()
    
    //웹뷰 네비게이션 액션에 대한 이벤트 코드
    var webNavigationSubject = PassthroughSubject<WEB_NAVIGATION, Never>() // WEB_NAVIGATION = WebViewModel.NAVIGATION 
//
    var changedUrlSubject = PassthroughSubject<WebViewModel.URL_TYPE, Never>()
//    //로딩 여부 이벤트
    var shouldShowIndicator = PassthroughSubject<Bool, Never>()
    
    var isStateWebCalendarView = PassthroughSubject<Bool, Never>()
    var isStateWebCalendarViewBoolean: Bool = false
    
    var captureMode = PassthroughSubject<Bool, Never>()
    var captureImageString = PassthroughSubject<String, Never>()
    
    var captureModeBoolean: Bool = false
    
    var chatImage: UIImage?
//    var chatImageString: String?
    
    var chatRoomIdx: Int = 0
    
    
}

