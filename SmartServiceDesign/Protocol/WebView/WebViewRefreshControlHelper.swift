//
//  WebViewRefreshControlHelper.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/31/24.
//

//웹뷰 새로고침 관련 코드인데 일단 주석처리함
//
//import Foundation
//import UIKit
//
//class WebViewRefreshControlHelper {
//    
//    //MARK: Properties
//    var  refreshControl: UIRefreshControl?
//    var viewModel: WebViewModel?
//    
//    // 리프레시 컨트롤에 붙일 메소드
//    @objc func didRefresh(){
//        print("WebViewRefreshControlHelper - didRefresh() called")
//        
//        guard let refreshControl = refreshControl,
//              let viewModel = viewModel else {
//            print("refreshControl, viewModel 이 없음.")
//            return
//        }
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8, execute: { // 지금으로부터 0.8초라는 딜레이를 준 후에 execute 의 코드 실행
//            print("리프레시 액션 들어옴")
//            //뷰모델에 리프레시 하라고 알려주기
//            viewModel.webNavigationSubject.send(.Refresh)
//            
//            refreshControl.endRefreshing() // 리프레싱 활동 종료
//        })
//    }
//    
//    
//}
