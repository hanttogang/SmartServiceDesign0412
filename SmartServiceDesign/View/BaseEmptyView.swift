//
//  BaseEmptyView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 3/6/24.
//

import SwiftUI

//enum ActiveView {
//    case marketingProjectMain, customerManagement, marketingChannelManagement, employeeAndCreatorManagement, costManagement, attendanceManagementMainView, salaryManagement, billManagement, cardPaymentHistoryManagement, bankPaymentHistoryManagement, aiAnalysisMainView, myPage, chatView, emptyView, baseEmptyView
//}

struct BaseEmptyView: View {
    @State private var isAssessmentView: Bool = false
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @EnvironmentObject var loginData: LoginData
    
    //    @State private var activeView: ActiveView? = nil
    @EnvironmentObject var menuViewModel: MenuViewModel
    
    @State private var selectViewString: String = ""
    
    var body: some View {
        ZStack{
            switch selectViewString {
            case "project":
                MainMarketingProjectManagementView()
            case "brand":
                MainCustomerManagementView()
            case "marketing":
                MainMarketingChannelManagementView()
            case "employeeCreator":
                MainEmployeeAndCreatorManagementView()
            case "salary":
                SalaryManagementMainView()
            case "attendance":
                AttendanceManagementMainView()
            case "cost":
                CostMainView()
            case "bill":
                BillManagementView()
            case "card":
                CardPaymentHistoryManagementView()
            case "bank":
                BankPaymentHistoryManagementView()
            case "ai":
                AiAnalysisMainView()
            case "myPage":
                MyPageView()
            case "message":
                ChatMainView()
                //            case .baseEmptyView:
                //                BaseEmptyView()
                //            case .emptyView:
                //                EmptyView()
                
            default: if(loginData.userType == "master"){
                MainMarketingProjectManagementView()
            }else{
                ChatMainView()
            }
            }
        }
        .onReceive(menuViewModel.selectView, perform: { selectView in
            
            selectViewString = selectView
            
        })
        
    }
}

//#Preview {
//    BaseEmptyView()
//}
