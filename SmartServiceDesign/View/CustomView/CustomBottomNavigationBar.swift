//
//  CustomBottomNavigationBar.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/14/23.
//

import SwiftUI
//import WebKit

struct CustomBottomNavigationBar: View {
    
    @EnvironmentObject var myWebVM: WebViewModel
    @EnvironmentObject var menuViewModel: MenuViewModel
    
    
    @State var jsValue: JsValue?
    
    @EnvironmentObject var loginData: LoginData
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State private var chatWebView: Bool = false
    @State private var myPageView: Bool = false
    
    @State private var selectViewString: String = ""
    
    @State private var isApiLoading: Bool = false
    
    @State private var webViewTrigger: Bool = false
    
    
    
    var body: some View {
        
        
        ZStack{
            
            VStack(spacing: 0){
                
                
                Spacer()
              
                
                HStack{
                    
                    
                    Button(action: {
                        
//                        isApiLoading = chatWebView
                        print("메시지 관리 클릭")

                        menuViewModel.selectView.send("message")
                        
//                        myWebVM.messageManagementMode = true
                        
                    }, label: {
                        
                        
                        VStack() {
                            Image(systemName: "list.bullet")
                                .foregroundColor(chatWebView ? .blue : .gray)
                                .imageScale(.large)
                            
                            
                            
                            Text("메세지 관리")
                            
                                .foregroundColor(chatWebView ? .blue : .gray)
                                .padding(.vertical, 4)
                            
                            
                        }
                        .frame(width: screenWidth/2, height: screenHeight/13.588)
                        
                    })
                    .padding(.top, 4)
                    
                    Spacer()
                    
                    
                    Button {
                        print("마이페이지 클릭")
                        
                        menuViewModel.selectView.send("myPage")
                        
                    } label: {
                        VStack() {
                            Image(systemName: "person.crop.circle")
                                .foregroundColor(myPageView ? .blue : .gray)
                                .imageScale(.large)
                            
                            
                            
                            Text("마이 페이지")
                                .foregroundColor(myPageView ? .blue : .gray)
                                .padding(.vertical, 4)
                            
                            
                        }
                        .frame(width: screenWidth/2, height: screenHeight/13.588)
                    
                    }
                    .padding(.top, 4)
                    
                    
                }//HStack
                .background(.colorF8F8F8)
                
                
                
                
            }//VStack
            .padding(.vertical, screenHeight/73.818)
            
//            
//            // menuViewModel.sideMenuView 상태에 따라 메뉴 여닫기 zIndex 으로 조정
//            if menuViewModel.sideMenuBool {
//                
//                MenuView()
//                    .transition(.move(edge: .leading))
//                    .zIndex(2)
//                
//                // 메뉴 외부를 클릭하면 메뉴를 닫습니다.
//                Button(action: {
//                    withAnimation {
//                        menuViewModel.sideMenuBool = false
//                    }
//                }) {
//                    Color.gray
//                        .edgesIgnoringSafeArea(.all)
//                        .opacity(0.5)
//                }
//                .zIndex(1)
//                
//            }
//           
            
        }//ZStack
        .edgesIgnoringSafeArea(.bottom)
        .onReceive(menuViewModel.selectView, perform: { selectBottom in
            
            if(selectBottom == "message"){
                chatWebView = true
                myPageView = false
            }else if(selectBottom == "myPage"){
                chatWebView = false
                myPageView = true
            }
            
        })

        
        
        
    }//body
}

//
//extension CustomBottomNavigationBar {
//    
//    func createAlert(_ alert: JsValue) -> Alert {
////        Alert(title: Text(alert.type.description), message: Text(alert.message), dismissButton: .default(Text("확인"), action: {
//        Alert(title: Text(alert.type.description), message: Text("이미 존재하는 채팅방입니다."), dismissButton: .default(Text("확인"), action: {
//            print("알림창 확인 버튼이 클릭되었다.")
//        }))
//    }
////
////    //Text 입력 얼럿 창
////    func createTextAlert() -> MyTextAlertView {
////        MyTextAlertView(textString: $textString, showAlert: $shouldShowAlert, title: "iOS -> Js 보내기", message: "")
////    }
//}
//
//
//struct ChatAlertBridgeMessage: Decodable {
//    let idx: String
//    let title: String
////    let data: String
//}

//
//#Preview {
//    CustomBottomNavigationBar()
//}
