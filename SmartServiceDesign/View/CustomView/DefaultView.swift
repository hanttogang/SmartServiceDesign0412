//
//  DefaultView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/20/23.
//

import SwiftUI

struct DefaultView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State var customerNameForRegistration: String = ""
    @State var businessRegistrationNumberForRegistration: String = ""
    
    
    var body: some View {
        
        
        ZStack{
            
            
            NavigationView{
                ZStack {
                    
                    VStack(spacing: 0){
                        
                        ZStack{
                            
                            HStack{
                                
                                
                                ZStack{
                                    Button(action: {
                                        self.presentationMode.wrappedValue.dismiss()
                                    }, label: {
                                        Text("이전")
                                            .foregroundColor(.black)
                                      
                                    })
                                    .frame(width: screenWidth/7.211, height: screenHeight/31.346)
                                    .background(Color("colorE5E5E5"))
                                    .cornerRadius(4.0)
                                }
                                
                                Spacer()
                            }
                            .padding()
                            
                            HStack{
                                Spacer()
                                
                                HStack{
                                    Circle()
                                        .frame(width: screenWidth/46.875)
                                        .foregroundColor(Color("colorE0E0E0"))
                                    
                                    Circle()
                                        .frame(width: screenWidth/46.875)
                                        .foregroundColor(Color("colorE0E0E0"))
                                    
                                    Circle()
                                        .frame(width: screenWidth/46.875)
                                        .foregroundColor(Color("colorE0E0E0"))
                                    
                                    Circle()
                                        .frame(width: screenWidth/46.875)
                                        .foregroundColor(Color("colorE0E0E0"))
       
                                }
                                Spacer()
                                
                            }
                            
                            HStack{
                                Spacer()
                                
                                ZStack{
                                    Button(action: {
                                        
                                    }, label: {
                                        
                                        NavigationLink{
                                            
                                        } label: {
                                            Text("다음")
                                                .foregroundColor(.white)
                                        }
                                    })
                                    .frame(width: screenWidth/7.211, height: screenHeight/31.346)
                                    .background(.blue)
                                    .cornerRadius(4.0)
                                }
                            }
                            .padding()
                            
                            
                        }
                       
                        
                        
                        VStack{
                            
                            HStack(spacing: 0){
                                Text("고객(브랜드)명")
                                    .font(.system(size: 14))
                                
                                Spacer()
                            }
                            .padding(.leading, screenWidth / 15.625)
                            
                            HStack{
                                TextField("", text: $customerNameForRegistration, prompt: Text("고객(브랜드)명 입력해주세요.")
                                    .foregroundColor(Color("hintTextColor")))
                                .keyboardType(.default)
                                .font(.system(size: 14))
                                .padding(.leading, 14)
                                .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                            }
                            .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                            )
                            
                        }//VStack 이름
                        .padding(.top, screenHeight / 54.133) //15
                        
                        VStack{
                            
                            HStack(spacing: 0){
                                Text("사업자 등록 번호")
                                    .font(.system(size: 14))
                                
                                Spacer()
                            }
                            .padding(.leading, screenWidth / 15.625)
                            
                            HStack{
                                TextField("", text: $businessRegistrationNumberForRegistration, prompt: Text("사업자 등록번호를 입력해주세요.")
                                    .foregroundColor(Color("hintTextColor")))
                                .keyboardType(.default)
                                .font(.system(size: 14))
                                .padding(.leading, 14)
                                .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                            }
                            .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                            )
                            
                        }//VStack 이름
                        .padding(.top, screenHeight / 54.133) //15
                        
                        
                        
                    }
                    
                    
                    Spacer()
                    
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    
                    self.presentationMode.wrappedValue.dismiss()
                    
                    
                }) {
                    
                    HStack(spacing: 0){
                        
                        Image(systemName: "chevron.backward")
                        
                    }
                    
                    
                    
                })
                .navigationBarTitle("고객(브랜드) 등록", displayMode: .inline)
                
                
                
            }//ZStack
            
            
            
            
        }//NavigationView
        //    .toast(isShowing: $showToast, text: Text(toastText))
        .navigationBarHidden(true)
        .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
            UIApplication.shared.endEditing()
            
        }
        
        
    }//ZStack
    
    
    
}

#Preview {
    DefaultView()
}
