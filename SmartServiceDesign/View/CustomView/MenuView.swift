//
//  MenuView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/13/23.
//

import SwiftUI

//enum ActiveView {
//    case marketingProjectMain, customerManagement, marketingChannelManagement, employeeAndCreatorManagement, costManagement, attendanceManagementMainView, salaryManagement, billManagement, cardPaymentHistoryManagement, bankPaymentHistoryManagement, aiAnalysisMainView, myPage, chatView, emptyView, baseEmptyView
//}

struct MenuView: View {
    
    @EnvironmentObject var loginData: LoginData
    @EnvironmentObject var menuViewModel: MenuViewModel
    @EnvironmentObject var myWebVM: WebViewModel
    
    @Environment(\.presentationMode) var presentationMode
    
    @State var isMarketingProjectMenu = false
    @State var isPersonnelManagementMenu = false
    @State var isCostPriceManagementMenu = false
    @State var isSalesManagementMenu = false
    @State var isMessageManagementMenu = false
    @State var isAIAnalysisMenu = false
    @State var isMyPageMenu = false
    
    @State var navigateToLoginView = false
    
    @State var chatWebView: Bool = false
    @State private var myPageView: Bool = false
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    let defaults = UserDefaults.standard
    
    
//    @State private var activeView: ActiveView? = nil
    
    var body: some View {
        
        ZStack{
                // 기본 뷰를 여기에 표시
                HStack{
                    ScrollView(showsIndicators: false){
                        
                            VStack(spacing: 0){
                                
                                
                                
                                Group{ // 광고 프로젝트 관리
                                    Button(action: {
                                        
                                        withAnimation(.none){
                                            self.isMarketingProjectMenu.toggle()
                                        }
                                        
                                    }, label: {
                                        
                                        
                                        HStack{
                                            Image(isMarketingProjectMenu ? "marketing_management_w" : "marketing_management_b")
                                                .resizable()
                                                .aspectRatio(contentMode: .fit)
                                                .frame(width: screenWidth/15.625, height: screenHeight/33.958)
                                                .foregroundColor(isMarketingProjectMenu ? Color(.white) : Color(.black))
                                                
                                            
                                            Text("광고 프로젝트 관리")
                                                .foregroundColor(isMarketingProjectMenu ? Color(.white) : Color(.black))
                                                .font(.headline)
                                            
                                            Spacer()
                                            
                                            Image( isMarketingProjectMenu ? "arrowUp" : "arrowBottom")
                                                .imageScale(.large)
                                            
                                        }
                                        
                                        
                                    })
                                    .padding(.horizontal, screenWidth/15)
                                    .frame(height: screenHeight/16)
                                    .background(isMarketingProjectMenu ? Color("mainColor") : Color(.white))
                                    
                                    
                                    if isMarketingProjectMenu {
                                        Button(action: {
                                            
                                            menuViewModel.selectView.send("project")
                                            
                                        }) {
                                            HStack{
                                                Text("프로젝트 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            menuViewModel.selectView.send("brand")

                                        }) {
                                            HStack{
                                                Text("고객 (브랜드)관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            menuViewModel.selectView.send("marketing")
                                            
                                        }) {
                                            HStack{
                                                Text("마케팅 채널 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                    }
                                }//Group 광고 프로젝트 관리
                                
                                Group{ // 인사 관리
                                    
                                    Button(action: {
                                        
                                        withAnimation(.none){
                                            self.isPersonnelManagementMenu.toggle()
                                        }
                                        
                                        
                                    }, label: {
                                        
                                        
                                        HStack{
                                            Image(isPersonnelManagementMenu ? "person_management_w" : "person_management_b")
                                                .resizable()
                                                .aspectRatio(contentMode: .fit)
                                                .frame(width: screenWidth/15.625, height: screenHeight/33.958)
                                                .foregroundColor(isPersonnelManagementMenu ? Color(.white) : Color(.black))
                                                .imageScale(.large)
                                            
                                            Text("인사 관리")
                                                .foregroundColor(isPersonnelManagementMenu ? Color(.white) : Color(.black))
                                                .font(.headline)
                                            
                                            Spacer()
                                            
                                            Image( isPersonnelManagementMenu ? "arrowUp" : "arrowBottom")
                                                .imageScale(.large)
                                            
                                        }
                                        
                                        
                                    })
                                    .padding(.horizontal, screenWidth/15)
                                    .frame(height: screenHeight/16)
                                    .background(isPersonnelManagementMenu ? Color("mainColor") : Color(.white))

                                    
                                    
                                    if isPersonnelManagementMenu {
                                        Button(action: {
                                            menuViewModel.selectView.send("employeeCreator")
                                        }) {
                                            HStack{
                                                Text("직원/크리에이터 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            menuViewModel.selectView.send("attendance")
                                        }) {
                                            HStack{
                                                Text("근태 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            menuViewModel.selectView.send("salary")
                                        }) {
                                            HStack{
                                                Text("급여 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                    }
                                    
                                }//Group 인사관리
                                
                                Group{ //원가 관리
                                    
                                    Button(action: {
                                        
                                        withAnimation(.none){
                                            self.isCostPriceManagementMenu.toggle()
                                            
                                            menuViewModel.selectView.send("cost")
                                        }
                                        
                                        
                                    }, label: {
                                        
                                        
                                        HStack{
                                            Image("cost_management_b")
                                                .resizable()
                                                .aspectRatio(contentMode: .fit)
                                                .frame(width: screenWidth/15.625, height: screenHeight/33.958)
                                                .foregroundColor(isCostPriceManagementMenu ? Color(.white) : Color(.black))
                                                .imageScale(.large)
                                            
                                            Text("원가 관리")
                                                .foregroundColor(isCostPriceManagementMenu ? Color(.white) : Color(.black))
                                                .font(.headline)
                                            
                                            Spacer()
                                            
                                        }
                                        
                                        
                                    })
                                    .padding(.horizontal, screenWidth/15)
                                    .frame(height: screenHeight/16)
                                    .background(isCostPriceManagementMenu ? Color("mainColor") : Color(.white))
                                    
                                    
                                }//Group 원가관리
                                
                                Group{ //매출 관리
                                    
                                    Button(action: {
                                        
                                        withAnimation(.none){
                                            self.isSalesManagementMenu.toggle()
                                        }
                                        
                                        
                                    }, label: {
                                        
                                        
                                        HStack{
                                            Image(isSalesManagementMenu ? "sales_management_w" : "sales_management_b")
                                                .resizable()
                                                .aspectRatio(contentMode: .fit)
                                                .frame(width: screenWidth/15.625, height: screenHeight/33.958)
                                                .foregroundColor(isSalesManagementMenu ? Color(.white) : Color(.black))
                                                .imageScale(.large)
                                            
                                            Text("매출 관리")
                                                .foregroundColor(isSalesManagementMenu ? Color(.white) : Color(.black))
                                                .font(.headline)
                                            
                                            Spacer()
                                            
                                            Image( isSalesManagementMenu ? "arrowUp" : "arrowBottom")
                                                .imageScale(.large)
                                            
                                        }
                                        
                                        
                                    })
                                    .padding(.horizontal, screenWidth/15)
                                    .frame(height: screenHeight/16)
                                    .background(isSalesManagementMenu ? Color("mainColor") : Color(.white))
                                    
                                    
                                    if isSalesManagementMenu {
                                        Button(action: {
                                            menuViewModel.selectView.send("bill")
                                        }) {
                                            HStack{
                                                Text("계산서 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            menuViewModel.selectView.send("card")
                                        }) {
                                            HStack{
                                                Text("카드 결제 내역 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            menuViewModel.selectView.send("bank")
                                        }) {
                                            HStack{
                                                Text("은행 거래 내역 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                    }
                                    
                                }//Group 매출관리
                                
                                Group{ //메세지 관리
                                    
                                    Button(action: {
                                        
                                        withAnimation(.none){
                                            menuViewModel.selectView.send("message")
                                            
//                                            self.isMessageManagementMenu.toggle()
//                                            self.activeView = .chatView
                                            
                                            
                                            
//                                            chatWebView = true
//                                            menuViewModel.chatView.send(chatWebView)
//                                            
//                                            myPageView = false
//                                            menuViewModel.myPageView.send(false)
//                                            
//                                            menuViewModel.sideMenuBool = false
////
//                                            myWebVM.messageManagementMode = true
//                                            
//                                            self.presentationMode.wrappedValue.dismiss()
                                            
//                                            isMessageManagementMenu = true
//                                            
//                                            isAIAnalysisMenu = false
//                                            isMyPageMenu = false
                                        }
                                        
                                        
                                    }, label: {
                                        
                                        
                                        HStack{
                                            Image("chat_management_b")
                                                .resizable()
                                                .aspectRatio(contentMode: .fit)
                                                .frame(width: screenWidth/15.625, height: screenHeight/33.958)
                                                .foregroundColor(isMessageManagementMenu ? Color(.white) : Color(.black))
                                                .imageScale(.large)
                                            
                                            Text("메세지 관리")
                                                .foregroundColor(isMessageManagementMenu ? Color(.white) : Color(.black))
                                                .font(.headline)
                                            
                                            Spacer()
                                            
                                        }
                                        
                                        
                                    })
                                    .padding(.horizontal, screenWidth/15)
                                    .frame(height: screenHeight/16)
                                    
                                    
                                }//Group 메세지 관리
                                
                                Group{ //AI 분석
                                    
                                    Button(action: {
                                        
                                        withAnimation(.none){
                                            
                                            menuViewModel.selectView.send("ai")

                                        }
                                        
                                        
                                    }, label: {
                                        
                                        
                                        HStack{
                                            Image("ai_analysis_b")
                                                .resizable()
                                                .aspectRatio(contentMode: .fit)
                                                .frame(width: screenWidth/15.625, height: screenHeight/33.958)
                                                .foregroundColor(isAIAnalysisMenu ? Color(.white) : Color(.black))
                                                .imageScale(.large)
                                            
                                            Text("AI 분석")
                                                .foregroundColor(isAIAnalysisMenu ? Color(.white) : Color(.black))
                                                .font(.headline)
                                            
                                            Spacer()
                                            
                                        }
                                        
                                        
                                    })
                                    .padding(.horizontal, screenWidth/15)
                                    .frame(height: screenHeight/16)
                                    
                                    
                                    
                                    
                                    
                                }//Group AI 분석
                                
                                Group{ //마이페이지 관리
                                    
                                    Button(action: {
                                        
                                        withAnimation(.none){
//                                            self.activeView = .myPage
                                            menuViewModel.selectView.send("myPage")
                                            
//                                            
//                                            chatWebView = false
//                                            menuViewModel.chatView.send(false)
//                                            
//                                            myPageView = true
//                                            menuViewModel.myPageView.send(myPageView)
//                                            
//                                            menuViewModel.sideMenuBool = false
                                            
//                                            self.presentationMode.wrappedValue.dismiss()
//                                            self.isMyPageMenu.toggle()
                                            

//                                            
//                                            isMyPageMenu = true
//                                            
//                                            isAIAnalysisMenu = false
//                                            isMessageManagementMenu = false
                                        }
                                        
                                        
                                    }, label: {
                                        
                                        
                                        HStack{
                                            Image("my_page_b")
                                                .resizable()
                                                .aspectRatio(contentMode: .fit)
                                                .frame(width: screenWidth/15.625, height: screenHeight/33.958)
                                                .foregroundColor(isAIAnalysisMenu ? Color(.white) : Color(.black))
                                                .imageScale(.large)
                                            
                                            Text("마이페이지")
                                                .foregroundColor(isMyPageMenu ? Color(.white) : Color(.black))
                                                .font(.headline)
                                            
                                            Spacer()
                                            
                                        }
                                        
                                        
                                    })
                                    .padding(.horizontal, screenWidth/15)
                                    .frame(height: screenHeight/16)
                                    
                                    
                                    
                                    
                                }//Group 마이페이지 관리
                                
                                
                                
                                

    //                            Spacer()
                                                    
                                
                            }
                        
                        
                    }//ScrollView
                    .frame(width: screenWidth/1.23)
                    .background(.white)
                    
                    
                    Spacer()
                }//HStack
            }
            
            
        }//ZStack
        
        //        .edgesIgnoringSafeArea(.all)
        
        
//    }
}

#Preview {
    MenuView()
}
