//
//  Toast.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/11/23.
//


import Foundation
import SwiftUI


struct Toast<Presenting>: View where Presenting: View {
    @Binding var isShowing: Bool
    let presenting: () -> Presenting
    let text: Text
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    var body: some View {
        
        
        
        
        
        ZStack(alignment: .center) {
            
            
//                Spacer()
                
                self.presenting()
                //                    .blur(radius: self.isShowing ? 3 : 0)
                
                VStack {
                    self.text
                }
                .frame(width: screenWidth / 1.5,
                       height: screenHeight / 20)
                .background(Color.secondary.colorInvert())
                .foregroundColor(Color.primary)
                .cornerRadius(16)
                .transition(.slide)
                .opacity(self.isShowing ? 1 : 0)
                .padding(.top, screenHeight / 1.5)
                
            
        }
        .animation(.default)
//        .ignoresSafeArea(.all)

    }
}

extension View {
    func toast(isShowing: Binding<Bool>, text: Text) -> some View {
        Toast(isShowing: isShowing,
              presenting: { self },
              text: text)
    }
}
