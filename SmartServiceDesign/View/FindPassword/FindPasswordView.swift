//
//  FindPasswordView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/13/23.
//

import SwiftUI
import Alamofire

struct FindPasswordView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    enum Flavor: String, CaseIterable, Identifiable {
      case employee, creator
      var id: Self { self }
    }
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var inputEmailInfo: String = ""
    @State private var inputNameInfo: String = ""
    @State private var inputPhoneNumInfo: String = ""
    
    @State private var AlarmAgreeChecked: Bool = true
    
    @State private var navigate: Bool = false
    
    @State private var selectedUserType = Flavor.employee
    
    init() {
        let appearance = UINavigationBarAppearance()
        //        appearance.configureWithOpaqueBackground()
        //        appearance.backgroundColor = UIColor(named: "textColorHint") // 원하는 색상으로 변경하세요.
        appearance.shadowColor = nil // 이 부분을 추가하면 네비게이션 바 하단 선이 사라집니다.
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        
    }
    
    let defaultUrl = "\(ApiClient.BASE_URL)"
    
    var body: some View {
        
        
        
        NavigationView{
            ZStack{
                ScrollView{
                    
                    VStack{ //이메일 입력
                        
                        HStack(spacing: 0){
                            Text("이메일")
                                .font(.system(size: 14))
                            Text("  *")
                                .font(.system(size: 14))
                                .foregroundColor(.red)
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField(".", text: $inputEmailInfo, prompt: Text("이메일을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.emailAddress)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    } //VStack 아이디
                    .padding(.top, screenHeight / 61.6)
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("이름")
                                .font(.system(size: 14))
                            Text("  *")
                                .font(.system(size: 14))
                                .foregroundColor(.red)
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField(".", text: $inputNameInfo, prompt: Text("이름을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이름
                    .padding(.top, screenHeight / 77.0)
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("휴대전화번호")
                                .font(.system(size: 14))
                            Text("  *")
                                .font(.system(size: 14))
                                .foregroundColor(.red)
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField(".", text: $inputPhoneNumInfo, prompt: Text("휴대폰번호를 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.numberPad)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 휴대폰번호
                    .padding(.top, screenHeight / 77.0)
                    .padding(.bottom, screenHeight / 2.9)
                    
                    
                    
                    
                    HStack{
                        
                        
                        Button(action: {
                            print("비밀번호 찾기 확인 버튼 클릭")
                            
                            if(inputEmailInfo == "" || inputNameInfo == "" || inputPhoneNumInfo == "" ){
                                
                                showAlert = true
                                
                                alertTitle = "비밀번호 찾기 실패"
                                alertMessage = "필수 입력칸을 비울 수 없습니다."
                                
                                //뷰 테스트를 위해 임시로 true
                                self.navigate = true
                                
                            }else if (!validateEmail(email: inputEmailInfo)){
                                
                                showAlert = true
                                
                                alertTitle = "비밀번호 찾기 실패"
                                alertMessage = "옳바른 이메일을 입력해주세요."
                                
                            }else{
                                findPassword()
//                                self.navigate = true  //findPassword() 에서 navigate true 처리
                            }
                            
                            
                            
                        }, label: {
                            
                            Text("확인")
                                .foregroundColor(.white)
                                .font(.system(size: 18))
                                .bold()
                                .frame(width: screenWidth/1.14, height: screenHeight/15.03)
                            
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/41.066)
                        .padding(.bottom, 29)
                        
                        
                        
                        NavigationLink(destination: SendEmailPasswordView(), isActive: $navigate) {
                            EmptyView()
                        }
                        .hidden() // NavigationLink를 숨김
                        
                    }//HStack 하단 버튼
                    .padding(.bottom, screenHeight/25.375)
                    
                    
                    
                    
                }//ScrollView
                
                
                
            }//ZStack
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading: Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }) {
                
                HStack(spacing: 0){
                    
                    Image(systemName: "chevron.backward")
                    
                }
                
                
                
            })
            .navigationBarTitle("비밀번호 찾기")
            
        }//NavigationView
//        .toast(isShowing: $showToast, text: Text(toastText))
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        .navigationBarHidden(true)
        .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
            UIApplication.shared.endEditing()
            
        }
        
        
    }
    
    func validatePassword(password: String) -> Bool {
        // 정규표현식: 최소 하나의 소문자, 최소 하나의 대문자, 최소 하나의 숫자, 최소 하나의 특수문자를 포함하며, 총 8자 이상
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*\\d)(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: password)
    }
    
    func validateEmail(email: String) -> Bool {
        // 정규표현식: 이메일 형식
        let emailTest = NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}")
        return emailTest.evaluate(with: email)
        
        /*
         [A-Z0-9a-z._%+-]+: 알파벳 대소문자, 숫자, 그리고 ., _, %, +, -를 포함하는 하나 이상의 문자
         @: @ 문자
         [A-Za-z0-9.-]+: 알파벳 대소문자, 숫자, 그리고 ., -를 포함하는 하나 이상의 문자
         \\.: . 문자
         [A-Za-z]{2,64}: 알파벳 대소문자 2개 이상 64개 이하
         */
    }
    
    private func findPassword() {
        let parameters: [String: Any] = [
            "user_email": inputEmailInfo,
        ]

        AF.request("\(defaultUrl)/api/auth/send/reset", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                switch response.result {
                    
                case .success(let value):
                    
                    print("api 연결 성공: \(value)")
                    
                    guard let json = value as? [String: Any],
                                      let result = json["result"] as? Int else {
                                    print("응답 형식이 올바르지 않습니다: \(value)")
                                    return
                                }

                                if result == 0 {
                                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                                    print("이메일 전송 실패: \(errorMessage ?? "알 수 없는 오류")")
                                    
                                    showAlert = true
                                    
                                    alertTitle = "비밀번호 찾기 실패"
                                    alertMessage = "\(errorMessage ?? "알 수 없는 오류")"
                                    
                                } else {
                                    print("이메일 전송 성공: \(value)")
                                    
                                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                                    self.navigate = true
                                    
                                }
                    
                    
                    
                case .failure(let error):
                    // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                    print("이메일 전송 실패: \(error)")
                    
                    showAlert = true
                    
                    alertTitle = "비밀번호 찾기 실패"
                    alertMessage = "\(error)"
                    
                }
            }
    }
    
    
//    func register(registerUser: RegisterUser, completion: @escaping (Result<Data?, AFError>) -> Void) {
//        let url = "\(ApiClient.BASE_URL)/api/auth/signup"
//        let headers: HTTPHeaders = [
//            "accept": "application/json",
//            "Content-Type": "application/json"
//        ]
//
//        AF.request(url, method: .post, parameters: registerUser, encoder: JSONParameterEncoder.default, headers: headers)
//            .validate()
//            .response { response in
//                switch response.result {
//                case .success(let data):
//                    completion(.success(data))
//                case .failure(let error):
//                    if let data = response.data {
//                        let decoder = JSONDecoder()
//                        if let errorResponse = try? decoder.decode(ErrorResponse.self, from: data),
//                           errorResponse.err.status == 400 && errorResponse.err.errorMessage == "Duplicate ID" {
//                            print("called - register:  중복된 아이디")
//
//                            toastText = "이미 가입된 아이디입니다."
//                            showToast = true
//
//                            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//                                self.showToast = false
//                                toastText = ""
//                            }
//
//                        }
//                    } else {
//                        completion(.failure(error))
//                    }
//                }
//            }
//    }
    
}

#Preview {
    FindPasswordView()
}
