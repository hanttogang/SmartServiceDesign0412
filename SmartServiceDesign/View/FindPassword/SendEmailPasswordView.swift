//
//  SendEmailView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/13/23.
//

import SwiftUI

struct SendEmailPasswordView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    init() {
        let appearance = UINavigationBarAppearance()
        //        appearance.configureWithOpaqueBackground()
        //        appearance.backgroundColor = UIColor(named: "textColorHint") // 원하는 색상으로 변경하세요.
        appearance.shadowColor = nil // 이 부분을 추가하면 네비게이션 바 하단 선이 사라집니다.
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        
    }
    
    @State var loginView: Bool = false
    
    var body: some View {
        
        
        
        NavigationView{
            ZStack {
                if loginView{
                    
                    
                        LoginView()
                    
                }else{
                    VStack{
                        Spacer()
                        
                        Image("imgComplete")
                        
                        
                        Text("이메일이 발송되었습니다.")
                            .font(.title2)
                            .padding(.top, 40)
                        
                        Text("발송된 이메일 링크를 눌러\n비밀번호를 변경해주세요")
                            .font(.system(size: 14))
                            .multilineTextAlignment(.center)
                            .foregroundColor(Color("color979797"))
                            .padding(.top, 27)
                        
                        
                        Spacer()
                        
                        Button(action: {
                            print("loginBtn Click")
                            
                            loginView = true
                            
                            UIApplication.shared.windows.first?.rootViewController?.dismiss(animated: true, completion: nil)
                            
                        }, label: {
                            
                            Text("확인")
                                .foregroundColor(.white)
                                .font(.system(size: 18))
                                .bold()
                                .frame(width: screenWidth/1.14, height: screenHeight/15.03)
                            
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/41.066)
                        .padding(.bottom, 29)
                        
                        
                    }//VStack
                    .navigationBarBackButtonHidden(true)
                    .navigationBarItems(leading: Button(action: {
                        self.presentationMode.wrappedValue.dismiss()
                    }) {
                        
                        HStack(spacing: 0){
                            
                            Image(systemName: "chevron.backward")
                            
                        }
                        
                        
                        
                    })
                    .navigationBarTitle("이메일 발송")
                }
                
                
            }//ZStack
           
            
        }//NavigationView
        //    .toast(isShowing: $showToast, text: Text(toastText))
        .navigationBarHidden(true)
        .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
            UIApplication.shared.endEditing()
            
        }
    
        
    }
    
}

#Preview {
    SendEmailPasswordView()
}
