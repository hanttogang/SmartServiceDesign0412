//
//  LoginPageView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/11/23.
//

import SwiftUI


import SwiftUI
import Alamofire

struct LoginView: View {
    
    //    @State var isContentReady: Bool = false
    
    @State var inputID: String = ""
    @State var inputPassword: String = ""
    
    @State private var isLoading = false
    
    @State private var loginResult: LoginResult?
    
    @EnvironmentObject var loginData: LoginData
    
    
    @State private var showToast: Bool = false
    @State private var toastText: String = ""
    
    //    @Binding let
    
    let defaults = UserDefaults.standard
    
    var body: some View {
        
        NavigationView{
            
            ZStack{
                
                
                
                
                
                
                GeometryReader{ geometry in
                    HStack{
                        
                        Spacer()
                        
                        VStack{
                            
                            Spacer()
                            
                            Image("logo_login")
                            
                            
                            
                            ZStack{
                                
                                
                                VStack{
                                    
                                    HStack{
                                        //                                           Image("input_email")
                                        //                                               .padding(.leading, geometry.size.width/44)
                                        TextField(".", text: $inputID, prompt: Text("아이디를 입력해주세요.")
                                            .foregroundColor(Color("hintTextColor")))
                                        .keyboardType(.emailAddress)
                                        .font(.system(size: 14))
                                        .padding(.leading, 14)
                                        .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                    }
                                    .frame(width: geometry.size.width/1.0975, height: geometry.size.height/16)
                                    .overlay(RoundedRectangle(cornerRadius: 4)
                                        .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                                    )
                                    .padding(.top, geometry.size.height/15.4)
                                    
                                    
                                    HStack{
                                        SecureField(".", text: $inputPassword, prompt: Text("비밀번호를 입력해주세요.")
                                            .foregroundColor(Color("hintTextColor")))
                                        .keyboardType(.emailAddress)
                                        .font(.system(size: 14))
                                        .padding(.leading, 14)
                                    }
                                    .frame(width: geometry.size.width/1.0975, height: geometry.size.height/16)
                                    .overlay(RoundedRectangle(cornerRadius: 4)
                                        .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                                    )
                                    
                                    
                                    Button(action: {
                                        print("loginBtn Click")
                                        
                                        if(inputID == "" || inputPassword == ""){
                                            
                                            
                                            
                                            toastText = "필수 항목을 입력하세요"
                                            showToast = true
                                            
                                            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                                self.showToast = false
                                                toastText = ""
                                            }
                                        } else {
                                            
                                            //로딩중
                                            isLoading = true
                                            
                                            
                                            let user = User(id: inputID, password: inputPassword, last_login_env: "admin", fcm_token: "fcm_token")
                                            login(user: user) { result in
                                                switch result {
                                                case .success(let loginResult):
                                                    switch loginResult {
                                                    case .success(let successResult):
                                                        print("로그인 성공: \(successResult)")
                                                        
                                                        //저장될 로그인 정보
                                                        defaults.set(successResult.data.user_id, forKey: "userID")
                                                        defaults.set(inputPassword, forKey: "userPassword")
                                                        
                                                        
                                                        
                                                        loginData.isLogin = true
                                                        
                                                    case .failure(let failureResult):
                                                        print("로그인 실패: \(failureResult.errorMessage)")
                                                        
                                                        
                                                        
                                                        loginData.isLogin = false
                                                        isLoading = false
                                                        
                                                        
                                                        
                                                    }
                                                case .failure(let error):
                                                    print("요청 실패: \(error)")
                                                    
                                                    loginData.isLogin = false
                                                    
                                                    isLoading = false
                                                    
                                                    toastText = "존재하지 않는 계정입니다."
                                                    showToast = true
                                                    
                                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                                        self.showToast = false
                                                        toastText = ""
                                                    }
                                                    
                                                }
                                                
                                                
                                            }
                                        }
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                    }, label: {
                                        Text("로그인")
                                            .foregroundColor(.white)
                                            .font(.system(size: 16))
                                            .frame(width: geometry.size.width/1.0975, height: geometry.size.height/16)
                                    })
                                    //                                    .frame(width: geometry.size.width/1.0975, height: geometry.size.height/16)
                                    .background(Color("mainColor"))
                                    .cornerRadius(4)
                                    .padding(.top, geometry.size.height/41.066)
                                    .disabled(isLoading)
                                    
                                    
                                    
                                    
                                    
                                    Button(action: {
                                        print("registerBtn Click")
                                    }, label: {
                                        
                                        NavigationLink{
                                            RegisterView()
                                        } label: {
                                            Text("회원가입")
                                                .foregroundColor(Color("mainColor"))
                                                .font(.system(size: 16))
                                                .frame(width: geometry.size.width/1.0975, height: geometry.size.height/16)
                                        }
                                    })
                                    .background(.white)
                                    .overlay(RoundedRectangle(cornerRadius: 4)
                                        .stroke(Color("mainColor"), lineWidth: 1)
                                    )
                                    
                                    
                                    
                                    Button(action: {
                                        print("I dont know ID/Password Btn Click")
                                        
                                    }, label: {
                                        NavigationLink {
                                            FindIDView()
                                        } label: {
                                            
                                            Text("아이디 / 비밀번호 찾기")
                                                .foregroundColor(Color("mainColor"))
                                                .font(.system(size: 16))
                                                .frame(width: geometry.size.width/1.0975, height: geometry.size.height/16)
                                        }
                                    })
                                    .background(.white)
                                    .overlay(RoundedRectangle(cornerRadius: 4)
                                        .stroke(Color("mainColor"), lineWidth: 1)
                                    )
                                    
                                    
                                    
                                    
                                    
                                }//VStack
                                
                                
                                
                            }//ZStack
                            
                            Spacer()
                            
                        } //VStack
                        
                        
                        Spacer()
                    }//HStack
                    
                    
                }//Geometry
                
                //                if !isContentReady {
                //                    SplashView()
                //                        .background(.white)
                //                        .edgesIgnoringSafeArea(.all)
                //
                //                    //                        .transition(.opacity)
                //
                //                    //                mySlashScreenView.transition(.opacity)
                //                }
                
                
            }//ZStack
            .animation(.none)
            .edgesIgnoringSafeArea(.all)
            .toast(isShowing: $showToast, text: Text(toastText))
            
            
            
            
        }//NavigationView
        .navigationBarHidden(true)
        .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
            UIApplication.shared.endEditing()
        }
        .onAppear{ // 현재로부터 deadlin 후에 execute 내의 코드 실행
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                
                autoLogin()

            })
        }
        
    }
    
    
    func login(user: User, completion: @escaping (Result<LoginResult, Error>) -> Void) {
        let url = "\(ApiClient.BASE_URL)/api/auth/signin"
        
        let headers: HTTPHeaders = [
            "accept": "application/json",
            "Content-Type": "application/json"
        ]
        
        AF.request(url, method: .post, parameters: user, encoder: JSONParameterEncoder.default, headers: headers)
            .validate()
            .responseData { response in
                switch response.result {
                case .success(let data):
                    let decoder = JSONDecoder()
                    if let successResult = try? decoder.decode(SuccessResult.self, from: data) {
                        completion(.success(.success(successResult)))
                    } else if let failureResult = try? decoder.decode(FailureResult.self, from: data) {
                        completion(.success(.failure(failureResult)))
                    } else {
                        completion(.failure(DecodingError.dataCorrupted(DecodingError.Context(codingPath: [], debugDescription: "데이터를 디코딩할 수 없습니다."))))
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
            }
    }
    
    
    
    func autoLogin() {
        
        inputID = defaults.string(forKey: "userID") ?? ""
        inputPassword = defaults.string(forKey: "userPassword") ?? ""
        
        if(inputID == "" || inputPassword == ""){
            
            print("자동로그인 실패, 저장된 아이디 정보가 없음")
            
        } else {
            
            //로딩중
            isLoading = true
            
            
            let user = User(id: inputID, password: inputPassword, last_login_env: "admin", fcm_token: "fcm_token")
            login(user: user) { result in
                switch result {
                case .success(let loginResult):
                    switch loginResult {
                    case .success(let successResult):
                        print("로그인 성공: \(successResult)")
                        
                        defaults.set(successResult.data.user_id, forKey: "userID")
                        
//                                                        defaults.set(successResult.data.user_id, forKey: "userID")
                        defaults.set(inputPassword, forKey: "accessToken")
                        
                        
                        
                        loginData.isLogin = true
                        
                    case .failure(let failureResult):
                        print("로그인 실패: \(failureResult.errorMessage)")
                        
                        
                        
                        loginData.isLogin = false
                        isLoading = false
                        
                        
                        
                    }
                case .failure(let error):
                    print("요청 실패: \(error)")
                    
                    loginData.isLogin = false
                    
                    isLoading = false

                    
                }
                
                
            }
        }
        
    }
    
    
}






#Preview {
    LoginView()
}
