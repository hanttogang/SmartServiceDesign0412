//
//  LoginView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/11/23.
//


import SwiftUI
import Alamofire

struct LoginView: View {
    
    let defaultUrl = "\(ApiClient.BASE_URL)"
    
    //    @State var isContentReady: Bool = false
    
    @ObservedObject private var keyboard = KeyboardResponder()
    
    
//    @State private var inputID: String = "user123@example.com"
//    @State private var inputPassword: String = "123123"
    
//    @State private var inputID: String = "tester0226@test.com"
//    @State private var inputPassword: String = "tpgus!0619"
//    @State private var inputID: String = "test0226@test.com"
//    @State private var inputPassword: String = "tpgus!0619"
    @State private var inputID: String = "testhan@test.com"
    @State private var inputPassword: String = "tpgus!0619"
    
    
    
    
    
    
    
    
    
    @State private var isLoading = false
    
    //    @State private var loginResult: LoginResult?
    
    @EnvironmentObject var loginData: LoginData
    @EnvironmentObject var menuViewModel: MenuViewModel

    
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    let defaults = UserDefaults.standard
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    
    var body: some View {
        
        ZStack{
            NavigationView{
                
                ScrollView(.vertical, showsIndicators: false){
                    
                    HStack{ //ScrollView 중앙에 오도록
                        
                    }//HStack
                    .frame(width: screenWidth, height: screenHeight/5)
                    
                    VStack{
                        
                        Spacer()
                        
                        
                        
                        Image("imgLogo")
                        
                        HStack(spacing: 0){
                            
                            Image(systemName: "envelope")
                                .frame(width: 24, height: 24)
                                .padding(.leading, 14)
                                .foregroundColor(Color("hintTextColor"))
                            
                            
                            TextField(".", text: $inputID, prompt: Text("이메일 주소를 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.emailAddress)
                            .font(.system(size: 14))
                            .padding(.leading, 12)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        .padding(.top, screenHeight/15.4)
                        
                        
                        HStack(spacing: 0){
                            Image(systemName: "lock")
                                .frame(width: 24, height: 24)
                                .padding(.leading, 14)
                                .foregroundColor(Color("hintTextColor"))
                            
                            
                            SecureField(".", text: $inputPassword, prompt: Text("비밀번호를 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.emailAddress)
                            .font(.system(size: 14))
                            .padding(.leading, 12)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                        
                        Button(action: {
                            print("loginBtn Click")
                            
                            UIApplication.shared.endEditing()
                            
                            if(inputID == "" || inputPassword == ""){
                                
                                showAlert = true
                                
                                alertTitle = "로그인 실패"
                                alertMessage = "아이디와 비밀번호를 확인해주세요"
                                
                            } else {
                                
                                //로딩중
                                isLoading = true
                                
                                
                                //                        let user = SignIn(userEmail: inputID, userPwd: inputPassword)
                                print("입력된 이메일: \(inputID)\n 입력된 비밀번호: \(inputPassword)")
                                
                                loginUser(email: inputID, password: inputPassword)
                                
                                isLoading = false
                                
                                print("로그인 완료, 로딩상태: \(isLoading)")
                                
                                
                            }
                            
                            
                        }, label: {
                            Text("로그인")
                                .foregroundColor(.white)
                                .font(.system(size: 16))
                                .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                                .bold()
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/41.066)
                        .disabled(isLoading)
                        .padding(.bottom, 29)
                        
                        
                        
                        HStack{
                            
                            Button(action: {
                                print("registerBtn Click")
                                
                                
                                
                            }, label: {
                                
                                NavigationLink{
                                    agreementToGuideView()
                                } label: {
                                    Text("회원가입")
                                        .foregroundColor(Color("color828282"))
                                        .font(.system(size: 16))
                                }
                            })
                            
                            
                            Rectangle()
                                .frame(width: 1, height: 14)
                                .foregroundColor(.gray)
                            
                            
                            Button(action: {
                                print("I dont know ID/Password Btn Click")
                                
                            }, label: {
                                NavigationLink {
                                    FindPasswordView()
                                } label: {
                                    
                                    Text("비밀번호 찾기")
                                        .foregroundColor(Color("color828282"))
                                        .font(.system(size: 16))
                                }
                            })
                        }//HStack
                        .padding(.bottom, 0)
                        
                        Spacer()
                        
                    }//VStack
                    
                    //                .frame(width: screenWidth, height: screenHeight)
//                    .padding(.bottom, keyboard.currentHeight)
                    .animation(.none)
    //                .edgesIgnoringSafeArea(.all)
                    
                } //ScrollView
                
                
                
                
                
                
                
            }//NavigationView
            //        .navigationBarHidden(true)
            .onAppear{ // 현재로부터 deadlin 후에 execute 내의 코드 실행
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    
                    //                autoLogin()
                    
                })
            }
        }
        .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
            UIApplication.shared.endEditing()
            
        }
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        
        
    }
    
    func loginUser(email: String, password: String) {
        let parameters: [String: Any] = [
            "user_email": email,
            "user_pwd": password
        ]
        
        var userToken: String?
        var userIdx: Int?
//        var userEmail: String?
//        var userName: String?
//        var userType: String?
//        var userPhoneNumber: String?
        
        AF.request("\(defaultUrl)/api/auth/signIn", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                switch response.result {
                    
                case .success(let value):
                    
                    
                    
//                    print("api 연결 성공: \(value)")
                    print("api 연결 성공")
                    
                    guard let json = value as? [String: Any],
                          let result = json["result"] as? Int else {
                        print("응답 형식이 올바르지 않습니다: \(value)")
                        return
                    }
                    
                    
                    
                    // token 값을 추출하여 변수에 저장
                    if let token = json["token"] as? String {
                        userToken = token
                        
                        print("토큰: \(userToken!)")
                        
                    } else {
                        print("토큰 값이 없습니다.")
                    }
                    
                    
                    
                    
                    if result == 1 {
                        guard let json = value as? [String: Any],
                                          let result = json["result"] as? Bool,
                                          result,
                                          let token = json["token"] as? String,
                                          let data = json["data"] as? [String: Any],
                                          let user_idx = data["user_idx"] as? Int,
                                          let user_email = data["user_email"] as? String,
                                          let user_name = data["user_name"] as? String,
                                          let user_type = data["user_type"] as? String,
                                          let user_phone = data["user_phone"] as? String else {
                                        print("Invalid JSON data.")
                                        return
                                    }
                        loginData.userName = user_name
                        loginData.userEmail = user_email
                        loginData.userPhoneNumber = user_phone
                        loginData.userType = user_type
                          
                        loginData.userIdx = user_idx
                            
                            
                            
                    } else {
                        let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                        print("로그인 실패: \(errorMessage ?? "알 수 없는 오류")")
                        
                        
                    }
                    
                    
                    if result == 0 {
                        let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                        print("로그인 실패: \(errorMessage ?? "알 수 없는 오류")")
                        
                        
                        showAlert = true
                        
                        alertTitle = "로그인 실패"
                        alertMessage = "\(errorMessage ?? "알 수 없는 오류")"
                        
                    } else {
                        print("로그인 성공: \(value)")
                        
                        // 성공한 경우, value를 사용하여 필요한 작업을 수행
                        loginData.isLogin = true
                        loginData.token = "\(userToken!)"
                        
                    }
                    
                    
                    
                case .failure(let error):
                    // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                    print("로그인 실패: \(error)")
                    
                }
            }
    }
    
    
    //    func autoLogin() {
    //
    //        inputID = defaults.string(forKey: "userID") ?? ""
    //        inputPassword = defaults.string(forKey: "userPassword") ?? ""
    //
    //        if(inputID == "" || inputPassword == ""){
    //
    //            print("자동로그인 실패, 저장된 아이디 정보가 없음")
    //
    //        } else {
    //
    //            //로딩중
    //            isLoading = true
    //
    //
    //            let user = User(id: inputID, password: inputPassword, last_login_env: "admin", fcm_token: "fcm_token")
    //            login(user: user) { result in
    //                switch result {
    //                case .success(let loginResult):
    //                    switch loginResult {
    //                    case .success(let successResult):
    //                        print("로그인 성공: \(successResult)")
    //
    //                        defaults.set(successResult.data.user_id, forKey: "userID")
    //
    //                        //                                                        defaults.set(successResult.data.user_id, forKey: "userID")
    //                        defaults.set(inputPassword, forKey: "accessToken")
    //
    //
    //
    //                        //                        loginData.isLogin = true
    //
    //                    case .failure(let failureResult):
    //                        print("로그인 실패: \(failureResult.errorMessage)")
    //
    //
    //
    //                        loginData.isLogin = false
    //                        isLoading = false
    //
    //
    //
    //                    }
    //                case .failure(let error):
    //                    print("요청 실패: \(error)")
    //
    //                    loginData.isLogin = false
    //
    //                    isLoading = false
    //
    //
    //                }
    //
    //
    //            }
    //        }
    //
    //    }
    
//    private func setMenu(){
//        
//        
//        self.menuViewModel.chatView.send(false)
//        self.menuViewModel.myPageView.send(false)
//        
//        print("called func setMenu")
//        
//        
//        
//    }
}


extension UIApplication { //키패드 내리는 코드인 UIApplication.shared.endEditing() 사용하기 위해 extension
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}




#Preview {
    LoginView()
}
