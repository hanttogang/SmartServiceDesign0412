//
//  DetailCustomerBrandView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/21/23.
//

import SwiftUI
import Alamofire

struct DetailCustomerBrandView: View {
    
    var selectedCustomerBrandIdx: Int
    var selectedBrandName: String
    
    // 이 init 메소드를 통해 외부에서 selectedCustomerBrandIdx 값을 설정할 수 있습니다.
    init(selectedCustomerBrandIdx: Int, selectedBrandName: String) {
        self.selectedCustomerBrandIdx = selectedCustomerBrandIdx
        self.selectedBrandName = selectedBrandName
    }
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    //Api
    @State private var brandDataList = [BrandData]()
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var brandImage = ""
    @State private var registrationImage = ""
    @State private var bankbookCopyImage = ""
    
    @State private var contractDataList = [BrandContractData]()
    @State private var contractCount: Int = 0

    @State private var isApiLoading: Bool = false
    
    //View
    var selectedDetailCustomerName: String = ""
    
    @State private var navigate: Bool = false
    @State private var navigateContractDetailForDetailCustomerBrandView: Bool = false
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var modificationMode: Bool = false
    
    @State private var showingImagePickerForDetailBrandImage = false
    @State private var selectedDetailBrandImage: UIImage? = nil
    
    @State private var customerBrandNameForDetailBrand: String = ""
    
    @State private var businessRegistrationNumberForDetailBrand: String = ""
    @State private var typeOfBusinessForDetailBrand: String = ""
    @State private var placeOfBusinessAddressForDetailBrand: String = ""
    
    @State private var ownerNameForDetailBrand: String = ""
    @State private var ownerPhoneNumberForDetailBrand: String = ""
    @State private var ownerEmailForDetailBrand: String = ""
    
    @State private var managerNameForDetailBrand: String = ""
    @State private var managerPhoneNumberForDetailBrand: String = ""
    @State private var managerEmailForDetailBrand: String = ""
    
    @State private var workAreaForDetailBrand: String = "광고마케팅"
    @State private var selecteWorkAreaForDetailBrand: Bool = false
    @State private var workAreaModificationMode: Bool = false
    
    @State private var showingBusinessLicenseImagePickerForDetailBrand = false
    @State private var selectedBusniessLicenseImageForDetailBrand: UIImage? = nil
    
    @State private var showingBankRecordCopyImagePickerForDetailBrand = false
    @State private var selectedBankRecordCopyImageForDetailBrand: UIImage? = nil
    
    @State private var contractImageForDetailBrandContractList: UIImage? = nil
    
    
    @State private var showAllContractList: Bool = false
    @State private var showShowAllButton: Bool = false
    
    
    
    
    var body: some View {
        
        ZStack {
            if (isApiLoading){
                
                ProgressView()
                    .zIndex(3)
                
            }
            
            ScrollView(.vertical){
                VStack(spacing: 0){
                    
                    
                    HStack{
                        
                        
                        VStack(alignment: .leading){// 브랜드 이미지
                            ZStack{
                                if modificationMode{
                                    
                                    Button(action: {
                                        showingImagePickerForDetailBrandImage = true
                                    }, label: {
                                        
                                        ZStack {
                                            if let image = selectedDetailBrandImage {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                    .cornerRadius(12.0)
                                                
                                            } else {
                                                AsyncImage(url: URL(string: imageS3Url + "/" + brandImage)) { image in
                                                    image.resizable()
                                                        .aspectRatio(contentMode: .fill)
                                                        .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                        .cornerRadius(12.0)
                                                }placeholder: {
                                                    ProgressView()
                                                        .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                }
                                                
                                            }
                                        }
                                    })
                                    
                                }else{
                                    
                                    ZStack {
                                        if let image = selectedDetailBrandImage {
                                            Image(uiImage: image)
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                .cornerRadius(12.0)
                                            
                                        } else {
                                            
                                            AsyncImage(url: URL(string: imageS3Url + "/" + brandImage)) { image in
                                                image.resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                    .cornerRadius(12.0)
                                            }placeholder: {
                                                ProgressView()
                                                    .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                            }
                                            
                                        }
                                    }
                                    
                                }
                            } //ZStack BrandImage
                            .sheet(isPresented: $showingImagePickerForDetailBrandImage, onDismiss: loadDetailBrandImage) {
                                ImagePicker(selectedImage: $selectedDetailBrandImage)
                            }
                            
                        }//VStack 브랜드 이미지
                        
                        
                        if modificationMode{
                            
                            TextField(".", text: $customerBrandNameForDetailBrand, prompt: Text("\(customerBrandNameForDetailBrand)")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.title2)
                            
                        }else {
                            
                            Text("\(customerBrandNameForDetailBrand)")
                                .font(.title2)
                        }
                        
                        
                        
                        
                        
                        Spacer()
                        
                        if(modificationMode){
                            Button(action: {
                                
                                if (!isApiLoading){
                                    
                                    if (customerBrandNameForDetailBrand == "" || businessRegistrationNumberForDetailBrand == "" || typeOfBusinessForDetailBrand == "" || placeOfBusinessAddressForDetailBrand == "" || ownerNameForDetailBrand == "" || ownerPhoneNumberForDetailBrand == "" || ownerEmailForDetailBrand == "" || managerNameForDetailBrand == "" || managerPhoneNumberForDetailBrand == "" || managerEmailForDetailBrand == "" || workAreaForDetailBrand == ""){
                                        
                                        showAlert = true
                                        
                                        alertTitle = "고객 브랜드 수정 실패"
                                        alertMessage = "칸을 비울 수 없습니다."
                                        
                                    } else {
                                        
                                        patchCustomerBrand()
                                        
                                        modificationMode = false
                                    }
                                    
                                }
                                
                            }, label: {
                                Text("저장")
                                
                            })
                        } else {
                            Button(action: {
                                
                                modificationMode = true
                                
                                
                            }, label: {
                                Text("수정")
                                
                            })
                        }
                        
                        
                    }
                    .padding(.horizontal)
                    .padding(.vertical, screenHeight / 81.5)
                    .padding(.top, screenHeight / 81.5)
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        
                        Text("사업자등록번호")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            TextField("", text: $businessRegistrationNumberForDetailBrand)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .keyboardType(.numberPad)
                            
//                            RightAlignedTextField(text: $businessRegistrationNumberForDetailBrand)
//                                .keyboardType(.numberPad)
//                            
                            
                            
                        }else {
                            
                            Text("\(businessRegistrationNumberForDetailBrand)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("업종/업태")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            
                            TextField("", text: $typeOfBusinessForDetailBrand)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                            
//                            RightAlignedTextField(text: $typeOfBusinessForDetailBrand)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
                            
                            
                            
                        }else {
                            
                            Text("\(typeOfBusinessForDetailBrand)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{
                        Text("사업장 주소")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            TextField("", text: $placeOfBusinessAddressForDetailBrand)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                            
//                            RightAlignedTextField(text: $placeOfBusinessAddressForDetailBrand)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
//                            
                            
                            
                        }else {
                            
                            Text("\(placeOfBusinessAddressForDetailBrand)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("대표자명")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            TextField("", text: $ownerNameForDetailBrand)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                            
                            
//                            RightAlignedTextField(text: $ownerNameForDetailBrand)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(ownerNameForDetailBrand)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{
                        Text("전화번호")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            
                            TextField("", text: $ownerPhoneNumberForDetailBrand)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .keyboardType(.numberPad)
                            
//                            RightAlignedTextField(text: $ownerPhoneNumberForDetailBrand)
//                                .keyboardType(.numberPad)
                        }else {
                            
                            Text("\(ownerPhoneNumberForDetailBrand)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("이메일")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            
                            TextField("", text: $ownerEmailForDetailBrand)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .keyboardType(.emailAddress)
                                .autocapitalization(.none)
                            
//                            RightAlignedTextField(text: $ownerEmailForDetailBrand)
//                                .keyboardType(.emailAddress)
//                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(ownerEmailForDetailBrand)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{
                        Text("담당자명")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            
                            
                            TextField("", text: $managerNameForDetailBrand)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                            
//                            RightAlignedTextField(text: $managerNameForDetailBrand)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(managerNameForDetailBrand)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("담당자 전화번호")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            
                            
                            TextField("", text: $managerPhoneNumberForDetailBrand)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                                .keyboardType(.numberPad)
                            
//                            RightAlignedTextField(text: $managerPhoneNumberForDetailBrand)
//                                .keyboardType(.numberPad)
//                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(managerPhoneNumberForDetailBrand)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{
                        Text("담당자 이메일")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            
                            
                            TextField("", text: $managerEmailForDetailBrand)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                                .keyboardType(.emailAddress)
                            
//                            RightAlignedTextField(text: $managerEmailForDetailBrand)
//                                .keyboardType(.emailAddress)
//                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(managerEmailForDetailBrand)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("업무영역")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        
                        
                        if modificationMode{
                            Button(action: {
                                selecteWorkAreaForDetailBrand = true
                            }, label: {
                                
                                //HStack 으로 버튼 세 개 만든 후 각각 터치 이벤트에 selecteWorkAreaForDetailBrand = false 처리하기
                                
                                
                                
                                
                                
                                if workAreaModificationMode{
                                    
                                    Button(action: {
                                        workAreaModificationMode = false
                                        
                                        workAreaForDetailBrand = "광고마케팅"
                                        
                                    }, label: {
                                        Text("광고마케팅")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                    
                                    Button(action: {
                                        workAreaModificationMode = false
                                        
                                        workAreaForDetailBrand = "MCN"
                                        
                                    }, label: {
                                        Text("MCN")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                    
                                    Button(action: {
                                        workAreaModificationMode = false
                                        
                                        workAreaForDetailBrand = "기타"
                                        
                                    }, label: {
                                        Text("기타")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                    
                                    
                                }else{
                                    
                                    Button(action: {
                                        workAreaModificationMode = true
                                        
                                    }, label: {
                                        Text("\(workAreaForDetailBrand)")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                }
                                
                                
                                
                                
                            })
                        } else{
                            
                            Text("\(workAreaForDetailBrand)")
                                .foregroundColor(.white)
                                .bold()
                                .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                .background(Color("mainColor"))
                                .cornerRadius(16.0)
                        }
                        
                        
                    }
                    .padding(.horizontal)
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack(spacing: 0){
                        
                        Spacer()
                        
                        VStack(alignment: .leading){// 사업자등록증 이미지
                            
                            Text("사업자등록증")
                                .padding(.top, screenHeight/30.75)
                            
                            ZStack{
                                
                                
                                if modificationMode{
                                    
                                    Button(action: {
                                        showingBusinessLicenseImagePickerForDetailBrand = true
                                    }, label: {
                                        
                                        ZStack {
                                            if let image = selectedBusniessLicenseImageForDetailBrand {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                    .cornerRadius(12.0)
                                                
                                            } else {
                                                
                                                
                                                AsyncImage(url: URL(string: imageS3Url + "/" + registrationImage)) { image in
                                                    image.resizable()
                                                        .aspectRatio(contentMode: .fill)
                                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                        .cornerRadius(12.0)
                                                }placeholder: {
                                                    ProgressView()
                                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                }
                                                
                                            }
                                        }
                                    })
                                    
                                }else{
                                    
                                    ZStack {
                                        if let image = selectedBusniessLicenseImageForDetailBrand {
                                            Image(uiImage: image)
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                .cornerRadius(12.0)
                                            
                                        } else {
                                            
                                            AsyncImage(url: URL(string: imageS3Url + "/" + registrationImage)) { image in
                                                image.resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                    .cornerRadius(12.0)
                                            }placeholder: {
                                                ProgressView()
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                            }
                                            
                                        }
                                    }
                                    
                                }
                            } //ZStack 사업자등록증이미지
                            .sheet(isPresented: $showingBusinessLicenseImagePickerForDetailBrand, onDismiss: loadBusniessLicenseImage) {
                                ImagePicker(selectedImage: $selectedBusniessLicenseImageForDetailBrand)
                            }
                            
                            
                            
                        }//VStack 사업자등록증 이미지
                        
                        
                        
                        Spacer()
                        
                        
                        VStack(alignment: .leading){// 통장사본 이미지
                            
                            Text("통장사본")
                                .padding(.top, screenHeight/30.75)
                            
                            
                            ZStack{
                                
                                
                                if modificationMode{
                                    
                                    Button(action: {
                                        showingBankRecordCopyImagePickerForDetailBrand = true
                                    }, label: {
                                        
                                        ZStack {
                                            if let image = selectedBankRecordCopyImageForDetailBrand {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                    .cornerRadius(12.0)
                                                
                                            } else {
                                                
                                                AsyncImage(url: URL(string: imageS3Url + "/" + bankbookCopyImage)) { image in
                                                    image.resizable()
                                                        .aspectRatio(contentMode: .fill)
                                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                        .cornerRadius(12.0)
                                                }placeholder: {
                                                    ProgressView()
                                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                }
                                                
                                            }
                                        }
                                    })
                                    
                                }else{
                                    
                                    ZStack {
                                        if let image = selectedBankRecordCopyImageForDetailBrand {
                                            Image(uiImage: image)
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                .cornerRadius(12.0)
                                            
                                        } else {
                                            AsyncImage(url: URL(string: imageS3Url + "/" + bankbookCopyImage)) { image in
                                                image.resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                    .cornerRadius(12.0)
                                            }placeholder: {
                                                ProgressView()
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                            }
                                            
                                        }
                                    }
                                    
                                }
                            } //ZStack 통장사본 이미지
                            .sheet(isPresented: $showingBankRecordCopyImagePickerForDetailBrand, onDismiss: loadBankRecordCopyImage) {
                                ImagePicker(selectedImage: $selectedBankRecordCopyImageForDetailBrand)
                            }
                            
                            
                            
                        }//VStack 통장사본 이미지
                        
                        Spacer()
                    }//HStack
                    
                    
                    
                    VStack(spacing: 0) {
                        HStack{
                            
                            Text("계약 리스트")
                                .font(.title2)
                                .bold()
                                .padding()
                            //                                .padding(.top, -screenHeight/13.5)
                            Spacer()
                        }
                        .padding(0)
                        
                        Button(action: {
                            print("계약 추가 버튼 클릭됨")
                            
                            self.navigate = true
                            
                        }, label: {
                            
                            NavigationLink(destination: AddContractOfCustomerBrandView(selectedCustomerBrandIdx: selectedCustomerBrandIdx, selectedBrandName: selectedBrandName), isActive: $navigate) {
                                EmptyView()
                            }
                            
                            Text("+  계약 추가")
                                .foregroundColor(.white)
                                .font(.system(size: 16))
                                .bold()
                                .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        
                        
                        

                        VStack{

                            ForEach(contractDataList.prefix(showAllContractList ? contractDataList.count : 3), id: \.brandContractIdx) { contractData in
                            
//                            ForEach(Array(contractDataList.prefix(showAllContractList ? contractDataList.count : 3)), id: \.brandContractIdx) { contractData in
                                
                                NavigationLink(destination: ContractDetailForDetailCustomerBrandView(selectedCustomerBrandContractIdx: contractData.brandContractIdx, selectedCustomerBrandIdx: selectedCustomerBrandIdx, selectedBrandName: selectedBrandName)) {
                                    
                                    VStack{
                                        HStack{ //리스트항목의 모델
                                            
                                            if contractData.contractImage != nil {
                                                
                                                AsyncImage(url: URL(string: imageS3Url + "/" + contractData.contractImage)) { image in
                                                    image.resizable()
                                                         .aspectRatio(contentMode: .fill)
                                                         .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                         .cornerRadius(12.0)
                                                } placeholder: {
                                                    ProgressView()
                                                        .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                }
                                                
                                            } else {
                                                
                                                Rectangle()
                                                    .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                    .cornerRadius(12.0)
                                                    .foregroundColor(.colorE5E5E5)
                                            }
                                            
                                            VStack(alignment: .leading){
                                                Text(contractData.contractName)
                                                    .foregroundColor(.black)
                                                
                                                Text("\(dateFormatter(getDate: contractData.contractStartDt)) ~ \(dateFormatter(getDate: contractData.contractEndDt))")
                                                    .foregroundColor(Color("color00000040"))
                                                    .font(.caption)
                                            }
                                            
                                            Spacer()
                                            
                                            Text(contractData.contractStatus)
                                                .foregroundColor(.white)
                                                .bold()
                                                .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                                .background(Color("mainColor"))
                                                .cornerRadius(16.0)
                                            
                                            
                                        }
                                        
                                        Divider()
                                            .padding(.leading, screenWidth/8)
                                    }
                                    
                                }//NavigationLink
                            }
                            
                            
                            
                            
                        } //VStack 계약 리스트
                        .listStyle(PlainListStyle())
                        .padding(.horizontal)
                        .padding(.top, screenHeight/50.9375)
                        
                        if showShowAllButton{
                            
                            Button(action: {
                                
                                showShowAllButton = false
                                showAllContractList = true
                                
                            }, label: {
                                
                                Text("더 보기")
                                    .foregroundColor(.black)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                            })
                            .background(.white)
                            .cornerRadius(4)
                            .padding()
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color(.black), lineWidth: 1)
                                .padding()
                            )
                            
                            
                        }
                        
                        Spacer()
                        
                        
                        
                        
                    }//VStack
                    .frame(width: screenWidth/1.0932)
                    .background(Color.white)
                    .cornerRadius(20)
                    .shadow(color: Color("color00000040"), radius: 20, x: 0, y: 30)
                    .padding(.top, screenHeight/31.346)
                    
                    
                    
                    //                    .sheet(isPresented: $showingBankRecordCopyImagePickerForDetailBrand, onDismiss: loadContractImageForDetailBrandContractList) {
                    //                        ImagePicker(selectedImage: $selectedBankRecordCopyImageForDetailBrand)
                    //                    }
                    
                    
                    
                    //하단 네비게이션을 위한 패딩
                    HStack{
                        Rectangle()
                            .frame(width: 0, height: 0)
                            .foregroundColor(.white)
                    }
                    .padding(.bottom, screenHeight/8.826)
                    
                    Spacer()
                    
                    
                    
                    
                } //VStack
                
                
            }//ScrollView
            
        }//ZStack
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            
            self.presentationMode.wrappedValue.dismiss()
            
            
        }) {
            
            HStack(spacing: 0){
                
                Image(systemName: "chevron.backward")
                
            }
            
            
            
        })
        .navigationBarTitle("\(selectedBrandName)", displayMode: .inline)
        .onAppear {
            
            brandInquiry(accessToken: loginData.token)
            contractInquiry(accessToken: loginData.token)
            
        }
        
        
        
        
    }//body
    
    
    private func loadDetailBrandImage() {
        guard let selectedDetailBrandImage = selectedDetailBrandImage else { return }
        // You can do something with the selected brand image here
        
        isApiLoading = true
        
        uploadImage(image: selectedDetailBrandImage, imageType: "brand")
        print(brandImage)
    }
    
    private func loadBusniessLicenseImage() {
        guard let selectedBusniessLicenseImageForDetailBrand = selectedBusniessLicenseImageForDetailBrand else { return }
        // You can do something with the selected brand image here
        
        isApiLoading = true
        
        uploadImage(image: selectedBusniessLicenseImageForDetailBrand, imageType: "registration")
        print(registrationImage)
        
    }
    
    private func loadBankRecordCopyImage() {
        guard let selectedBankRecordCopyImageForDetailBrand = selectedBankRecordCopyImageForDetailBrand else { return }
        // You can do something with the selected brand image here
        
        isApiLoading = true
        
        uploadImage(image: selectedBankRecordCopyImageForDetailBrand, imageType: "bankbook")
        print(bankbookCopyImage)
    }
    
    //브랜드 정보 가져옴
    func brandInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/api/brand?brand_idx=\(selectedCustomerBrandIdx)&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: BrandDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    for brandData in response.data {
                        // 브랜드 이름을 출력합니다.
                        print("브랜드 번호: \(brandData.brandIdx)")
                        
                        customerBrandNameForDetailBrand = brandData.brandName
                        businessRegistrationNumberForDetailBrand = brandData.registrationNumber
                        typeOfBusinessForDetailBrand = brandData.buisnessType
                        placeOfBusinessAddressForDetailBrand = "\(brandData.address) \(brandData.addressDetail)"
                        
                        ownerNameForDetailBrand = brandData.representativeName
                        ownerPhoneNumberForDetailBrand = brandData.representativePhone
                        ownerEmailForDetailBrand = brandData.representativeEmail
                        
                        managerNameForDetailBrand = brandData.managerName!
                        managerPhoneNumberForDetailBrand = brandData.managerPhone
                        managerEmailForDetailBrand = brandData.managerEmail
                        
                        workAreaForDetailBrand = brandData.manageType
                        
                        brandImage = brandData.brandImage
                        registrationImage = brandData.registrationImage
                        bankbookCopyImage = brandData.bankbookCopy
                        
                        //
                        //                    print("브랜드 사진: \(brandData.brandImage)")
                        //                    print("브랜드 이름: \(brandData.brandName)")
                        //                    print("비즈니스 타입: \(brandData.buisnessType)")
                        //                    print("등록일: \(brandData.firstCreateDt)")
                        
                    }
                    DispatchQueue.main.async {
                        self.brandDataList = response.data
                    }
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
            
        })
        
        
    }
    
    
    
    //최종적으로 수정
    private func patchCustomerBrand() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any] = [
            "brand_name": customerBrandNameForDetailBrand,
            "registration_number": businessRegistrationNumberForDetailBrand,
            "buisness_type": typeOfBusinessForDetailBrand,
            "address": placeOfBusinessAddressForDetailBrand,
            "address_detail": "",
            "representative_name": ownerNameForDetailBrand,
            "representative_phone": ownerPhoneNumberForDetailBrand,
            "representative_email": ownerEmailForDetailBrand,
            "manager_name": managerNameForDetailBrand,
            "manager_phone": managerPhoneNumberForDetailBrand,
            "manager_email": managerEmailForDetailBrand,
            "manage_type": workAreaForDetailBrand,
            "brand_image": brandImage,
            "registration_image": registrationImage,
            "bankbook_copy": bankbookCopyImage
        ]
        
        
        AF.request("\(defaultUrl)/api/brand/\(selectedCustomerBrandIdx)",
                   method: .patch,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("고객 브랜드 수정 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("고객 브랜드 수정 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("고객 브랜드 수정 실패: \(error)")
                
            }
        }
    }
    
    
    //Api 를통해 필요한 값 호출 (url, key 값)
    private func getApiCommonBrandImageURL(imageType: String, completion: @escaping (String?, String?) -> Void) {
        
        let headers: HTTPHeaders = [
            "accept": "application/json",
            "Authorization": "Bearer \(loginData.token)"
        ]
        
        let parameters: Parameters = [
            "mimetype": "image/png",
            "type": "brand",
            "extension": "png"
        ]
        
        AF.request("\(defaultUrl)/api/common/image/url", method: .get, parameters: parameters, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    let url = JSON["data"] as? String
                    let key = JSON["key"] as? String
                    completion(url, key)
                }
            case .failure(let error):
                print(error)
                completion(nil, nil)
            }
        }
    }
    
    //사진 선택시 uploadImag 함수를 호출
    private func uploadImage(image: UIImage, imageType: String) {
        getApiCommonBrandImageURL(imageType: imageType) { (url, key) in
            
            guard let preSignedUrl = url else {
                print("Failed to get pre-signed URL.")
                return
            }
            
            guard let imageData = image.pngData() else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: url!, method: .put).response { response in
                switch response.result {
                case .success:
                    print("Image uploaded successfully.")
                    
                    isApiLoading = false
                    
                case .failure(let error):
                    print("Failed to upload image: \(error)")
                }
            }
            
            
            guard let imageData = image.jpegData(compressionQuality: 1.0) else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: preSignedUrl).response { response in
                if let error = response.error {
                    print("Image upload failed with error: \(error)")
                } else {
                    print("Image uploaded successfully.")
                    switch imageType {
                    case "brand":
                        brandImage = key!
                        
                        print("\(brandImage)")
                        
                    case "registration":
                        registrationImage = key!
                        
                        print("\(registrationImage)")
                        
                    case "bankbook":
                        bankbookCopyImage = key!
                        
                        print("\(bankbookCopyImage)")
                        
                    default:
                        print("Unknown image type: \(imageType)")
                    }
                }
            }
        }
    }
    
    
    
    
    //계약 가져오는 api
    func contractInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/brand-contract?brand_idx=\(selectedCustomerBrandIdx)&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: BrandContractDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    
                    DispatchQueue.main.async {
                        contractDataList = response.data
                        
                        contractCount = response.pagination.total
                        
                        print("계약 리스트 호출 성공 \(response.data)")
                        
//                        if contractCount > 3 {
//                            showShowAllButton = true //더 보기 버튼 보임
//                        }
//                        
                        if (contractCount <= 3) { //
                            showShowAllButton = false
                            showAllContractList = false
                        }else{
                            showShowAllButton = true //더 보기 버튼 보임
                            showAllContractList = false
                        }
                    }
                    
                    
                    
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
        })
        
    }
    
    private func dateFormatter(getDate: String) -> String {
        
        let isoDateFormatter = ISO8601DateFormatter()
        isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

        let getDate = getDate
        if let date = isoDateFormatter.date(from: getDate) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy.MM.dd"
            let dateString = dateFormatter.string(from: date)
//            print(dateString) //ex "2024.01.05" 출력
            return dateString
        } else {
            print("날짜 변환에 실패했습니다.")
            return ""
        }

    }
}
//
//#Preview {
//    DetailCustomerBrandView()
//}
