//
//  CompleteCustomerRegistrationView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/20/23.
//

import SwiftUI

struct CompleteCustomerRegistrationView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State var registrationComplete: Bool = false
    
    var body: some View {
        
        
        
        
        
        if registrationComplete{
            MainCustomerManagementView()
        } else{
            
            
            
            
            ZStack {
                
                VStack(spacing: 0){
                    ScrollView{
                        
                        
                        ZStack{
                            
                            HStack{
                                
                                
                                ZStack{
                                    Button(action: {
                                        //                                    self.presentationMode.wrappedValue.dismiss()
                                        registrationComplete = true
                                        
                                    }, label: {
                                        Text("이전")
                                            .foregroundColor(.black)
                                        
                                    })
                                    .frame(width: screenWidth/7.211, height: screenHeight/31.346)
                                    .background(Color("colorE5E5E5"))
                                    .cornerRadius(4.0)
                                }
                                
                                Spacer()
                            }
                            .padding()
                            
                            HStack{
                                Spacer()
                                
                                HStack{
                                    Circle()
                                        .frame(width: screenWidth/46.875)
                                        .foregroundColor(Color("colorE0E0E0"))
                                    
                                    Circle()
                                        .frame(width: screenWidth/46.875)
                                        .foregroundColor(Color("colorE0E0E0"))
                                    
                                    Circle()
                                        .frame(width: screenWidth/46.875)
                                        .foregroundColor(Color("colorE0E0E0"))
                                    
                                    Circle()
                                        .frame(width: screenWidth/46.875)
                                        .foregroundColor(Color("mainColor"))
                                    
                                }
                                Spacer()
                                
                            }
                            
                        }
                        
                        Spacer()
                        
                        Image("imgComplete")
                        
                        
                        Text("등록이 완료되었습니다.")
                            .font(.title2)
                            .padding(.top, 40)
                        
                        Text("확인 버튼을 누르면\n고객(브랜드) 리스트로 이동합니다.")
                            .font(.system(size: 14))
                            .multilineTextAlignment(.center)
                            .foregroundColor(Color("color979797"))
                            .padding(.top, 27)
                        
                        
                        Spacer()
                        
                        Button(action: {
                            print("확인 버튼 Click")
                            registrationComplete = true
                            
                        }, label: {
                            
                            Text("확인")
                                .foregroundColor(.white)
                                .font(.system(size: 18))
                                .bold()
                                .frame(width: screenWidth/1.14, height: screenHeight/15.03)
                            
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/41.066)
                        .padding(.bottom, 29)
                        
                        
                        
                    }
                    
                    //하단 네비게이션을 위한 패딩
                    HStack{
                        Rectangle()
                            .frame(width: 0, height: 0)
                            .foregroundColor(.white)
                    }
                    .padding(.bottom, screenHeight/8.826)
                    
                    Spacer()
                    
                    
                    
                }
                
            }//VStack
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading: Button(action: {
                
//                self.presentationMode.wrappedValue.dismiss()
                registrationComplete = true
                
            }) {
                
                HStack(spacing: 0){
                    
                    Image(systemName: "chevron.backward")
                    
                }
                
                
                
            })
            .navigationBarTitle("고객(브랜드) 등록", displayMode: .inline)
            
            
            
            
        }
        
        
        
        
    }//ZStack
    
}

#Preview {
    CompleteCustomerRegistrationView()
}
