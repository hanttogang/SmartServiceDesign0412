//
//  FirtstCustomerRegistrationView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/20/23.
//

import SwiftUI
import UIKit

struct FirtstCustomerRegistrationView: View {
    
    @EnvironmentObject var registrationCustomerBrandData: RegistrationCustomerBrandData
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var navigate: Bool = false
    
    @State var customerNameForRegistration: String = ""
    @State var businessRegistrationNumberForRegistration: String = ""
    @State var typeOfBusinessForRegistration: String = ""
    
    @State var placeOfBusinessAddress: String = ""
    
    @State var ownerNameForRegistration: String = ""
    @State var ownerPhoneNumberForRegistration: String = ""
    @State var ownerEmailForRegistration: String = ""
    
    @State var chatImage: UIImage?
    
    
    
    var body: some View {
        
        
        ZStack{
            
            
                
                    
                    ScrollView{
                        
                        VStack(spacing: 0){
                            
                            ZStack{
                                
                                HStack{
                                    Spacer()
                                    
                                    HStack{
                                        Circle()
                                            .frame(width: screenWidth/46.875)
                                            .foregroundColor(Color("mainColor"))
                                        
                                        Circle()
                                            .frame(width: screenWidth/46.875)
                                            .foregroundColor(Color("colorE0E0E0"))
                                        
                                        Circle()
                                            .frame(width: screenWidth/46.875)
                                            .foregroundColor(Color("colorE0E0E0"))
                                        
                                        Circle()
                                            .frame(width: screenWidth/46.875)
                                            .foregroundColor(Color("colorE0E0E0"))
                                        
                                    }
                                    Spacer()
                                    
                                } //HStack 원 색 진행도
                                
                                HStack{
                                    Spacer()
                                    
                                    ZStack{
                                        Button(action: {
//                                            viewCapture()
                                            
                                            print(chatImage)
                                            
                                            if(customerNameForRegistration == "" || businessRegistrationNumberForRegistration == "" || typeOfBusinessForRegistration == "" ||
                                               placeOfBusinessAddress == "" ||
                                               ownerNameForRegistration == "" ||
                                               ownerPhoneNumberForRegistration == "" ||
                                               ownerEmailForRegistration == ""){
                                                
                                                
                                                showAlert = true
                                                
                                                alertTitle = "마케팅 채널 등록 실패"
                                                alertMessage = "칸을 비울 수 없습니다."
                                                
//                                                //임시로 넘어가게 함
//                                                self.navigate = true
                                                
                                                
                                                
                                            }else{
                                                
                                                registrationCustomerBrandData.brandName = customerNameForRegistration
                                                registrationCustomerBrandData.registrationNumber = businessRegistrationNumberForRegistration
                                                registrationCustomerBrandData.businessType = typeOfBusinessForRegistration
                                                registrationCustomerBrandData.addressDetail = placeOfBusinessAddress
                                                registrationCustomerBrandData.representativeName = ownerNameForRegistration
                                                registrationCustomerBrandData.representativePhone = ownerPhoneNumberForRegistration
                                                registrationCustomerBrandData.representativeEmail = ownerEmailForRegistration
                                                
                                                
                                                self.navigate = true
                                            }
                                            
                                        }, label: {
                                            
                                            NavigationLink(destination: SecondCustomerRegistrationView(), isActive: $navigate) {
                                                EmptyView()
                                            }
                                            
                                            Text("다음")
                                                .foregroundColor(.white)
                                            
                                        })
                                        .frame(width: screenWidth/7.211, height: screenHeight/31.346)
                                        .background(.blue)
                                        .cornerRadius(4.0)
                                    }
                                }
                                .padding()
                                
                                
                            } // ZStack 다음버튼
                            .padding(.top, screenHeight/45.1) // 18
                            
                            
                            
                            VStack{
                                
                                HStack(spacing: 0){
                                    Text("고객(브랜드)명")
                                        .font(.system(size: 14))
                                    
                                    Spacer()
                                }
                                .padding(.leading, screenWidth / 15.625)
                                
                                HStack{
                                    TextField("", text: $customerNameForRegistration, prompt: Text("고객(브랜드)명 입력해주세요.")
                                        .foregroundColor(Color("hintTextColor")))
                                    .keyboardType(.default)
                                    .font(.system(size: 14))
                                    .padding(.leading, 14)
                                    .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                }
                                .frame(width: screenWidth / 1.14, height: screenHeight / 18.52)
                                .overlay(RoundedRectangle(cornerRadius: 4)
                                    .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                                )
                                
                            }//VStack 고객 명
                            
                            VStack{
                                
                                HStack(spacing: 0){
                                    Text("사업자 등록 번호")
                                        .font(.system(size: 14))
                                    
                                    Spacer()
                                }
                                .padding(.leading, screenWidth / 15.625)
                                
                                HStack{
                                    TextField("", text: $businessRegistrationNumberForRegistration, prompt: Text("사업자 등록번호를 입력해주세요.")
                                        .foregroundColor(Color("hintTextColor")))
                                    .keyboardType(.numberPad)
                                    .font(.system(size: 14))
                                    .padding(.leading, 14)
                                    .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                }
                                .frame(width: screenWidth / 1.14, height: screenHeight / 18.52)
                                .overlay(RoundedRectangle(cornerRadius: 4)
                                    .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                                )
                                
                            }//VStack 사업자 등록 번호
                            .padding(.top, screenHeight / 54.133) //15
                            
                            VStack{
                                
                                HStack(spacing: 0){
                                    Text("업종/업태")
                                        .font(.system(size: 14))
                                    
                                    Spacer()
                                }
                                .padding(.leading, screenWidth / 15.625)
                                
                                HStack{
                                    TextField("", text: $typeOfBusinessForRegistration, prompt: Text("업종/업태를 입력해주세요.")
                                        .foregroundColor(Color("hintTextColor")))
                                    .keyboardType(.default)
                                    .font(.system(size: 14))
                                    .padding(.leading, 14)
                                    .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                }
                                .frame(width: screenWidth / 1.14, height: screenHeight / 18.52)
                                .overlay(RoundedRectangle(cornerRadius: 4)
                                    .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                                )
                                
                            }//VStack 업종/업태
                            .padding(.top, screenHeight / 54.133) //15
                            
                            VStack{// VStack 사업장 조회
                                
                                VStack{
                                    
                                    HStack(spacing: 0){
                                        Text("사업장 주소")
                                            .font(.system(size: 14))
                                        
                                    
                                        
                                        Spacer()
                                    }
                                    .padding(.leading, screenWidth / 15.625)
                                    
                                    HStack(spacing: 0){
                                        
                                        
                                        HStack(spacing: 0){
//                                            Text("\(placeOfBusinessAddress)")
//                                                .foregroundColor(Color("hintTextColor"))
//                                                .font(.system(size: 14))
//                                                .padding(.leading, 14)
                                            
                                            TextField("", text: $placeOfBusinessAddress, prompt: Text("사업장 주소를 입력해주세요.")
                                                .foregroundColor(Color("hintTextColor")))
                                            .keyboardType(.default)
                                            .font(.system(size: 14))
                                            .padding(.leading, 14)
                                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                            
                                            Spacer()
                                            
                                        }
                                        
                                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.52)
                                        .cornerRadius(4)
                                        .overlay(RoundedRectangle(cornerRadius: 4)
                                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                                        )
                                        .padding(.trailing, screenWidth / 31.25) //12
                                        
                                        
                                        Spacer()
                                    }
                                    .padding(.leading, screenWidth / 15.625)
                                    
                                    
                                    
                                }// VStack
                                .padding(.top, screenHeight / 54.133) //15
                                
                                
                                
                            }// VStack 사업장 조회
                            
                            VStack{
                                
                                HStack(spacing: 0){
                                    Text("대표자명")
                                        .font(.system(size: 14))
                                    
                                    Spacer()
                                }
                                .padding(.leading, screenWidth / 15.625)
                                
                                HStack{
                                    TextField("", text: $ownerNameForRegistration, prompt: Text("대표자명을 입력해주세요.")
                                        .foregroundColor(Color("hintTextColor")))
                                    .keyboardType(.default)
                                    .font(.system(size: 14))
                                    .padding(.leading, 14)
                                    .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                }
                                .frame(width: screenWidth / 1.14, height: screenHeight / 18.52)
                                .overlay(RoundedRectangle(cornerRadius: 4)
                                    .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                                )
                                
                            }//VStack 대표자명
                            .padding(.top, screenHeight / 54.133) //15
                            
                            VStack{
                                
                                HStack(spacing: 0){
                                    Text("전화번호")
                                        .font(.system(size: 14))
                                    
                                    Spacer()
                                }
                                .padding(.leading, screenWidth / 15.625)
                                
                                HStack{
                                    TextField("", text: $ownerPhoneNumberForRegistration, prompt: Text("대표 전화번호를 입력해주세요.")
                                        .foregroundColor(Color("hintTextColor")))
                                    .keyboardType(.phonePad)
                                    .font(.system(size: 14))
                                    .padding(.leading, 14)
                                    .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                }
                                .frame(width: screenWidth / 1.14, height: screenHeight / 18.52)
                                .overlay(RoundedRectangle(cornerRadius: 4)
                                    .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                                )
                                
                            }//VStack 대표 전화번호
                            .padding(.top, screenHeight / 54.133) //15
                            
                            VStack{
                                
                                HStack(spacing: 0){
                                    Text("이메일")
                                        .font(.system(size: 14))
                                    
                                    Spacer()
                                }
                                .padding(.leading, screenWidth / 15.625)
                                
                                HStack{
                                    TextField("", text: $ownerEmailForRegistration, prompt: Text("대표 이메일을 입력해주세요.")
                                        .foregroundColor(Color("hintTextColor")))
                                    .keyboardType(.emailAddress)
                                    .font(.system(size: 14))
                                    .padding(.leading, 14)
                                    .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                }
                                .frame(width: screenWidth / 1.14, height: screenHeight / 18.52)
                                .overlay(RoundedRectangle(cornerRadius: 4)
                                    .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                                )
                                
                            }//VStack 이메일
                            .padding(.top, screenHeight / 54.133) //15
                            
//                            if let image = chatImage {
//                                        Image(uiImage: image)
//                                    } else {
//                                        Text("No image available")
//                                    }
                            
                            //하단 네비게이션을 위한 패딩
                            HStack{
                                Rectangle()
                                    .frame(width: 0, height: 0)
                                    .foregroundColor(.white)
                            }
                            .padding(.bottom, screenHeight/8.826)
                            
                            
                        }//VStack
                        .navigationBarBackButtonHidden(true)
                        .navigationBarItems(leading: Button(action: {
                            
                            self.presentationMode.wrappedValue.dismiss()
                            
                            
                        }) {
                            
                            HStack(spacing: 0){
                                
                                Image(systemName: "chevron.backward")
                                
                            }
                            
                            
                            
                        })
                        .navigationBarTitle("고객(브랜드) 등록", displayMode: .inline)
                        
                        
                    }//ScrollView
                    .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                        UIApplication.shared.endEditing()
                        
                    }
            
            
        }//ZStack
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        
    }
    
//    func viewCapture() {
//        
//        let controller = UIHostingController(rootView: FirtstCustomerRegistrationView())
//        controller.view.frame = CGRect(origin: .zero, size: CGSize(width: screenWidth, height: screenHeight))
//        
//                    
//        if let rootVC = UIApplication.shared.windows.first?.rootViewController {
//            rootVC.view.insertSubview(controller.view, at: 0)
//
//            /////이미지 캡쳐//////
//            let renderer = UIGraphicsImageRenderer(size: CGSize(width: screenWidth, height: screenHeight))
//
//            chatImage = renderer.image { context in
//                controller.view.layer.render(in: context.cgContext)
//            }
//            ///////////////////
//            
//            controller.view.removeFromSuperview()
//        }
//    }
}


//#Preview {
//    FirtstCustomerRegistrationView()
//}
