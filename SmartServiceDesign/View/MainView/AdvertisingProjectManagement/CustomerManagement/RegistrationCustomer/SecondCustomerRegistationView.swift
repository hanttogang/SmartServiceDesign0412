//
//  SecondCustomerRegistrationView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/20/23.
//

import SwiftUI

enum WorkArea: CaseIterable {
    case advertisingMarketing
    case mcn
    case anotherExample
}


struct SecondCustomerRegistrationView: View {
    @EnvironmentObject var registrationCustomerBrandData: RegistrationCustomerBrandData
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State var nameOfManagerForRegistation: String = ""
    @State var phoneNumberOfManagerForRegistation: String = ""
    
    @State private var selectedWorkArea: WorkArea = .advertisingMarketing
    
    @State var emailOfManagerForRegistation: String = ""
    
    @State private var navigate: Bool = false
    
    var body: some View {
        
        
        ZStack{
            
            
            
            
            
            ScrollView{
                
                VStack(spacing: 0){
                    
                    ZStack{
                        
                        HStack{
                            
                            
                            ZStack{
                                Button(action: {
                                    self.presentationMode.wrappedValue.dismiss()
                                }, label: {
                                    Text("이전")
                                        .foregroundColor(.black)
                                    
                                })
                                .frame(width: screenWidth/7.211, height: screenHeight/31.346)
                                .background(Color("colorE5E5E5"))
                                .cornerRadius(4.0)
                            }
                            
                            Spacer()
                        }
                        .padding()
                        
                        HStack{
                            Spacer()
                            
                            HStack{
                                Circle()
                                    .frame(width: screenWidth/46.875)
                                    .foregroundColor(Color("colorE0E0E0"))
                                
                                Circle()
                                    .frame(width: screenWidth/46.875)
                                    .foregroundColor(Color("mainColor"))
                                
                                Circle()
                                    .frame(width: screenWidth/46.875)
                                    .foregroundColor(Color("colorE0E0E0"))
                                
                                Circle()
                                    .frame(width: screenWidth/46.875)
                                    .foregroundColor(Color("colorE0E0E0"))
                                
                            }
                            Spacer()
                            
                        }
                        
                        HStack{
                            Spacer()
                            
                            ZStack{
                                Button(action: {
                                    
                                    
                                    if(nameOfManagerForRegistation == "" || phoneNumberOfManagerForRegistation == "" || emailOfManagerForRegistation == ""){
                                        
                                        
                                        showAlert = true
                                        
                                        alertTitle = "마케팅 채널 등록 실패"
                                        alertMessage = "칸을 비울 수 없습니다."
                                        
                                        
                                        
//                                        //임시로 넘어가게 함
//                                        self.navigate = true
                                        
                                        
                                    }else{
                                        registrationCustomerBrandData.managerName = nameOfManagerForRegistation
                                        registrationCustomerBrandData.managerPhone = phoneNumberOfManagerForRegistation
                                        registrationCustomerBrandData.managerEmail = emailOfManagerForRegistation
                                        registrationCustomerBrandData.manageType = "marketing"
                                        
                                        
                                        self.navigate = true
                                        
                                    }
                                    
                                }, label: {
                                    
                                    NavigationLink(destination: ThirdCustomerRegistrationView(), isActive: $navigate) {
                                        EmptyView()
                                    }
                                    
                                    Text("다음")
                                        .foregroundColor(.white)
                                    
                                })
                                .frame(width: screenWidth/7.211, height: screenHeight/31.346)
                                .background(.blue)
                                .cornerRadius(4.0)
                            }
                        }
                        .padding()
                        
                        
                    }
                    .padding(.top, screenHeight/45.1) // 18
                    
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("담당자명")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $nameOfManagerForRegistation, prompt: Text("담당자명을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이름
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("담당자 전화번호")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $phoneNumberOfManagerForRegistation, prompt: Text("담당자 전화번호를 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.numberPad)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이름
                    .padding(.top, screenHeight / 54.133) //15
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("담당자 이메일")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $emailOfManagerForRegistation, prompt: Text("담당자 이메일을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.emailAddress)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이름
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("업무영역")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack(spacing: 0) {
                            
                            Button(action: {
                                
                                selectedWorkArea = .advertisingMarketing
                                
                            }) {
                                Text("광고마케팅")
                                    .frame(width: screenWidth/3.26, height: screenHeight/23.97)
                                    .font(.system(size: 16))
                                
                                
                            }
                            .background(selectedWorkArea == .advertisingMarketing ? Color.blue : Color.clear)
                            .foregroundColor(selectedWorkArea == .advertisingMarketing ? .white : .blue)
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("mainColor"), lineWidth: 1)
                            )
                            
                            Button(action: {
                                
                                selectedWorkArea = .mcn
                                
                            }) {
                                Text("MCN")
                                    .frame(width: screenWidth/3.26, height: screenHeight/23.97)
                                    .font(.system(size: 16))
                                
                                
                            }
                            .background(selectedWorkArea == .mcn ? Color.blue : Color.clear)
                            .foregroundColor(selectedWorkArea == .mcn ? .white : .blue)
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("mainColor"), lineWidth: 1)
                            )
                            
                            Button(action: {
                                
                                selectedWorkArea = .anotherExample
                                
                            }) {
                                Text("기타")
                                    .frame(width: screenWidth/3.26, height: screenHeight/23.97)
                                    .font(.system(size: 16))
                                
                                
                            }
                            .background(selectedWorkArea == .anotherExample ? Color.blue : Color.clear)
                            .foregroundColor(selectedWorkArea == .anotherExample ? .white : .blue)
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("mainColor"), lineWidth: 1)
                            )
                            
                            
                        }//HStack
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("mainColor"), lineWidth: 1)
                        )
                        
                        
                    } //VStack 업무 영역
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    Spacer()
                    
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    
                    self.presentationMode.wrappedValue.dismiss()
                    
                    
                }) {
                    
                    HStack(spacing: 0){
                        
                        Image(systemName: "chevron.backward")
                        
                    }
                    
                    
                    
                })
                .navigationBarTitle("고객(브랜드) 등록", displayMode: .inline)
                
                
            }//ScrollView
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
            
            
        }//ZStack
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        
    }
}

#Preview {
    SecondCustomerRegistrationView()
}
