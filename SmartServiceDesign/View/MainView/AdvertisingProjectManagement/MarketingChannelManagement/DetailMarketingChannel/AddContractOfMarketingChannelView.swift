//
//  AddContractOfMarketingChannelView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/26/23.
//

import SwiftUI
import Alamofire



enum ContractStatusOfDetailMarketingChannel: CaseIterable {
    case request
    case ongoing
    case feedback
    case complete
    case hold
}

struct AddContractOfMarketingChannelView: View {
    //Api
    var selectedMarketingChannelIdx: Int
    var selectedDetailMarketingChannelName: String
    
    var selectedMarketingIdx: Int //임시 저장
    var detailMarketingChannelView: DetailMarketingChannelView
    
    // 이 init 메소드를 통해 외부에서 selectedCustomerBrandIdx 값을 설정할 수 있습니다.
    init(selectedMarketingChannelIdx: Int, selectedDetailMarketingChannelName: String, selectedMarketingIdx: Int) {
        self.selectedMarketingChannelIdx = selectedMarketingChannelIdx
        self.selectedDetailMarketingChannelName = selectedDetailMarketingChannelName
         
        self.selectedMarketingIdx = selectedMarketingIdx
        self.detailMarketingChannelView = DetailMarketingChannelView(selectedMarketingIdx: selectedMarketingIdx)
    }
    
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    private var brandIdx: Int = 0
    @State private var contractImageStringFor_MarketingChannel: String = ""
    
    @State private var isApiLoading: Bool = false
    
    //View
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var showToast: Bool = false
    @State private var toastText: String = "시작 날짜 선택 후 확인을 눌러주세요"
    
    @State private var contractNameFor_MarketingChannel: String = ""
    @State private var contractOneLineExplanFor_MarketingChannel: String = ""
    @State private var contractStartDateFor_MarketingChannel: String = ""
    @State private var contractEndDateFor_MarketingChannel: String = ""
    @State private var contractPriceFor_MarketingChannel: String = ""
    @State private var contractStatusFor_MarketingChannel: String = "요청"
    
    @State private var showingImagePickerContractImageFor_MarketingChannel = false
    @State private var selectedContractImageFor_MarketingChannel: UIImage? = nil
    
    @State private var contractStatus: ContractStatusOfDetailMarketingChannel = ContractStatusOfDetailMarketingChannel.request
    
    
    
    @State private var navigate: Bool = false
    
    //날짜 관련
    @State private var showingDatePicker = false
    @State private var dateSelection = Date()
    @State private var startDate: Date? = nil
    @State private var endDate: Date? = nil
    @State private var dateCount = 0
    
    
    var body: some View {
        
        
        ZStack{
            
            if (isApiLoading){
                
                ProgressView()
                    .zIndex(3)
                
            }
            
            ScrollView{
                
                VStack(spacing: 0){
                    
                    
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("계약명")
                                .font(.system(size: 14))
                            
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $contractNameFor_MarketingChannel, prompt: Text("계약명을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 계약명
                    .padding(.top, screenHeight / 25.46875) //32
                    
                    
                    
                    VStack{ // 한줄 설명
                        
                        HStack(spacing: 0){
                            Text("한줄 설명")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $contractOneLineExplanFor_MarketingChannel, prompt: Text("한줄 설명을 입력해주세요")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 한줄 설명
                    .padding(.top, screenHeight / 54.133) //15
                    
                    VStack{ // 계약기간
                        
                        HStack(spacing: 0){
                            Text("계약기간")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        Button {
                            
                                startDate = nil
                                endDate = nil
                                
                                self.showingDatePicker = true
                                
                                toastText = "시작 날짜 선택 후 확인을 눌러주세요"
                                showToast = true
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                    self.showToast = false
                                    toastText = ""
                                }
                            
                            
                        } label: {
                            HStack{
                                Text("\(contractStartDateFor_MarketingChannel) ~ \(contractEndDateFor_MarketingChannel)")
                                    .foregroundColor(Color("hintTextColor"))
                                .keyboardType(.emailAddress)
                                .font(.system(size: 14))
                                .padding(.leading, 14)
                                
                                Spacer()
                                
                            }
                            .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                            )
                        }

                        
                    }//VStack 계약기간
                    .sheet(isPresented: $showingDatePicker) {
                        VStack {
                            
                                DatePicker("날짜를 선택해주세요", selection: $dateSelection, displayedComponents: .date)
                                    .datePickerStyle(GraphicalDatePickerStyle())
                                    .padding()
                                    .overlay{
                                        ZStack{}
                                        .toast(isShowing: $showToast, text: Text(toastText))
                                    }
                            
                            
                            
                            Button("확인") {
                                if self.dateCount == 0 {
                                    self.startDate = dateSelection
                                    self.dateCount += 1
                                } else if self.dateCount == 1 {
                                    if dateSelection > self.startDate! {
                                        self.endDate = dateSelection
                                        self.dateCount = 0
                                    } else {
                                        self.startDate = nil
                                        self.endDate = nil
                                        self.dateCount = 0
                                    }
                                }
                                
                                
                                if (startDate != nil && endDate == nil){
                                    
                                    toastText = "종료 날짜 선택 후 확인을 주세요"
                                    showToast = true
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                        self.showToast = false
                                        toastText = ""
                                    }
                                    
                                    self.showingDatePicker = true
                                }
                                
                                
                                if (startDate != nil && endDate != nil){
                                    contractStartDateFor_MarketingChannel = convertDateToString(date: startDate)
                                    contractEndDateFor_MarketingChannel = convertDateToString(date: endDate)
                                    
                                    self.showingDatePicker = false
                                }else{
                                    contractStartDateFor_MarketingChannel = ""
                                    contractEndDateFor_MarketingChannel = ""
                                }
                            }
                        }
                    }
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    VStack{//계약 금액
                        
                        HStack(spacing: 0){
                            Text("계약 금액")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $contractPriceFor_MarketingChannel, prompt: Text("계약금액을 입력해주세요")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.phonePad)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 계약 금액
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    VStack{ //진행상태
                        
                        HStack(spacing: 0){
                            Text("진행상태")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        
                        HStack(spacing: screenWidth / 125 ){ //SelectedBtn
                            
                            Spacer()
                            
                            Button(action: {
                                
                                contractStatus = .request
                                contractStatusFor_MarketingChannel = "요청"
                                print("\(contractStatusFor_MarketingChannel)")
                                
                            }, label: {
                                Text("요청")
                                    .foregroundColor(contractStatus == .request ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(contractStatus == .request ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            Button(action: {
                                
                                contractStatus = .ongoing
                                contractStatusFor_MarketingChannel = "진행"
                                print("\(contractStatusFor_MarketingChannel)")
                                
                            }, label: {
                                Text("진행")
                                    .foregroundColor(contractStatus == .ongoing ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(contractStatus == .ongoing ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            Button(action: {
                                
                                contractStatus = .feedback
                                contractStatusFor_MarketingChannel = "피드백"
                                print("\(contractStatusFor_MarketingChannel)")
                                
                            }, label: {
                                Text("피드백")
                                    .foregroundColor(contractStatus == .feedback ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(contractStatus == .feedback ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            Button(action: {
                                
                                contractStatus = .complete
                                contractStatusFor_MarketingChannel = "완료"
                                print("\(contractStatusFor_MarketingChannel)")
                                
                            }, label: {
                                Text("완료")
                                    .foregroundColor(contractStatus == .complete ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(contractStatus == .complete ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            Button(action: {
                                
                                contractStatus = .hold
                                contractStatusFor_MarketingChannel = "보류"
                                
                                print("\(contractStatusFor_MarketingChannel)")
                                
                            }, label: {
                                Text("보류")
                                    .foregroundColor(contractStatus == .hold ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(contractStatus == .hold ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            
                            
                            Spacer()
                            
                            
                        }//HStack (SelectedBtn 을 위한 HStack)
                        
                    } //VStack 진행상태
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    
                    VStack(alignment: .leading){//계약서 등록
                        
                        HStack(spacing: 0){
                            Text("계약서 등록")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        
                        Button(action: {
                            showingImagePickerContractImageFor_MarketingChannel = true
                        }, label: {
                            
                            ZStack {
                                if let image = selectedContractImageFor_MarketingChannel {
                                    Image(uiImage: image)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                } else {
                                    Rectangle()
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                        .foregroundColor(.colorE5E5E5)
                                    
                                    Circle()
                                        .frame(width: screenWidth/10.416)
                                        .foregroundColor(.colorEFEFF4)
                                        .overlay{
                                            Image(systemName: "plus")
                                                .imageScale(.medium)
                                        }
                                }
                            }
                            
                        })
                        .padding(.leading, screenWidth / 18.75)
                        
                    }//VStack 계약서 등록 이미지
                    .padding(.top, screenHeight / 54.133) //20    54.133 = 15
                    .sheet(isPresented: $showingImagePickerContractImageFor_MarketingChannel, onDismiss: loadContractImageForMarketingChannel) {
                        ImagePicker(selectedImage: $selectedContractImageFor_MarketingChannel)
                    }
                    
                    HStack{
                        
                        Button(action: {
                            
                            if (!isApiLoading){
                                
                                if(contractNameFor_MarketingChannel == "" || contractOneLineExplanFor_MarketingChannel == "" || contractStartDateFor_MarketingChannel == "" || contractEndDateFor_MarketingChannel == "" || contractPriceFor_MarketingChannel == "" || contractStatusFor_MarketingChannel == ""){
                                    
                                    showAlert = true
                                    
                                    alertTitle = "마케팅 채널 계약 추가 실패"
                                    alertMessage = "칸을 비울 수 없습니다."
                                    
                                }else{
                                    
                                    addMarketingChannelContract()
                                    
                                    self.presentationMode.wrappedValue.dismiss()
                                    
                                    detailMarketingChannelView.contractInquiry(accessToken: loginData.token)
                                }
                                
                            }
                            
                            
                        }, label: {
                            
                                Text("계약 추가")
                                    .foregroundColor(.white)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/67.9166) // 37
                        
                        
                        
                    }
                    
                    
                    
                    //하단 네비게이션을 위한 패딩
                    HStack{
                        Rectangle()
                            .frame(width: 0, height: 0)
                            .foregroundColor(.white)
                    }
                    .padding(.bottom, screenHeight/8.826)
                    
                    Spacer()
                    
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    
                    self.presentationMode.wrappedValue.dismiss()
                    
                    
                }) {
                    
                    HStack(spacing: 0){
                        
                        Image(systemName: "chevron.backward")
                        
                    }
                    
                    
                    
                })
                .navigationBarTitle("\(selectedDetailMarketingChannelName) 계약 추가", displayMode: .inline)
                
            }//ScrollView
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
            
            
        }//ZStack
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        
    }//body
    
    func loadContractImageForMarketingChannel() {
        guard let selectedContractImageFor_MarketingChannel = selectedContractImageFor_MarketingChannel else { return }
        // You can do something with the selected brand image here
        isApiLoading = true
        
        uploadImage(image: selectedContractImageFor_MarketingChannel, imageType: "marketing_contract")
    }
    
    
    //Api 를통해 필요한 값 호출 (url, key 값)
    private func getApiCommonMarketingChannelContractImageURL(imageType: String, completion: @escaping (String?, String?) -> Void) {
        
        let headers: HTTPHeaders = [
            "accept": "application/json",
            "Authorization": "Bearer \(loginData.token)"
        ]
        
        let parameters: Parameters = [
            "mimetype": "image/png",
            "type": "marketing_contract",
            "extension": "png"
        ]
        
        AF.request("\(defaultUrl)/api/common/image/url", method: .get, parameters: parameters, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    let url = JSON["data"] as? String
                    let key = JSON["key"] as? String
                    completion(url, key)
                }
            case .failure(let error):
                print(error)
                completion(nil, nil)
            }
        }
    }
    
    //사진 선택시 uploadImag 함수를 호출
    private func uploadImage(image: UIImage, imageType: String) {
        getApiCommonMarketingChannelContractImageURL(imageType: imageType) { (url, key) in
            
            guard let preSignedUrl = url else {
                print("Failed to get pre-signed URL.")
                return
            }
            
            guard let imageData = image.pngData() else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: url!, method: .put).response { response in
                switch response.result {
                case .success:
                    print("Image uploaded successfully.")
                    
                    isApiLoading = false
                case .failure(let error):
                    print("Failed to upload image: \(error)")
                }
            }
            
            
            guard let imageData = image.jpegData(compressionQuality: 1.0) else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: preSignedUrl).response { response in
                if let error = response.error {
                    print("Image upload failed with error: \(error)")
                } else {
                    print("Image uploaded successfully.")
                    switch imageType {
                    case "marketing_contract":
                        contractImageStringFor_MarketingChannel = key!
                        
                        print("\(contractImageStringFor_MarketingChannel)")
                        
                    default:
                        print("Unknown image type: \(imageType)")
                    }
                }
            }
        }
    }
    
    
    //최종적으로 등록
    private func addMarketingChannelContract() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any] = [
            "marketing_idx": selectedMarketingChannelIdx,
              "contract_name": contractNameFor_MarketingChannel,
              "one_line_explan": contractOneLineExplanFor_MarketingChannel,
            "contract_start_dt": contractStartDateFor_MarketingChannel,
            "contract_end_dt": contractEndDateFor_MarketingChannel,
              "contract_price": Int(contractPriceFor_MarketingChannel),
              "contract_status": contractStatusFor_MarketingChannel,
              "contract_image": contractImageStringFor_MarketingChannel
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/marketing-contract",
                   method: .post,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("마케팅 채널 계약 등록 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("마케팅 채널 계약 등록 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    self.navigate = true
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("마케팅 채널 계약 등록 실패: \(error)")
                
            }
        }
    }
    
    func convertDateToString(date: Date?) -> String {
        guard let date = date else {
            return ""
        }

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
}

//#Preview {
//    AddContractOfMarketingChannelView()
//}
