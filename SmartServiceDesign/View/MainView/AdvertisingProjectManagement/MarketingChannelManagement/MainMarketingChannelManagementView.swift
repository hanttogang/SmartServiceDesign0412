//
//  MainMarketingChannelManagementView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/26/23.
//

import SwiftUI
import Alamofire

struct MainMarketingChannelManagementView: View {
    
    //API 관련
    @State private var marketingDataList = [MarketingData]()
    @EnvironmentObject var loginData: LoginData
    @EnvironmentObject var menuViewModel: MenuViewModel
    
    @State var isShowingMenu: Bool = false
    
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    //View 관련
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    init() {
        let appearance = UINavigationBarAppearance()
        //                appearance.configureWithOpaqueBackground()
        //                appearance.backgroundColor = UIColor(.white) // 원하는 색상으로 변경하세요.
        appearance.shadowColor = nil // 이 부분을 추가하면 네비게이션 바 하단 선이 사라집니다.
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        
    }
    
    
    @State private var searchMarketingChannelText: String = ""
    @State private var editMarketingChannelText: Bool = false
    
    @State var marketingChannelName: String = ""
    
    var body: some View {
        
        ZStack{
            
            NavigationView{
                
                
                VStack{
                    
                    HStack(spacing: 0){
                        
                        Button(action: {
                            withAnimation {
                                isShowingMenu = true
                            }
                        }) {
                            
                            HStack(spacing: 0){
                                Image("img_menu")
                            }
                            .padding(.leading)
                            
                        }
                        
                        Spacer()
                    }
                    
                    
                    HStack{
                        
                        Text("마케팅 채널 관리")
                            .font(.title)
                            .bold()
                        
                        
                        Spacer()
                        
                    }
                    .padding(.leading, 16)
                    
                    
                    
                    HStack{
                        
                        HStack{
                            
                        }
                        .frame(width: screenWidth, height: screenHeight/14.5)
                        .overlay{
                            //검색창을 받을수있는 택스트필드
                            TextField("Search" , text : self.$searchMarketingChannelText)
                                .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                .foregroundColor(Color("hintTextColor"))
                                .padding(10)
                                .padding(.leading, screenWidth/12.5) //30
                                .background(Color(.white))
                                .cornerRadius(15)
                                .overlay(
                                    HStack{
                                        Image(systemName: "magnifyingglass")
                                            .foregroundColor(Color("hintTextColor"))
                                            .padding()
                                        //                                            }
                                        
                                        
                                        
                                        Spacer()
                                        
                                        if self.editMarketingChannelText{
                                            //x버튼이미지를 클릭하게되면 입력되어있던값들을 취소하고
                                            //키입력 이벤트를 종료해야한다.
                                            Button(action : {
                                                self.editMarketingChannelText = false
                                                self.searchMarketingChannelText = ""
                                                //키보드에서 입력을 끝내게하는 코드
                                                UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                                            }){
                                                Image(systemName: "multiply.circle.fill")
                                                    .foregroundColor(Color("hintTextColor"))
                                                    .padding()
                                            }
                                        }
                                        
                                    }
                                ).onTapGesture {
                                    self.editMarketingChannelText = true
                                }
                                .padding(.horizontal, 21.945) //37
                            
                            
                            
                        }//overlay
                        
                        
                        
                        
                        
                    }//HStack
                    .background(Color("colorF8F8F892"))
                    
                    
                    HStack{
                        
                        Button(action: {
                            
                        }, label: {
                            NavigationLink {
                                
                                RegistrationMarketingChannelView()
                                //                                    AddNewProject(isSelectedCustomerBrand: "", isSearchingCustomerBrand: false)
                            } label: {
                                Text("+  마케팅 채널 등록")
                                    .foregroundColor(.white)
                                    .bold()
                                    .font(.system(size: 16))
                                    .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                            }
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/50.75)
                        
                        
                        
                    }
                    
                    
                    ScrollView {
                        VStack(spacing: 0) {
                            ForEach(marketingDataList.filter { searchMarketingChannelText.isEmpty || $0.channelName.contains(searchMarketingChannelText) || $0.manageType.contains(searchMarketingChannelText) }, id: \.marketingIdx) { marketingData in
                                NavigationLink(destination: DetailMarketingChannelView(selectedMarketingIdx: marketingData.marketingIdx)){
                                    VStack {
                                        HStack {
                                            AsyncImage(url: URL(string: imageS3Url + "/" + marketingData.representativeImage)) { image in
                                                image.resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/6.25, height: screenHeight/13.583)
                                                    .cornerRadius(12.0)
                                            } placeholder: {
                                                ProgressView()
                                                    .frame(width: screenWidth/6.25, height: screenHeight/13.583)
                                            }
                                            
                                            VStack(alignment: .leading){
                                                Text(marketingData.channelName)
                                                    .font(.title2)
                                                    .bold()
                                                    .foregroundColor(.black)
                                                
                                                Text(marketingData.manageType)
                                                    .foregroundColor(.black)
                                                
                                                Text("등록일: \(dateFormatter(getDate: marketingData.firstCreateDt))")
                                                    .foregroundColor(.gray)
                                                    .font(.caption)
                                            }
                                            
                                            Spacer()
                                            
                                            Image(systemName: "chevron.right")
                                                .padding()
                                                .foregroundColor(.gray)
                                        }
                                        .padding(.bottom, screenWidth/17.857)
                                        .padding(.top, screenWidth/15.625)
                                        
                                        Divider()
                                        
                                        
                                        
                                        
                                    }//VStack
                                    .background(Color.white)
                                    .padding(.leading)
                                }//NavigationLink
                            }
                        }
                    }
                    .onAppear {
                        marketingInquiry(accessToken: loginData.token)
                    }
                    
                    //하단 네비게이션을 위한 패딩
                    HStack{
                        Rectangle()
                            .frame(width: 0, height: 0)
                            .foregroundColor(.white)
                    }
                    .padding(.bottom, screenHeight/28) //29
                    
                    
                    
                    Spacer()
                    
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                //                    .navigationBarItems(leading: Button(action: {
                //
                //                        withAnimation {
                //                            isShowingMenu = true
                //                        }
                //
                //                    }) {
                //
                //                        HStack(spacing: 0){
                //
                //                            Image("img_menu")
                //
                //                        }
                //
                //
                //
                //
                //                    })
                
                
                
                
                
                
                
                
                
                
            }//NavigationView
            //    .toast(isShowing: $showToast, text: Text(toastText))
            .navigationBarHidden(true)
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
            
            
            //하단 네비게이션 및 채팅 웹뷰
            CustomBottomNavigationBar()

            // isShowingMenu 상태에 따라 메뉴 여닫기 zIndex 으로 조정
            if isShowingMenu {
                
                MenuView()
                    .transition(.move(edge: .leading))
                    .zIndex(2)
                
                // 메뉴 외부를 클릭하면 메뉴를 닫습니다.
                Button(action: {
                    withAnimation {
                        menuViewModel.sideMenuBool = false
                    }
                }) {
                    Color.gray
                        .edgesIgnoringSafeArea(.all)
                        .opacity(0.5)
                }
                .zIndex(1)
                
            }
            
        }//ZStack
        
    }
    
    
    
    
    func marketingInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/marketing", method: .get, headers: headers).responseDecodable(of: MarketingDataResponse.self) { response in
                
                switch response.result {
                    
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    //                    for brandData in response.data {
                    //                         // 브랜드 이름을 출력합니다.
    //                                        print("브랜드 번호: \(brandData.brandIdx)")
                    //                        print("브랜드 사진: \(brandData.brandImage)")
                    //                        print("브랜드 이름: \(brandData.brandName)")
                    //                        print("비즈니스 타입: \(brandData.buisnessType)")
                    //                        print("등록일: \(brandData.firstCreateDt)")
                    //
                    //                    }
                    
                    print(response.data)
                    
    //                brandIdx = brandData.brandIdx
                    DispatchQueue.main.async {
                        marketingDataList = response.data
                    }
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
            
        })
        
        
    }
    
    private func dateFormatter(getDate: String) -> String {
        
        let isoDateFormatter = ISO8601DateFormatter()
        isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

        let getDate = getDate
        if let date = isoDateFormatter.date(from: getDate) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy.MM.dd"
            let dateString = dateFormatter.string(from: date)
            print(dateString) // "2024.01.03" 출력
            return dateString
        } else {
            print("날짜 변환에 실패했습니다.")
            return ""
        }

    }
    
}

//#Preview {
//    MainMarketingChannelManagementView()
//}
