//
//  AddNewProject.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/14/23.
//

import SwiftUI
import Alamofire

struct AddNewProjectView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    //API 관련
    private let mainMarketingProjectManagementView = MainMarketingProjectManagementView()
    
    @State private var brandDataList = [BrandData]()
    
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var selectedBrandName: String = ""
    @State private var selectedBrandIdx: Int = 0
    @State var projectName: String = ""
    
    
    //view
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var navigateCustomerSearchView: Bool = false
    //
//    @State private var isSelectedCustomerBrand: String = ""
    //    @State private var isSearchingCustomerBrand: Bool = false
    
    //    @Binding var isSelectedCustomerBrand: String
    //    @Binding var isSearchingCustomerBrand: Bool = false
    
    @State private var searchCustomerBrand: String = ""
    @State private var editText: Bool = false
    
    var body: some View {
        
        if(!navigateCustomerSearchView){
            ZStack{
                ScrollView {
                    
                    VStack(spacing: 0){
                        
                        VStack{
                            
                            HStack(spacing: 0){
                                Text("고객 (브랜드)")
                                    .font(.system(size: 14))
                                
                                Spacer()
                            }
                            .padding(.leading, screenWidth / 15.625)
                            
                            
                            HStack(spacing: 0){
                                
                                HStack(spacing: 0){
                                    Text(selectedBrandName)
                                        .foregroundColor(Color("hintTextColor"))
                                        .font(.system(size: 14))
                                        .padding(.leading, 14)
                                    
                                    Spacer()
                                    
                                }
                                .frame(width: screenWidth / 1.358, height: screenHeight / 18.45)
                                .background(Color("colorF8F8F8"))
                                .cornerRadius(4)
                                .overlay(RoundedRectangle(cornerRadius: 4)
                                    .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                                )
                                .padding(.trailing, screenWidth / 17.857) //21
                                
                                
                                Button(action: {
                                    
                                    self.navigateCustomerSearchView = true
                                    
                                }, label: {
                                    
                                    Circle()
                                        .frame(width: screenWidth/10.135)
                                        .foregroundColor(Color("mainColor"))
                                        .overlay{
                                            
                                            Image(systemName: "magnifyingglass")
                                                .foregroundColor(.white)
                                        }
                                    
                                    
                                    
                                })
                                
                                Spacer()
                            }
                            .padding(.leading, screenWidth / 15.625)
                            
                            
                            
                        }//VStack 고객(브랜드)
                        .padding(.top, screenHeight / 20.82) //39
                        
                        
                        VStack{
                            
                            HStack(spacing: 0){
                                Text("프로젝트명")
                                    .font(.system(size: 14))
                                
                                Spacer()
                            }
                            .padding(.leading, screenWidth / 15.625)
                            
                            HStack{
                                TextField(".", text: $projectName, prompt: Text("프로젝트명을 입력해주세요.")
                                    .foregroundColor(Color("hintTextColor")))
                                .keyboardType(.default)
                                .font(.system(size: 14))
                                .padding(.leading, 14)
                                .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                            }
                            .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                            )
                            
                        }//VStack 이름
                        .padding(.top, screenHeight / 54.133) //15
                        
                        
                        
                        HStack{
                            
                            Button(action: {
                                
                                if(selectedBrandIdx != 0 && selectedBrandName != ""){
                                    
                                    addProject()
                                    
                                    self.presentationMode.wrappedValue.dismiss()
                                    
                                    mainMarketingProjectManagementView.brandInquiry(accesToekn: loginData.token)
                                    mainMarketingProjectManagementView.projectInquiry(accesToekn: loginData.token)
                                
                                    
                                }else{
                                    
                                    showAlert = true
                                    
                                    alertTitle = "프로젝트 등록 실패"
                                    alertMessage = "칸을 비울 수 없습니다."
                                    
                                }
                                
                                
                            }, label: {
                                
                                Text("등록")
                                    .foregroundColor(.white)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                                
                            })
                            .background(Color("mainColor"))
                            .cornerRadius(4)
                            .padding(.top, screenHeight/21.945) // 37
                            
                            
                            
                        }
                        
                        
                        Spacer()
                        
                        
                        
                        
                        
                    }//VStack
                    .navigationBarBackButtonHidden(true)
                    .navigationBarItems(leading: Button(action: {
                        
                        self.presentationMode.wrappedValue.dismiss()
                        
                        
                    }) {
                        
                        HStack(spacing: 0){
                            
                            Image(systemName: "chevron.backward")
                            
                        }
                        
                        
                        
                    })
                    .navigationBarTitle("신규 프로젝트 등록", displayMode: .inline)
                    .alert(isPresented: $showAlert, content: {
                        Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                            print("알림창 확인 버튼이 클릭되었다.")
                            showAlert = false
                            
                            alertTitle = ""
                            alertMessage = ""
                        }))
                    })
                    
                }//ScrollView
                .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                    UIApplication.shared.endEditing()
                    
                }
            }//ZStack
            
            
            
            
        }else{
            ZStack{ //검색 뷰
                VStack(spacing: 0){
                    
                    
                    HStack{
                        
                        HStack{
                            
                        }
                        .frame(width: screenWidth, height: screenHeight/14.5)
                        .overlay{
                            
                            HStack{
                                
                                //검색창을 받을수있는 택스트필드
                                TextField("Search" , text : self.$searchCustomerBrand)
                                    .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                    .foregroundColor(Color(.black))
                                //hint와 태두리에 간격을 띄우기위해 15정도의 간격을주고
                                    .padding(10)
                                //양옆은 추가로 15를 더줌
                                    .padding(.leading, screenWidth/12.5) //30
                                //배경색상은 자유롭게선택
                                    .background(Color(.white))
                                //검색창이 너무각지면 딱딱해보이기때문에 모서리를 둥글게
                                    .frame(width: screenWidth/1.3)
                                //숫자는 취향것
                                    .cornerRadius(15)
                                //내가만든 검색창 상단에
                                //돋보기를 넣어주기위해
                                //오버레이를 선언
                                    .overlay(
                                        
                                        //가로로 view를 쌓을수있도록 하나 만들고
                                        HStack{
                                            //맨오른쪽으로 밀기위해 Spacer()로 밀어준다.
                                            //magnifyingglass 를 사용
                                            //색상은 자유롭게 변경가능
                                            Image(systemName: "magnifyingglass")
                                                .foregroundColor(Color("hintTextColor"))
                                                .padding()
                                            
                                            
                                            
                                            Spacer()
                                            
                                            if self.editText{
                                                //x버튼이미지를 클릭하게되면 입력되어있던값들을 취소하고
                                                //키입력 이벤트를 종료해야한다.
                                                Button(action : {
                                                    //                                                searchButtonClick = false
                                                    self.editText = false
                                                    self.searchCustomerBrand = ""
                                                    //키보드에서 입력을 끝내게하는 코드
                                                    UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                                                }){
                                                    Image(systemName: "multiply.circle.fill")
                                                        .foregroundColor(Color("hintTextColor"))
                                                        .padding()
                                                }
                                            }
                                            
                                            
                                        }//HStack
                                        
                                        
                                        
                                        
                                        
                                        
                                    )//overlay
                                    .onTapGesture {
                                        self.editText = true
                                    }
                                
                                Button(action: {
                                    self.navigateCustomerSearchView = false
                                    
                                }, label: {
                                    
                                    Text("취소")
                                    
                                    
                                    
                                })
                                .padding(.horizontal, screenWidth/46.875)
                                //                            }
                                
                                
                            }
                        }//overlay
                    }//HStack 검색창
                    .background(Color("colorF8F8F892"))
                    
                    HStack{
                        Text("고객(브랜드) 검색")
                            .font(.title2)
                            .padding(.leading, screenWidth/23.4375) //16
                            .padding(.top, screenWidth/20.833) //18
                        
                        Spacer()
                    }
                    Divider() // 가로 선
                        .padding(.top, 8)
                        .padding(.leading, screenWidth/23.4375) //16

                    ScrollView {
                        
                        VStack(alignment: .leading, spacing: 0) {
                            ForEach(brandDataList.filter { searchCustomerBrand.isEmpty || $0.brandName.contains(searchCustomerBrand) }, id: \.brandIdx) { brand in
                                
                                    Button {
                                        selectedBrandName = brand.brandName
                                        
                                        selectedBrandIdx = brand.brandIdx
                                        
                                        navigateCustomerSearchView = false
                                    } label: {
                                        HStack(){
                                            Text(brand.brandName)
                                                .frame(minWidth: 0, minHeight: 0, maxHeight: .infinity)
                                                .foregroundColor(.blue)
                                                
                                        }
                                        .padding()
                                    }
                                    
                                    Divider()
                                        .padding(.leading, screenWidth/23.4375) //16
                                
                                
                                
                            }
                        }
                        
                        
                    }
                    .background(.white)
                    .onAppear{
                        brandInquiry()
                    }
                    
                    Spacer()
                    
                } //VStack
                .navigationBarHidden(true)
                
                
                
            }//ZStack 검색 뷰
            
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
            .background(.white)
            
            
        }
    }
    
    
    private func brandInquiry() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        AF.request("\(defaultUrl)/api/brand?page=1&limit=100", method: .get, headers: headers).responseDecodable(of: BrandDataResponse.self) { response in
            switch response.result {
            case .success(let response):
                // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                
                //                brandIdx = brandData.brandIdx
                
                print("브랜드 정보: \(response.data)")
                
                DispatchQueue.main.async {
                    brandDataList = response.data
                }
                
                
            case .failure(let error):
                // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                print(error)
            }
        }
        
        
    }
    
    
    //최종적으로 등록
    private func addProject() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any?] = [
            
            "brand_idx": selectedBrandIdx,
             "project_name": projectName
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/project",
                   method: .post,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("프로젝트 등록 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("프로젝트 등록 성공: \(value)")
                    
                    self.navigateCustomerSearchView = false
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("프로젝트 등록 실패: \(error)")
                
            }
        }
    }
    
}
//
//#Preview {
//    AddNewProjectView()
//}
