//
//  FindPasswordView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/13/23.
//

import SwiftUI
import Alamofire

struct AddDetailScheduleView: View {
    
    //Api
    @EnvironmentObject var myWebVM: WebViewModel
    
    var selectedBrandProjectIdx: Int
    var titleProjectName: String
    var detailMarketingProjectListView: DetailMarketingProjectListView
    //    var detailMarketingProjectListView: DetailMarketingProjectListView
    
    //      이 init 메소드를 통해 외부에서 selectedCustomerBrandIdx 값을 설정할 수 있습니다.
    init(selectedBrandProjectIdx: Int, titleProjectName: String) {
        self.selectedBrandProjectIdx = selectedBrandProjectIdx
        self.titleProjectName = titleProjectName
        
        self.detailMarketingProjectListView = DetailMarketingProjectListView(selectedBrandProjectIdx: selectedBrandProjectIdx, titleProjectName: titleProjectName)
        
     }
    
    
 
     @State private var scheduleDataList = [ProjectDetailScheduleData]()
     @State private var brandDataList = [BrandData]()
     
     @EnvironmentObject var loginData: LoginData
     let defaultUrl = "\(ApiClient.BASE_URL)"
     let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    
    
    
    @State var scheduleName: String = ""
    @State var scheduleOneLineDescription: String = ""
    
    
    @State private var today = Date()

    @State private var isStartDatePickerShown = false
    @State private var isEndDatePickerShown = false
    
    //
    @State private var isSelectedCustomerBrand: String = ""
    
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var showToast: Bool = false
    @State private var toastText: String = "시작 날짜 선택 후 확인을 눌러주세요"
    
    //날짜 관련
    @State private var showingDatePicker = false
    @State private var dateSelection = Date()
    @State private var startDate: Date? = nil
    @State private var endDate: Date? = nil
    @State private var dateCount = 0
    
    
    @State private var startDateString: String = ""
    @State private var endDateString: String = ""
    
    
    var body: some View {
        
        
        ZStack{
            
            
            //            NavigationView{
            ScrollView {
                
                VStack(spacing: 0){
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("일정명")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField(".", text: $scheduleName, prompt: Text("일정명을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이름
                    .padding(.top, screenHeight / 54.133) //15
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("한 줄 설명")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField(".", text: $scheduleOneLineDescription, prompt: Text("한 줄 설명을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이름
                    .padding(.top, screenHeight / 54.133) //15
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("일정 등록")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        HStack{
                            Button(action: {
                                
                                
                                startDate = nil
                                endDate = nil
                                
                                toastText = "시작 날짜 선택 후 확인을 눌러주세요"
                                
                                self.showingDatePicker = true
                                
                                showToast = true
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                    self.showToast = false
                                    toastText = ""
                                }
                                
                                
                            }, label: {
                                
                                HStack{
                                    Text("\(startDateString) ~ \(endDateString)")
                                        .foregroundColor(Color("hintTextColor"))
                                        .keyboardType(.emailAddress)
                                        .font(.system(size: 14))
                                        .padding(.leading, 14)
                                    
                                    Spacer()
                                    
                                }
                                .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                                .overlay(RoundedRectangle(cornerRadius: 4)
                                    .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                                )
                                
                                
                                
                                
                            })
                            
                            //TODO                                일정 등록 관련 TextField 작성해야함
                            //
                            //                            TextField(".", text: $startDateString, prompt: Text("일정을 등록해주세요.")
                            
                        }
                        
                        
                    }//VStack 계약기간
                    .sheet(isPresented: $showingDatePicker) {
                        VStack {
                            
                            DatePicker("날짜를 선택해주세요", selection: $dateSelection, displayedComponents: .date)
                                .datePickerStyle(GraphicalDatePickerStyle())
                                .padding()
                                .overlay{
                                    ZStack{}
                                        .toast(isShowing: $showToast, text: Text(toastText))
                                }
                            
                            
                            
                            Button("확인") {
                                if self.dateCount == 0 {
                                    self.startDate = dateSelection
                                    self.dateCount += 1
                                } else if self.dateCount == 1 {
                                    if dateSelection > self.startDate! {
                                        self.endDate = dateSelection
                                        self.dateCount = 0
                                    } else {
                                        self.startDate = nil
                                        self.endDate = nil
                                        self.dateCount = 0
                                    }
                                }
                                
                                
                                if (startDate != nil && endDate == nil){
                                    
                                    toastText = "종료 날짜 선택 후 확인을 주세요"
                                    showToast = true
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                        self.showToast = false
                                        toastText = ""
                                    }
                                    
                                    self.showingDatePicker = true
                                }
                                
                                
                                if (startDate != nil && endDate != nil){
                                    startDateString = convertDateToString(date: startDate)
                                    endDateString = convertDateToString(date: endDate)
                                    
                                    self.showingDatePicker = false
                                }else{
                                    startDateString = ""
                                    endDateString = ""
                                }
                            }
                        }
                    }
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    
                    HStack{
                        
                        Button(action: {
                            
                            if(scheduleName != "" && scheduleOneLineDescription != "" && startDateString != "" && endDateString != ""){
                                
                                //                                settingSelectedBrandProjectIdx = selectedBrandProjectIdx
                                //                                settingTitleProjectName = titleProjectName
                                
                                addDetailSchedule()
                                
                                self.presentationMode.wrappedValue.dismiss()
                                
                            
//                                
//                                if(myWebVM.isStateWebCalendarViewBoolean){
//                                    
//                                    
//                                    myWebVM.isStateWebCalendarView.send(true)
//                                    
//                                        
//                                }else{
//                                    detailMarketingProjectListView.brandInquiry(accessToken: loginData.token)
//                                    detailMarketingProjectListView.detailProjectScheduleInquiry(accessToken: loginData.token)
//                                }
                                
                                
                                    detailMarketingProjectListView.brandInquiry(accessToken: loginData.token)
                                    detailMarketingProjectListView.detailProjectScheduleInquiry(accessToken: loginData.token)
                                
                                
                                
                            }else{
                                
                                showAlert = true
                                
                                alertTitle = "세부 일정 추가 실패"
                                alertMessage = "칸을 비울 수 없습니다."
                            }
                            
                            
                        }, label: {
                            Text("추가")
                                .foregroundColor(.white)
                                .font(.system(size: 16))
                                .bold()
                                .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/21.945) // 37
                        
                        
                        
                    }
                    
                    
                    Spacer()
                    
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    
                    self.presentationMode.wrappedValue.dismiss()
                    
                    if(myWebVM.isStateWebCalendarViewBoolean){
                        print("isStateWebCalendarView 상태 \(myWebVM.isStateWebCalendarView)")

                        myWebVM.isStateWebCalendarView.send(true)
                        
                    }else{
                        
                        print("isStateWebCalendarView 상태 \(myWebVM.isStateWebCalendarView)")
                        
                        detailMarketingProjectListView.brandInquiry(accessToken: loginData.token)
                        detailMarketingProjectListView.detailProjectScheduleInquiry(accessToken: loginData.token)
                    }
                    
                    
                }) {
                    
                    HStack(spacing: 0){
                        
                        Image(systemName: "chevron.backward")
                        
                    }
                    
                    
                    
                })
                .navigationBarTitle("\(titleProjectName)", displayMode: .inline)
                
            }//ScrollView
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
            
            
        }//ZStack
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        
        
    }
    
    //최종적으로 등록
    private func addDetailSchedule() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any] = [
            
            "project_idx": selectedBrandProjectIdx,
              "schedule_name": scheduleName,
              "one_line_explain": scheduleOneLineDescription,
              "schedule_start_dt": startDateString,
              "schedule_end_dt": endDateString
            
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/schedule",
                   method: .post,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("세부 일정 등록 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("세부 일정 등록 성공: \(value)")
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("세부 일정 등록 실패: \(error)")
                
            }
        }
    }
    
    func convertDateToString(date: Date?) -> String {
        guard let date = date else {
            return ""
        }

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
    
    
}

//#Preview {
//    AddDetailScheduleView(titleProjectName: "")
//}
