//
//  DetailMarketingProjectListView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/18/23.
//

import SwiftUI
import Alamofire
import WebKit

struct DetailMarketingProjectListView: View {
//    
    //Api
    @EnvironmentObject var myWebVM: WebViewModel
    @State var isStateWebCalendarView: Bool = false
    @State var reloadTrigger: Bool = false
    
    @State var jsValue: JsValue?
    
    var selectedBrandProjectIdx: Int = 0
   var titleProjectName: String = ""
    
    // 이 init 메소드를 통해 외부에서 selectedCustomerBrandIdx 값을 설정할 수 있습니다.
   init(selectedBrandProjectIdx: Int, titleProjectName: String) {
        self.selectedBrandProjectIdx = selectedBrandProjectIdx
       self.titleProjectName = titleProjectName
    }
//
    @State private var scheduleDataList = [ProjectDetailScheduleData]()
    @State private var brandDataList = [BrandData]()
    
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var brandImageUrl: String = ""
    
    //View
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State private var addSchedulNavigate: Bool = false
    @State private var detailSchedulNavigate: Bool = false
    
    
    @State var searchText: String = ""
    @State var editText: Bool = false
    
    @State var initCustomerBrand: String = ""
    @State private var initIsSearchingCustomerBrand: Bool = false
    
    @State private var bridgeIdx: Int = 0
    @State private var bridgeTitle: String = ""
    @State private var bridgeBrandImage: String = ""
    
    var body: some View {
        
        ZStack{
                
                
                VStack{
                    
                    
                    HStack{
                        
                        HStack{
                            
                        }
                        .frame(width: screenWidth, height: screenHeight/14.5)
                        .overlay{
                            //검색창을 받을수있는 택스트필드
                            TextField("Search" , text : self.$searchText)
                                .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                .foregroundColor(Color("hintTextColor"))
                            //hint와 태두리에 간격을 띄우기위해 15정도의 간격을주고
                                .padding(10)
                            //양옆은 추가로 15를 더줌
                                .padding(.leading, screenWidth/12.5) //30
                            //배경색상은 자유롭게선택
                                .background(Color(.white))
                            //검색창이 너무각지면 딱딱해보이기때문에 모서리를 둥글게
                            //숫자는 취향것
                                .cornerRadius(15)
                            //내가만든 검색창 상단에
                            //돋보기를 넣어주기위해
                            //오버레이를 선언
                                .overlay(
                                    //HStack을 선언하여
                                    //가로로 view를 쌓을수있도록 하나 만들고
                                    HStack{
                                        //맨오른쪽으로 밀기위해 Spacer()로 밀어준다.
                                        
                                        //xcode에서 지원해주는 이미지
                                        //                                            if !self.editText {
                                        //magnifyingglass 를 사용
                                        //색상은 자유롭게 변경가능
                                        Image(systemName: "magnifyingglass")
                                            .foregroundColor(Color("hintTextColor"))
                                            .padding()
                                        //                                            }
                                        
                                        
                                        
                                        Spacer()
                                        
                                        if self.editText{
                                            //x버튼이미지를 클릭하게되면 입력되어있던값들을 취소하고
                                            //키입력 이벤트를 종료해야한다.
                                            Button(action : {
                                                self.editText = false
                                                self.searchText = ""
                                                //키보드에서 입력을 끝내게하는 코드
                                                UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                                            }){
                                                Image(systemName: "multiply.circle.fill")
                                                    .foregroundColor(Color("hintTextColor"))
                                                    .padding()
                                            }
                                        }
                                        
                                    }
                                ).onTapGesture {
                                    self.editText = true
                                }
                                .padding(.horizontal, 21.945) //37
                            
                            
                            
                        }//overlay 
                        
                        
                        
                        
                        
                    }//HStack
                    .background(Color("colorF8F8F892"))
                    
                    
                    HStack{
                        
                        Button(action: {
                            
                            self.addSchedulNavigate = true
                            
                        }, label: {
                            
                            NavigationLink(destination: AddDetailScheduleView(selectedBrandProjectIdx: selectedBrandProjectIdx, titleProjectName: titleProjectName), isActive: $addSchedulNavigate) {
                                EmptyView()
                            }
                            
                            
                            
                                
                                //                                    AddNewProject(isSelectedCustomerBrand: "", isSearchingCustomerBrand: false)
                            
                                Text("+  세부 일정 추가")
                                    .foregroundColor(.white)
                                    .bold()
                                    .font(.system(size: 16))
                                    .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/50.75)
                        
                        
                        
                    }
                    
                    
                    HStack{
                        Spacer()
                        
                        if( isStateWebCalendarView ) {
                            
                            Button(action: {
                              
                                print("리스트 전환")
                              
                                myWebVM.isStateWebCalendarView.send(false)

                            }, label: {
                                Text("리스트 전환")
                                    .foregroundColor(.black)
                                    .font(.caption)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                        }else{
                            
                            Button(action: {
                              
                                print("캘린더 전환")
                              
                                myWebVM.isStateWebCalendarView.send(true)
                                
                                
                            }, label: {
                                Text("캘린더 전환")
                                    .foregroundColor(.black)
                                    .font(.caption)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                        }
                        
                    }
                    .onReceive(myWebVM.isStateWebCalendarView, perform: { calendarState in
                        isStateWebCalendarView = calendarState
                        reloadTrigger.toggle()
                    })
                    .padding()
                    
                    
                    if( isStateWebCalendarView ) {
                        
                        if (reloadTrigger) {
                            MyWebView(urlToLoad: "https://smart-service-web.team-everywhere.com/work?token=\(loginData.token)&idx=\(selectedBrandProjectIdx)")
                                .padding(.horizontal, 4)
                        }else{
                            MyWebView(urlToLoad: "https://smart-service-web.team-everywhere.com/work?token=\(loginData.token)&idx=\(selectedBrandProjectIdx)")
                                .padding(.horizontal, 4)
                        }

                        //하단 네비게이션을 위한 패딩
                        HStack{
                            Rectangle()
                                .frame(width: 0, height: 0)
                                .foregroundColor(.white)
                        }
                        .padding(.bottom, screenHeight/28) //29
                        
                    }else{
                        ScrollView {
                            VStack(spacing: 0) {
                                ForEach(scheduleDataList.filter {searchText.isEmpty || $0.scheduleName.contains(searchText) || $0.oneLineExplain.contains(searchText) }, id: \.scheduleIdx) { scheduleData in
                                    
                                    let matchingBrand = brandDataList.first { $0.brandIdx == scheduleData.project.brandIdx }
                                    
                                    
                                    NavigationLink(destination: DetailSchedulDetailView(selectedBrandScheduleIdx: scheduleData.scheduleIdx, titleProjectName: titleProjectName, matchingBrandImage: imageS3Url + "/" + matchingBrand!.brandImage)){
                                        
                                        VStack {
                                            HStack {
                                                AsyncImage(url: URL(string: imageS3Url + "/" + matchingBrand!.brandImage)) { image in
                                                    image.resizable()
                                                        .aspectRatio(contentMode: .fill)
                                                        .frame(width: screenWidth/6.25, height: screenHeight/13.583)
                                                        .cornerRadius(12.0)
                                                    
                                                } placeholder: {
                                                    ProgressView()
                                                        .frame(width: screenWidth/6.25, height: screenHeight/13.583)
                                                }
                                                
                                                VStack(alignment: .leading){
                                                    Text(scheduleData.scheduleName)
                                                        .font(.title2)
                                                        .bold()
                                                        .foregroundColor(.black)
                                                    
                                                    Text("\(dateFormatter(getDate: scheduleData.scheduleStartDt)) ~ \(dateFormatter(getDate: scheduleData.scheduleEndDt))")
                                                        .foregroundColor(.black)
                                                    
                                                    HStack{
                                                        Text("참여인원: \(scheduleData.humanCount)")
                                                            .foregroundColor(.gray)
                                                            .font(.caption)
                                                        
                                                        Divider()
                                                        
                                                        Text("과업 수: \(scheduleData.taskCount)")
                                                            .foregroundColor(.gray)
                                                            .font(.caption)
                                                    }
                                                    
                                                }
                                                
                                                Spacer()
                                                
                                                Image(systemName: "chevron.right")
                                                    .padding()
                                                    .foregroundColor(.gray)
                                            }
                                            .padding(.bottom, screenWidth/17.857)
                                            .padding(.top, screenWidth/15.625)
                                            
                                            Divider()
                                            
                                            
                                            
                                            
                                        }//VStack
                                        .onAppear {
                                            brandInquiry(accessToken: loginData.token)
                                            detailProjectScheduleInquiry(accessToken: loginData.token)
                                        }
                                        .background(Color.white)
                                        .padding(.leading)
                                    }//NavigationLink
                                  
                                   
                                }
                            }
                        }//ScrollView
                        
                        
                        //하단 네비게이션을 위한 패딩
                        HStack{
                            Rectangle()
                                .frame(width: 0, height: 0)
                                .foregroundColor(.white)
                        }
                        .padding(.bottom, screenHeight/28) //29
                        
                    }//else
                    
                    Spacer()
                    
                }//VStack
            //onReceive : 이벤트를 받는다
            //myWebVM.jsAlertEvent 을 받고
            //perform 을 보내준다.
            .onReceive(myWebVM.jsBridgeEvent, perform: { jsValue in
                var matchingBrand: BrandData?
                
                
                for scheduleData in scheduleDataList{
                    matchingBrand = brandDataList.first { $0.brandIdx == scheduleData.project.brandIdx }
                }
                
                
                
                print("contentView - jsValue", jsValue)
                self.jsValue = jsValue
                
                let decoder = JSONDecoder()
                if let data = jsValue.message.data(using: .utf8),
                   
                   let message = try? decoder.decode(CalendarBridgeMessage.self, from: data) {
                    print("idx: \(message.idx), title: \(message.title), matchingBrandImage: \(imageS3Url + "/" + matchingBrand!.brandImage)")
                    
                    bridgeIdx = Int("\(message.idx)") ?? 0
                    bridgeTitle = message.title
                    bridgeBrandImage = "\(imageS3Url + "/" + matchingBrand!.brandImage)"
                    
                      self.detailSchedulNavigate = true
                    
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
//                        isStateWebCalendarView = false
//                        
//                        brandInquiry(accessToken: loginData.token)
//                        detailProjectScheduleInquiry(accessToken: loginData.token)
//                    })
                    
                    
                }
            })
            .background(
                NavigationLink(destination: DetailSchedulDetailView(selectedBrandScheduleIdx: bridgeIdx, titleProjectName: bridgeTitle, matchingBrandImage: bridgeBrandImage), isActive: $detailSchedulNavigate){
                    
                        EmptyView()

                }
            )
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    
                    self.presentationMode.wrappedValue.dismiss()
                    
                    
                }) {
                    
                    HStack(spacing: 0){
                        
                        Image(systemName: "chevron.backward")
                        
                    }
                    
                    
                    
                })
                .navigationBarTitle("\(titleProjectName)", displayMode: .inline)
            //    .toast(isShowing: $showToast, text: Text(toastText))
//            .navigationBarHidden(true)
            
           
            
        }//ZStack
        .onAppear {
            brandInquiry(accessToken: loginData.token)
            detailProjectScheduleInquiry(accessToken: loginData.token)
        }
        
        
        
    }
    
    
    
    func brandInquiry(accessToken: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/api/brand?page=1&limit=100", method: .get, headers: headers).responseDecodable(of: BrandDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                   
                    
                    print("@@@@@@@@@@@@브랜드 정보: \(response.data)")
                    
                    DispatchQueue.main.async {
                      
                       
                        brandDataList = response.data
                    }
                    
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
        })
            
            
            
        
        
        
        
    }
    
    
    func detailProjectScheduleInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/schedule?project_idx=\(selectedBrandProjectIdx)&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: ProjectScheduleDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    
                    print("프로젝트 정보: \(response.data)")
                    
    //                brandIdx = brandData.brandIdx
                    DispatchQueue.main.async {
                        
                        scheduleDataList = response.data
                        
                    }
                    

                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
        })
            
            
            
            
        
        
        
    }
    
  

    private func dateFormatter(getDate: String) -> String {
        
        let isoDateFormatter = ISO8601DateFormatter()
        isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

        let getDate = getDate
        if let date = isoDateFormatter.date(from: getDate) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy.MM.dd"
            let dateString = dateFormatter.string(from: date)
            return dateString
        } else {
            print("날짜 변환에 실패했습니다.")
            return ""
        }

    }
    
    
}

struct CalendarBridgeMessage: Decodable {
    let idx: String
    let title: String
}

//#Preview {
//
//    DetailMarketingProjectListView(titleProjectName: "")
//}
