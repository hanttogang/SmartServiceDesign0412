//
//  FindPasswordView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/13/23.
//

import SwiftUI
import Alamofire


enum TaskStatus {
    case request
    case ongoing
    case feedback
    case complete
    case hold
}

struct AddTaskView: View {
    
    
    //Api
    var selectedBrandScheduleIdx: Int
    var titleScheduleName: String
    var matchingBrandImage: String
    
    var detailSchedulDetailView: DetailSchedulDetailView
    
    // 이 init 메소드를 통해 외부에서 selectedCustomerBrandIdx 값을 설정할 수 있습니다.
    init(selectedBrandScheduleIdx: Int, titleScheduleName: String, matchingBrandImage: String) {
        self.selectedBrandScheduleIdx = selectedBrandScheduleIdx
        self.titleScheduleName = titleScheduleName
        self.matchingBrandImage = matchingBrandImage
        
        self.detailSchedulDetailView = DetailSchedulDetailView(selectedBrandScheduleIdx: selectedBrandScheduleIdx, titleProjectName: titleScheduleName, matchingBrandImage: matchingBrandImage)
    }
    
    @State private var brandDataList = [BrandData]()
    @State private var allEmployeeCreatorList = [AllEmployeeCreatorData]()
    
    
    @EnvironmentObject private var searchViewModel: SearchViewModel
    @EnvironmentObject private var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var selectedManagerIdx: Int = 0
    
    @State var addedHumanList: [String] = []
    @State private var humanResourceUsersList: [Int] = []
    
    
    
    //View
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State var taskName: String = ""
    @State var taskOneLineDescription: String = ""
    
    
    //
    @State private var isSelectedCustomerBrand: String = ""
    
    @State private var selectedBtn = TaskStatus.request
    @State private var selectedTaskStatus: String = "요청"
    
    @State var searchManagerName: String = ""
    @State private var editText: Bool = false
    
    @State private var selectedManagerName: String = ""
    
    @State private var swipeStaus: Bool = false
    
    @State private var isHumanListEmpty: Bool = true
    
    //날짜 관련
    @State private var showingDatePicker = false
    @State private var dateSelection = Date()
    @State private var startDate: Date? = nil
    @State private var endDate: Date? = nil
    @State private var dateCount = 0
    
    
    @State private var startDateString: String = ""
    @State private var endDateString: String = ""
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var showToast: Bool = false
    @State private var toastText: String = "시작 날짜 선택 후 확인을 눌러주세요"
    
    
    var body: some View {
        
        
 
            
            
            if(searchViewModel.navigateAllUserSearchView){
                ZStack{
                    VStack(spacing: 0){
                        
                        
                        HStack{
                            
                            HStack{
                                
                            }
                            .frame(width: screenWidth, height: screenHeight/14.5)
                            .overlay{
                                
                                HStack{
                                    
                                    //검색창을 받을수있는 택스트필드
                                    TextField("Search" , text : self.$searchManagerName)
                                        .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                        .foregroundColor(Color(.black))
                                        .padding(10)
                                        .padding(.leading, screenWidth/12.5) //30
                                        .background(Color(.white))
                                        .frame(width: screenWidth/1.3)
                                        .cornerRadius(15)
                                        .overlay(
                                            
                                            //가로로 view를 쌓을수있도록 하나 만들고
                                            HStack{
                                                Image(systemName: "magnifyingglass")
                                                    .foregroundColor(Color("hintTextColor"))
                                                    .padding()
                                                
                                                Spacer()
                                                
                                                if self.editText{
                                                    Button(action : {
                                                        
                                                        self.editText = false
                                                        self.searchManagerName = ""
                                                        
                                                        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                                                    }){
                                                        Image(systemName: "multiply.circle.fill")
                                                            .foregroundColor(Color("hintTextColor"))
                                                            .padding()
                                                    }
                                                }
                                                
                                                
                                            }//HStack
                                            
                                            
                                            
                                            
                                            
                                            
                                        )//overlay
                                        .onTapGesture {
                                            self.editText = true
                                        }
                                    
                                    Button(action: {
                                        
                                        print("취소 버튼 클릭")
                                        searchViewModel.navigateAllUserSearchView = false
                                        
                                    }, label: {
                                        Text("취소")
                                    })
                                    .padding(.horizontal, screenWidth/46.875)
                                    //                            }
                                    
                                    
                                }
                            }//overlay
                        }//HStack 검색창
                        .background(Color("colorF8F8F892"))
                        
                        HStack{
                            Text("담당자 검색")
                                .font(.title2)
                                .padding(.leading, screenWidth/23.4375) //16
                                .padding(.top, screenWidth/20.833) //18
                                
                            
                            Spacer()
                        }
                        
                        Divider() // 가로 선
                            .padding(.top, 8)
                            .padding(.leading, screenWidth/23.4375) //16
                        
                        List(allEmployeeCreatorList.filter{ searchManagerName.isEmpty || $0.userName.contains(searchManagerName) }, id: \.userIdx) { userData in
                            VStack(alignment: .leading){
                                HStack(spacing: 0){
                                    
                                    
                                    if(userData.userType == "employee"){
                                        
                                        Text("\(userData.userName) \(userData.position!)")
                                            .frame(minWidth: 0, minHeight: 0, maxHeight: .infinity)
                                            .foregroundColor(.blue)
                                        
                                    }else{
                                        Text(userData.userName)
                                            .frame(minWidth: 0, minHeight: 0, maxHeight: .infinity)
                                            .foregroundColor(.blue)
                                    }
                                    
                                    
                                    Spacer()
                                    
                                    Text("추가")
                                        .foregroundColor(.gray)
                                    
//                                    let isInList = humanResourceUsersList.contains(where: { $0.userName == userData.userName })
//                                                let userStatus = isInList ? "선택" : "추가"
//                                                Text(userStatus)
                                }
                                
                                
                            }
                            .swipeActions {
                                Button(action: {
                                    print("제외 버튼 클릭")
                                    
                                    selectedManagerName = "\(userData.userName)"
                                    selectedManagerIdx = userData.humanResourceIdx
                                  
                                    
                                    if let index = humanResourceUsersList.firstIndex(of: selectedManagerIdx) {
                                        humanResourceUsersList.remove(at: index)
                                        addedHumanList.remove(at: index)
                                    }
                                    
                                    print(addedHumanList)
                                    print(humanResourceUsersList)
                                    
                                    
                                    if(addedHumanList.isEmpty){
                                        isHumanListEmpty = true
                                    }
                                    
                                    searchViewModel.navigateAllUserSearchView = false
                                    
                                }) {
                                    Text("제외")
                                }
                                .tint(.red)
                                
                                Button(action: {
                                    print("선택 버튼 클릭")
                                    
                                    selectedManagerName = "\(userData.userName)"
                                    selectedManagerIdx = userData.humanResourceIdx
                                    
                                    isHumanListEmpty = false //리스트 비어있지 않음
                                    
                                    
                                    if !humanResourceUsersList.contains(selectedManagerIdx) {
                                        
                                        addedHumanList.append(selectedManagerName)
                                        humanResourceUsersList.append(selectedManagerIdx)
                                    }
                                
                                    print(addedHumanList)
                                    print(humanResourceUsersList)
                                    
                                    searchViewModel.navigateAllUserSearchView = false
                                    
                                }) {
                                    Text("선택")
                                }
                                .tint(.gray)
                                
                                
                            }
//                            .onTapGesture {
//                                print(userData.userName)
//                                
//                                selectedManagerName = "\(userData.userName)"
//                                searchViewModel.navigateAllUserSearchView = false
//                                
//                            }
                            
                            
                        }//List
                        .listStyle(PlainListStyle())
                        .background(.white)
                        .onAppear{
                            allEmployeeCreatorInquiry()
                        }
                        
                        
                        
//                        List {
//                            ForEach(allEmployeeCreatorList.filter { searchManagerName.isEmpty || $0.userName.contains(searchManagerName) }, id: \.userIdx) { user in
//                                
//                                Button {
//                                    swipeStaus = true
//                                } label: {
//                                    HStack{
//                                        Text(user.userName)
//                                            .foregroundColor(.blue)
//                                            
//                                        Spacer()
//                                        
//                                        if(swipeStaus){
//                                            
//                                            HStack{
//                                                Text("선택")
//                                                    .foregroundColor(.white)
//                                                    .background(.gray)
//
//                                                    .onTapGesture {
//                                                        searchViewModel.humanResourceUsersName = user.userName
//                                                        searchViewModel.humanResourceUsersIdx = user.userIdx
//                                                        
//                                                        searchViewModel.navigateAllUserSearchView = false
//                                                        swipeStaus = false
//                                                    }
//                                                
//                                                Text("제외")
//                                                    .foregroundColor(.white)
//                                                
//                                                    .background(.red)
//                                                    .onTapGesture {
//                                                        searchViewModel.navigateAllUserSearchView = false
//                                                        swipeStaus = false
//                                                    }
//                                            }
//                                            
//                                        }
//                                        
//                                    }
//                                }
//
//                            }
//                            
//                        }
//                        .listStyle(PlainListStyle())
//                        .background(.white)
//                        .onAppear{
//                            allEmployeeCreatorInquiry()
//                        }
//                       //하단 네비게이션을 위한 패딩
                        HStack{
                            Rectangle()
                                .frame(width: 0, height: 0)
                                .foregroundColor(.white)
                        }
                        .padding(.bottom, screenHeight/28) //29
                        
                        
                        Spacer()
                        
                    } //VStack
                    .navigationBarHidden(true)
                    .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                        UIApplication.shared.endEditing()
                        
                    }
                    
                }//ZStack
                .padding()
                .background(.white)
                
            }
            else{
                ZStack{
                    ScrollView {
                        
                        VStack(spacing: 0){
                            
                            
                            VStack{
                                
                                HStack(spacing: 0){
                                    Text("과업명")
                                        .font(.system(size: 14))
                                        .padding(.leading)
                                    
                                    Spacer()
                                }
                                .padding(.leading, screenWidth / 15.625)
                                
                                HStack{
                                    TextField(".", text: $taskName, prompt: Text("과업명을 입력해주세요.")
                                        .foregroundColor(Color("hintTextColor")))
                                    .keyboardType(.default)
                                    .font(.system(size: 14))
                                    .padding(.leading, 14)
                                    .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                }
                                .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                                .overlay(RoundedRectangle(cornerRadius: 4)
                                    .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                                )
                                
                            }//VStack 이름
                            .padding(.top, screenHeight / 54.133) //15
                            
                            VStack{
                                
                                HStack(spacing: 0){
                                    Text("한 줄 설명")
                                        .font(.system(size: 14))
                                        .padding(.leading)
                                    
                                    Spacer()
                                }
                                .padding(.leading, screenWidth / 15.625)
                                
                                HStack{
                                    TextField(".", text: $taskOneLineDescription, prompt: Text("한 줄 설명을 입력해주세요.")
                                        .foregroundColor(Color("hintTextColor")))
                                    .keyboardType(.default)
                                    .font(.system(size: 14))
                                    .padding(.leading, 14)
                                    .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                }
                                .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                                .overlay(RoundedRectangle(cornerRadius: 4)
                                    .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                                )
                                
                            }//VStack 이름
                            .padding(.top, screenHeight / 54.133) //15
                            
                            VStack{
                                
                                HStack(spacing: 0){
                                    Text("일정 등록")
                                        .font(.system(size: 14))
                                        .padding(.leading)
                                    
                                    Spacer()
                                }
                                .padding(.leading, screenWidth / 15.625)
                                
                                
                                HStack{
                                    Button(action: {
                                        
                                        
                                            startDate = nil
                                            endDate = nil
                                            
                                            self.showingDatePicker = true
                                            
                                            toastText = "시작 날짜 선택 후 확인을 눌러주세요"
                                            showToast = true
                                            
                                            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                                self.showToast = false
                                                toastText = ""
                                            }
                                        
                                        
                                    }, label: {
                                        
                                        HStack{
                                            Text("\(startDateString) ~ \(endDateString)")
                                                .foregroundColor(Color("hintTextColor"))
                                            .keyboardType(.emailAddress)
                                            .font(.system(size: 14))
                                            .padding(.leading, 14)
                                            
                                            Spacer()
                                            
                                        }
                                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                                        .overlay(RoundedRectangle(cornerRadius: 4)
                                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                                        )
                                        
                                        
                                      
                                        
                                    })
                                    
                                    //TODO                                일정 등록 관련 TextField 작성해야함
                                    //
        //                            TextField(".", text: $startDateString, prompt: Text("일정을 등록해주세요.")
                                       
                                }
                                
                               
                            }//VStack 계약기간
                            .sheet(isPresented: $showingDatePicker) {
                                VStack {
                                    
                                        DatePicker("날짜를 선택해주세요", selection: $dateSelection, displayedComponents: .date)
                                            .datePickerStyle(GraphicalDatePickerStyle())
                                            .padding()
                                            .overlay{
                                                ZStack{}
                                                .toast(isShowing: $showToast, text: Text(toastText))
                                            }
                                    
                                    
                                    
                                    Button("확인") {
                                        if self.dateCount == 0 {
                                            self.startDate = dateSelection
                                            self.dateCount += 1
                                        } else if self.dateCount == 1 {
                                            if dateSelection > self.startDate! {
                                                self.endDate = dateSelection
                                                self.dateCount = 0
                                            } else {
                                                self.startDate = nil
                                                self.endDate = nil
                                                self.dateCount = 0
                                            }
                                        }
                                        
                                        
                                        if (startDate != nil && endDate == nil){
                                            
                                            toastText = "종료 날짜 선택 후 확인을 주세요"
                                            showToast = true
                                            
                                            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                                self.showToast = false
                                                toastText = ""
                                            }
                                            
                                            self.showingDatePicker = true
                                        }
                                        
                                        
                                        if (startDate != nil && endDate != nil){
                                            startDateString = convertDateToString(date: startDate)
                                            endDateString = convertDateToString(date: endDate)
                                            
                                            self.showingDatePicker = false
                                        }else{
                                            startDateString = ""
                                            endDateString = ""
                                        }
                                    }
                                }
                            }
                            .padding(.top, screenHeight / 54.133) //15
                            
                            
                            VStack{
                                
                                HStack{
                                    Text("진행상태")
                                        .font(.system(size: 14))
                                        .padding(.leading)
                                    
                                    Spacer()
                                }
                                .padding(.horizontal, screenWidth / 15.625)
                                
                                HStack(spacing: screenWidth / 125 ){ //SelectedBtn
                                    
                                    Spacer()
                                    
                                    Button(action: {
                                        
                                        selectedBtn = .request
                                        selectedTaskStatus = "요청"
                                    }, label: {
                                        Text("요청")
                                            .foregroundColor(selectedBtn == .request ? .white : .black)
                                        
                                    })
                                    .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                                    .background(selectedBtn == .request ? Color("mainColor") : Color("colorEFEFF4"))
                                    .cornerRadius(16.0)
                                    
                                    Button(action: {
                                        
                                        selectedBtn = .ongoing
                                        selectedTaskStatus = "진행"
                                        
                                    }, label: {
                                        Text("진행")
                                            .foregroundColor(selectedBtn == .ongoing ? .white : .black)
                                        
                                    })
                                    .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                                    .background(selectedBtn == .ongoing ? Color("mainColor") : Color("colorEFEFF4"))
                                    .cornerRadius(16.0)
                                    
                                    Button(action: {
                                        
                                        selectedBtn = .feedback
                                        selectedTaskStatus = "피드백"
                                        
                                    }, label: {
                                        Text("피드백")
                                            .foregroundColor(selectedBtn == .feedback ? .white : .black)
                                        
                                    })
                                    .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                                    .background(selectedBtn == .feedback ? Color("mainColor") : Color("colorEFEFF4"))
                                    .cornerRadius(16.0)
                                    
                                    Button(action: {
                                        
                                        selectedBtn = .complete
                                        selectedTaskStatus = "완료"
                                        
                                    }, label: {
                                        Text("완료")
                                            .foregroundColor(selectedBtn == .complete ? .white : .black)
                                        
                                    })
                                    .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                                    .background(selectedBtn == .complete ? Color("mainColor") : Color("colorEFEFF4"))
                                    .cornerRadius(16.0)
                                    
                                    Button(action: {
                                        
                                        selectedBtn = .hold
                                        selectedTaskStatus = "보류"
                                        
                                    }, label: {
                                        Text("보류")
                                            .foregroundColor(selectedBtn == .hold ? .white : .black)
                                        
                                    })
                                    .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                                    .background(selectedBtn == .hold ? Color("mainColor") : Color("colorEFEFF4"))
                                    .cornerRadius(16.0)
                                    
                                    
                                    
                                    Spacer()
                                    
                                    
                                }//HStack (SelectedBtn 을 위한 HStack)
                                .padding(.horizontal, screenWidth / 15.625)
                                
                                HStack{
                                    Text("참여 인원 추가")
                                        .padding(.leading)
                                    
                                    
                                    Button(action: {
                                        
                                        print("참여 인원 추가 버튼 클릭")
                                        
                                        searchViewModel.navigateAllUserSearchView = true
                                        
                                    }, label: {
                                        
                                    
                                        Circle()
                                            .frame(width: screenWidth/12.5)
                                            .foregroundColor(Color("colorEFEFF4"))
                                            .overlay{
                                                Text("+")
                                                    .foregroundColor(Color("mainColor"))
                                                    .bold()
                                            }
                                        
                                    })
                                    
                                    
                                    Spacer()
                                }
                                .padding(.leading, screenWidth / 15.625)
                                .padding(.top, screenHeight / 54.133) //15
                                
                                
                            }
                            .padding(.top, screenHeight / 54.133) //15
                            
                            if(!isHumanListEmpty){
                                HStack{
                                    ScrollView(.horizontal, showsIndicators: false){
                                        HStack(spacing: 8){
                                            ForEach(addedHumanList, id: \.self) { item in
                                                Text(item)
                                                    .foregroundColor(Color("mainColor"))
                                                    .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                                                    .background(Color("colorEFEFF4"))
                                                    .cornerRadius(16.0)
                                            }
                                        }
                                    }
                                    .padding(.leading, screenWidth / 15.625)
                                }
                                .padding()
        //                        .padding(.bottom, screenWidth/20.833) //18
                            }
                            
                            HStack{
                                
                                Button(action: {
                                    
                                    if(taskName == "" || taskOneLineDescription == "" || startDateString == "" || endDateString == ""){
                                        
                                        showAlert = true
                                        
                                        alertTitle = "과업 등록 실패"
                                        alertMessage = "칸을 비울 수 없습니다."
                                       
                                        
                                    }else if (humanResourceUsersList.isEmpty) {
                                        
                                        showAlert = true
                                        
                                        alertTitle = "과업 등록 실패"
                                        alertMessage = "참여 인원을 선택해주세요"
                                        
                                    }else{
                                        addHumanTask()
                                        
                                        self.presentationMode.wrappedValue.dismiss()
                                        
                                    
                                        
                                        detailSchedulDetailView.detailProjectScheduleInquiry(accessToken: loginData.token)
                                        
                                        detailSchedulDetailView.taskInquiry(accessToken: loginData.token)
                                            
                                    
                                    }
                                    
                                }, label: {
                                    
                                    Text("과업 추가")
                                        .foregroundColor(.white)
                                        .font(.system(size: 16))
                                        .bold()
                                        .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                                    
                                })
                                .background(Color("mainColor"))
                                .cornerRadius(4)
                                .padding(.top, isHumanListEmpty ? screenHeight/21.945 : 0) // 리스트가 비어있으면 패딩 37 리스트가 있으면 패딩 0
                                
                                
                                
                            }
                            
                            //하단 네비게이션을 위한 패딩
                            HStack{
                                Rectangle()
                                    .frame(width: 0, height: 0)
                                    .foregroundColor(.white)
                            }
                            .padding(.bottom, screenHeight/28) //29
                            
                            Spacer()
                            
                            
                            
                            
                        }//VStack
                        .navigationBarBackButtonHidden(true)
                        .navigationBarItems(leading: Button(action: {
                            
                            self.presentationMode.wrappedValue.dismiss()
                            
                            
                        }) {
                            
                            HStack(spacing: 0){
                                
                                Image(systemName: "chevron.backward")
                                
                            }
                            
                            
                            
                        })
                        .navigationBarTitle("\(titleScheduleName)", displayMode: .inline)
                        
                        
                    }//ScrollView
                    .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                        UIApplication.shared.endEditing()
                        
                    }
                }//ZStack
                .alert(isPresented: $showAlert, content: {
                    Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                        print("알림창 확인 버튼이 클릭되었다.")
                        showAlert = false
                        
                        alertTitle = ""
                        alertMessage = ""
                    }))
                })
                
            }
        
    }
    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        return formatter
    }()
    
    //모든 직원/크리에이터 api
    private func allEmployeeCreatorInquiry() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        AF.request("\(defaultUrl)/admin/api/human-resource?page=1&limit=100", method: .get, headers: headers).responseDecodable(of: AllEmployeeCreatorDataResponse.self) { response in
            switch response.result {
            case .success(let response):
                // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
           
                print(response.data)
                

                DispatchQueue.main.async {
                    allEmployeeCreatorList = response.data
                }
                
            case .failure(let error):
                // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                print(error)
            }
        }
    }
    
    
    
    //최종적으로 등록
    private func addHumanTask() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any] = [
            
            "schedule_idx": selectedBrandScheduleIdx,
              "task_name": taskName,
              "one_line_explain": taskOneLineDescription,
              "task_start_dt": startDateString,
              "task_end_dt": endDateString,
              "task_status": selectedTaskStatus,
              "hide_or_not": "x",
              "human_resource_users": humanResourceUsersList
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/task",
                   method: .post,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("소속직원 계약 등록 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("소속직원 계약 등록 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("소속직원 계약 등록 실패: \(error)")
                
            }
        }
    }
 
    func convertDateToString(date: Date?) -> String {
        guard let date = date else {
            return ""
        }

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
}







//
//struct AddTask_PreviewPreviewProvider {
//
//
//    static var previews: some View {
//        AddTask(titleProjectName: "", selectedBtn: .request)
//    }
//}


//#Preview {
//    AddTaskView(titleProjectName: "")
//}
