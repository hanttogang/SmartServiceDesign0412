//
//  DetailSchedulDetailView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/27/23.
//

import SwiftUI
import Alamofire

struct DetailSchedulDetailView: View {
    
    //Api

    var selectedBrandScheduleIdx: Int
   var titleProjectName: String
    var matchingBrandImage: String
    
    var detailMarketingProjectListView: DetailMarketingProjectListView
    // 이 init 메소드를 통해 외부에서 selectedCustomerBrandIdx 값을 설정할 수 있습니다.
    init(selectedBrandScheduleIdx: Int, titleProjectName: String, matchingBrandImage: String) {
        self.selectedBrandScheduleIdx = selectedBrandScheduleIdx
       self.titleProjectName = titleProjectName
        self.matchingBrandImage = matchingBrandImage
        
        
        self.detailMarketingProjectListView = DetailMarketingProjectListView(selectedBrandProjectIdx: selectedBrandScheduleIdx, titleProjectName: titleProjectName)
    }
//
    @State private var taskDataList = [TaskData]()
    @State private var taskDataListCount: Int = 0
    
    @State private var brandDataList = [BrandData]()
    
    @EnvironmentObject var myWebVM: WebViewModel
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    //View
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State private var navigate: Bool = false
    
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""

    @State private var showToast: Bool = false
    @State private var toastText: String = "시작 날짜 선택 후 확인을 눌러주세요"
    
    @State private var modificationMode: Bool = false
   
    
    @State private var scheduleNameFor_DetailSchedulDetail: String = ""
    
    @State private var oneLineExplainFor_DetailSchedulDetail: String = ""
    
    @State private var contractStartDateFor_DetailCreator: String = ""
    @State private var contractEndDateFor_DetailCreator: String = ""
    
    @State private var humanCount: String = ""
    
    
    @State private var selecteWorkAreaFor_DetailSchedulDetail: Bool = false
    @State private var workAreaModificationModeFor_DetailSchedulDetail: Bool = false
    
    @State private var contractImageForDetailSchedulDetailContractList: UIImage? = nil
    
    
    @State private var showAllContractList: Bool = false
    @State private var showShowAllButton: Bool = false
    
    
    //날짜 관련
    @State private var showingDatePicker = false
    @State private var dateSelection = Date()
    @State private var startDate: Date? = nil
    @State private var endDate: Date? = nil
    @State private var dateCount = 0
    
    var body: some View {
        
        ZStack {
            ScrollView(.vertical){
                VStack(spacing: 0){
                    
                    
                    HStack{
                        
                        
                        
                        
                        
                            
                            HStack{
                                
                                //크리에이터이름
                                if modificationMode{
                                    
                                    TextField(".", text: $scheduleNameFor_DetailSchedulDetail, prompt: Text("\(scheduleNameFor_DetailSchedulDetail)")
                                        .foregroundColor(Color("hintTextColor")))
                                    .keyboardType(.default)
                                    .font(.title2)
                                    .frame(width: screenWidth/1.3, height: screenHeight/29.1)
    //                                .padding(.leading, -screenWidth/150)
                                    
                                    
                                }else {
                                    
                                    Text("\(scheduleNameFor_DetailSchedulDetail)")
                                        .font(.title2)
                                        .frame(height: screenHeight/29.1)
                                        
//                                        .frame(width: screenWidth/6.3)
                                }
                            }
                            
                            
                        
                        
                        
                        
                        
                        
                        
                        
                        Spacer()
                        
                        if(modificationMode){
                            Button(action: {
                                
                                
                                if (scheduleNameFor_DetailSchedulDetail == "" ||
                                    oneLineExplainFor_DetailSchedulDetail == "" ||
                                    humanCount == ""){
                                    
                                    showAlert = true
                                    
                                    alertTitle = "일정 수정 실패"
                                    alertMessage = "칸을 비울 수 없습니다."
                                    
                                } else {
                                    patchDetailSchedule()
                                    
                                    modificationMode = false
                                }
                                
                                
                                
                            }, label: {
                                Text("저장")
                                
                            })
                        } else {
                            Button(action: {
                                
                                modificationMode = true
                                
                                
                            }, label: {
                                Text("수정")
                                
                            })
                        }
                        
                        
                    }
                    .padding(.horizontal)
                    .padding(.vertical, screenHeight / 81.5)
                    .padding(.top, screenHeight / 81.5)
                    
                    Divider()
                        .padding(.leading)
                   

                    
                    
                    HStack{
                        
                        Text("한줄 설명")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            TextField("", text: $oneLineExplainFor_DetailSchedulDetail)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)

                            
//                            TextEditor(text: $oneLineExplainFor_DetailSchedulDetail)
//                                .multilineTextAlignment(.trailing)
//                                .foregroundColor(Color("color00000040"))
//                                .frame(height: screenHeight/40.5)
//                            RightAlignedTextField(text: $oneLineExplainFor_DetailSchedulDetail)
//                                .keyboardType(.numberPad)
                            
                            
                            
                        }else {
                            
                            Text("\(oneLineExplainFor_DetailSchedulDetail)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    
                    HStack{
                        Text("계약기간")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            
                            Button {
                                
                                startDate = nil
                                endDate = nil
                                
                                self.showingDatePicker = true
                                
                                toastText = "시작 날짜 선택 후 확인을 눌러주세요"
                                showToast = true
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                    self.showToast = false
                                    toastText = ""
                                }
                                
                            } label: {
                                
                                
                                Text("\(contractStartDateFor_DetailCreator) ~ \(contractEndDateFor_DetailCreator)")
                                    .foregroundColor(Color("color00000040"))

                            }

                            
                            
                        }else {
                            
                            Text("\(contractStartDateFor_DetailCreator) ~ \(contractEndDateFor_DetailCreator)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .sheet(isPresented: $showingDatePicker) {
                        VStack {
                            
                                DatePicker("날짜를 선택해주세요", selection: $dateSelection, displayedComponents: .date)
                                    .datePickerStyle(GraphicalDatePickerStyle())
                                    .padding()
                                    .overlay{
                                        ZStack{}
                                        .toast(isShowing: $showToast, text: Text(toastText))
                                    }
                            
                            
                            
                            Button("확인") {
                                if self.dateCount == 0 {
                                    self.startDate = dateSelection
                                    self.dateCount += 1
                                } else if self.dateCount == 1 {
                                    if dateSelection > self.startDate! {
                                        self.endDate = dateSelection
                                        self.dateCount = 0
                                    } else {
                                        self.startDate = nil
                                        self.endDate = nil
                                        self.dateCount = 0
                                    }
                                }
                                
                                
                                if (startDate != nil && endDate == nil){
                                    
                                    toastText = "종료 날짜 선택 후 확인을 주세요"
                                    showToast = true
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                        self.showToast = false
                                        toastText = ""
                                    }
                                    
                                    self.showingDatePicker = true
                                }
                                
                                
                                if (startDate != nil && endDate != nil){
                                    contractStartDateFor_DetailCreator = convertDateToString(date: startDate)
                                    contractEndDateFor_DetailCreator = convertDateToString(date: endDate)
                                    
                                    self.showingDatePicker = false
                                }else{
                                    contractStartDateFor_DetailCreator = ""
                                    contractEndDateFor_DetailCreator = ""
                                }
                            }
                        }
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("참여인원")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        // 인원수는 수정 못함
//                        if modificationMode{
//                            RightAlignedTextField(text: $humanCount)
//                                .keyboardType(.numberPad)
//                                .autocapitalization(.none)
//
//                        }else {
                            
                            Text("\(humanCount)" + "명")
                                .foregroundColor(Color("color00000040"))
//                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
              
                    
                    VStack(spacing: 0) {
                        HStack{
                            
                            Text("과업 리스트")
                                .font(.title2)
                                .bold()
                                .padding()
                            //                                .padding(.top, -screenHeight/13.5)
                            Spacer()
                        }
                        .padding(0)
                        
                        Button(action: {
                            print("과업 추가 버튼 클릭됨")
                            
                            self.navigate = true
                            
                        }, label: {
                            
                            NavigationLink(destination: AddTaskView(selectedBrandScheduleIdx: selectedBrandScheduleIdx, titleScheduleName: scheduleNameFor_DetailSchedulDetail, matchingBrandImage: matchingBrandImage), isActive: $navigate) {
                                EmptyView()
                            }
                            
                            Text("+  과업 추가")
                                .foregroundColor(.white)
                                .font(.system(size: 16))
                                .bold()
                                .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        
                        
                        VStack{//계약 리스트 터치시
                            
                            ForEach(taskDataList.prefix(showAllContractList ? taskDataList.count : 3), id: \.taskIdx) { taskData in
                                
                                
                                
                                NavigationLink(destination: DetailTaskView(selectedTaskIdx: taskData.taskIdx, selectedBrandScheduleIdx: selectedBrandScheduleIdx, selectedTaskName: taskData.taskName, humanCount: humanCount, titleProjectName: titleProjectName, matchingBrandImage: matchingBrandImage)) {
                                    
                                    VStack{
                                        HStack{ //리스트항목의 모델
                                            
                                            AsyncImage(url: URL(string: matchingBrandImage)) { image in
                                                image.resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                    .cornerRadius(12.0)
                                            } placeholder: {
                                                ProgressView()
                                                    .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                            }

                                            
                                            VStack(alignment: .leading){
                                                Text(taskData.taskName)
                                                    .foregroundColor(.black)
                                                
                                                Text("\(dateFormatter(getDate: taskData.taskStartDt)) ~ \(dateFormatter(getDate: taskData.taskEndDt))")
                                                    .foregroundColor(Color("color00000040"))
                                                    .font(.caption)
                                            }
                                            
                                            Spacer()
                                            
                                            
                                            Text("\(taskData.taskStatus)")
                                                .foregroundColor(.white)
                                                .bold()
                                                .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                                .background(Color("mainColor"))
                                                .cornerRadius(16.0)
                                            
                                            
                                        }
                                        
                                        Divider()
                                            .padding(.leading, screenWidth/8)
                                    }
                                    
                                }//NavigationLink
                            }//ForEach
                            .onAppear{
                                taskInquiry(accessToken: loginData.token)
                            }
                            
                            
                            
                            
                        } //VStack 과업 리스트
                        .listStyle(PlainListStyle())
                        .padding(.horizontal)
                        .padding(.top, screenHeight/50.9375)
                        
                        
                        if showShowAllButton{
                            
                            Button(action: {
                                
                                showShowAllButton = false
                                showAllContractList = true
                                
                            }, label: {
                                
                                Text("더 보기")
                                    .foregroundColor(.black)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                            })
                            .background(.white)
                            .cornerRadius(4)
                            .padding()
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color(.black), lineWidth: 1)
                                .padding()
                            )
                            
                        }
                        
                        Spacer()
                        
                        
                        
                        
                    }//VStack
                    .frame(width: screenWidth/1.0932)
                    .background(Color.white)
                    .cornerRadius(20)
                    .shadow(color: Color("color00000040"), radius: 20, x: 0, y: 30)
                    .padding(.top, screenHeight/31.346)
                    
                    
                    //하단 네비게이션을 위한 패딩
                    HStack{
                        Rectangle()
                            .frame(width: 0, height: 0)
                            .foregroundColor(.white)
                    }
                    .padding(.bottom, screenHeight/28) //29
                    
                } //VStack
                .alert(isPresented: $showAlert, content: {
                    Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                        print("알림창 확인 버튼이 클릭되었다.")
                        showAlert = false
                        
                        alertTitle = ""
                        alertMessage = ""
                    }))
                })
                
            }//ScrollView
            
            
        }//ZStack
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            
            self.presentationMode.wrappedValue.dismiss()
            
            detailMarketingProjectListView.brandInquiry(accessToken: loginData.token)
            detailMarketingProjectListView.detailProjectScheduleInquiry(accessToken: loginData.token)
            
            if(myWebVM.isStateWebCalendarViewBoolean){
                myWebVM.isStateWebCalendarView.send(true)
            }
            
            
            
            
            
        }) {
            HStack(spacing: 0){
                Image(systemName: "chevron.backward")
            }
        })
        .navigationBarTitle("\(scheduleNameFor_DetailSchedulDetail)", displayMode: .inline)
        .onAppear {
            detailProjectScheduleInquiry(accessToken: loginData.token)
            
            taskInquiry(accessToken: loginData.token)
        }
        
    }//body
    
    func detailProjectScheduleInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/schedule?schedule_idx=\(selectedBrandScheduleIdx)&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: ProjectScheduleDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    
                    print("프로젝트 정보: \(response.data)")
                    
                    for detailProjectSchedule in response.data{
                        scheduleNameFor_DetailSchedulDetail = detailProjectSchedule.scheduleName
                        oneLineExplainFor_DetailSchedulDetail = detailProjectSchedule.oneLineExplain
                        
                        contractStartDateFor_DetailCreator = dateFormatterMinusForm(getDate: detailProjectSchedule.scheduleStartDt)
                        contractEndDateFor_DetailCreator = dateFormatterMinusForm(getDate: detailProjectSchedule.scheduleEndDt)
                        
                        humanCount = String(detailProjectSchedule.humanCount)

                    }
                    

                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
        })
      
        
        
    }
    
    
    //과업 가져오는 api
    func taskInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/task?schedule_idx=\(selectedBrandScheduleIdx)&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: TaskDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    
                    DispatchQueue.main.async {
                        taskDataList = response.data
                        
                        taskDataListCount = response.pagination.total
                        
                        print("--------------과업 리스트 호출 성공 \(response.data)")
                        
                        if (taskDataListCount <= 3) { //
                            showShowAllButton = false
                            showAllContractList = false
                        }else{
                            showShowAllButton = true //더 보기 버튼 보임
                            showAllContractList = false
                        }
                    }
                    
                    
                    
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
        })
        
        
    }

                                               
    private func brandInquiry() {
       
       let headers: HTTPHeaders = [
           "Authorization": "Bearer \(loginData.token)",
           "Accept": "application/json"
       ]
       
       AF.request("\(defaultUrl)/api/brand?page=1&limit=100", method: .get, headers: headers).responseDecodable(of: BrandDataResponse.self) { response in
           switch response.result {
           case .success(let response):
               // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
               
               //                brandIdx = brandData.brandIdx
               
               print("브랜드 정보: \(response.data)")
               
               DispatchQueue.main.async {
                   brandDataList = response.data
               }
               
               
           case .failure(let error):
               // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
               print(error)
           }
       }
       
       
    }

   private func dateFormatter(getDate: String) -> String {
       
       let isoDateFormatter = ISO8601DateFormatter()
       isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

       let getDate = getDate
       if let date = isoDateFormatter.date(from: getDate) {
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "yyyy.MM.dd"
           let dateString = dateFormatter.string(from: date)
           print(dateString) // "2024.01.03" 출력
           return dateString
       } else {
           print("날짜 변환에 실패했습니다.")
           return ""
       }

   }
    
    private func dateFormatterMinusForm(getDate: String) -> String {
        
        let isoDateFormatter = ISO8601DateFormatter()
        isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

        let getDate = getDate
        if let date = isoDateFormatter.date(from: getDate) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateString = dateFormatter.string(from: date)
            print(dateString) // "2024.01.03" 출력
            return dateString
        } else {
            print("날짜 변환에 실패했습니다.")
            return ""
        }

    }
    
    //최종적으로 수정
    private func patchDetailSchedule() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any?] = [
            "schedule_name": scheduleNameFor_DetailSchedulDetail,
             "one_line_explain": oneLineExplainFor_DetailSchedulDetail,
             "schedule_start_dt": contractStartDateFor_DetailCreator,
             "schedule_end_dt": contractEndDateFor_DetailCreator
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/schedule/\(selectedBrandScheduleIdx)",
                   method: .patch,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("스케줄 수정 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("스케줄 수정 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("스케줄 수정 실패: \(error)")
                
            }
        }
    }
    
    func convertDateToString(date: Date?) -> String {
        guard let date = date else {
            return ""
        }

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
    
}
//
//#Preview {
//    DetailSchedulDetailView()
//}
