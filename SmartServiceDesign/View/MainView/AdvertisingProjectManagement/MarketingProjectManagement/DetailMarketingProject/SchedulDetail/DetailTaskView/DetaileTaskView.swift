//
//  DetailTaskView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/19/23.
//

import SwiftUI
import Alamofire

struct DetailTaskView: View {
    //Api
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var memoDataList = [TaskMemoData]()
    
    @State private var allEmployeeCreatorList = [AllEmployeeCreatorData]()
    
    
    //View
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    
    var selectedTaskIdx: Int
    var selectedBrandScheduleIdx: Int
    var selectedTaskName: String
    var humanCount: String
    
    var titleProjectName: String
    var matchingBrandImage: String
    
    var detailSchedulDetailView: DetailSchedulDetailView
    
    init(selectedTaskIdx: Int, selectedBrandScheduleIdx: Int, selectedTaskName: String, humanCount: String, titleProjectName: String, matchingBrandImage: String){
        self.selectedTaskIdx = selectedTaskIdx
        self.selectedBrandScheduleIdx = selectedBrandScheduleIdx
        self.selectedTaskName = selectedTaskName
        self.humanCount = humanCount
        
        self.titleProjectName = titleProjectName
        self.matchingBrandImage = matchingBrandImage
        
        self.detailSchedulDetailView = DetailSchedulDetailView(selectedBrandScheduleIdx: selectedBrandScheduleIdx, titleProjectName: titleProjectName, matchingBrandImage: matchingBrandImage)
    }
    
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var showToast: Bool = false
    @State private var toastText: String = "시작 날짜 선택 후 확인을 눌러주세요"
    
    
    @State private var navigate: Bool = false
    
    @State private var modificationModeFor_DetailTask: Bool = false
    
    
    @State private var taskNameFor_DetailTask: String = ""
    @State private var taskOneLineExplanFor_DetailTask: String = ""
    @State private var taskStartDateFor_DetailTask: String = "2023.12.01"
    @State private var taskEndDateFor_DetailTask: String = "2023.12.31"
    
    @State private var taskNumberOfParticipantsFor_DetailTask: String = "10"
    
    @State private var taskStatusFor_DetailTask: String = ""
    @State private var hideStatusFor_DetailTask: String = ""
    
    
    @State private var selecteTaskStatusFor_DetailTask: Bool = false
    @State private var selecteHideStatusFor_DetailTask: Bool = false
    
    @State private var taskStatusModificationModeFor_DetailTask: Bool = false
    
    
    @State private var showMemoList: Bool = false
    @State private var showShowAllButton: Bool = false
    
    @State var isShowingMemo: Bool = false
    @State var writedMemo: String = ""
    
    //날짜 관련
    @State private var showingDatePicker = false
    @State private var dateSelection = Date()
    @State private var startDate: Date? = nil
    @State private var endDate: Date? = nil
    @State private var dateCount = 0
    
    var body: some View {
        ZStack{
            ZStack {
                ScrollView(.vertical){
                    VStack(spacing: 0){
                        
                        HStack{
                            
                        
                            
                            if modificationModeFor_DetailTask{
                                
                                TextField(".", text: $taskNameFor_DetailTask, prompt: Text("\(taskNameFor_DetailTask)")
                                    .foregroundColor(Color("hintTextColor")))
                                .keyboardType(.default)
                                .font(.title2)
                                
                                
                            }else {
                                
                                Text("\(taskNameFor_DetailTask)")
                                    .font(.title2)
                            }
                            
                            
                            
                            
                            
                            Spacer()
                            
                            if(modificationModeFor_DetailTask){
                                Button(action: {
                                    
                                    
                                    if (taskNameFor_DetailTask == "" || taskOneLineExplanFor_DetailTask == "" || taskStartDateFor_DetailTask == "" || taskEndDateFor_DetailTask == "" || taskNumberOfParticipantsFor_DetailTask == ""){
                                        
                                        showAlert = true
                                        
                                        alertTitle = "과업 수정 실패"
                                        alertMessage = "칸을 비울 수 없습니다."
                                        
                                        
                                    } else {
                                        
                                        if (hideStatusFor_DetailTask == "숨김"){
                                            hideStatusFor_DetailTask = "o"
                                        }else if(hideStatusFor_DetailTask == "미처리"){
                                            hideStatusFor_DetailTask = "x"
                                        }
                                        
                                        patchEmployeeContract()
                                        
                                        if (hideStatusFor_DetailTask == "o"){
                                            hideStatusFor_DetailTask = "숨김"
                                        }else if(hideStatusFor_DetailTask == "x"){
                                            hideStatusFor_DetailTask = "미처리"
                                        }
                                        
                                        modificationModeFor_DetailTask = false
                                    }
                                    
                                    
                                    
                                }, label: {
                                    Text("저장")
                                    
                                })
                            } else {
                                Button(action: {
                                    
                                    modificationModeFor_DetailTask = true
                                    
                                    
                                }, label: {
                                    Text("수정")
                                    
                                })
                                
                            }
                            
                            
                        }// HStack 계약 이름
                        .padding(.horizontal)
                        .padding(.vertical, screenHeight / 81.5)
    //                    .padding(.top, screenHeight / 81.5)
                        
                        
                        Divider()
                            .padding(.leading)
                        
                        
                        HStack{
                            
                            Text("한줄 설명")
                                .padding(.vertical, screenHeight / 81.5)
                            
                            Spacer()
                            
                            if modificationModeFor_DetailTask{
                                
                                TextField("", text: $taskOneLineExplanFor_DetailTask)
                                    .multilineTextAlignment(.trailing)
                                    .foregroundColor(Color("color00000040"))
                                    .frame(height: screenHeight/40.5)
//
//                                RightAlignedTextField(text: $taskOneLineExplanFor_DetailTask)
//                                    .keyboardType(.numberPad)
                                
                                
                                
                            }else {
                                
                                Text("\(taskOneLineExplanFor_DetailTask)")
                                    .foregroundColor(Color("color00000040"))
                            }
                            
                            
                            
                            
                            
                            
                        }
                        .padding(.horizontal)
                        
                        
                        Divider()
                            .padding(.leading)
                        
                        
                        HStack{
                            Text("일정")
                                .padding(.vertical, screenHeight / 81.5)
                            
                            Spacer()
                            
                            if modificationModeFor_DetailTask{
                                Button {
                                    
                                    startDate = nil
                                    endDate = nil
                                    
                                    self.showingDatePicker = true
                                    
                                    toastText = "시작 날짜 선택 후 확인을 눌러주세요"
                                    showToast = true
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                        self.showToast = false
                                        toastText = ""
                                    }
                                    
                                } label: {
                                    
                                    
                                    Text("\(taskStartDateFor_DetailTask) ~ \(taskEndDateFor_DetailTask)")
                                        .foregroundColor(Color("color00000040"))

                                }
                                
                                
                            }else {
                                
                                Text("\(taskStartDateFor_DetailTask) ~ \(taskEndDateFor_DetailTask)")
                                    .foregroundColor(Color("color00000040"))
                            }
                            
                        }
                        .sheet(isPresented: $showingDatePicker) {
                            VStack {
                                
                                    DatePicker("날짜를 선택해주세요", selection: $dateSelection, displayedComponents: .date)
                                        .datePickerStyle(GraphicalDatePickerStyle())
                                        .padding()
                                        .overlay{
                                            ZStack{}
                                            .toast(isShowing: $showToast, text: Text(toastText))
                                        }
                                
                                
                                
                                Button("확인") {
                                    if self.dateCount == 0 {
                                        self.startDate = dateSelection
                                        self.dateCount += 1
                                    } else if self.dateCount == 1 {
                                        if dateSelection > self.startDate! {
                                            self.endDate = dateSelection
                                            self.dateCount = 0
                                        } else {
                                            self.startDate = nil
                                            self.endDate = nil
                                            self.dateCount = 0
                                        }
                                    }
                                    
                                    
                                    if (startDate != nil && endDate == nil){
                                        
                                        toastText = "종료 날짜 선택 후 확인을 주세요"
                                        showToast = true
                                        
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                            self.showToast = false
                                            toastText = ""
                                        }
                                        
                                        self.showingDatePicker = true
                                    }
                                    
                                    
                                    if (startDate != nil && endDate != nil){
                                        taskStartDateFor_DetailTask = convertDateToString(date: startDate)
                                        taskEndDateFor_DetailTask = convertDateToString(date: endDate)
                                        
                                        self.showingDatePicker = false
                                    }else{
                                        taskStartDateFor_DetailTask = ""
                                        taskEndDateFor_DetailTask = ""
                                    }
                                }
                            }
                        }
                        .padding(.horizontal)
                        
                        
                        Divider()
                            .padding(.leading)
                        
                        HStack{
                            Text("참여인원")
                                .padding(.vertical, screenHeight / 81.5)
                            
                            Spacer()
                            
                            if modificationModeFor_DetailTask{
                                
                                TextField("", text: $taskNumberOfParticipantsFor_DetailTask)
                                    .multilineTextAlignment(.trailing)
                                    .foregroundColor(Color("color00000040"))
                                    .frame(height: screenHeight/40.5)
                                    .keyboardType(.numberPad)
                                
//                                RightAlignedTextField(text: $taskNumberOfParticipantsFor_DetailTask)
//                                    .keyboardType(.default)
//                                    .autocapitalization(.none)
                                
                                
                                
                            }else {
                                
                                Text("\(taskNumberOfParticipantsFor_DetailTask)")
                                    .foregroundColor(Color("color00000040"))
                            }
                            
                            
                            
                        }
                        .padding(.horizontal)
                        
                        
                        Divider()
                            .padding(.leading)
                        
                        
                        HStack(spacing: 0){
                            Text("진행상태")
                                .padding(.vertical, screenHeight / 81.5)
                            
                            Spacer()
                            
                            
                            HStack(spacing: 0){
                                
                                if modificationModeFor_DetailTask{
                                    
                                    
                                    Button(action: {
                                        taskStatusModificationModeFor_DetailTask = true
                                    }, label: {
                                        
                                        if taskStatusModificationModeFor_DetailTask{
                                            
                                            HStack(spacing: screenWidth/128.3){ // spacing 3
                                                Button(action: {
                                                    taskStatusModificationModeFor_DetailTask = false
                                                    
                                                    taskStatusFor_DetailTask = "요청"
                                                    
                                                }, label: {
                                                    Text("요청")
                                                        .foregroundColor(.white)
                                                        .bold()
                                                        .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                        .background(Color("mainColor"))
                                                        .cornerRadius(16.0)
                                                })
                                                
                                                Button(action: {
                                                    taskStatusModificationModeFor_DetailTask = false
                                                    
                                                    taskStatusFor_DetailTask = "진행"
                                                    
                                                }, label: {
                                                    Text("진행")
                                                        .foregroundColor(.white)
                                                        .bold()
                                                        .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                        .background(Color("mainColor"))
                                                        .cornerRadius(16.0)
                                                })
                                                
                                                Button(action: {
                                                    taskStatusModificationModeFor_DetailTask = false
                                                    
                                                    taskStatusFor_DetailTask = "피드백"
                                                    
                                                }, label: {
                                                    Text("피드백")
                                                        .foregroundColor(.white)
                                                        .bold()
                                                        .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                        .background(Color("mainColor"))
                                                        .cornerRadius(16.0)
                                                })
                                                
                                                Button(action: {
                                                    taskStatusModificationModeFor_DetailTask = false
                                                    
                                                    taskStatusFor_DetailTask = "완료"
                                                    
                                                }, label: {
                                                    Text("완료")
                                                        .foregroundColor(.white)
                                                        .bold()
                                                        .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                        .background(Color("mainColor"))
                                                        .cornerRadius(16.0)
                                                })
                                                
                                                Button(action: {
                                                    taskStatusModificationModeFor_DetailTask = false
                                                    
                                                    taskStatusFor_DetailTask = "보류"
                                                    
                                                }, label: {
                                                    Text("보류")
                                                        .foregroundColor(.white)
                                                        .bold()
                                                        .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                        .background(Color("mainColor"))
                                                        .cornerRadius(16.0)
                                                })
                                            } //HStack
                                            
                                            
                                        }else{
                                            
                                            Button(action: {
                                                taskStatusModificationModeFor_DetailTask = true
                                                
                                            }, label: {
                                                Text("\(taskStatusFor_DetailTask)")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                        }
                                        
                                    })
                                } else{
                                    
                                    Text("\(taskStatusFor_DetailTask)")
                                        .foregroundColor(.white)
                                        .bold()
                                        .frame(width: screenWidth/7, height: screenHeight/29.107)
                                        .background(Color("mainColor"))
                                        .cornerRadius(16.0)
                                }
                            }
                            
                        }
                        .padding(.horizontal)
                        
                        Divider()
                            .padding(.leading)
                        
                        
                        
                        HStack(spacing: 0){
                            Text("숨김처리")
                                .padding(.vertical, screenHeight / 81.5)
                            
                            Spacer()
                            
                            
                            HStack(spacing: 0){
                                
                                if modificationModeFor_DetailTask{
                                    
                                    
                                    Button(action: {
                                        selecteHideStatusFor_DetailTask = true
                                    }, label: {
                                        
                                        if selecteHideStatusFor_DetailTask{
                                            
                                            HStack(spacing: screenWidth/128.3){ // spacing 3
                                                Button(action: {
                                                    selecteHideStatusFor_DetailTask = false
                                                    
                                                    hideStatusFor_DetailTask = "숨김"
                                                    
                                                }, label: {
                                                    Text("숨김")
                                                        .foregroundColor(.white)
                                                        .bold()
                                                        .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                        .background(Color("mainColor"))
                                                        .cornerRadius(16.0)
                                                })
                                                
                                                Button(action: {
                                                    selecteHideStatusFor_DetailTask = false
                                                    
                                                    hideStatusFor_DetailTask = "미처리"
                                                    
                                                }, label: {
                                                    Text("미처리")
                                                        .foregroundColor(.white)
                                                        .bold()
                                                        .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                        .background(Color("mainColor"))
                                                        .cornerRadius(16.0)
                                                })
                                                
                                            } //HStack
                                            
                                            
                                        }else{
                                            
                                            Button(action: {
                                                selecteHideStatusFor_DetailTask = true
                                                
                                            }, label: {
                                                Text("\(hideStatusFor_DetailTask)")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                        }
                                        
                                    })
                                } else{
                                    
                                    Text("\(hideStatusFor_DetailTask)")
                                        .foregroundColor(.white)
                                        .bold()
                                        .frame(width: screenWidth/7, height: screenHeight/29.107)
                                        .background(Color("mainColor"))
                                        .cornerRadius(16.0)
                                }
                            }
                            
                        }
                        .padding(.horizontal)
                        
                        Divider()
                            .padding(.leading)
                        
                       
                        
                        
                        HStack{
                            
                            Button(action: {
                                print("메모 작성 버튼 클릭")
                                writedMemo = ""
                                
                                isShowingMemo = true

                                
                                
                            }, label: {
                                    Text("메모 작성")
                                        .foregroundColor(.white)
                                        .font(.system(size: 16))
                                        .bold()
                                        .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                                
                                }
                            )
                            .background(Color("mainColor"))
                            .cornerRadius(4)
                            .padding(.top, screenHeight/21.945) // 37
                            
                            
                            
                        }
                        
                        VStack(spacing: 0) {
                            
                            
                            VStack{//메모 리스트
                                
                                
                                
                                ForEach(memoDataList.prefix(showMemoList ? memoDataList.count : 3), id: \.taskMemoIdx) { memoData in
                                    
                                    //이미지 로직 수정 필요 - 마스터계정은 이미지 등록할 수 없으며, 현재 메모는 마스터계정만 작성이 가능하기 때문
                                    let matchingEmployee = allEmployeeCreatorList.first {$0.masterUserIdx == memoData.userIdx }
                                    
                                    
                                    VStack{
                                        HStack{ //리스트항목의 모델
                                            
                                            if matchingEmployee != nil {
                                                
                                                AsyncImage(url: URL(string: imageS3Url + "/" + matchingEmployee!.userImage)) { image in
                                                    image.resizable()
                                                        .aspectRatio(contentMode: .fill)
                                                        .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                        .cornerRadius(12.0)
                                                } placeholder: {
                                                    ProgressView()
                                                        .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                }
                                                
                                            } else {
                                                
                                                Rectangle()
                                                    .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                    .cornerRadius(12.0)
                                                    .foregroundColor(.colorE5E5E5)
                                            }
                                            
                                            
                                            
                                            VStack(alignment: .leading){
                                                HStack{
                                                    
                                                    if let userName = memoData.user.humanResource?.userName,
                                                       let position = memoData.user.humanResource?.position {
                                                        Text("\(userName) \(position)")
                                                            .foregroundColor(.black)
                                                    }
                                                    
                                                    Text(convertMemoDateFormat(input: memoData.firstCreateDt))
                                                        .foregroundColor(Color("color00000040"))
                                                        .font(.caption)
                                                }
                                                
                                                
                                                Text(memoData.content)
                                                    .foregroundColor(Color("color00000040"))
                                                    .font(.caption)
                                            }
                                            
                                            Spacer()
                                            
                                        }
                                        
                                        Divider()
                                            .padding(.leading, screenWidth/8)
                                    }
                                    
                                    
                                    
                                    //                                    for userList in allEmployeeCreatorList {
                                    //                                        if (userList.user.userIdx == memoData.user.userIdx) {
                                    //                                            let userImage = userList.userImage
                                    //
                                    //                                            print(userImage)
                                    //                                            break
                                    //                                        }
                                    //                                    }
                                    
                                    
                                    
                                    
                                    
                                }
                                
                                
                                
                                
                            } //VStack 계약 리스트
                            .listStyle(PlainListStyle())
                            .padding(.horizontal)
                            .padding(.top, screenHeight/50.9375)
                            
                            
                            if showShowAllButton{
                                
                                Button(action: {
                                    
                                    showShowAllButton = false
                                    showMemoList = true
                                    
                                }, label: {
                                    
                                    Text("더 보기")
                                        .foregroundColor(.black)
                                        .font(.system(size: 16))
                                        .bold()
                                        .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                                })
                                .background(.white)
                                .cornerRadius(4)
                                .padding()
                                .overlay(RoundedRectangle(cornerRadius: 4)
                                    .stroke(Color(.black), lineWidth: 1)
                                    .padding()
                                )
                                
                            }
                            
                            Spacer()
                            
                            
                            
                            
                        }//VStack
                        .frame(width: screenWidth/1.0932)
                        .background(Color.white)
                        .cornerRadius(20)
                        .shadow(color: Color("color00000040"), radius: 20, x: 0, y: 30)
                        .padding(.top, screenHeight/31.346)
                        
                        
                    } //VStack
                    .alert(isPresented: $showAlert, content: {
                        Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                            print("알림창 확인 버튼이 클릭되었다.")
                            showAlert = false
                            
                            alertTitle = ""
                            alertMessage = ""
                        }))
                    })
                    
                }//ScrollView
                
                
                
                
                
                
            }//ZStack
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading: Button(action: {
                
                
                
                self.presentationMode.wrappedValue.dismiss()
                
                
                
                detailSchedulDetailView.detailProjectScheduleInquiry(accessToken: loginData.token)
                
                detailSchedulDetailView.taskInquiry(accessToken: loginData.token)
                    
            
                
            }) {
                
                HStack(spacing: 0){
                    
                    Image(systemName: "chevron.backward")
                    
                }
                
                
                
            })
            .navigationBarTitle("\(selectedTaskName)", displayMode: .inline)
            
            
            
            ZStack{
                
                if isShowingMemo {
                    
                    ZStack{//메모 뷰
                            
                        VStack{
                            
                            Spacer()
                            
                            VStack{
                                Spacer()
                                
                                Text("메모 작성")
                                    .padding(.vertical, screenHeight/74.09)
                                    .padding(.top)
                                    .padding(.top)
                                    .bold()
                            }
                            .padding(.top)
                            
                            Divider()
                            
                            
                            
                            VStack{
                                //TODO 텍스트 배경 색 흰색으로 나옴
                                TextEditor(text: $writedMemo)
                                    .background(Color.clear)
                                    .foregroundColor(Color("hintTextColor"))
                                    .keyboardType(.default)
                                    .frame(width: screenWidth/1.2, height: screenHeight/8.15)
//                                    .background(Color("colorF8F8F892"))
                                    .autocapitalization(.none)
                                    
                                
//                                TextField(".", text: $writedMemo, prompt: Text("\(writedMemo)")
//                                    .foregroundColor(Color("hintTextColor")))
//                                    .keyboardType(.default)
//                                    .frame(width: screenWidth/1.2, height: screenHeight/8.15)
//                                    .autocapitalization(.none)
                                
                                Spacer()
                                
//                                TopAlignedTextField(text: $writedMemo)
//                                    .frame(width: screenWidth/1.2, height: screenHeight/8.15)
//                                    .autocapitalization(.none)
//                                    .foregroundColor(Color("hintTextColor"))
                                
                                
                            }
                            
                            
                            Spacer()
                            
                            Divider()
                            
                            VStack{
                                
                                HStack{
                                    Spacer()
                                    
                                    Button(action: {
                                        
                                        addTaskMemo()
                                        
                                        self.isShowingMemo = false
                                        
                                        allEmployeeCreatorInquiry()
                                        memoInquiry()
                                        taskInquiry()
                                        
                                        
                                        self.showShowAllButton = false
                                        self.showMemoList = true
                                        
                                        
                                    }, label: {
                                        
                                        Text("등록")
                                            .bold()
                                            .foregroundColor(.black)
                                            .padding(.bottom)
                                            
                                        
                                        Image(systemName: "chevron.right")
                                            .foregroundColor(.black)
                                            .padding(.bottom)
                                            
                                    })
                                    .padding(.trailing)
                                    .padding(.bottom)
                                    .padding(.bottom)
                                }
                                .padding(.bottom)
                                
                            }
                            
                            Spacer()
                            
                        }
                        .frame(width: screenWidth/1.0932, height: screenHeight/3.844)
                        .background(.white)
                        .cornerRadius(8.0)
                        
                        
                        //하단 네비게이션을 위한 패딩
                        HStack{
                            Rectangle()
                                .frame(width: 0, height: 0)
                                .foregroundColor(.white)
                        }
                        .padding(.bottom, screenHeight/28) //29
                        
                        
                    }//ZStack 메모
                        .transition(.move(edge: .leading))
                        .zIndex(2)
                    
                    // 메뉴 외부를 클릭하면 메뉴를 닫습니다.
                    Button(action: {
    //                    withAnimation {
                            self.isShowingMemo = false
    //                    }
                    }) {
                        Color.gray
                            .edgesIgnoringSafeArea(.all)
                            .opacity(0.9)
                    }
                    .zIndex(1)
                    
                }
            }
                
          
        }//ZStack
        .onAppear{
            allEmployeeCreatorInquiry()
            memoInquiry()
            taskInquiry()
        }
       
        
        
        
        
    }//body
    
    
    //과업 가져오는 api
    private func taskInquiry() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        //

        AF.request("\(defaultUrl)/admin/api/task?task_idx=\(selectedTaskIdx)&schedule_idx=\(selectedBrandScheduleIdx)&task_name=\(selectedTaskName)&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: TaskDataResponse.self) { response in
            switch response.result {
            case .success(let response):
                // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                
                for detailTaskData in response.data {
                    taskNameFor_DetailTask = detailTaskData.taskName
                    taskOneLineExplanFor_DetailTask = detailTaskData.oneLineExplain
                    taskStartDateFor_DetailTask = dateFormatter(getDate: detailTaskData.taskStartDt)
                    taskEndDateFor_DetailTask = dateFormatter(getDate: detailTaskData.taskEndDt)
                    
                    
                    taskStatusFor_DetailTask = detailTaskData.taskStatus
                    hideStatusFor_DetailTask = detailTaskData.hideOrNot
                    
                    
                }
                
                if (hideStatusFor_DetailTask == "o"){
                    hideStatusFor_DetailTask = "미처리"
                }else{
                    hideStatusFor_DetailTask = "숨김"
                }
                
                DispatchQueue.main.async {
                    print("-------------------------선택한 과업 호출 성공 \(response.data)")
                    
                    
//                    taskDataList = response.data
//
//                    taskDataListCount = response.pagination.total
//
                    
//
//                    if taskDataListCount > 3 {
//                        showShowAllButton = true //더 보기 버튼 보임
//                    }
                }
                
                
                
                
            case .failure(let error):
                // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                print(error)
            }
        }
    }
    
    //최종적으로 과업 수정
    private func patchEmployeeContract() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        
        let parameters: [String: Any] = [
            "task_name": taskNameFor_DetailTask,
              "one_line_explain": taskOneLineExplanFor_DetailTask,
              "task_start_dt": taskStartDateFor_DetailTask,
              "task_end_dt": taskEndDateFor_DetailTask,
              "task_status": taskStatusFor_DetailTask,
              "hide_or_not": hideStatusFor_DetailTask
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/task/\(selectedTaskIdx)",
                   method: .patch,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("과업 수정 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("과업 수정 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("과업 수정 실패: \(error)")
                
            }
        }
    }
    
    private func memoInquiry() {
       
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(loginData.token)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/task-memo?task_idx=\(selectedTaskIdx)&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: TaskMemoDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    //                brandIdx = brandData.brandIdx
                    
                    print("@@@@@@@@@@@@@@@@@@@@@메모 정보: \(response.data)")
                    
                    DispatchQueue.main.async {
                        memoDataList = response.data
     //
     //                   if(response.pagination.total > 3){
     //                       showShowAllButton = true
     //                   }
                        
                        if (response.pagination.total <= 3) { //
                            showShowAllButton = false
                            showMemoList = false
                        }else{
                            showShowAllButton = true //더 보기 버튼 보임
                            showMemoList = false
                        }
                        
                    }
                    
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
            
            
        })
        
       
    }
    
    
    private func addTaskMemo() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any] = [
            
            "task_idx": selectedTaskIdx,
              "content": writedMemo
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/task-memo",
                   method: .post,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("메모 등록 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("메모 등록 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("메모 등록 실패: \(error)")
                
            }
        }
    }
    
    
    //모든 직원/크리에이터 api
    private func allEmployeeCreatorInquiry() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(loginData.token)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/human-resource?page=1&limit=100", method: .get, headers: headers).responseDecodable(of: AllEmployeeCreatorDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
               
                    print("@@@@@@@@@@@@@@@@@@@@@유저정보 \(response.data)")
                    

                    DispatchQueue.main.async {
                        allEmployeeCreatorList = response.data
                    }
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
        })
    }
    
    
    private func dateFormatter(getDate: String) -> String {
        
        let isoDateFormatter = ISO8601DateFormatter()
        isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

        let getDate = getDate
        if let date = isoDateFormatter.date(from: getDate) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateString = dateFormatter.string(from: date)
//            print(dateString) //ex "2024.01.05" 출력
            return dateString
        } else {
            print("날짜 변환에 실패했습니다.")
            return getDate
        }

    }
    func convertDateToString(date: Date?) -> String {
        guard let date = date else {
            return ""
        }

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
    
    
    func convertMemoDateFormat(input: String) -> String {
        // 입력 형식의 DateFormatter
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        inputFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        // 입력 문자열을 Date 객체로 변환
        guard let date = inputFormatter.date(from: input) else {
            print("Date conversion failed due to wrong format.")
            return ""
        }
        
        // 출력 형식의 DateFormatter
        let outputFormatter = DateFormatter()
        outputFormatter.dateFormat = "yyyy.MM.dd HH:mm:ss"
        
        // Date 객체를 출력 형식의 문자열로 변환
        let resultString = outputFormatter.string(from: date)
        
        return resultString
    }

}

