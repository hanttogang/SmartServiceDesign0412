////
////  DetailedScheduleDetailsView.swift
////  SmartServiceDesign
////
////  Created by Teameverywhere on 12/19/23.
////
//
//import SwiftUI
//
//struct DetailedScheduleDetailsView: View {
//    
//    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
//    
//    let screenWidth = UIScreen.main.bounds.width
//    let screenHeight = UIScreen.main.bounds.height
//    
//    var titleProjectName: String
//    @State var schedulName: String = "2023년 3월 마케팅"
//    @State var scheculOneLineExplain: String = "2023년 3월 마케팅 관련 세부 일정"
//    @State var scheculUserCount: String = "12"
//    
//    var body: some View {
//        
//        
//        
//        
//        ZStack {
//            
//            VStack(spacing: 0){
//                
//                HStack{
//                    
//                    Text("\(schedulName)")
//                        .font(.title2)
//                    
//                    
//                    Spacer()
//                    
//                    Button(action: {
//                        
//                    }, label: {
//                        Text("수정")
//                        
//                    })
//                }
//                .padding(.horizontal)
//                .padding(.vertical, screenHeight / 81.5)
//                .padding(.top, screenHeight / 81.5)
//                
//                Divider()
//                    .padding(.leading)
//                
//                
//                HStack{
//                    Text("한줄 설명")
//                        .padding(.vertical, screenHeight / 81.5)
//                    
//                    Spacer()
//                    
//                    Text("\(scheculOneLineExplain)")
//                        .foregroundColor(Color("color00000040"))
//                    
//                    
//                    
//                }
//                .padding(.horizontal)
//                
//                
//                Divider()
//                    .padding(.leading)
//                
//                
//                HStack{
//                    Text("참여인원")
//                        .padding(.vertical, screenHeight / 81.5)
//                    
//                    Spacer()
//                    
//                    Text("\(scheculUserCount)명")
//                        .foregroundColor(Color("color00000040"))
//                    
//                }
//                .padding(.horizontal)
//                
//                
//                Divider()
//                    .padding(.leading)
//                
//                
//                
//                VStack {
//                    
//                    HStack{
//                        
//                        Text("과업 리스트")
//                            .font(.title2)
//                            .fontWeight(.bold)
//                        
//                        Spacer()
//                        
//                    }
//                    .padding()
//                    
//                    HStack{
//                        
//                        Button(action: {
//                            
//                        }, label: {
//                            NavigationLink {
//                                AddTaskView(titleProjectName: titleProjectName)
//                                
//                            } label: {
//                                
//                                Text("+  과업 추가")
//                                    .foregroundColor(.white)
//                                    .font(.system(size: 16))
//                                    .bold()
//                                    .frame(width: screenWidth/1.25, height: screenHeight/20.375)
//                            }
//                        })
//                        .background(Color("mainColor"))
//                        .cornerRadius(4)
//                        
//                        
//                    }
//                    
//                    
//                    List{
//                        
//                        NavigationLink(destination: DetailedTaskView(titleProjectName: titleProjectName)){
//                            
//                            
//                        }//NavigationLink
//                        .overlay{
//                            
//                            HStack{
//                                Image(systemName: "person")
//                                    .resizable()
//                                    .aspectRatio(contentMode: .fit)
//                                    .frame(width: screenWidth/9.375, height: screenHeight/20.375)
//                                
//                                
//                                VStack(alignment: .leading){
//                                    Text("유튜브 PPL 브랜디드")
//                                    
//                                    Text("2023.03.01 ~ 2023.03.31")
//                                        .foregroundColor(Color("color00000040"))
//                                        .font(.caption)
//                                    
//                                }
//                                
//                                Spacer()
//                                
//                                
//                                Text("진행")
//                                    .font(.footnote)
//                                    .foregroundColor(.white)
//                                    .frame(width: screenWidth/5.208, height: screenHeight/29.107)
//                                    .background(.blue)
//                                    .cornerRadius(16.0)
//                                    .padding(.trailing, -12)
//                                
//                                
//                            }
//                            
//                            
//                        }
//                        .padding(.vertical)
//                        
//                        
//                        
//                    }
//                    .listStyle(PlainListStyle())
//                    .padding(.trailing)
//                    
//                    
//                    
//                    
//                }
//                .frame(width: screenWidth/1.0932 , height: screenHeight/2.328)
//                .background(Color.white)
//                .cornerRadius(20)
//                .shadow(color: Color("color00000040"), radius: 20, x: 0, y: 30)
//                .padding(.top, screenHeight/31.346)
//                
//                
//                
//                Spacer()
//                
//                
//                
//                
//                
//            }//VStack
//            .navigationBarBackButtonHidden(true)
//            .navigationBarItems(leading: Button(action: {
//                
//                self.presentationMode.wrappedValue.dismiss()
//                
//                
//            }) {
//                HStack(spacing: 0){
//                    
//                    Image(systemName: "chevron.backward")
//                    
//                }
//            })
//            .navigationBarTitle("\(titleProjectName)", displayMode: .inline)
//            
//            
//            
//        }//ZStack
//        
//        
//        
//        
//        
//        //    .toast(isShowing: $showToast, text: Text(toastText))
//        
//        
//        
//        
//        
//        
//        
//        
//    }
//    
//}
//
//#Preview {
//    DetailedScheduleDetailsView(titleProjectName: "프로젝트 이름")
//}
