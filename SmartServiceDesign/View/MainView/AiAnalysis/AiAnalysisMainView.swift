//
//  AiAnalysisMainView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/26/24.
//

import SwiftUI
import Alamofire

struct AiAnalysisMainView: View {
    
    @EnvironmentObject var loginData: LoginData
    @EnvironmentObject var myWebVM: WebViewModel
    @EnvironmentObject var menuViewModel: MenuViewModel
    
    @State var isShowingMenu: Bool = false
    
    let defaultUrl = "\(ApiClient.BASE_URL)"
    
    @State private var isApiLoading: Bool = false
    
    @State private var analysisText: String = ""
    
    @State private var isAssessmentView: Bool = false
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    
    var body: some View {
        
        ZStack{
            
            
            
            if (isApiLoading){
                VStack{
                    
                    ProgressView()
                        .scaleEffect(2.0)
                    
                    Text("차트를 분석중입니다.")
                        .font(.title3)
                        .foregroundColor(.blue)
                        .padding(.top)
                    
                }
                .zIndex(3)
                
               
                
            }
            
            VStack{
                
                HStack(spacing: 0){
                    
                    Button(action: {
                        withAnimation {
                            isShowingMenu = true
                        }
                    }) {
                        
                        HStack(spacing: 0){
                            Image("img_menu")
                        }
                        .padding(.leading)
                        
                    }
                    
                    Spacer()
                }
                
                HStack(spacing: 0){
                    
                    Button(action: {
                        
                        isAssessmentView = false
                        
                    }, label: {
                        VStack(spacing: 0){
                            
                            Rectangle()
                                .frame(width: screenWidth/2, height: screenHeight/15)
                                .foregroundColor(.white)
                                .overlay{
                                    Text("차트보기")
                                }
                            
                            Rectangle()
                                .frame(width: screenWidth/2, height: screenHeight/250)
                                .foregroundColor(isAssessmentView ? .white : .blue)
                        }
                    })
                    .padding(0)
                    
                    Button(action: {
                        
                        isAssessmentView = true
                        
                    }, label: {
                        VStack(spacing: 0){
                            
                            Rectangle()
                                .frame(width: screenWidth/2, height: screenHeight/15)
                                .foregroundColor(.white)
                                .overlay{
                                    Text("평가보기")
                                }
                            
                            
                            Rectangle()
                                .frame(width: screenWidth/2, height: screenHeight/250)
                                .foregroundColor(isAssessmentView ? .blue : .white)
                        }
                    })
                    .padding(0)
                    
                    
                }
                .padding(0)
                
                if !isAssessmentView{
                    
                    VStack{
                        
                        
                        
                        MyWebView(urlToLoad: "https://smart-service-web.team-everywhere.com/ai?token=\(loginData.token)")
                            .onReceive(myWebVM.jsBridgeEvent, perform: { jsValue in
                                
                                
                                print("채팅 contentView - jsValue.message", jsValue.message)
                                //
                                isApiLoading = true
                                aiAnalysis(accessToken: loginData.token, value: jsValue.message)
                                
                            })
                            
                        
                        //하단 네비게이션을 위한 패딩
                        HStack{
                            Rectangle()
                                .frame(width: 0, height: 0)
                                .foregroundColor(.white)
                        }
                        .padding(.bottom, screenHeight/25.375) //32
                        
                    }//VStack
                }else{
                    VStack{
                        ScrollView{
                            Text(analysisText)
                                .foregroundColor(.black)
                                .padding()
                            
                            
                            //하단 네비게이션을 위한 패딩
                            HStack{
                                Rectangle()
                                    .frame(width: 0, height: 0)
                                    .foregroundColor(.white)
                            }
                            .padding(.bottom, screenHeight/25.375) //32
                        }
                        
                    }//VStack
                    
                }
                
            }
            
            
            
            CustomBottomNavigationBar()
            
            
            if isShowingMenu {
                
                MenuView()
                    .transition(.move(edge: .leading))
                    .zIndex(2)
                
                // 메뉴 외부를 클릭하면 메뉴를 닫습니다.
                Button(action: {
                    withAnimation {
                        isShowingMenu = false
                    }
                }) {
                    Color.gray
                        .edgesIgnoringSafeArea(.all)
                        .opacity(0.5)
                }
                .zIndex(1)
                
            }
        }//ZStack
        .background(.white)
        
        
    }
    
    //최종적으로 등록
    private func aiAnalysis(accessToken: String, value: String) {
        
        let jsonString = """
        \(value)
        """
        
        print("전달할 jsonString: \(jsonString)")
        
        if let data = jsonString.data(using: .utf8) {
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
                   let idx = json["idx"] as? [String: Any] {
                    
                    var result: [String: Any] = [:]
                    
                    let keys = ["top_brand_contracts", "top_marketing_contracts", "top_total_contracts", "paying_credit", "tax_purchase", "tax_sales", "bank_withdrawal", "bank_deposit"]
                    
                    for key in keys {
                        if let values = idx[key] as? [[String: Any]] {
                            var newValues: [[String: Any]] = []
                            for value in values {
                                var newValue: [String: Any] = [:]
                                newValue["contract_month"] = value["contract_month"]
                                newValue["brand_idx"] = value["brand_idx"]
                                newValue["contract_name"] = value["contract_name"]
                                newValue["total_price"] = Int(value["total_price"] as? String ?? "0")
                                newValue["brand"] = value["brand"]
                                newValue["biz_number"] = value["biz_number"]
                                newValue["corporation_name"] = value["corporation_name"]
                                newValue["total"] = value["total"]
                                newValues.append(newValue)
                            }
                            result[key] = newValues
                        }
                    }
                    
                    
                    let resultData = try JSONSerialization.data(withJSONObject: result, options: [.prettyPrinted])
                    let resultString = String(data: resultData, encoding: .utf8)!
                    print(resultString)
                    
                    
                    let headers: HTTPHeaders = [
                        "Authorization": "Bearer \(accessToken)",
                        "Accept": "application/json"
                    ]
                    let parameters = try? JSONSerialization.jsonObject(with: resultData, options: []) as? [String: Any]
                    
                    AF.request("\(defaultUrl)/admin/api/ai/analyze",
                               method: .post,
                               parameters: parameters,
                               encoding: JSONEncoding.default,
                               headers: headers)
                    .responseJSON { response in
                        switch response.result {
                            
                        case .success(let value):
                            
                            print("api 연결 성공: \(value)")
                            
                            guard let json = value as? [String: Any],
                                  let result = json["result"] as? Int else {
                                print("응답 형식이 올바르지 않습니다: \(value)")
                                return
                            }
                            
                            if result == 0 {
                                let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                                print("AI 분석 실패: \(errorMessage ?? "알 수 없는 오류")")
                            } else if result == 1 {
                                guard let json = value as? [String: Any],
                                      let result = json["result"] as? Bool,
                                      result,
                                      let data = json["data"] as? String else {
                                    print("Invalid JSON data.")
                                    return
                                }
                                
                                analysisText = data
                                
                                
                                print(analysisText)
                                
                                isApiLoading = false
                                isAssessmentView = true
                            }
                            
                            
                            
                            
                        case .failure(let error):
                            // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                            print("AI 분석 실패: \(error)")
                            
                            
                            isApiLoading = false
                            isAssessmentView = false
                            
                        }
                    }
                }
            } catch {
                print("JSON 파싱 또는 변환 에러: \(error)")
            }
        }
        
    }
    
    func convertToKorean(input: String) -> String {
        let transformed = input
            .replacingOccurrences(of: "\\U", with: "\\u")
            .unicodeScalars
            .compactMap { UnicodeScalar($0.value) }
            .map { Character($0) }
            .reduce(into: "") { $0.unicodeScalars.append($1.unicodeScalars.first!) }
        return transformed
    }
}


struct AIBridgeMessage: Decodable {
    let idx: String
    let title: String
//    let data: String
}
// JSON 데이터의 구조에 맞는 모델 생성
struct AiTextResponseData: Codable {
    let data: String
    let result: Int
}
