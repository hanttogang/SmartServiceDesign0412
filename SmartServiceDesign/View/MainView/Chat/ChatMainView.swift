//
//  ChatMainView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/26/24.
//

import SwiftUI


struct ChatMainView: View {
    
    
    @EnvironmentObject var loginData: LoginData
    @EnvironmentObject var myWebVM: WebViewModel
    @EnvironmentObject var menuViewModel: MenuViewModel
    
    @State var jsValue: JsValue?
    
    @State private var webViewTrigger: Bool = false
    
    @State var isShowingMenu: Bool = false
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State private var sendImageString: String = ""
    
    var body: some View {
        ZStack{
            VStack{
                
                //웹 페이지에서 보여질 상단 네비게이션 작성하기
                VStack(spacing: 0){
                    HStack(spacing: 0){
                        
                        if loginData.userType != "master"{
                            
                            HStack(spacing: 0){
                                Image("img_menu")
                            }
                            .padding(.leading)
                        }else{
                            Button(action: {
                                withAnimation {
                                    self.isShowingMenu = true
                                }
                            }) {
                                
                                HStack(spacing: 0){
                                    Image("img_menu")
                                }
                                .padding(.leading)
                                
                            }
                        }
                        
                        Spacer()
                    }

                    
                   
                    
                    VStack{
                        
                        if(sendImageString != ""){
                            MyWebView(urlToLoad: "https://smart-service-web.team-everywhere.com/chat/detail?img=\(sendImageString)")
                        }else{
                            if(webViewTrigger){
                                MyWebView(urlToLoad: "https://smart-service-web.team-everywhere.com/chat?token=\(loginData.token)")
                            }else{
                                MyWebView(urlToLoad: "https://smart-service-web.team-everywhere.com/chat?token=\(loginData.token)")
                            }
                        }
                        
                    }
//                    //onReceive : 이벤트를 받는다
//                    //myWebVM.jsAlertEvent 을 받고
//                    //perform 을 보내준다.
                    .onReceive(myWebVM.jsBridgeEvent, perform: { jsValue in
                        
                        
                        print("채팅 contentView - jsValue22222222", jsValue)
                        
                        let decoder = JSONDecoder()
                           if let data = jsValue.message.data(using: .utf8),
                              let message = try? decoder.decode(ChatAlertBridgeMessage.self, from: data) {
                               if message.idx == "back" {
                                   // 'idx'의 값이 'back'인 경우에만 실행할 코드를 이곳에 작성합니다.
                                   print("Back event received")
                                   
                                   sendImageString = ""
                                   
                                   webViewTrigger.toggle()
                                   
                                   
//                                   myWebVM.messageManagementMode = true
                                   
                               }else if message.idx == "alert" {
                                   // 'idx'의 값이 'alert'인 경우에만 실행할 코드를 이곳에 작성합니다.
                                   print("Alert event received")
                                   
                                   self.jsValue = jsValue
                               }else if message.title == "message" {
                                   
                                   
                                   let sms = "sms://\(message.idx)"
                                   
                                   guard let url = URL(string: sms) else { return }
                                   UIApplication.shared.open(url)
                                   
                               }else if message.title == "call" {
                                   
                                   let telephone = "telprompt://\(message.idx)"
                                   
                                   guard let url = URL(string: telephone) else { return }
                                   UIApplication.shared.open(url)
                                   
                               }else if message.title == "project" {
                                   
                                   
                                   sendImageString = ""
                                   
                                   myWebVM.chatRoomIdx = Int(message.idx)!
                                   
                                   print("chatRoomIdx: \(myWebVM.chatRoomIdx)")
                                   
                                   myWebVM.captureMode.send(true)
                                   myWebVM.captureModeBoolean = true
                                   
                                   print("call project")
                               }
                               
                               
                               
                           }
                        
                    })
                    .onReceive(myWebVM.captureImageString, perform: { captureImageString in
                        sendImageString = captureImageString
                    })
                    .alert(item: $jsValue, content: { alert in
                        createAlert(alert)
                    })
                    
                    
                    
                    
                    
                    
                    
                    //하단 네비게이션을 위한 패딩
                    HStack{
                        Rectangle()
                            .frame(width: 0, height: 0)
                            .foregroundColor(.white)
                    }
                    .padding(.bottom, screenHeight/10.15) //80
                        
                    
                }
                
            }//VStack
            
            CustomBottomNavigationBar()
            
            // isShowingMenu 상태에 따라 메뉴 여닫기 zIndex 으로 조정
            if isShowingMenu {
                
                MenuView()
                    .transition(.move(edge: .leading))
                    .zIndex(2)
                
                // 메뉴 외부를 클릭하면 메뉴를 닫습니다.
                Button(action: {
                    withAnimation {
                        self.isShowingMenu = false
                    }
                }) {
                    Color.gray
                        .edgesIgnoringSafeArea(.all)
                        .opacity(0.5)
                }
                .zIndex(1)
                
            }
            
            
        }//ZStack
        .edgesIgnoringSafeArea(.bottom)
        .onAppear{
            menuViewModel.selectView.send("message")
        }
    }
}



extension ChatMainView {
    
    func createAlert(_ alert: JsValue) -> Alert {
//        Alert(title: Text(alert.type.description), message: Text(alert.message), dismissButton: .default(Text("확인"), action: {
        Alert(title: Text(alert.type.description), message: Text("이미 존재하는 채팅방입니다."), dismissButton: .default(Text("확인"), action: {
            print("알림창 확인 버튼이 클릭되었다.")
        }))
    }
//
//    //Text 입력 얼럿 창
//    func createTextAlert() -> MyTextAlertView {
//        MyTextAlertView(textString: $textString, showAlert: $shouldShowAlert, title: "iOS -> Js 보내기", message: "")
//    }
}



struct ChatAlertBridgeMessage: Decodable {
    let idx: String
    let title: String
//    let data: String
}


//
//#Preview {
//    ChatMainView()
//}
