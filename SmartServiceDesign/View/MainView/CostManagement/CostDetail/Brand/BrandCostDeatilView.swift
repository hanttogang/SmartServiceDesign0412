//
//  BrandCostDetailView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 3/5/24.
//

import SwiftUI
import Alamofire

struct BrandCostDetailView: View {
    
    //Api
    var brandContractIdx: Int
    var brandContactName: String
    
    // 이 init 메소드를 통해 외부에서 selectedCreatorContractIdx 값을 설정할 수 있습니다.
    init(brandContractIdx: Int, brandContactName: String) {
        self.brandContractIdx = brandContractIdx
        self.brandContactName = brandContactName
    }
    
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var detailSendSalaryImageString = ""
    
    @State private var isApiLoading: Bool = false
    
    //View
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State private var navigate: Bool = false
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var showToast: Bool = false
    @State private var toastText: String = "시작 날짜 선택 후 확인을 눌러주세요"
    
    @State private var modificationMode: Bool = false
    
    
    @State private var detailBrandContractName: String = ""
    @State private var detailBrandContractPrice: String = ""
    @State private var detailBrandAllCost: String = ""
    @State private var detailBrandPersonnelCost: String = ""
    @State private var detailBrandOtherCost: String = ""
    
    @State private var brandCostDataList = [BrandCostData]()
    @State private var brandCostDataListCount: Int = 0
    
    @State private var showAllContractList: Bool = false
    @State private var showShowAllButton: Bool = false
    
    
    
    
    @State private var contractStartDateFor_DetailEmployeeContract: String = ""
    @State private var contractEndDateFor_DetailEmployeeContract: String = ""
    @State private var detailSendSalaryPrice: String = ""
    
    @State private var detailSendSalaryMemo: String = ""
    
    @State private var detailSendSalaryStatus: String = ""
    
    @State private var selectDetailSendSalaryStatus: Bool = false
    
    @State private var showingEvidenceImagePicker = false
    @State private var selectedEvidenceImage: UIImage? = nil
    
    //날짜 관련
    @State private var showingDatePicker = false
    @State private var dateSelection = Date()
//    @State private var startDate: Date? = nil
//    @State private var endDate: Date? = nil
    @State private var dateCount = 0
  
    
    var body: some View {
        
        ZStack {
            
            if (isApiLoading){
                
                ProgressView()
                    .zIndex(3)
                
            }
            
            ScrollView(.vertical){
                VStack(spacing: 0){
                    
                    
                    HStack{
                        
                        //계약명
                        Text("\(brandContactName)")
                            .font(.title2)
                        
                      
                        
                        Spacer()
                        
                        
                    }
                    .padding(.horizontal)
                    .padding(.vertical, screenHeight / 81.5)
                    .padding(.top, screenHeight / 81.5)
                    
                    Divider()
                        .padding(.leading)
                   

                    
                    HStack{ //계약금액
                        Text("계약금액")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                    
                        
                        Text("\(detailBrandContractPrice)")
                            .foregroundColor(Color("color00000040"))
                        
                        Text("원")
                            .foregroundColor(Color("color00000040"))
                    
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{ //원가총액
                        Text("원가총액")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                    
                        
                        Text("\(detailBrandAllCost)")
                            .foregroundColor(Color("color00000040"))
                        
                        Text("원")
                            .foregroundColor(Color("color00000040"))
                    
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{ //인건비용
                        Text("인건비용")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                    
                        
                        Text("\(detailBrandPersonnelCost)")
                            .foregroundColor(Color("color00000040"))
                        
                        Text("원")
                            .foregroundColor(Color("color00000040"))
                    
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{ //기타비용
                        Text("기타비용")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                    
                        
                        Text("\(detailBrandOtherCost)")
                            .foregroundColor(Color("color00000040"))
                        
                        Text("원")
                            .foregroundColor(Color("color00000040"))
                    
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    
                    
                    
                    VStack(spacing: 0) {
                        HStack{
                            
                            Text("원가 등록")
                                .font(.title2)
                                .bold()
                                .padding()
                            //                                .padding(.top, -screenHeight/13.5)
                            Spacer()
                        }
                        .padding(0)
                        
                        Button(action: {
                            print("원가 추가 버튼 클릭됨")
                            
                            self.navigate = true
                            
                        }, label: {
                            
                            NavigationLink(destination: AddBrandCostView(brandContractIdx: brandContractIdx, brandContactName: brandContactName), isActive: $navigate) {
                                EmptyView()
                            }
                            
                            Text("+  원가 내역 추가")
                                .foregroundColor(.white)
                                .font(.system(size: 16))
                                .bold()
                                .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        
                        
                        VStack{//계약 리스트 터치시
                            
                            ForEach(brandCostDataList.prefix(showAllContractList ? brandCostDataList.count : 3), id: \.brandCostIdx) { brandCostData in
                                
                               
                                    
                                    VStack{
                                        HStack{ //리스트항목의 모델
                                            
                                            VStack(alignment: .leading){
                                                Text("\(brandCostData.costName)")
                                                    .foregroundColor(.black)
                                                    .bold()
                                                
                                                Text("금액: \(brandCostData.costAmount)")
                                                    .foregroundColor(Color("color00000040"))
                                                    .font(.caption)
                                            }
                                            
                                            Spacer()
                                            
                                            
                                            Text("\(brandCostData.processingStatus)")
                                                .foregroundColor(.white)
                                                .bold()
                                                .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                                .background(Color("mainColor"))
                                                .cornerRadius(16.0)
                                            
                                            
                                        }
                                        .padding(.horizontal)
                                        
                                        Divider()
                                            .padding(.horizontal)
                                        
                                    }
                                
                            }
                            
                            
                            
                            
                        } //VStack 계약 리스트
                        .listStyle(PlainListStyle())
                        .padding(.horizontal)
                        .padding(.top, screenHeight/50.9375)
                        
                        
                        if showShowAllButton{
                            
                            Button(action: {
                                
                                showShowAllButton = false
                                showAllContractList = true
                                
                            }, label: {
                                
                                Text("더 보기")
                                    .foregroundColor(.black)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                            })
                            .background(.white)
                            .cornerRadius(4)
                            .padding()
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color(.black), lineWidth: 1)
                                .padding()
                            )
                            
                        }
                        
                        
                        
                        Spacer()
                        
                    }//VStack
                    .frame(width: screenWidth/1.0932)
                    .background(Color.white)
                    .cornerRadius(20)
                    .shadow(color: Color("color00000040"), radius: 20, x: 0, y: 30)
                    .padding(.top, screenHeight/31.346)
                    
                    //                    .sheet(isPresented: $showingBankRecordCopyImagePickerForDetailBrand, onDismiss: loadContractImageForDetailBrandContractList) {
                    //                        ImagePicker(selectedImage: $selectedBankRecordCopyImageForDetailBrand)
                    //                    }
                    
                    
                    
                    
                } //VStack
            }//ScrollView
            //하단 네비게이션을 위한 패딩
            HStack{
                Rectangle()
                    .frame(width: 0, height: 0)
                    .foregroundColor(.white)
            }
            .padding(.bottom, screenHeight/28) //29
            
            
            
        }//ZStack
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            
            self.presentationMode.wrappedValue.dismiss()
            
        }) {
            HStack(spacing: 0){
                Image(systemName: "chevron.backward")
            }
        })
        .navigationBarTitle("\(brandContactName)", displayMode: .inline)
        .onAppear {
            
            costInfo(accessToken: loginData.token)
            
            
            brandCostListInquiry(accessToken: loginData.token)
            
        }
        
        
      
        
        
        
    }//body

  
    //계약 가져오는 api
    func costInfo(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/brand-cost/summary/\(brandContractIdx)", method: .get, headers: headers).responseDecodable(of: CostDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    detailBrandContractPrice = String(response.contractPrice)
                    detailBrandAllCost = String(response.totalCost)
                    detailBrandPersonnelCost = String(response.costsByType["인건비"]!)
                    detailBrandOtherCost = String(response.costsByType["기타비용"]!)
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
            
        })
        
    }
    
    
    //계약 가져오는 api
    func brandCostListInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/brand-cost?brand_contract_idx=\(brandContractIdx)&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: BrandCostDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    
                    DispatchQueue.main.async {
                        brandCostDataList = response.data
                        
                        brandCostDataListCount = response.pagination.total
                        
                        print("계약 리스트 호출 성공 \(response.data)")
                        
                        if (brandCostDataListCount <= 3) { //
                            showShowAllButton = false
                            showAllContractList = false
                        }else{
                            showShowAllButton = true //더 보기 버튼 보임
                            showAllContractList = false
                        }
                    }
                    
                    
                    
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
            
        })
        
    }
    
    
    
    
    private func loadEvidenceImage() {
        guard let selectedEvidenceImage = selectedEvidenceImage else { return }
        
        isApiLoading = true
        
        uploadImage(image: selectedEvidenceImage, imageType: "human_resource_contract")
    }
    
    //Api 를통해 필요한 값 호출 (url, key 값)
    private func getApiCommonSendSalaryImageURL(imageType: String, completion: @escaping (String?, String?) -> Void) {
        
        let headers: HTTPHeaders = [
            "accept": "application/json",
            "Authorization": "Bearer \(loginData.token)"
        ]
        
        let parameters: Parameters = [
            "mimetype": "image/png",
            "type": "user",
            "extension": "png"
        ]
        
        AF.request("\(defaultUrl)/api/common/image/url", method: .get, parameters: parameters, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    let url = JSON["data"] as? String
                    let key = JSON["key"] as? String
                    completion(url, key)
                }
            case .failure(let error):
                print(error)
                completion(nil, nil)
            }
        }
    }
    
    
    //사진 선택시 uploadImag 함수를 호출
    private func uploadImage(image: UIImage, imageType: String) {
        getApiCommonSendSalaryImageURL(imageType: imageType) { (url, key) in
            
            guard let preSignedUrl = url else {
                print("Failed to get pre-signed URL.")
                return
            }
            
            guard let imageData = image.pngData() else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: url!, method: .put).response { response in
                switch response.result {
                case .success:
                    print("Image uploaded successfully.")
                    
                    isApiLoading = false
                case .failure(let error):
                    print("Failed to upload image: \(error)")
                }
            }
            
            
            guard let imageData = image.jpegData(compressionQuality: 1.0) else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: preSignedUrl).response { response in
                if let error = response.error {
                    print("Image upload failed with error: \(error)")
                } else {
                    print("Image uploaded successfully.")
                    switch imageType {
                    case "human_resource_contract":
                        detailSendSalaryImageString = key!
                        
                        print("\(detailSendSalaryImageString)")
                        
                    default:
                        print("Unknown image type: \(imageType)")
                    }
                }
            }
        }
    }
    
    
    
    
    
    private func dateFormatter(getDate: String) -> String {
        
        let isoDateFormatter = ISO8601DateFormatter()
        isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

        let getDate = getDate
        if let date = isoDateFormatter.date(from: getDate) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateString = dateFormatter.string(from: date)
//            print(dateString) //ex "2024.01.05" 출력
            return dateString
        } else {
            print("날짜 변환에 실패했습니다.")
            return getDate
        }

    }
    
    
    
    
    
    
    func convertDateToString(date: Date?) -> String {
        guard let date = date else {
            return ""
        }

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
    
    func convertDateStringToStringFormat(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        guard let date = dateFormatter.date(from: dateString) else {
            print("Invalid date string")
            return ""
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    
}
