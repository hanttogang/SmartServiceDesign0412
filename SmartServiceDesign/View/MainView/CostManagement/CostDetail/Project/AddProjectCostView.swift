//
//  AddProjectCostView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 3/5/24.
//

import SwiftUI
import Alamofire

enum ProjectCostType: CaseIterable {
    case personal
    case other
}

enum ProjectProcessingStatus: CaseIterable {
    case stand
    case complete
}

struct AddProjectCostView: View {
    
    //Api
    var projectIdx: Int
    var projectContactName: String
    
    var projectCostDetailView: ProjectCostDetailView
    
    init(projectIdx: Int, projectContactName: String) {
        self.projectIdx = projectIdx
        self.projectContactName = projectContactName
        
        self.projectCostDetailView = ProjectCostDetailView(projectIdx: projectIdx, projectContactName: projectContactName)
    }
    
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var isApiLoading: Bool = false
    
    //View
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var projectCostName: String = ""
    @State private var projectCostString: String = ""
    @State private var projectCostMonth: String = ""
    
    @State private var projectCostType: ProjectCostType = ProjectCostType.personal
    @State private var projectCostTypeString: String = "인건비"
    
    @State private var projectProcessingStatus: ProjectProcessingStatus = ProjectProcessingStatus.stand
    @State private var projectStatusString: String = "대기"
    
    @State private var navigate: Bool = false
    
    var body: some View {
        
        
        ZStack{
            
            if (isApiLoading){
                
                ProgressView()
                    .zIndex(3)
                
            }
            
            ScrollView{
                
                VStack(spacing: 0){
                    HStack(spacing: 0) { //원가 관리 리스트 종류 선택
                        
                        Button(action: {
                            
                            projectCostType = .personal
                            projectCostTypeString = "인건비"
                        }) {
                            Text("인건비")
                                .frame(width: screenWidth/2.272, height: screenHeight/23.97)
                                .font(.system(size: 16))
                            
                            
                        }
                        .background(projectCostType == .personal ? Color.blue : Color.clear)
                        .foregroundColor(projectCostType == .personal ? .white : .blue)
                        .cornerRadius(4)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("mainColor"), lineWidth: 1)
                        )
                        
                        Button(action: {
                            
                            projectCostType = .other
                            projectCostTypeString = "기타비용"
                        }) {
                            Text("기타비용")
                                .frame(width: screenWidth/2.272, height: screenHeight/23.97)
                                .font(.system(size: 16))
                            
                            
                        }
                        .background(projectCostType == .other ? Color.blue : Color.clear)
                        .foregroundColor(projectCostType == .other ? .white : .blue)
                        .cornerRadius(4)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("mainColor"), lineWidth: 1)
                        )
                        
                        
                    }//HStack
                    .overlay(RoundedRectangle(cornerRadius: 4)
                        .stroke(Color("mainColor"), lineWidth: 1)
                    )
                    .padding(.top)
                    
                    
                    VStack{//비용명
                        
                        HStack(spacing: 0){
                            Text("비용명")
                                .font(.system(size: 14))
                            
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $projectCostName, prompt: Text("비용명을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 비용명
                    .padding(.top, screenHeight / 25.46875) //32
                    
                    
                    VStack{//비용액
                        
                        HStack(spacing: 0){
                            Text("비용액")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $projectCostString, prompt: Text("이체금액을 입력해주세요")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.phonePad)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이체금액
                    .padding(.top, screenHeight / 54.133) //15
                   
                    
                    VStack{//기준월
                        
                        HStack(spacing: 0){
                            Text("기준월")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $projectCostMonth, prompt: Text("기준 월을 입력해주세요")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.phonePad)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이체금액
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    
                     
                    
                    VStack{ //이체상태
                        
                        HStack(spacing: 0){
                            Text("이체상태")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        
                        HStack(spacing: screenWidth / 125 ){ //SelectedBtn
                            
                            Button(action: {
                                
                                projectProcessingStatus = .stand
                                projectStatusString = "대기"
                                print("\(projectProcessingStatus)")
                                
                            }, label: {
                                Text("대기")
                                    .foregroundColor(projectProcessingStatus == .stand ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(projectProcessingStatus == .stand ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            Button(action: {
                                
                                projectProcessingStatus = .complete
                                projectStatusString = "완료"
                                print("\(projectProcessingStatus)")
                                
                            }, label: {
                                Text("완료")
                                    .foregroundColor(projectProcessingStatus == .complete ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(projectProcessingStatus == .complete ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            
                            Spacer()
                            
                            
                        }//HStack (SelectedBtn 을 위한 HStack)
                        .padding(.leading, screenWidth / 15.625)
                        
                    } //VStack 이체상태
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    Spacer()
                    
                    HStack{
                        
                        Button(action: {
                            
                            if (!isApiLoading){
                                
                                if(projectCostName == "" || projectCostString == "" || projectCostMonth == ""){
                                    
                                    showAlert = true
                                    
                                    alertTitle = "급여이체 내역 추가 실패"
                                    alertMessage = "칸을 비울 수 없습니다."
                                    
                                    
                                }else{
                                    
                                    if( 1 <= Int(projectCostMonth)! && Int(projectCostMonth)! <= 31){
                                        
                                        addProjectCost(accessToken: loginData.token)
                                        
                                        self.presentationMode.wrappedValue.dismiss()
                                        
                                        projectCostDetailView.costInfo(accessToken: loginData.token)
                                        
                                    }else{
                                        showAlert = true
                                        
                                        alertTitle = "급여 등록 실패"
                                        alertMessage = "급여일을 확인해주세요."
                                    }
                                    
                                }
                            }
                            
                            
                            
                        }, label: {
                            
                                Text("원가 내역 추가")
                                    .foregroundColor(.white)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/67.9166) // 37
                        
                        
                        
                    }
                    
                    //하단 네비게이션을 위한 패딩
                    HStack{
                        Rectangle()
                            .frame(width: 0, height: 0)
                            .foregroundColor(.white)
                    }
                    .padding(.bottom, screenHeight/8.826)
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    
                    self.presentationMode.wrappedValue.dismiss()
                    
//                    detailEmployeeView.contractInquiry(accessToken: loginData.token)
                    
                }) {
                    
                    HStack(spacing: 0){
                        
                        Image(systemName: "chevron.backward")
                        
                    }
                    
                    
                    
                })
                .navigationBarTitle("\(projectContactName)", displayMode: .inline)
                
                
            }//ScrollView
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
        }//ZStack
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        
    }//body
    
    
    
    
//    최종적으로 등록
    private func addProjectCost(accessToken: String) {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any] = [
            
            "project_idx": projectIdx,
            "cost_type": "\(projectCostTypeString)",
            "cost_name": "\(projectCostName)",
            "cost_amount": Int(projectCostString)!,
            "base_month": "2023-01",//일단 임시 설정
            "processing_status": "\(projectStatusString)"
            
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/project-cost",
                   method: .post,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("ProjectCost 등록 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("ProjectCost 등록 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    self.navigate = true
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("ProjectCost 등록 실패: \(error)")
                
            }
        }
    }
    
    func convertDateToString(date: Date?) -> String {
        guard let date = date else {
            return ""
        }

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
    
}
