//
//  CostMainView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 3/5/24.
//

import SwiftUI
import Alamofire


enum CostListEnum: CaseIterable {
    case project
    case brand
    case marketing
}

struct CostMainView: View {
    
    
    //Api 관련
    @State private var hideSelectCostList: Bool = false
    
    @State private var selectedCostList: CostListEnum = .project
    @State private var selectedCostListType: String = "project"
    
    //리스트 관련 ----------------
    @State private var projectList = [AdvertisingProjectData]()
    
    @State private var brandDataList = [BrandData]()
    @State private var statusBarndContractDataList: Bool = false
    @State private var selectedBrandDataListIdx: Int = 0
    @State private var brandContractDataList = [BrandContractData]()
    
    
    
    @State private var marketingDataList = [MarketingData]()
    @State private var statusMarketingContractDataList: Bool = false
    @State private var selectedMarketingDataListIdx: Int = 0
    @State private var marketingContractDataList = [ChannelContractData]()
    
    
    //리스트 관련 ----------------
    
    
    //검색어 관련
    
    @State private var searchText: String = ""
    @State private var editText: Bool = false
    
    
    @EnvironmentObject var loginData: LoginData
    @EnvironmentObject var menuViewModel: MenuViewModel
    
    @State var isShowingMenu: Bool = false
    
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    init() {
        let appearance = UINavigationBarAppearance()
        //                appearance.configureWithOpaqueBackground()
        //                appearance.backgroundColor = UIColor(.white) // 원하는 색상으로 변경하세요.
        appearance.shadowColor = nil // 이 부분을 추가하면 네비게이션 바 하단 선이 사라집니다.
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        
    }
    
    @State private var navigate: Bool = false
    
    var body: some View {
        
        ZStack{
            
            NavigationView{
                
                
                VStack{
                    
                    HStack(spacing: 0){
                        
                        Button(action: {
                            withAnimation {
                                isShowingMenu = true
                            }
                        }) {
                            
                            HStack(spacing: 0){
                                Image("img_menu")
                            }
                            .padding(.leading)
                            
                        }
                        
                        Spacer()
                    }
                    
                    
                    HStack{
                        
                        Text("원가 관리")
                            .font(.title)
                            .bold()
                        
                        
                        Spacer()
                        
                    }
                    .padding(.leading, 16)
                    
                    
                    
                    HStack{ //검색창
                        
                        HStack{
                            
                        }
                        .frame(width: screenWidth, height: screenHeight/14.5)
                        .overlay{
                            //검색창을 받을수있는 택스트필드
                            TextField("Search" , text : self.$searchText)
                                .autocapitalization(.none)
                                .foregroundColor(Color("hintTextColor"))
                                .padding(10)
                                .padding(.leading, screenWidth/12.5) //30
                                .background(Color(.white))
                                .cornerRadius(15)
                                .overlay(
                                    HStack{
                                        Image(systemName: "magnifyingglass")
                                            .foregroundColor(Color("hintTextColor"))
                                            .padding()
                                        
                                        
                                        
                                        Spacer()
                                        
                                        if self.editText{
                                            Button(action : {
                                                self.editText = false
                                                self.searchText = ""
                                                //키보드에서 입력을 끝내게하는 코드
                                                UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                                            }){
                                                Image(systemName: "multiply.circle.fill")
                                                    .foregroundColor(Color("hintTextColor"))
                                                    .padding()
                                            }
                                        }
                                        
                                    }
                                ).onTapGesture {
                                    self.editText = true
                                }
                                .padding(.horizontal, 21.945) //37
                            
                            
                            
                        }//overlay
                        
                    }//HStack
                    .background(Color("colorF8F8F892"))
                    
                    
                   
                    
                    if( !hideSelectCostList ){
                        HStack(spacing: 0) { //원가 관리 리스트 종류 선택
                            
                            Button(action: {
                                
                                selectedCostList = .project
                                selectedCostListType = "project"
                            }) {
                                Text("프로젝트")
                                    .frame(width: screenWidth/3.26, height: screenHeight/23.97)
                                    .font(.system(size: 16))
                                
                                
                            }
                            .background(selectedCostList == .project ? Color.blue : Color.clear)
                            .foregroundColor(selectedCostList == .project ? .white : .blue)
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("mainColor"), lineWidth: 1)
                            )
                            
                            Button(action: {
                                
                                selectedCostList = .brand
                                selectedCostListType = "brand"
                            }) {
                                Text("고객/브랜드")
                                    .frame(width: screenWidth/3.26, height: screenHeight/23.97)
                                    .font(.system(size: 16))
                                
                                
                            }
                            .background(selectedCostList == .brand ? Color.blue : Color.clear)
                            .foregroundColor(selectedCostList == .brand ? .white : .blue)
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("mainColor"), lineWidth: 1)
                            )
                            
                            Button(action: {
                                
                                selectedCostList = .marketing
                                selectedCostListType = "marketing"
                            }) {
                                Text("마케팅채널")
                                    .frame(width: screenWidth/3.26, height: screenHeight/23.97)
                                    .font(.system(size: 16))
                                
                                
                            }
                            .background(selectedCostList == .marketing ? Color.blue : Color.clear)
                            .foregroundColor(selectedCostList == .marketing ? .white : .blue)
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("mainColor"), lineWidth: 1)
                            )
                            
                            
                        }//HStack
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("mainColor"), lineWidth: 1)
                        )
                        .padding(.top)
                    }
                    
                    
                    if(selectedCostListType == "project"){
                        
                        ScrollView {
                            VStack(spacing: 0) {
                                ForEach(projectList.filter { searchText.isEmpty || $0.projectName.contains(searchText)}, id: \.projectIdx) { projectData in
                                    
                                    // brandDataList에서 projectData의 brandIdx와 일치하는 brand 찾기
                                    let matchingBrand = brandDataList.first { $0.brandIdx == projectData.brandIdx }
                                    
                                    NavigationLink(destination: ProjectCostDetailView(projectIdx: projectData.projectIdx, projectContactName: projectData.projectName)){
                                                   VStack {
                                                       HStack {
                                                           AsyncImage(url: URL(string: imageS3Url + "/\(matchingBrand!.brandImage)")) { image in
                                                               image.resizable()
                                                                   .aspectRatio(contentMode: .fill)
                                                                   .frame(width: screenWidth/6.25, height: screenHeight/13.583)
                                                                   .cornerRadius(12.0)
                                                           } placeholder: {
                                                               ProgressView()
                                                                   .frame(width: screenWidth/6.25, height: screenHeight/13.583)
                                                           }

                                                           VStack(alignment: .leading){
                                                               Text(matchingBrand!.brandName)
                                                                   .font(.title2)
                                                                   .bold()
                                                                   .foregroundColor(.black)

                                                               Text(projectData.projectName)
                                                                   .foregroundColor(.black)

                                                               Text("참여인원: \(projectData.humanCount)")
                                                                   .foregroundColor(.gray)
                                                                   .font(.caption)
                                                           }

                                                           Spacer()

                                                           Image(systemName: "chevron.right")
                                                               .padding()
                                                               .foregroundColor(.gray)
                                                       }
                                                       .padding(.bottom, screenWidth/17.857)
                                                       .padding(.top, screenWidth/15.625)

                                                       Divider()
                                                   }//VStack
                                                   .background(Color.white)
                                                   .padding(.leading)
                                               }//NavigationLink
                                }
                            }
                        }.onAppear {
                            brandInquiry(accessToken: loginData.token)
                            projectInquiry(accessToken: loginData.token)
                        }
                        
                    } else if(selectedCostListType == "brand"){
                    
                        
                        
                        if !statusBarndContractDataList{
                            
                            ScrollView {
                                VStack(spacing: 0) {
                                    ForEach(brandDataList.filter { searchText.isEmpty || $0.brandName.contains(searchText) || $0.buisnessType.contains(searchText) }, id: \.brandIdx) { brandData in
                                        //                                    NavigationLink(destination: DetailCustomerBrandView(selectedCustomerBrandIdx: brandData.brandIdx, selectedBrandName: brandData.brandName)){
                                        
                                        Button {
                                            print("브랜드 버튼")
                                            selectedBrandDataListIdx = brandData.brandIdx
                                            
                                            self.statusBarndContractDataList = true
                                            
                                            hideSelectCostList = true
                                            
                                        } label: {
                                            
                                            VStack {
                                                HStack {
                                                    AsyncImage(url: URL(string: imageS3Url + "/" + brandData.brandImage)) { image in
                                                        image.resizable()
                                                            .aspectRatio(contentMode: .fill)
                                                            .frame(width: screenWidth/6.25, height: screenHeight/13.583)
                                                            .cornerRadius(12.0)
                                                    } placeholder: {
                                                        ProgressView()
                                                            .frame(width: screenWidth/6.25, height: screenHeight/13.583)
                                                    }
                                                    
                                                    VStack(alignment: .leading){
                                                        Text(brandData.brandName)
                                                            .font(.title2)
                                                            .bold()
                                                            .foregroundColor(.black)
                                                        
                                                        Text(brandData.buisnessType)
                                                            .foregroundColor(.black)
                                                        
                                                        Text("등록일: \(dateFormatter(getDate: brandData.firstCreateDt))")
                                                            .foregroundColor(.gray)
                                                            .font(.caption)
                                                    }
                                                    
                                                    Spacer()
                                                    
                                                    Image(systemName: "chevron.right")
                                                        .padding()
                                                        .foregroundColor(.gray)
                                                }
                                                .padding(.bottom, screenWidth/17.857)
                                                .padding(.top, screenWidth/15.625)
                                                
                                                Divider()
                                                
                                                
                                                
                                                
                                            }//VStack
                                            .background(Color.white)
                                            .padding(.leading)
                                        }
                                        
                                        
                                        //                                    }//NavigationLink
                                    } //ForEach
                                    
                                    
                                    
                                    
                                }//VStack
                            }.onAppear {
                                brandInquiry(accessToken: loginData.token)
                            }
                        }else{ //statusBarndContractDataList == true
                            HStack{
                                Button {
                                    self.statusBarndContractDataList = false
                                    
                                    hideSelectCostList = false
                                } label: {
                                    Image(systemName: "chevron.backward")
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: screenWidth/27, height: screenHeight/54.3)
                                        .padding()
                                }
                                
                                Spacer()
                            }
                            
                            ScrollView {
                                VStack(spacing: 0) {
                                    ForEach(brandContractDataList.filter { searchText.isEmpty || $0.contractName.contains(searchText)}, id: \.brandContractIdx) { brandContractData in
                                        
                                        let totalCostForThisData = brandContractData.brandCost.reduce(0) { $0 + $1.costAmount }
                                        NavigationLink(destination: BrandCostDetailView(brandContractIdx: brandContractData.brandContractIdx, brandContactName: brandContractData.contractName)){
                                            
                                            
                                            VStack {
                                                HStack {
                                                    AsyncImage(url: URL(string: imageS3Url + "/" + brandContractData.contractImage)) { image in
                                                        image.resizable()
                                                            .aspectRatio(contentMode: .fill)
                                                            .frame(width: screenWidth/6.25, height: screenHeight/13.583)
                                                            .cornerRadius(12.0)
                                                    } placeholder: {
                                                        ProgressView()
                                                            .frame(width: screenWidth/6.25, height: screenHeight/13.583)
                                                    }
                                                    
                                                    VStack(alignment: .leading){
                                                        Text(brandContractData.contractName)
                                                            .font(.title2)
                                                            .bold()
                                                            .foregroundColor(.black)
                                                        
                                                        Text("계약 총액: \(brandContractData.contractPrice)")
                                                            .foregroundColor(.black)
                                                        
                                                        Text("원가 총액: \(totalCostForThisData)")
                                                            .foregroundColor(.gray)
                                                            .font(.caption)
                                                    }
                                                    
                                                    Spacer()
                                                    
                                                    Image(systemName: "chevron.right")
                                                        .padding()
                                                        .foregroundColor(.gray)
                                                }
                                                .padding(.bottom, screenWidth/17.857)
                                                .padding(.top, screenWidth/15.625)
                                                
                                                Divider()
                                                
                                                
                                                
                                                
                                            }//VStack
                                            .background(Color.white)
                                            .padding(.leading)
                                            
                                            
                                        }//NavigationLink
                                    } //ForEach
                                    
                                    
                                    
                                    
                                }//VStack
                            }.onAppear {
                                brandContractInquiry(accessToken: loginData.token)
                            }
                            
                            
                            
                        } //statusBarndContractDataList 관련 else 문 끝
                        
                        
                        
                    } else if(selectedCostListType == "marketing"){
                        
                        if !statusMarketingContractDataList{
                            
                            ScrollView {
                                VStack(spacing: 0) {
                                    ForEach(marketingDataList.filter { searchText.isEmpty || $0.channelName.contains(searchText) || $0.manageType.contains(searchText) }, id: \.marketingIdx) { marketingData in
    //                                    NavigationLink(destination: DetailMarketingChannelView(selectedMarketingIdx: marketingData.marketingIdx)){
                                        
                                        Button {
                                            print("마케팅 리스트 버튼")
                                            
                                            self.statusMarketingContractDataList = true
                                            
                                            hideSelectCostList = true
                                        } label: {
                                            VStack {
                                                HStack {
                                                    AsyncImage(url: URL(string: imageS3Url + "/" + marketingData.representativeImage)) { image in
                                                        image.resizable()
                                                            .aspectRatio(contentMode: .fill)
                                                            .frame(width: screenWidth/6.25, height: screenHeight/13.583)
                                                            .cornerRadius(12.0)
                                                    } placeholder: {
                                                        ProgressView()
                                                            .frame(width: screenWidth/6.25, height: screenHeight/13.583)
                                                    }
                                                    
                                                    VStack(alignment: .leading){
                                                        Text(marketingData.channelName)
                                                            .font(.title2)
                                                            .bold()
                                                            .foregroundColor(.black)
                                                        
                                                        Text(marketingData.manageType)
                                                            .foregroundColor(.black)
                                                        
                                                        Text("등록일: \(dateFormatter(getDate: marketingData.firstCreateDt))")
                                                            .foregroundColor(.gray)
                                                            .font(.caption)
                                                    }
                                                    
                                                    Spacer()
                                                    
                                                    Image(systemName: "chevron.right")
                                                        .padding()
                                                        .foregroundColor(.gray)
                                                }
                                                .padding(.bottom, screenWidth/17.857)
                                                .padding(.top, screenWidth/15.625)
                                                
                                                Divider()
                                                
                                                
                                                
                                                
                                            }//VStack
                                            .background(Color.white)
                                            .padding(.leading)
                                        }

                                           
    //                                    }//NavigationLink
                                    }
                                }
                            }
                            .onAppear {
                                marketingInquiry(accessToken: loginData.token)
                            }
                            
                        }else{
                            
                            HStack{
                                Button {
                                    self.statusMarketingContractDataList = false
                                    hideSelectCostList = false
                                } label: {
                                    Image(systemName: "chevron.backward")
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: screenWidth/27, height: screenHeight/54.3)
                                        .padding()
                                }
                                
                                Spacer()
                                
                                
                                
                            }
                            
                            ScrollView {
                                VStack(spacing: 0) {
                                    ForEach(marketingContractDataList.filter { searchText.isEmpty || $0.contractName.contains(searchText)}, id: \.marketingContractIdx) { marketingContractData in
                                        
                                        let totalMarketingCostForThisData = marketingContractData.marketingCost.reduce(0) { $0 + $1.costAmount }
                                        NavigationLink(destination: MarketingCostDetailView(marketingContractIdx: marketingContractData.marketingContractIdx, marketingContactName: marketingContractData.contractName)){
                                            
                                            
                                            VStack {
                                                HStack {
                                                    AsyncImage(url: URL(string: imageS3Url + "/" + marketingContractData.contractImage)) { image in
                                                        image.resizable()
                                                            .aspectRatio(contentMode: .fill)
                                                            .frame(width: screenWidth/6.25, height: screenHeight/13.583)
                                                            .cornerRadius(12.0)
                                                    } placeholder: {
                                                        ProgressView()
                                                            .frame(width: screenWidth/6.25, height: screenHeight/13.583)
                                                    }
                                                    
                                                    VStack(alignment: .leading){
                                                        Text(marketingContractData.contractName)
                                                            .font(.title2)
                                                            .bold()
                                                            .foregroundColor(.black)
                                                        
                                                        Text("계약 총액: \(marketingContractData.contractPrice)")
                                                            .foregroundColor(.black)
                                                        
                                                        Text("원가 총액: \(totalMarketingCostForThisData)")
                                                            .foregroundColor(.gray)
                                                            .font(.caption)
                                                    }
                                                    
                                                    Spacer()
                                                    
                                                    Image(systemName: "chevron.right")
                                                        .padding()
                                                        .foregroundColor(.gray)
                                                }
                                                .padding(.bottom, screenWidth/17.857)
                                                .padding(.top, screenWidth/15.625)
                                                
                                                Divider()
                                                
                                                
                                                
                                                
                                            }//VStack
                                            .background(Color.white)
                                            .padding(.leading)
                                            
                                            
                                        }//NavigationLink
                                    } //ForEach
                                    
                                    
                                    
                                    
                                }//VStack
                            }.onAppear {
                                marketingContractInquiry(accessToken: loginData.token)
                            }
                            
                            
                        }
                       
                    }
                    
                    

                    
                    //하단 네비게이션을 위한 패딩
                    HStack{
                        Rectangle()
                            .frame(width: 0, height: 0)
                            .foregroundColor(.white)
                    }
                    .padding(.bottom, screenHeight/28) //29
                    
                    
                    Spacer()
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                
                
                
            }//NavigationView
            //    .toast(isShowing: $showToast, text: Text(toastText))
            .navigationBarHidden(true)
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
            
            //하단 네비게이션 및 채팅 웹뷰
            CustomBottomNavigationBar()

            // isShowingMenu 상태에 따라 메뉴 여닫기 zIndex 으로 조정
            if isShowingMenu {
                
                MenuView()
                    .transition(.move(edge: .leading))
                    .zIndex(2)
                
                // 메뉴 외부를 클릭하면 메뉴를 닫습니다.
                Button(action: {
                    withAnimation {
                        isShowingMenu = false
                    }
                }) {
                    Color.gray
                        .edgesIgnoringSafeArea(.all)
                        .opacity(0.5)
                }
                .zIndex(1)
                
            }
            
        }//ZStack
        
    } //body
    
    //프로젝트 Api -------------------------------------------------------
    
    
    func projectInquiry(accessToken: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            print("@@@@@@@@@@@@@@@@@@@@@@projectInquiry called@@@@@@@@@@@@@@@@@@@@@@@@@")
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/project?page=1&limit=100", method: .get, headers: headers).responseDecodable(of: ProjectDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    
                    print("프로젝트 정보: \(response.data)")
                    
    //                brandIdx = brandData.brandIdx
                    DispatchQueue.main.async {
                        projectList = response.data
                    }
                    

                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
        })
        
    }
    
    //프로젝트 Api -------------------------------------------------------
    
    //브랜드 api---------------------------------------------------------
    // * 프로젝트 에도 사용됨
    func brandInquiry(accessToken: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            print("@@@@@@@@@@@@@@@@@@@@@@brandInquiry called@@@@@@@@@@@@@@@@@@@@@@@@@")
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/brand?user_idx=\(loginData.userIdx)&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: BrandDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    //                brandIdx = brandData.brandIdx
                    
                    print("브랜드 정보: \(response.data)")
                    
                    DispatchQueue.main.async {
                        brandDataList = response.data
                    }
                    
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
        })
        
    }
    
    //계약 가져오는 api
    func brandContractInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/brand-contract?brand_idx=\(selectedBrandDataListIdx)&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: BrandContractDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    
                    DispatchQueue.main.async {
                        brandContractDataList = response.data
                        
                        print("계약 리스트 호출 성공 \(response.data)")
  
                    }
                    
                    
                    
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
        })
        
    }
    //브랜드 api---------------------------------------------------------
    
    //마케팅 api---------------------------------------------------------
    func marketingInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/marketing", method: .get, headers: headers).responseDecodable(of: MarketingDataResponse.self) { response in
                
                switch response.result {
                    
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    //                    for brandData in response.data {
                    //                         // 브랜드 이름을 출력합니다.
    //                                        print("브랜드 번호: \(brandData.brandIdx)")
                    //                        print("브랜드 사진: \(brandData.brandImage)")
                    //                        print("브랜드 이름: \(brandData.brandName)")
                    //                        print("비즈니스 타입: \(brandData.buisnessType)")
                    //                        print("등록일: \(brandData.firstCreateDt)")
                    //
                    //                    }
                    
                    print(response.data)
                    
    //                brandIdx = brandData.brandIdx
                    DispatchQueue.main.async {
                        marketingDataList = response.data
                    }
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
            
        })
        
        
    }
    
    //계약 가져오는 api
    func marketingContractInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/marketing-contract?marketing_idx=\(selectedMarketingDataListIdx)", method: .get, headers: headers).responseDecodable(of: ChannelContractDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    
                    DispatchQueue.main.async {
                        marketingContractDataList = response.data
                        
                        print("계약 리스트 호출 성공 \(response.data)")
                        
                    }
                    
                    
                    
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
            
        })
        
    }
    //마케팅 api---------------------------------------------------------
    
    private func dateFormatter(getDate: String) -> String {
        
        let isoDateFormatter = ISO8601DateFormatter()
        isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

        let getDate = getDate
        if let date = isoDateFormatter.date(from: getDate) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy.MM.dd"
            let dateString = dateFormatter.string(from: date)
            return dateString
        } else {
            print("날짜 변환에 실패했습니다.")
            return ""
        }

    }
    
//    
//    func getDestinationView() -> AnyView {
//        if salaryManagementEmployeeIdx != nil {
//            if existEmployeeSalaryInfo {
//                //사원 선택 및 급여 정보 있는 경우
//                return AnyView(DetailUserSalaryView(selectedUserIdx: salaryManagementEmployeeIdx!, selectedUserType: userTypeText))
//            } else {
//                //사원 선택 및 급여 정보 없는 경우
//                return AnyView(AddSalaryOfUserView(selectedUserIdx: salaryManagementEmployeeIdx!, selectedUserType: userTypeText))
//            }
//        } else if salaryManagementCreatorIdx != nil {
//            if existCreatorSalaryInfo {
//                //크리에이터 선택 및 급여 정보 있는 경우
//                return AnyView(DetailUserSalaryView(selectedUserIdx: salaryManagementCreatorIdx!, selectedUserType: userTypeText))
//            } else {
//                //크리에이터 선택 및 급여 정보 없는 경우
//                return AnyView(AddSalaryOfUserView(selectedUserIdx: salaryManagementCreatorIdx!, selectedUserType: userTypeText))
//            }
//        }
//        return AnyView(EmptyView()) // 기본 반환 값
//    }
    
}

