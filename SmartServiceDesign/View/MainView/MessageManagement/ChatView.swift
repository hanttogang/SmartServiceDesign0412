//
//  ChatView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/24/24.
//

import SwiftUI
import Foundation
import Starscream
import UIKit
import Alamofire

struct ChatView: View {
    @ObservedObject var chatManager: ChatManager

    @State private var newMessage: String = ""

    var body: some View {
        VStack {
            ScrollView {
                ForEach(chatManager.messages, id: \.self) { message in
                    
                    Text(message)
                        .foregroundStyle(.black)
                    
                }
            }

            HStack {
                TextField("New message...", text: $newMessage)
                    .textFieldStyle(RoundedBorderTextFieldStyle())

                Button(action: {
                    self.chatManager.sendChatMessage(self.newMessage)
                    self.newMessage = ""
                }) {
                    Text("Send")
                }
            }
            .padding()
        }
    }
}
//
//
//
//
//
//#Preview {
//    ChatView()
//}
