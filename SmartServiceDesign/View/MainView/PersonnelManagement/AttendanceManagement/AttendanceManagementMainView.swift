//
//  AttendanceManagementMainView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/21/24.
//

import SwiftUI
import Alamofire

struct AttendanceManagementMainView: View {
    
    @EnvironmentObject var loginData: LoginData
    @EnvironmentObject var menuViewModel: MenuViewModel
    
    @State var isShowingMenu: Bool = false
    
    //Api 관련
    @State private var workDataList = [WorkDataForAdmin]()
    
    @State private var creatorList = [CreatorData]()

    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    
    init() {
        let appearance = UINavigationBarAppearance()
        //                appearance.configureWithOpaqueBackground()
        //                appearance.backgroundColor = UIColor(.white) // 원하는 색상으로 변경하세요.
        appearance.shadowColor = nil // 이 부분을 추가하면 네비게이션 바 하단 선이 사라집니다.
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        
    }
    
    @State private var searchEmployeeAndCreatorText: String = ""
    @State private var editEmployeeAndCreatorText: Bool = false
    @State private var selectedCreatorIdx: Int = 0
    
    @State private var userTypeText: String = "employee"
    
//    @State private var selectedUserType: UserTypeInSalaryManagementMainView = UserTypeInSalaryManagementMainView.employee
    
    @State private var endWorkTime = ""
    
    @State private var userWorking: Bool = false
    @State private var leaveWork: Bool = false
    
    
    @State private var startWorkTime = ""
    
    @State private var workingTime: String = ""
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    var body: some View {
        
        if loginData.userType == "master"{
            ZStack{
                
                NavigationView{
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            
                            Button(action: {
                                withAnimation {
                                    isShowingMenu = true
                                }
                            }) {
                                
                                HStack(spacing: 0){
                                    Image("img_menu")
                                }
                                .padding(.leading)
                                
                            }
                            
                            Spacer()
                        }
                        
                        
                        HStack{
                            
                            Text("근태 관리")
                                .font(.title)
                                .bold()
                                
                            
                            
                            Spacer()
                            
                        }
                        
                        .padding(.leading, 16)
                        
                        
                        
                        VStack{
                            
                            VStack{
                                
                                HStack{
                                    
                                    Text("출퇴근 관리")
                                        .padding()
                                        .font(.title3)
                                        .bold()
                                    
                                    Spacer()
                                }
                            
                                VStack{
                                    
                                    ZStack{
                                        Text("출근")
                                            .foregroundColor(.white)
                                            .font(.system(size: 16))
                                            .bold()
                                            .frame(width: screenWidth/1.2, height: screenHeight/18.45)
                                        
                                    }
                                    .background(Color("mainColor"))
                                    .cornerRadius(4)
                                    .padding(.top)
                                    
                                        
                                    ZStack{
                                        Text(leaveWork ? "퇴근 \(endWorkTime)" : "퇴근")
                                                .foregroundColor(.white)
                                                .font(.system(size: 16))
                                                .bold()
                                                .frame(width: screenWidth/1.2, height: screenHeight/18.45)
                                    }
                                    .background(Color("mainColor"))
                                    .cornerRadius(4)
                                    .padding(.horizontal)
                                    
                                    
                                    
                                    
                                    ZStack{
                                        HStack{
                                            Image(systemName: "clock.badge")
                                                .foregroundColor(Color("hintTextColor"))
                                            Text("근무시간")
                                                .foregroundColor(.black)
                                         
                                            Spacer()
                                            
                                        }
                                        .padding()
                                        
                                    }
                                    
                                    
                                    
                                    
                                    
                                }
                                .frame(width: screenWidth/1.1)
                                .background(.white)
                                .cornerRadius(4)
                                
                            }//VStack 출퇴근 관리
                            .frame(width: screenWidth)
                            
                            
                            HStack{
                                
                                Text("당일 근무 현황")
                                    .padding()
                                    .font(.title3)
                                    .bold()
                                
                                Spacer()
                            }
                            
                            ScrollView(showsIndicators: false){
                                VStack{
                                    
                                    ForEach(workDataList, id: \.userIdx) { workDataList in
                                        NavigationLink(destination: DetailAttendanceManagementView(selectedUserIdxForAttendance: workDataList.userIdx, selectedUserNameForAttendance: workDataList.user.userName)){
                                                VStack{
                                                    
                                                    HStack{
                                                        Text("\(workDataList.user.userName)")
                                                            .padding()
                                                            .font(.callout)
                                                            .bold()
                                                            .foregroundColor(.black)
                                                        
                                                        Spacer()
                                                    }
                                                    .padding(.bottom, 0)
                                                    
                                                    HStack{
                                                        
                                                        VStack{//출퇴근 여부
                                                            
                                                            HStack{
                                                                Text("출근")
                                                                    .font(.callout)
                                                                    .foregroundColor(.black)
                                                                Spacer()
                                                            }
                                                            HStack{ //출퇴근 여부
                                                                Text("퇴근")
                                                                    .font(.callout)
                                                                    .foregroundColor(.black)
                                                                Spacer()
                                                                
                                                            }
                                                        }
                                                        
                                                        
                                                        VStack{
                                                            
                                                            HStack{ //출근 시간
                                                                Spacer()
                                                                
                                                                Text("\(convertUTCtoLocalTimeHHMM(utcDateStr: workDataList.startWorkDt))")
                                                                    .font(.callout)
                                                                    .foregroundColor(.black)
                                                             
                                                            }
                                                            
                                                            HStack{ //퇴근 시간
                                                                Spacer()
                                                                
                                                                if workDataList.endWorkDt != nil{
                                                                    
                                                                    Text("\(convertUTCtoLocalTimeHHMM(utcDateStr: workDataList.endWorkDt!))")
                                                                        .font(.callout)
                                                                        .foregroundColor(.black)
                                                                    
                                                                }else{
                                                                    
                                                                    Text("--:--")
                                                                        .font(.callout)
                                                                        .foregroundColor(.black)
                                                                }
                                                                
                                                             
                                                            }
                                                        }
                                                        
                                                        VStack{
                                                            //띄우기 위한 투명 뷰
                                                        }
                                                        .padding()
                                                        
                                                        
                                                        VStack{
                                                            
                                                            HStack{ //근태구분
                                                                Text("근무시간")
                                                                    .font(.callout)
                                                                    .foregroundColor(.black)
                                                                Spacer()
                                                            }
                                                            
                                                            HStack{ //퇴근
                                                                Text("근태구분")
                                                                    .font(.callout)
                                                                    .foregroundColor(.black)
                                                                Spacer()
                                                            }
                                                            
                                                            
                                                        }
                                                        
                                                        
                                                        VStack{
                                                            
                                                            HStack{ //근무 시간
                                                                if workDataList.workingTime != nil{
                                                                    Text("\(workDataList.workingTime!)")
                                                                        .font(.callout)
                                                                        .foregroundColor(.black)
                                                                }else{
                                                                    Text("--:--")
                                                                        .font(.callout)
                                                                        .foregroundColor(.black)
                                                                }
                                                                
                                                                
                                                            }
                                                            HStack{ //근태 사각형
                                                                Spacer()
                                                                
                                                                Rectangle()
                                                                    .frame(width: screenWidth/12.5, height: screenHeight/81.2)
                                                                    .foregroundColor(.blue)
                                                                    .cornerRadius(12.0)
                                                                    .padding(.leading)
                                                                    .padding(.leading)
                                                            }
                                                        }
                                                        
                                                        
                                                    }//HStack
                                                    .padding(.horizontal)
                                                    .padding(.bottom)
                                                    
                                                    
                                                }//VStack
                                                .frame(width: screenWidth/1.1)
                                                .background(.white)
                                                .cornerRadius(4)
                                            }//NavigationLink
                                        }
                                }//VStack
                            }//ScrollView
                            
                            
                            Spacer()
                            
                        }//VStack
                        .frame(width: screenWidth)
                        .background(Color("colorEFEFF4"))
                        
                        
                        
                        //하단 네비게이션을 위한 패딩
                        HStack{
                            Rectangle()
                                .frame(width: 0, height: 0)
                                .foregroundColor(.white)
                        }
                        .padding(.bottom, screenHeight/28) //29
                        
                        
                        Spacer()
                        
                    }//VStack
                    .navigationBarBackButtonHidden(true)
                    
                }//NavigationView
                .navigationBarHidden(true)
                .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                    UIApplication.shared.endEditing()
                    
                }
                
                //하단 네비게이션 및 채팅 웹뷰
                CustomBottomNavigationBar()

                // menuViewModel.sideMenuView 상태에 따라 메뉴 여닫기 zIndex 으로 조정
                if isShowingMenu {
                    
                    MenuView()
                        .transition(.move(edge: .leading))
                        .zIndex(2)
                    
                    // 메뉴 외부를 클릭하면 메뉴를 닫습니다.
                    Button(action: {
                        withAnimation {
                            isShowingMenu = false
                        }
                    }) {
                        Color.gray
                            .edgesIgnoringSafeArea(.all)
                            .opacity(0.5)
                    }
                    .zIndex(1)
                    
                }
                
            }//ZStack
            .onAppear{
                workInquiryForAdmin(accessToken: loginData.token)
            }
        }else{
            
            ZStack{
                
                NavigationView{
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            
                            Button(action: {
                                withAnimation {
                                    isShowingMenu = true
                                }
                            }) {
                                
                                HStack(spacing: 0){
                                    Image("img_menu")
                                }
                                .padding(.leading)
                                
                            }
                            
                            Spacer()
                        }
                        
                        
                        HStack{
                            
                            Text("근태 관리")
                                .font(.title)
                                .bold()
                                
                            
                            
                            Spacer()
                            
                        }
                        
                        .padding(.leading, 16)
                        
                        
                        
                        VStack{
                            
                            VStack{
                                
                                HStack{
                                    
                                    Text("출퇴근 관리")
                                        .padding()
                                        .font(.title3)
                                        .bold()
                                    
                                    Spacer()
                                }
                            
                                VStack{
                                   
                                    if userWorking{// userWorking == true 유저가 출근했으면 버튼 x
                                        ZStack{
                                            Text("출근 \(startWorkTime)")
                                                    .foregroundColor(.white)
                                                    .font(.system(size: 16))
                                                    .bold()
                                                    .frame(width: screenWidth/1.2, height: screenHeight/18.45)
                                            
                                        }
                                        .background(Color("colorDEE3FF"))
                                        
                                        .cornerRadius(4)
                                        .padding(.top)
                                            
                                        
                                    }else if !userWorking{ //출근하지 않은 경우 버튼 클릭 가능
                                        Button(action: {
                                            
                                            self.userWorking = true
                                            
                                            let dateFormatter = DateFormatter()
                                            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                            dateFormatter.timeZone = TimeZone(abbreviation: "KST")
                                            
                                            startWorkTime = dateFormatter.string(from: Date())
                                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                                                startWork(accessToken: loginData.token)
                                            })
                                            
                                        }, label: {
                                            
                                            Text(userWorking ? "출근 \(startWorkTime)" : "출근")
                                                    .foregroundColor(.white)
                                                    .font(.system(size: 16))
                                                    .bold()
                                                    .frame(width: screenWidth/1.2, height: screenHeight/18.45)
                                            
                                        })
                                        .background(Color("mainColor"))
                                        .cornerRadius(4)
                                        .padding(.top)
                                        
                                    }
                                    
                                    if leaveWork{ //퇴근상태면 퇴근 버튼 비활성화 되고, 시간이 나타남
                                        ZStack{
                                            Text("퇴근 \(endWorkTime)")
                                                    .foregroundColor(.white)
                                                    .font(.system(size: 16))
                                                    .bold()
                                                    .frame(width: screenWidth/1.2, height: screenHeight/18.45)
                                        }
                                        .background(Color("colorDEE3FF"))
                                        .cornerRadius(4)
                                        .padding(.horizontal)
                                    }else{ //퇴근 전이면 퇴근 버튼 활성화되어있음
                                        Button(action: {
                                            
                                            if userWorking {
                                                
                                                self.leaveWork = true
                                                
                                                let dateFormatter = DateFormatter()
                                                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                                dateFormatter.timeZone = TimeZone(abbreviation: "KST")
                                                
                                                endWorkTime = dateFormatter.string(from: Date())
                                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                                                    endWork(accessToken: loginData.token)
                                                })
                                            }
                                            
                                            
                                            
                                        }, label: {
                                            Text("퇴근")
                                                .foregroundColor(.white)
                                                .font(.system(size: 16))
                                                .bold()
                                                .frame(width: screenWidth/1.2, height: screenHeight/18.45)
                                            
                                        })
                                        .background(.blue)
                                        .cornerRadius(4)
                                        .padding(.horizontal)
                                    }
                                    
                                    
                                    
                                    
                                    ZStack{
                                        HStack{
                                            Image(systemName: "clock.badge")
                                                .foregroundColor(Color("hintTextColor"))
                                            Text("근무시간")
                                                .foregroundColor(.black)
                                         
                                            Spacer()
                                            
                                            Text(workingTime)
                                        }
                                        .padding()
                                        
                                    }
                                    
                                    
                                    
                                    
                                    
                                }
                                .frame(width: screenWidth/1.1)
                                .background(.white)
                                .cornerRadius(4)
                                
                            }//VStack 출퇴근 관리
                            .frame(width: screenWidth)
                            
                            
                            HStack{
                                
                                Text("근무 현황")
                                    .padding()
                                    .font(.title3)
                                    .bold()
                                
                                Spacer()
                            }
                            
                            ScrollView(showsIndicators: false){
                                VStack{
                                    
                                    ForEach(workDataList, id: \.userIdx) { workDataList in
                                            
                                                VStack{
                                                    
                                                    HStack{
                                                        Text("\(convertUTCtoLocalTimeYYYYMMDD(utcDateStr: workDataList.startWorkDt))")
                                                            .padding()
                                                            .font(.callout)
                                                            .bold()
                                                            .foregroundColor(.black)
                                                        
                                                        Spacer()
                                                    }
                                                    .padding(.bottom, 0)
                                                    
                                                    HStack{
                                                        
                                                        VStack{//출퇴근 여부
                                                            
                                                            HStack{
                                                                Text("출근")
                                                                    .font(.callout)
                                                                    .foregroundColor(.black)
                                                                Spacer()
                                                            }
                                                            HStack{ //출퇴근 여부
                                                                Text("퇴근")
                                                                    .font(.callout)
                                                                    .foregroundColor(.black)
                                                                Spacer()
                                                                
                                                            }
                                                        }
                                                        
                                                        
                                                        VStack{
                                                            
                                                            HStack{ //출근 시간
                                                                Spacer()
                                                                
                                                                Text("\(convertUTCtoLocalTimeHHMM(utcDateStr: workDataList.startWorkDt))")
                                                                    .font(.callout)
                                                                    .foregroundColor(.black)
                                                             
                                                            }
                                                            
                                                            HStack{ //퇴근 시간
                                                                Spacer()
                                                                
                                                                if workDataList.endWorkDt != nil{
                                                                    
                                                                    Text("\(convertUTCtoLocalTimeHHMM(utcDateStr: workDataList.endWorkDt!))")
                                                                        .font(.callout)
                                                                        .foregroundColor(.black)
                                                                }else{
                                                                    
                                                                    Text("--:--")
                                                                        .font(.callout)
                                                                        .foregroundColor(.black)
                                                                }
                                                                
                                                             
                                                            }
                                                        }
                                                        
                                                        VStack{
                                                            //띄우기 위한 투명 뷰
                                                        }
                                                        .padding()
                                                        
                                                        
                                                        VStack{
                                                            
                                                            HStack{ //근태구분
                                                                Text("근무시간")
                                                                    .font(.callout)
                                                                    .foregroundColor(.black)
                                                                Spacer()
                                                            }
                                                            
                                                            HStack{ //퇴근
                                                                Text("근태구분")
                                                                    .font(.callout)
                                                                    .foregroundColor(.black)
                                                                Spacer()
                                                            }
                                                            
                                                            
                                                        }
                                                        
                                                        
                                                        VStack{
                                                            
                                                            HStack{ //근무 시간
                                                                if workDataList.workingTime != nil{
                                                                    Text("\(workDataList.workingTime!)")
                                                                        .font(.callout)
                                                                        .foregroundColor(.black)
                                                                }else{
                                                                    Text("--:--")
                                                                        .font(.callout)
                                                                        .foregroundColor(.black)
                                                                }
                                                            }
                                                            HStack{ //근태 사각형
                                                                Spacer()
                                                                
                                                                Rectangle()
                                                                    .frame(width: screenWidth/12.5, height: screenHeight/81.2)
                                                                    .foregroundColor(.blue)
                                                                    .cornerRadius(12.0)
                                                                    .padding(.leading)
                                                                    .padding(.leading)
                                                            }
                                                        }
                                                        
                                                        
                                                    }//HStack
                                                    .padding(.horizontal)
                                                    .padding(.bottom)
                                                    
                                                    
                                                }//VStack
                                                .frame(width: screenWidth/1.1)
                                                .background(.white)
                                                .cornerRadius(4)
                                            
                                        }
                                }//VStack
                            }//ScrollView
                            
                            
                            Spacer()
                            
                        }//VStack
                        .frame(width: screenWidth)
                        .background(Color("colorEFEFF4"))
                        
                        
                        
                        //하단 네비게이션을 위한 패딩
                        HStack{
                            Rectangle()
                                .frame(width: 0, height: 0)
                                .foregroundColor(.white)
                        }
                        .padding(.bottom, screenHeight/28) //29
                        
                        
                        Spacer()
                        
                    }//VStack
                    .navigationBarBackButtonHidden(true)
                    
                }//NavigationView
                .navigationBarHidden(true)
                .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                    UIApplication.shared.endEditing()
                    
                }
                
                //하단 네비게이션 및 채팅 웹뷰
                CustomBottomNavigationBar()

                // menuViewModel.sideMenuView 상태에 따라 메뉴 여닫기 zIndex 으로 조정
                if isShowingMenu {
                    
                    MenuView()
                        .transition(.move(edge: .leading))
                        .zIndex(2)
                    
                    // 메뉴 외부를 클릭하면 메뉴를 닫습니다.
                    Button(action: {
                        withAnimation {
                            isShowingMenu = false
                        }
                    }) {
                        Color.gray
                            .edgesIgnoringSafeArea(.all)
                            .opacity(0.5)
                    }
                    .zIndex(1)
                    
                }
                
            }//ZStack
            .onAppear{
                userWorkInquiry(accessToken: loginData.token)
                recordsUserWorkInquiry(accessToken: loginData.token)
            }
            
        }
        
        
        
    } //body
    //출근
    private func startWork(accessToken: String) {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any?] = [
            "type" : "출근"
        ]
        
        
        AF.request("\(defaultUrl)/api/work",
                   method: .post,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("출근 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("출근 성공: \(value)")
                    
                    recordsUserWorkInquiry(accessToken: accessToken)
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("출근 실패: \(error)")
                
            }
        }
    }
    
    //퇴근
    private func endWork(accessToken: String) {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any?] = [
            "type" : "퇴근"
        ]
        
        
        AF.request("\(defaultUrl)/api/work",
                   method: .post,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("퇴근 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("퇴근 성공: \(value)")
                    
                    recordsUserWorkInquiry(accessToken: accessToken)
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("퇴근 실패: \(error)")
                
            }
        }
    }
    
    
    func convertUTCtoLocalTimeHHMM(utcDateStr: String, format: String = "HH:mm") -> String {
        let dateFormatter = DateFormatter()

        // Input Format
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "KST")

        // Convert String to Date
        guard let date = dateFormatter.date(from: utcDateStr) else {
            print("Date conversion failed due to wrong format.")
            return ""
        }

        // Output Format
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current

        // Convert Date to String
        let localTimeStr = dateFormatter.string(from: date)

        return localTimeStr
    }
    
    func convertUTCtoLocalTimeYYYYMMDD(utcDateStr: String, format: String = "yyyy-MM-dd") -> String {
        let dateFormatter = DateFormatter()

        // Input Format
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "KST")

        // Convert String to Date
        guard let date = dateFormatter.date(from: utcDateStr) else {
            print("Date conversion failed due to wrong format.")
            return ""
        }

        // Output Format
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current

        // Convert Date to String
        let localTimeStr = dateFormatter.string(from: date)

        return localTimeStr
    }
    
    
    func convertUTCtoLocalTimeYYYYMMDDHHmmss(utcDateStr: String, format: String = "yyyy-MM-dd HH:mm:ss") -> String {
        let dateFormatter = DateFormatter()

        // Input Format
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "KST")

        // Convert String to Date
        guard let date = dateFormatter.date(from: utcDateStr) else {
            print("Date conversion failed due to wrong format.")
            return ""
        }

        // Output Format
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current

        // Convert Date to String
        let localTimeStr = dateFormatter.string(from: date)

        return localTimeStr
    }
    
    

    func workInquiryForAdmin(accessToken: String) {
        
        
        let currentDate = Date()
        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "KST")
        let dateString = dateFormatter.string(from: currentDate)
        
        
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json"
        ]
        
        AF.request("\(defaultUrl)/admin/api/work/employee/records?work_dt=\(dateString)", method: .get, headers: headers).responseDecodable(of: AttendanceManageDataResponse.self) { response in
            switch response.result {
            case .success(let response):
                // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                
                print(response.data)
                
                
                DispatchQueue.main.async {
                    workDataList = response.data
                }
                
            case .failure(let error):
                // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                print(error)
                
            }
        }



    }
    
    func userWorkInquiry(accessToken: String) {

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {


            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]

            AF.request("\(defaultUrl)/api/work/status", method: .get, headers: headers).responseDecodable(of: UserWorkStatusDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.

                    print("userWorkInquiry: \(response.data)")

                    
                    
                    if response.data.workType.rawValue == "출근"{
                        
                        print("현재 출근중")
                        userWorking = true
                        startWorkTime = convertUTCtoLocalTimeYYYYMMDDHHmmss(utcDateStr: response.data.startWorkDt)
                        
                        
                        
                    }

//                    DispatchQueue.main.async {
//                        workDataListForUser = response.data
//                    }

                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                    print("현재 출근중이 아님.")
                }
            }

        })
        

    }
    
    func recordsUserWorkInquiry(accessToken: String) {
        
        
        let currentDate = Date()
        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "KST")
        let dateString = dateFormatter.string(from: currentDate)

        
        
        
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json"
        ]
        
        AF.request("\(defaultUrl)/admin/api/work/employee/records?user_idx=\(loginData.userIdx)", method: .get, headers: headers).responseDecodable(of: AttendanceManageDataResponse.self) { response in
            switch response.result {
            case .success(let response):
                // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                
                print("userWorkInquiry: \(response.data)")
                
                DispatchQueue.main.async {
                    workDataList = response.data
                    
                }
                
                if let firstData = response.data.first{
                    
                    //퇴근시간 로직
                    if firstData.endWorkDt != nil{ // 오늘의 퇴근 시간이 존재하면 퇴근 한것으로 생각
                        
                        print("dateString \(dateString)")
                        
                        if convertUTCtoLocalTimeYYYYMMDD(utcDateStr: firstData.endWorkDt!) == dateString {
                            
                            
                            userWorking = true
                            startWorkTime = convertUTCtoLocalTimeYYYYMMDDHHmmss(utcDateStr: response.data.first!.startWorkDt)
                            
                            leaveWork = true
                            endWorkTime = convertUTCtoLocalTimeYYYYMMDDHHmmss(utcDateStr: firstData.endWorkDt!)
                            //오늘의 출퇴근 시간을 나타냄
                            print("금일 출퇴근 완료")
                            
                            if response.data.first!.workingTime != nil{ //퇴근 완료했을 때, 근무 시간 나타냄
                                workingTime = response.data.first!.workingTime!
                            }else{
                                workingTime = "--:--"
                            }
                        }
                        
                        
                    }else if firstData.startWorkDt != nil { // 퇴근 시간은 없지만 오늘 날짜 기준 출근 일이 찍혀있으면 출근 한것으로 생각
                        
                        if convertUTCtoLocalTimeYYYYMMDD(utcDateStr: firstData.startWorkDt) == dateString {
                            print("출근했음")
                            
                            userWorking = true
                            
                            startWorkTime = convertUTCtoLocalTimeYYYYMMDDHHmmss(utcDateStr: response.data.first!.startWorkDt)
                            //completeWork = true
                        }
                        
                        
                    }
                    
                }
                
                
                
                
                
            case .failure(let error):
                // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                print(error)
                
                userWorking = false
                leaveWork = false
            }
        }

        

    }
    
//
//    func creatorInquiry(accessToken: String) {
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
//
//            let headers: HTTPHeaders = [
//                "Authorization": "Bearer \(accessToken)",
//                "Accept": "application/json"
//            ]
//
//            AF.request("\(defaultUrl)/admin/api/human-resource?user_type=creator&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: CreatorDataResponse.self) { response in
//                switch response.result {
//                case .success(let response):
//                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
//
//                    print(response.data)
//
//
//                    DispatchQueue.main.async {
//                        creatorList = response.data
//                    }
//
//                case .failure(let error):
//                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
//                    print(error)
//                }
//            }
//        })
//
//
//    }
//
//    private func dateFormatter(getDate: String) -> String {
//
//        let isoDateFormatter = ISO8601DateFormatter()
//        isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
//
//        let getDate = getDate
//        if let date = isoDateFormatter.date(from: getDate) {
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "yyyy.MM.dd"
//            let dateString = dateFormatter.string(from: date)
//            print(dateString) // "2024.01.03" 출력
//            return dateString
//        } else {
//            print("날짜 변환에 실패했습니다.")
//            return ""
//        }
//
//    }
//
//    func returnManageType(manageType: String) -> String {
//        switch manageType {
//        case "marketing":
//            return "광고마케팅"
//        case "mcn":
//            return "MCN"
//        case "etc":
//            return "기타"
//        default:
//            return ""
//        }
//    }
}
//
//#Preview {
//    AttendanceManagementMainView()
//}
