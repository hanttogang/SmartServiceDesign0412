//
//  DetailAttendanceManagementView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/27/24.
//

import SwiftUI
import Alamofire

struct DetailAttendanceManagementView: View {
    
    @EnvironmentObject var loginData: LoginData
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let defaultUrl = "\(ApiClient.BASE_URL)"
    //Api
    let selectedUserIdxForAttendance: Int
    let selectedUserNameForAttendance: String
    
    // 이 init 메소드를 통해 외부에서 selectedCustomerBrandIdx 값을 설정할 수 있습니다.
    init(selectedUserIdxForAttendance: Int, selectedUserNameForAttendance: String) {
        self.selectedUserIdxForAttendance = selectedUserIdxForAttendance
        self.selectedUserNameForAttendance = selectedUserNameForAttendance
    }
    
    @State private var workDataList = [WorkDataForAdmin]()
    
    
    @State private var endWorkTime = ""
    
    @State private var userWorking: Bool = false
    @State private var leaveWork: Bool = false
    
    
    @State private var startWorkTime = ""
    
    @State private var workingTime: String = ""
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    var body: some View {
        
        VStack{
            
            HStack{
                
                Text("\(selectedUserNameForAttendance)님 근무 현황")
                    .padding()
                    .font(.title3)
                    .bold()
                
                Spacer()
            }
            
            ScrollView(showsIndicators: false){
                VStack{
                    
                    ForEach(workDataList, id: \.userIdx) { workDataList in
                            
                                VStack{
                                    
                                    HStack{
                                        Text("\(convertUTCtoLocalTimeYYYYMMDD(utcDateStr: workDataList.startWorkDt))")
                                            .padding()
                                            .font(.callout)
                                            .bold()
                                            .foregroundColor(.black)
                                        
                                        Spacer()
                                    }
                                    .padding(.bottom, 0)
                                    
                                    HStack{
                                        
                                        VStack{//출퇴근 여부
                                            
                                            HStack{
                                                Text("출근")
                                                    .font(.callout)
                                                    .foregroundColor(.black)
                                                Spacer()
                                            }
                                            HStack{ //출퇴근 여부
                                                Text("퇴근")
                                                    .font(.callout)
                                                    .foregroundColor(.black)
                                                Spacer()
                                                
                                            }
                                        }
                                        
                                        
                                        VStack{
                                            
                                            HStack{ //출근 시간
                                                Spacer()
                                                
                                                Text("\(convertUTCtoLocalTimeHHMM(utcDateStr: workDataList.startWorkDt))")
                                                    .font(.callout)
                                                    .foregroundColor(.black)
                                             
                                            }
                                            
                                            HStack{ //퇴근 시간
                                                Spacer()
                                                
                                                if workDataList.endWorkDt != nil{
                                                    
                                                    Text("\(convertUTCtoLocalTimeHHMM(utcDateStr: workDataList.endWorkDt!))")
                                                        .font(.callout)
                                                        .foregroundColor(.black)
                                                }else{
                                                    
                                                    Text("--:--")
                                                        .font(.callout)
                                                        .foregroundColor(.black)
                                                }
                                                
                                             
                                            }
                                        }
                                        
                                        VStack{
                                            //띄우기 위한 투명 뷰
                                        }
                                        .padding()
                                        
                                        
                                        VStack{
                                            
                                            HStack{ //근태구분
                                                Text("근무시간")
                                                    .font(.callout)
                                                    .foregroundColor(.black)
                                                Spacer()
                                            }
                                            
                                            HStack{ //퇴근
                                                Text("근태구분")
                                                    .font(.callout)
                                                    .foregroundColor(.black)
                                                Spacer()
                                            }
                                            
                                            
                                        }
                                        
                                        
                                        VStack{
                                            
                                            HStack{ //근무 시간
                                                if workDataList.workingTime != nil{
                                                    Text("\(workDataList.workingTime!)")
                                                        .font(.callout)
                                                        .foregroundColor(.black)
                                                }else{
                                                    Text("--:--")
                                                        .font(.callout)
                                                        .foregroundColor(.black)
                                                }
                                            }
                                            HStack{ //근태 사각형
                                                Spacer()
                                                
                                                Rectangle()
                                                    .frame(width: screenWidth/12.5, height: screenHeight/81.2)
                                                    .foregroundColor(.blue)
                                                    .cornerRadius(12.0)
                                                    .padding(.leading)
                                                    .padding(.leading)
                                            }
                                        }
                                        
                                        
                                    }//HStack
                                    .padding(.horizontal)
                                    .padding(.bottom)
                                    
                                    
                                }//VStack
                                .frame(width: screenWidth/1.1)
                                .background(.white)
                                .cornerRadius(4)
                            
                        }
                }//VStack
            }//ScrollView
            .frame(width: screenWidth)
        }
        .onAppear{
//            userWorkInquiry(accessToken: loginData.token)
            recordsUserWorkInquiry(accessToken: loginData.token)
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            self.presentationMode.wrappedValue.dismiss()
        }) {
            HStack(spacing: 0){
                Image(systemName: "chevron.backward")
            }
        })
        .navigationBarTitle("\(selectedUserNameForAttendance)", displayMode: .inline)
        .background(Color("colorEFEFF4"))
        
        
    }
    
    func convertUTCtoLocalTimeHHMM(utcDateStr: String, format: String = "HH:mm") -> String {
        let dateFormatter = DateFormatter()

        // Input Format
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "KST")

        // Convert String to Date
        guard let date = dateFormatter.date(from: utcDateStr) else {
            print("Date conversion failed due to wrong format.")
            return ""
        }

        // Output Format
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current

        // Convert Date to String
        let localTimeStr = dateFormatter.string(from: date)

        return localTimeStr
    }
    
    func convertUTCtoLocalTimeYYYYMMDD(utcDateStr: String, format: String = "yyyy-MM-dd") -> String {
        let dateFormatter = DateFormatter()

        // Input Format
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "KST")

        // Convert String to Date
        guard let date = dateFormatter.date(from: utcDateStr) else {
            print("Date conversion failed due to wrong format.")
            return ""
        }

        // Output Format
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current

        // Convert Date to String
        let localTimeStr = dateFormatter.string(from: date)

        return localTimeStr
    }
    
    
    func convertUTCtoLocalTimeYYYYMMDDHHmmss(utcDateStr: String, format: String = "yyyy-MM-dd HH:mm:ss") -> String {
        let dateFormatter = DateFormatter()

        // Input Format
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "KST")

        // Convert String to Date
        guard let date = dateFormatter.date(from: utcDateStr) else {
            print("Date conversion failed due to wrong format.")
            return ""
        }

        // Output Format
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current

        // Convert Date to String
        let localTimeStr = dateFormatter.string(from: date)

        return localTimeStr
    }
    
    
    
    func recordsUserWorkInquiry(accessToken: String) {
        
        
        let currentDate = Date()
        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "KST")
        let dateString = dateFormatter.string(from: currentDate)

        
        
        
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json"
        ]
        
        AF.request("\(defaultUrl)/admin/api/work/employee/records?user_idx=\(selectedUserIdxForAttendance)", method: .get, headers: headers).responseDecodable(of: AttendanceManageDataResponse.self) { response in
            switch response.result {
            case .success(let response):
                // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                
                print("userWorkInquiry: \(response.data)")
                
                DispatchQueue.main.async {
                    workDataList = response.data
                    
                }
                
                if let firstData = response.data.first{
                    
                    //퇴근시간 로직
                    if firstData.endWorkDt != nil{ // 오늘의 퇴근 시간이 존재하면 퇴근 한것으로 생각
                        
                        print("dateString \(dateString)")
                        
                        if convertUTCtoLocalTimeYYYYMMDD(utcDateStr: firstData.endWorkDt!) == dateString {
                            
                            
                            userWorking = true
                            startWorkTime = convertUTCtoLocalTimeYYYYMMDDHHmmss(utcDateStr: response.data.first!.startWorkDt)
                            
                            leaveWork = true
                            endWorkTime = convertUTCtoLocalTimeYYYYMMDDHHmmss(utcDateStr: firstData.endWorkDt!)
                            //오늘의 출퇴근 시간을 나타냄
                            print("금일 출퇴근 완료")
                            
                            if response.data.first!.workingTime != nil{ //퇴근 완료했을 때, 근무 시간 나타냄
                                workingTime = response.data.first!.workingTime!
                            }else{
                                workingTime = "--:--"
                            }
                        }
                        
                        
                    }else if firstData.startWorkDt != nil { // 퇴근 시간은 없지만 오늘 날짜 기준 출근 일이 찍혀있으면 출근 한것으로 생각
                        
                        if convertUTCtoLocalTimeYYYYMMDD(utcDateStr: firstData.startWorkDt) == dateString {
                            print("출근했음")
                            
                            userWorking = true
                            
                            startWorkTime = convertUTCtoLocalTimeYYYYMMDDHHmmss(utcDateStr: response.data.first!.startWorkDt)
                            //completeWork = true
                        }
                        
                        
                    }
                    
                }
                
                
                
                
                
            case .failure(let error):
                // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                print(error)
                
                userWorking = false
                leaveWork = false
            }
        }

        

    }
    
}

    
