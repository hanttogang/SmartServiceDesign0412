//
//  ContractDetailOfCreator.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/27/23.
//

import SwiftUI
import Alamofire

struct ContractDetailOfCreatorView: View {
    
    //Api
    @State private var humanResourceContractIdx: Int = 0
    
    var selectedCreatorContractIdx: Int
    var selectedCreatorContractName: String
    
    var selectedCreatorUserIdx: Int
    var selectedCreatorHumanResourceIdx: Int
    var detailCreatorView: DetailCreatorView
    
    // 이 init 메소드를 통해 외부에서 selectedCreatorContractIdx 값을 설정할 수 있습니다.
    init(selectedCreatorContractIdx: Int, selectedCreatorContractName: String, selectedCreatorUserIdx: Int, selectedCreatorHumanResourceIdx: Int) {
        self.selectedCreatorContractIdx = selectedCreatorContractIdx
        self.selectedCreatorContractName = selectedCreatorContractName
        
        self.selectedCreatorUserIdx = selectedCreatorUserIdx
        self.selectedCreatorHumanResourceIdx = selectedCreatorHumanResourceIdx
        self.detailCreatorView = DetailCreatorView(selectedCreatorUserIdx: selectedCreatorUserIdx, selectedCreatorHumanResourceIdx: selectedCreatorHumanResourceIdx)
    }
    
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var creatorContractImageString = ""
    
    @State private var isApiLoading: Bool = false
    
    //View
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    
    var selectedDetailCreatorName: String = ""
    var selectedDetailContractNameFor_DetailCreator: String = ""
    
    
    @State private var navigate: Bool = false
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var showToast: Bool = false
    @State private var toastText: String = "시작 날짜 선택 후 확인을 눌러주세요"
    
    @State private var modificationModeFor_DetailCreatorContract: Bool = false
    
    
    @State private var contractNameFor_DetailCreator: String = ""
    @State private var contractOneLineExplanFor_DetailCreator: String = ""
    @State private var contractStartDateFor_DetailCreatorContract: String = ""
    @State private var contractEndDateFor_DetailCreatorContract: String = ""
    @State private var contractPriceFor_DetailCreator: String = ""
    @State private var contractStatusFor_DetailCreator: String = ""
    
    
    @State private var selecteContractStatusFor_DetailCreator: Bool = false
    @State private var contractStatusmodificationModeFor_DetailCreatorContract: Bool = false
    
    @State private var showingBusinessLicenseImagePickerFor_DetailCreator = false
    @State private var selectedBusniessLicenseImageFor_DetailCreator: UIImage? = nil
    
    
    //날짜 관련
    @State private var showingDatePicker = false
    @State private var dateSelection = Date()
    @State private var startDate: Date? = nil
    @State private var endDate: Date? = nil
    @State private var dateCount = 0
    
    
    var body: some View {
        
        ZStack {
            
            if (isApiLoading){

                ProgressView()
                    .zIndex(3)

            }
            
            ScrollView(.vertical){
                VStack(spacing: 0){
                    
                    HStack{
                        
                    
                        
                        if modificationModeFor_DetailCreatorContract{
                            
                            TextField(".", text: $contractNameFor_DetailCreator, prompt: Text("\(contractNameFor_DetailCreator)")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.title2)
                            .frame(height: screenHeight/29) //28
                            
                            
                        }else {
                            
                            Text("\(contractNameFor_DetailCreator)")
                                .font(.title2)
                                .frame(height: screenHeight/29) //28
                        }
                        
                        
                        
                        
                        
                        Spacer()
                        
                        if(modificationModeFor_DetailCreatorContract){
                            Button(action: {
                                
                                if (!isApiLoading){
                                    
                                    if (contractNameFor_DetailCreator == "" || contractOneLineExplanFor_DetailCreator == "" || contractStartDateFor_DetailCreatorContract == "" || contractEndDateFor_DetailCreatorContract == "" || contractPriceFor_DetailCreator == ""){
                                        
                                        showAlert = true
                                        
                                        alertTitle = "크리에이터 계약 수정 실패"
                                        alertMessage = "칸을 비울 수 없습니다."
                                        
                                    } else {
                                        
                                        patchCreatorContract(accessToken: loginData.token)
                                        
                                        detailCreatorView.contractInquiry(accessToken: loginData.token)
                                        
                                        modificationModeFor_DetailCreatorContract = false
                                    }
                                    
                                }
                                
                                
                                
                            }, label: {
                                Text("저장")
                                
                            })
                        } else {
                            Button(action: {
                                
                                modificationModeFor_DetailCreatorContract = true
                                
                                
                            }, label: {
                                Text("수정")
                                
                            })
                            
                        }
                        
                        
                    }// HStack 계약 이름
                    .padding(.horizontal)
                    .padding(.vertical, screenHeight / 81.5)
//                    .padding(.top, screenHeight / 81.5)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        
                        Text("한줄 설명")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailCreatorContract{
                            
                            TextField("", text: $contractOneLineExplanFor_DetailCreator)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                            
//                            RightAlignedTextField(text: $contractOneLineExplanFor_DetailCreator)
//                                .keyboardType(.numberPad)
                            
                            
                            
                        }else {
                            
                            Text("\(contractOneLineExplanFor_DetailCreator)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("계약기간")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailCreatorContract{

                            
                            Button {
                                
                                startDate = nil
                                endDate = nil
                                
                                self.showingDatePicker = true
                                
                                toastText = "시작 날짜 선택 후 확인을 눌러주세요"
                                showToast = true
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                    self.showToast = false
                                    toastText = ""
                                }
                                
                            } label: {
                                
                                
                                Text("\(contractStartDateFor_DetailCreatorContract) ~ \(contractEndDateFor_DetailCreatorContract)")
                                    .foregroundColor(Color("color00000040"))

                            }

                            
                            
                        }else {
                            
                            Text("\(contractStartDateFor_DetailCreatorContract) ~ \(contractEndDateFor_DetailCreatorContract)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .sheet(isPresented: $showingDatePicker) {
                        VStack {
                            
                                DatePicker("날짜를 선택해주세요", selection: $dateSelection, displayedComponents: .date)
                                    .datePickerStyle(GraphicalDatePickerStyle())
                                    .padding()
                                    .overlay{
                                        ZStack{}
                                        .toast(isShowing: $showToast, text: Text(toastText))
                                    }
                            
                            
                            
                            Button("확인") {
                                if self.dateCount == 0 {
                                    self.startDate = dateSelection
                                    self.dateCount += 1
                                } else if self.dateCount == 1 {
                                    if dateSelection > self.startDate! {
                                        self.endDate = dateSelection
                                        self.dateCount = 0
                                    } else {
                                        self.startDate = nil
                                        self.endDate = nil
                                        self.dateCount = 0
                                    }
                                }
                                
                                
                                if (startDate != nil && endDate == nil){
                                    
                                    toastText = "종료 날짜 선택 후 확인을 주세요"
                                    showToast = true
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                        self.showToast = false
                                        toastText = ""
                                    }
                                    
                                    self.showingDatePicker = true
                                }
                                
                                
                                if (startDate != nil && endDate != nil){
                                    contractStartDateFor_DetailCreatorContract = convertDateToString(date: startDate)
                                    contractEndDateFor_DetailCreatorContract = convertDateToString(date: endDate)
                                    
                                    self.showingDatePicker = false
                                }else{
                                    contractStartDateFor_DetailCreatorContract = ""
                                    contractEndDateFor_DetailCreatorContract = ""
                                }
                            }
                        }
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{
                        Text("계약금액")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailCreatorContract{
                            
                            TextField("", text: $contractPriceFor_DetailCreator)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                                .keyboardType(.numberPad)
                            
//                            RightAlignedTextField(text: $contractPriceFor_DetailCreator)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
                            
                            
                            
                        }else {
                            
                            Text("\(contractPriceFor_DetailCreator)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack(spacing: 0){
                        Text("진행상태")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        
                        HStack(spacing: 0){
                            
                            if modificationModeFor_DetailCreatorContract{
                                
                                
                                Button(action: {
                                    selecteContractStatusFor_DetailCreator = true
                                }, label: {
                                    
                                    if selecteContractStatusFor_DetailCreator{
                                        
                                        HStack(spacing: screenWidth/128.3){ // spacing 3
                                            Button(action: {
                                                selecteContractStatusFor_DetailCreator = false
                                                
                                                contractStatusFor_DetailCreator = "신규"
                                                
                                            }, label: {
                                                Text("신규")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailCreator = false
                                                
                                                contractStatusFor_DetailCreator = "갱신"
                                                
                                            }, label: {
                                                Text("갱신")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailCreator = false
                                                
                                                contractStatusFor_DetailCreator = "변경"
                                                
                                            }, label: {
                                                Text("변경")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailCreator = false
                                                
                                                contractStatusFor_DetailCreator = "완료"
                                                
                                            }, label: {
                                                Text("완료")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailCreator = false
                                                
                                                contractStatusFor_DetailCreator = "신규"
                                                
                                            }, label: {
                                                Text("신규")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                        } //HStack
                                        
                                        
                                    }else{
                                        
                                        Button(action: {
                                            selecteContractStatusFor_DetailCreator = true
                                            
                                        }, label: {
                                            Text("\(contractStatusFor_DetailCreator)")
                                                .foregroundColor(.white)
                                                .bold()
                                                .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                .background(Color("mainColor"))
                                                .cornerRadius(16.0)
                                        })
                                    }
                                    
                                })
                            } else{
                                
                                Text("\(contractStatusFor_DetailCreator)")
                                    .foregroundColor(.white)
                                    .bold()
                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                    .background(Color("mainColor"))
                                    .cornerRadius(16.0)
                            }
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    Divider()
                        .padding(.leading)
                    
                   
                    
                    
                    
                    HStack(spacing: 0){
                        
                        
                        VStack(alignment: .leading){// 사업자등록증 이미지
                            
                            Text("계약서 이미지")
                                .padding(.top, screenHeight/30.75)
                            
                            ZStack{
                                
                                
                                if modificationModeFor_DetailCreatorContract{
                                    
                                    Button(action: {
                                        showingBusinessLicenseImagePickerFor_DetailCreator = true
                                    }, label: {
                                        
                                        ZStack {
                                            if let image = selectedBusniessLicenseImageFor_DetailCreator {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                    .cornerRadius(12.0)
                                                
                                            } else {
                                                AsyncImage(url: URL(string: imageS3Url + "/" + creatorContractImageString)) { image in
                                                    image.resizable()
                                                         .aspectRatio(contentMode: .fill)
                                                         .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                         .cornerRadius(12.0)
                                                } placeholder: {
                                                    ProgressView()
                                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                }
                                                
                                            }
                                        }
                                    })
                                    
                                }else{
                                    
                                    ZStack {
                                        if let image = selectedBusniessLicenseImageFor_DetailCreator {
                                            Image(uiImage: image)
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                .cornerRadius(12.0)
                                            
                                        } else {
                                            AsyncImage(url: URL(string: imageS3Url + "/" + creatorContractImageString)) { image in
                                                image.resizable()
                                                     .aspectRatio(contentMode: .fill)
                                                     .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                     .cornerRadius(12.0)
                                            } placeholder: {
                                                ProgressView()
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                            }
                                            
                                        }
                                    }
                                    
                                }
                            } //ZStack 계약 이미지
                            .sheet(isPresented: $showingBusinessLicenseImagePickerFor_DetailCreator, onDismiss: loadBusniessLicenseImageFor_DetailCreator) {
                                ImagePicker(selectedImage: $selectedBusniessLicenseImageFor_DetailCreator)
                            }
                            
                            
                            
                        }//VStack 계약 이미지
                        
                        
                        
                        Spacer()
                        
                        
                      
                    }//HStack
                    .padding(.leading)
                    
                    Spacer()
                    
                } //VStack
                
            }//ScrollView
        }//ZStack
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            
            self.presentationMode.wrappedValue.dismiss()
            
            detailCreatorView.contractInquiry(accessToken: loginData.token)
            
        }) {
            HStack(spacing: 0){
                Image(systemName: "chevron.backward")
            }
        })
        .navigationBarTitle("\(selectedDetailCreatorName)", displayMode: .inline)
        .onAppear{
            contractInquiry()
        }
      
        
        
        
    }//body

    
  
    private func loadBusniessLicenseImageFor_DetailCreator() {
        guard let selectedBusniessLicenseImageFor_DetailCreator = selectedBusniessLicenseImageFor_DetailCreator else { return }
        // You can do something with the selected brand image here
        isApiLoading = true
        
        uploadImage(image: selectedBusniessLicenseImageFor_DetailCreator, imageType: "human_resource_contract")
    }
    
    
    
    
    //Api 를통해 필요한 값 호출 (url, key 값)
    private func getApiCommonCreatorContractImageURL(imageType: String, completion: @escaping (String?, String?) -> Void) {
        
        let headers: HTTPHeaders = [
            "accept": "application/json",
            "Authorization": "Bearer \(loginData.token)"
        ]
        
        let parameters: Parameters = [
            "user_idx": selectedCreatorContractIdx,
            "mimetype": "image/png",
            "type": "human_resource_contract",
            "extension": "png"
        ]
        
        AF.request("\(defaultUrl)/api/common/image/url", method: .get, parameters: parameters, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    let url = JSON["data"] as? String
                    let key = JSON["key"] as? String
                    completion(url, key)
                }
            case .failure(let error):
                print(error)
                completion(nil, nil)
            }
        }
    }
    
    //사진 선택시 uploadImag 함수를 호출
    private func uploadImage(image: UIImage, imageType: String) {
        getApiCommonCreatorContractImageURL(imageType: imageType) { (url, key) in
            
            guard let preSignedUrl = url else {
                print("Failed to get pre-signed URL.")
                return
            }
            
            guard let imageData = image.pngData() else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: url!, method: .put).response { response in
                switch response.result {
                case .success:
                    print("Image uploaded successfully.")
                    
                    isApiLoading = false
                case .failure(let error):
                    print("Failed to upload image: \(error)")
                }
            }
            
            
            guard let imageData = image.jpegData(compressionQuality: 1.0) else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: preSignedUrl).response { response in
                if let error = response.error {
                    print("Image upload failed with error: \(error)")
                } else {
                    print("Image uploaded successfully.")
                    switch imageType {
                    case "human_resource_contract":
                        creatorContractImageString = key!
                        
                        print("\(creatorContractImageString)")
                        
                    default:
                        print("Unknown image type: \(imageType)")
                    }
                }
            }
        }
    }
    
    
    
    
    
    private func dateFormatter(getDate: String) -> String {
        
        let isoDateFormatter = ISO8601DateFormatter()
        isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

        let getDate = getDate
        if let date = isoDateFormatter.date(from: getDate) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateString = dateFormatter.string(from: date)
//            print(dateString) //ex "2024.01.05" 출력
            return dateString
        } else {
            print("날짜 변환에 실패했습니다.")
            return getDate
        }

    }
    
    
    
    //계약 가져오는 api
    private func contractInquiry() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        AF.request("\(defaultUrl)/admin/api/human-resource-contract?human_resource_idx=\(selectedCreatorContractIdx)&contract_name=\(selectedCreatorContractName)&page=1&limit=10", method: .get, headers: headers).responseDecodable(of: EmployeeCreatorContractDataResponse.self) { response in
            switch response.result {
    
            case .success(let response):
                // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                
                for creatorContractData in response.data {
                    contractNameFor_DetailCreator = creatorContractData.contractName
                    contractOneLineExplanFor_DetailCreator = creatorContractData.oneLineExplain
                    contractStartDateFor_DetailCreatorContract = dateFormatter(getDate: creatorContractData.contractStartDt)
                    contractEndDateFor_DetailCreatorContract = dateFormatter(getDate: creatorContractData.contractEndDt)
                    contractStatusFor_DetailCreator = creatorContractData.contractStatus
                    contractPriceFor_DetailCreator = String(creatorContractData.contractAmount)
                    
                    creatorContractImageString = creatorContractData.contractImage
                    
                    humanResourceContractIdx = creatorContractData.humanResourceContractIdx
                    
                }
                
                
//
//                DispatchQueue.main.async {
////                    contractDataList = response.data
////
////                    contractCount = response.pagination.total
//
////                    print("\(selectedCustomerBrandContractIdx) 계약 리스트 호출 성공 \(response.data)")
////
//                }
                
                
                
                
            case .failure(let error):
                // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                print(error)
            }
        }
    }
    
    //최종적으로 계약 수정
    private func patchCreatorContract(accessToken: String) {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any] = [
            "human_resource_idx": humanResourceContractIdx,
            "contract_name": contractNameFor_DetailCreator,
              "one_line_explain": contractOneLineExplanFor_DetailCreator,
              "contract_start_dt": contractStartDateFor_DetailCreatorContract,
              "contract_end_dt": contractEndDateFor_DetailCreatorContract,
              "contract_status": contractStatusFor_DetailCreator,
              "contract_image": creatorContractImageString,
              "contract_amount": Int(contractPriceFor_DetailCreator)
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/human-resource-contract/\(humanResourceContractIdx)",
                   method: .patch,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("크리에이터 계약 수정 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("크리에이터 계약 수정 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("크리에이터 계약 수정 실패: \(error)")
                print(humanResourceContractIdx)
                
                print(contractNameFor_DetailCreator)
                print(contractOneLineExplanFor_DetailCreator)
                print(contractStartDateFor_DetailCreatorContract)
                print(contractEndDateFor_DetailCreatorContract)
                print(contractStatusFor_DetailCreator)
                print(creatorContractImageString)
                print(contractPriceFor_DetailCreator)
                
            }
        }
    }
    
    
    
    func convertDateToString(date: Date?) -> String {
        guard let date = date else {
            return ""
        }

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
    
}

//#Preview {
//    ContractDetailOfCreatorView()
//}
