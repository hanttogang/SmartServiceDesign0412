//
//  DetailCreatorView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/27/23.
//

import SwiftUI
import Alamofire


struct DetailCreatorView: View {
        
    let mainEmployeeAndCreatorManagementView = MainEmployeeAndCreatorManagementView()
    //Api
    var selectedCreatorUserIdx: Int
    var selectedCreatorHumanResourceIdx: Int
    
    // 이 init 메소드를 통해 외부에서 selectedCustomerBrandIdx 값을 설정할 수 있습니다.
    init(selectedCreatorUserIdx: Int, selectedCreatorHumanResourceIdx: Int) {
        self.selectedCreatorUserIdx = selectedCreatorUserIdx
        self.selectedCreatorHumanResourceIdx = selectedCreatorHumanResourceIdx
    }
    
    
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var creatorImageString: String = ""
    
    @State private var humanResourceContractList = [HumanResourceContract]()
    @State private var humanResourceContractListCount: Int = 0
    
    @State private var isApiLoading: Bool = false
    
    
    //View
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State private var navigate: Bool = false
    @State private var navigateContractDetailForDetailCustomerBrandView: Bool = false
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var modificationMode: Bool = false
    
    @State private var showingImagePickerFor_DetailCreatorInfo = false
    @State private var selectedDetailCreatorInfo: UIImage? = nil
    
    
    @State private var creatorNameFor_DetailCreatorInfo: String = ""
    
    @State private var classificationFor_DetailCreatorInfo: String = "크리에이터"
    
    @State private var oneLineExplanFor_DetailCreatorInfo: String = ""
    @State private var creatorPhoneNumberFor_DetailCreatorInfo: String = "031-123-1234"
    @State private var creatorEmailFor_DetailCreatorInfo: String = "bca321@mail.com"
    
    @State private var workAreaFor_DetailDetailCreatorInfo: String = "marketing"
    @State private var selecteWorkAreaFor_DetailCreatorInfo: Bool = false
    @State private var workAreaModificationModeFor_DetailCreatorInfo: Bool = false
    
    @State private var contractImageForDetailCreatorInfoContractList: UIImage? = nil
    
    
    @State private var showAllContractList: Bool = false
    @State private var showShowAllButton: Bool = false
    
    
    
    
    var body: some View {
        
        ZStack {
            
            if (isApiLoading){
                
                ProgressView()
                    .zIndex(3)
                
            }
            
            ScrollView(.vertical){
                VStack(spacing: 0){
                    
                    
                    HStack{
                        
                        
                        VStack(alignment: .leading){// 브랜드 이미지
                            ZStack{
                                if modificationMode{
                                    
                                    Button(action: {
                                        showingImagePickerFor_DetailCreatorInfo = true
                                    }, label: {
                                        
                                        ZStack {
                                            if let image = selectedDetailCreatorInfo {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                    .cornerRadius(12.0)
                                                
                                            } else {
                                                AsyncImage(url: URL(string: imageS3Url + "/" + creatorImageString)) { image in
                                                    image.resizable()
                                                        .aspectRatio(contentMode: .fill)
                                                        .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                        .cornerRadius(12.0)
                                                }placeholder: {
                                                    ProgressView()
                                                        .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                }
                                                
                                            }
                                        }
                                    })
                                    
                                }else{
                                    
                                    ZStack {
                                        if let image = selectedDetailCreatorInfo {
                                            Image(uiImage: image)
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                .cornerRadius(12.0)
                                            
                                        } else {
                                            AsyncImage(url: URL(string: imageS3Url + "/" + creatorImageString)) { image in
                                                image.resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                    .cornerRadius(12.0)
                                            }placeholder: {
                                                ProgressView()
                                                    .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                            }
                                            
                                            
                                        }
                                    }
                                    
                                }
                            } //ZStack BrandImage
                            .sheet(isPresented: $showingImagePickerFor_DetailCreatorInfo, onDismiss: loadDetailCreatorInfoImage) {
                                ImagePicker(selectedImage: $selectedDetailCreatorInfo)
                            }
                            
                        }//VStack 크리에이터 이미지
                        
                        HStack(){
                            
                            HStack{
                                
                                //크리에이터이름
                                if modificationMode{
                                    
                                    TextField(".", text: $creatorNameFor_DetailCreatorInfo, prompt: Text("\(creatorNameFor_DetailCreatorInfo)")
                                        .foregroundColor(Color("hintTextColor")))
                                    .keyboardType(.default)
                                    .font(.title2)
                                    .frame(width: screenWidth/2)//
    //                                .padding(.leading, -screenWidth/150)
                                    
                                    
                                }else {
                                    
                                    Text("\(creatorNameFor_DetailCreatorInfo)")
                                        .font(.title2)
                                    
//                                        .frame(width: screenWidth/6.3)
                                }
                            }
                            
                        }
                        
                        Spacer()
                        
                        if(modificationMode){
                            Button(action: {
                                if (!isApiLoading){
                                    
                                    if (creatorNameFor_DetailCreatorInfo == "" || classificationFor_DetailCreatorInfo == "" || creatorPhoneNumberFor_DetailCreatorInfo == "" || creatorEmailFor_DetailCreatorInfo == ""){
                                        
                                        showAlert = true
                                        
                                        alertTitle = "크리에이터 정보 수정 실패"
                                        alertMessage = "칸을 비울 수 없습니다."
                                        
                                    } else {
                                        patchHumanResource()
                                        
                                        mainEmployeeAndCreatorManagementView.creatorInquiry(accessToken: loginData.token)
                                        modificationMode = false
                                    }
                                    
                                }
                                
                                
                                
                                
                            }, label: {
                                Text("저장")
                                
                            })
                        } else {
                            Button(action: {
                                
                                modificationMode = true
                                
                                
                            }, label: {
                                Text("수정")
                                
                            })
                        }
                        
                        
                    }
                    .padding(.horizontal)
                    .padding(.vertical, screenHeight / 81.5)
                    .padding(.top, screenHeight / 81.5)
                    
                    Divider()
                        .padding(.leading)
                   

                    HStack{
                        Text("분류")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        Text("크리에이터")
                            .foregroundColor(.white)
                            .bold()
                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                            .background(Color("mainColor"))
                            .cornerRadius(16.0)
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    
                    HStack{
                        Text("한줄 설명")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            
                            TextField("", text: $oneLineExplanFor_DetailCreatorInfo)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                            
//                            RightAlignedTextField(text: $oneLineExplanFor_DetailCreatorInfo)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(oneLineExplanFor_DetailCreatorInfo)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("전화번호")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            
                            TextField("", text: $creatorPhoneNumberFor_DetailCreatorInfo)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                                .keyboardType(.numberPad)

//                            RightAlignedTextField(text: $creatorPhoneNumberFor_DetailCreatorInfo)
//                                .keyboardType(.numberPad)
//                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(creatorPhoneNumberFor_DetailCreatorInfo)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{
                        Text("이메일")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            
                            TextField("", text: $creatorEmailFor_DetailCreatorInfo)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                                .keyboardType(.emailAddress)
                            
                            
//                            RightAlignedTextField(text: $creatorEmailFor_DetailCreatorInfo)
//                                .keyboardType(.emailAddress)
//                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(creatorEmailFor_DetailCreatorInfo)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("업무영역")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        
                        
                        if modificationMode{
                            Button(action: {
//                                selecteWorkAreaForDetailBrand = true
                            }, label: {
                                
                                //HStack 으로 버튼 세 개 만든 후 각각 터치 이벤트에 selecteWorkAreaForDetailBrand = false 처리하기
                                
                                
                                
                                
                                
                                if workAreaModificationModeFor_DetailCreatorInfo{
                                    
                                    Button(action: {
                                        workAreaModificationModeFor_DetailCreatorInfo = false
                                        
                                        workAreaFor_DetailDetailCreatorInfo = "marketing"
                                        
                                    }, label: {
                                        Text("광고마케팅")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                    
                                    Button(action: {
                                        workAreaModificationModeFor_DetailCreatorInfo = false
                                        
                                        workAreaFor_DetailDetailCreatorInfo = "mcn"
                                        
                                    }, label: {
                                        Text("MCN")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                    
                                    Button(action: {
                                        workAreaModificationModeFor_DetailCreatorInfo = false
                                        
                                        workAreaFor_DetailDetailCreatorInfo = "etc"
                                        
                                    }, label: {
                                        Text("기타")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                    
                                    
                                }else{
                                    
                                    Button(action: {
                                        workAreaModificationModeFor_DetailCreatorInfo = true
                                        
                                    }, label: {
                                        Text("\(workAreaFor_DetailDetailCreatorInfo)")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                }
                                
                                
                                
                                
                            })
                        } else{
                            
                            Text("\(workAreaFor_DetailDetailCreatorInfo)")
                                .foregroundColor(.white)
                                .bold()
                                .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                .background(Color("mainColor"))
                                .cornerRadius(16.0)
                        }
                        
                        
                    }
                    .padding(.horizontal)
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    
                    VStack(spacing: 0) {
                        HStack{
                            
                            Text("계약 리스트")
                                .font(.title2)
                                .bold()
                                .padding()
                            //                                .padding(.top, -screenHeight/13.5)
                            Spacer()
                        }
                        .padding(0)
                        
                        Button(action: {
                            print("계약 추가 버튼 클릭됨")
                            
                            self.navigate = true
                            
                        }, label: {
                            
                            NavigationLink(destination: AddContractOfDetailCreatorView(selectedCreatorHumanResourceIdx: selectedCreatorHumanResourceIdx, selectedCreatorUserIdx: selectedCreatorUserIdx ), isActive: $navigate) {
                                EmptyView()
                            }
                            
                            Text("+  계약 추가")
                                .foregroundColor(.white)
                                .font(.system(size: 16))
                                .bold()
                                .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        
                        
                        VStack{//계약 리스트 터치시
                            
                            ForEach(humanResourceContractList.prefix(showAllContractList ? humanResourceContractList.count : 3), id: \.humanResourceContractIdx) { humanContractData in
                                
                                NavigationLink(destination: ContractDetailOfCreatorView(selectedCreatorContractIdx: humanContractData.humanResourceIdx, selectedCreatorContractName: humanContractData.contractName, selectedCreatorUserIdx: selectedCreatorUserIdx, selectedCreatorHumanResourceIdx: selectedCreatorHumanResourceIdx)) {
                                    
                                    VStack{
                                        HStack{ //리스트항목의 모델
                                            
                                            if humanContractData.contractImage != nil {
                                                
                                                AsyncImage(url: URL(string: imageS3Url + "/" + humanContractData.contractImage)) { image in
                                                    image.resizable()
                                                         .aspectRatio(contentMode: .fill)
                                                         .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                         .cornerRadius(12.0)
                                                } placeholder: {
                                                    ProgressView()
                                                        .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                }
                                                
                                            } else {
                                                
                                                Rectangle()
                                                    .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                    .cornerRadius(12.0)
                                                    .foregroundColor(.colorE5E5E5)
                                            }
                                            
                                            VStack(alignment: .leading){
                                                Text(humanContractData.contractName)
                                                    .foregroundColor(.black)
                                                
                                                Text("\(dateFormatter(getDate: humanContractData.contractStartDt)) ~ \(dateFormatter(getDate: humanContractData.contractEndDt))")
                                                    .foregroundColor(Color("color00000040"))
                                                    .font(.caption)
                                            }
                                            
                                            Spacer()
                                            
                                            
                                            Text(humanContractData.contractStatus)
                                                .foregroundColor(.white)
                                                .bold()
                                                .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                                .background(Color("mainColor"))
                                                .cornerRadius(16.0)
                                            
                                            
                                        }
                                        
                                        Divider()
                                            .padding(.leading, screenWidth/8)
                                    }
                                    
                                }//NavigationLink
                            }
                            
                            
                            
                            
                        } //VStack 계약 리스트
                        .listStyle(PlainListStyle())
                        .padding(.horizontal)
                        .padding(.top, screenHeight/50.9375)
                        
                        
                        if showShowAllButton{
                            
                            Button(action: {
                                
                                showShowAllButton = false
                                showAllContractList = true
                                
                            }, label: {
                                
                                Text("더 보기")
                                    .foregroundColor(.black)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                            })
                            .background(.white)
                            .cornerRadius(4)
                            .padding()
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color(.black), lineWidth: 1)
                                .padding()
                            )
                            
                        }
                        
                        Spacer()
                        
                        
                        
                        
                    }//VStack
                    .frame(width: screenWidth/1.0932)
                    .background(Color.white)
                    .cornerRadius(20)
                    .shadow(color: Color("color00000040"), radius: 20, x: 0, y: 30)
                    .padding(.top, screenHeight/31.346)
                    
                    //                    .sheet(isPresented: $showingBankRecordCopyImagePickerForDetailBrand, onDismiss: loadContractImageForDetailBrandContractList) {
                    //                        ImagePicker(selectedImage: $selectedBankRecordCopyImageForDetailBrand)
                    //                    }
                    
                    
                    
                    
                    
                } //VStack
                
            }//ScrollView
        }//ZStack
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            
            self.presentationMode.wrappedValue.dismiss()
            
            mainEmployeeAndCreatorManagementView.creatorInquiry(accessToken: loginData.token)
            
        }) {
            HStack(spacing: 0){
                Image(systemName: "chevron.backward")
            }
        })
        .navigationBarTitle("\(creatorNameFor_DetailCreatorInfo)", displayMode: .inline)
        .onAppear {
            creatorInquiry(accessToken: loginData.token)
            
            contractInquiry(accessToken: loginData.token)
        }
        
        
        
        
    }//body
    
    func returnManageType(manageType: String) -> String {
        switch manageType {
        case "marketing":
            return "광고마케팅"
        case "mcn":
            return "MCN"
        case "etc":
            return "기타"
        default:
            return workAreaFor_DetailDetailCreatorInfo
        }
    }

    func transManageType(manageType: String) -> String {
        switch manageType {
        case "광고마케팅":
            return "marketing"
        case "MCN":
            return "mcn"
        case "기타":
            return "etc"
        default:
            return workAreaFor_DetailDetailCreatorInfo
        }
    }



    
    private func loadDetailCreatorInfoImage() {
        guard let selectedDetailCreatorInfo = selectedDetailCreatorInfo else { return }
        
        isApiLoading = true
        
        uploadImage(image: selectedDetailCreatorInfo, imageType: "user")
    }
    
    private func creatorInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/human-resource?user_idx=\(selectedCreatorUserIdx)&user_type=creator&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: CreatorDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    for creatorData in response.data {
                        
                        creatorImageString = creatorData.userImage
                        creatorNameFor_DetailCreatorInfo = creatorData.userName
                        oneLineExplanFor_DetailCreatorInfo = creatorData.oneLineExplain!
                        creatorPhoneNumberFor_DetailCreatorInfo = creatorData.userPhone
                        creatorEmailFor_DetailCreatorInfo = creatorData.userEmail
                        workAreaFor_DetailDetailCreatorInfo = returnManageType(manageType: creatorData.manageType!)
                     
                    }
                    
                    
               
                    print(response.data)
                    
    //                brandIdx = brandData.brandIdx
    //                DispatchQueue.main.async {
    //                    creatorList = response.data
    //                }
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
            
        })
        
    }
    
    //Api 를통해 필요한 값 호출 (url, key 값)
    private func getApiCommonCreatorImageURL(imageType: String, completion: @escaping (String?, String?) -> Void) {
        
        let headers: HTTPHeaders = [
            "accept": "application/json",
            "Authorization": "Bearer \(loginData.token)"
        ]
        
        let parameters: Parameters = [
            "user_idx": selectedCreatorUserIdx,
            "mimetype": "image/png",
            "type": "user",
            "extension": "png"
        ]
        
        AF.request("\(defaultUrl)/api/common/image/url", method: .get, parameters: parameters, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    let url = JSON["data"] as? String
                    let key = JSON["key"] as? String
                    completion(url, key)
                }
            case .failure(let error):
                print(error)
                completion(nil, nil)
            }
        }
    }
    
    
    
    //사진 선택시 uploadImag 함수를 호출
    private func uploadImage(image: UIImage, imageType: String) {
        getApiCommonCreatorImageURL(imageType: imageType) { (url, key) in
            
            guard let preSignedUrl = url else {
                print("Failed to get pre-signed URL.")
                return
            }
            
            guard let imageData = image.pngData() else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: url!, method: .put).response { response in
                switch response.result {
                case .success:
                    print("Image uploaded successfully.")
                    
                    isApiLoading = false
                case .failure(let error):
                    print("Failed to upload image: \(error)")
                }
            }
            
            
            guard let imageData = image.jpegData(compressionQuality: 1.0) else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: preSignedUrl).response { response in
                if let error = response.error {
                    print("Image upload failed with error: \(error)")
                } else {
                    print("Image uploaded successfully.")
                    switch imageType {
                    case "user":
                        creatorImageString = key!
                        
                        print("\(creatorImageString)")
                        
                    default:
                        print("Unknown image type: \(imageType)")
                    }
                }
            }
        }
    }
    
    
    //최종적으로 수정
    private func patchHumanResource() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any] = [
            "user_idx": selectedCreatorUserIdx,
             "position": "",
             "department": "",
             "user_name": creatorNameFor_DetailCreatorInfo,
             "user_email": creatorEmailFor_DetailCreatorInfo,
             "user_image": creatorImageString,
             "user_phone": creatorPhoneNumberFor_DetailCreatorInfo,
             "manage_type": transManageType(manageType: workAreaFor_DetailDetailCreatorInfo),
             "one_line_explain": oneLineExplanFor_DetailCreatorInfo
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/human-resource/\(selectedCreatorHumanResourceIdx)",
                   method: .patch,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("크리에이터 수정 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("크리에이터 수정 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("크리에이터 수정 실패: \(error)")
                
            }
        }
    }
    
    
    private func dateFormatter(getDate: String) -> String {
        
        let isoDateFormatter = ISO8601DateFormatter()
        isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

        let getDate = getDate
        if let date = isoDateFormatter.date(from: getDate) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy.MM.dd"
            let dateString = dateFormatter.string(from: date)
            print(dateString) // "2024.01.03" 출력
            return dateString
        } else {
            print("날짜 변환에 실패했습니다.")
            return ""
        }

    }
 
    //계약 가져오는 api
    func contractInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/human-resource-contract?human_resource_idx=\(selectedCreatorHumanResourceIdx)&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: EmployeeCreatorContractDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    
                    DispatchQueue.main.async {
                        humanResourceContractList = response.data
                        
                        humanResourceContractListCount = response.pagination.total
                        
                        print("계약 리스트 호출 성공 \(response.data)")
                        
                        if (humanResourceContractListCount <= 3) { //
                            showShowAllButton = false
                            showAllContractList = false
                        }else{
                            showShowAllButton = true //더 보기 버튼 보임
                            showAllContractList = false
                        }
                    }
                    
                    
                    
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
        })
        
    }
    
}

//#Preview {
//    DetailCreatorView()
//}
