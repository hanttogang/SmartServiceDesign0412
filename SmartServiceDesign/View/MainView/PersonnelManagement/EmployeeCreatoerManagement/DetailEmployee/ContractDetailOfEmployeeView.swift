//
//  ContractDetailOfEmployeeView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/27/23.
//

import SwiftUI
import Alamofire

struct ContractDetailOfEmployeeView: View {
    
    //Api
    @State private var humanResourceContractIdx: Int = 0
    
    var selectedEmployeeContractIdx: Int
    var selectedEmployeeContractName: String
    
    var selectedEmployeeUserIdx: Int
    var selectedEmployeeHumanResourceIdx: Int
    var detailEmployeeView: DetailEmployeeView
    
    // 이 init 메소드를 통해 외부에서 selectedCreatorContractIdx 값을 설정할 수 있습니다.
    init(selectedEmployeeContractIdx: Int, selectedEmployeeContractName: String, selectedEmployeeUserIdx: Int, selectedEmployeeHumanResourceIdx: Int) {
        self.selectedEmployeeContractIdx = selectedEmployeeContractIdx
        self.selectedEmployeeContractName = selectedEmployeeContractName
        
        self.selectedEmployeeUserIdx = selectedEmployeeUserIdx
        self.selectedEmployeeHumanResourceIdx = selectedEmployeeHumanResourceIdx
        self.detailEmployeeView = DetailEmployeeView(selectedEmployeeUserIdx: selectedEmployeeUserIdx, selectedEmployeeHumanResourceIdx: selectedEmployeeHumanResourceIdx)
        
    }
    
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var employeeContractImageString = ""
    
    @State private var isApiLoading: Bool = false
    
    //View
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    var selectedDetailContractNameFor_DetailEmployee: String = ""
    var selectedDetailEmployeeName: String = ""
    
    @State private var navigate: Bool = false
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var showToast: Bool = false
    @State private var toastText: String = "시작 날짜 선택 후 확인을 눌러주세요"
    
    @State private var modificationModeFor_DetailEmployeeContract: Bool = false
    
    
    @State private var contractNameFor_DetailEmployee: String = ""
    @State private var contractOneLineExplanFor_DetailEmployee: String = ""
    @State private var contractStartDateFor_DetailEmployeeContract: String = ""
    @State private var contractEndDateFor_DetailEmployeeContract: String = ""
    @State private var contractPriceFor_DetailEmployee: String = ""
    @State private var contractStatusFor_DetailEmployee: String = ""
    
    
    @State private var selecteContractStatusFor_DetailEmployee: Bool = false
    @State private var contractStatusModificationModeFor_DetailEmployee: Bool = false
    
    @State private var showingBusinessLicenseImagePickerFor_DetailEmployee = false
    @State private var selectedBusniessLicenseImageFor_DetailEmployee: UIImage? = nil
    
    //날짜 관련
    @State private var showingDatePicker = false
    @State private var dateSelection = Date()
    @State private var startDate: Date? = nil
    @State private var endDate: Date? = nil
    @State private var dateCount = 0
  
    
    var body: some View {
        
        ZStack {
            
            if (isApiLoading){
                
                ProgressView()
                    .zIndex(3)
                
            }
            
            ScrollView(.vertical){
                VStack(spacing: 0){
                    
                    HStack{
                        
                    
                        
                        if modificationModeFor_DetailEmployeeContract{
                            
                            TextField(".", text: $contractNameFor_DetailEmployee, prompt: Text("\(contractNameFor_DetailEmployee)")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.title2)
                            .frame(height: screenHeight/29) //28
                            
                            
                        }else {
                            
                            Text("\(contractNameFor_DetailEmployee)")
                                .font(.title2)
                                .frame(height: screenHeight/29) //28
                        }
                        
                        
                        
                        
                        
                        Spacer()
                        
                        if(modificationModeFor_DetailEmployeeContract){
                            Button(action: {
                                
                                if (!isApiLoading){
                                    
                                    if (contractNameFor_DetailEmployee == "" || contractOneLineExplanFor_DetailEmployee == "" || contractStartDateFor_DetailEmployeeContract == "" || contractEndDateFor_DetailEmployeeContract == "" || contractPriceFor_DetailEmployee == ""){
                                        
                                        showAlert = true
                                        
                                        alertTitle = "소속직원 계약 수정 실패"
                                        alertMessage = "칸을 비울 수 없습니다."
                                        
                                    } else {
                                        
                                        patchEmployeeContract(accessToken: loginData.token)
                                        
                                        detailEmployeeView.contractInquiry(accessToken: loginData.token)
                                        
                                        modificationModeFor_DetailEmployeeContract = false
                                    }
                                }
                                
                            }, label: {
                                Text("저장")
                                
                            })
                        } else {
                            Button(action: {
                                
                                modificationModeFor_DetailEmployeeContract = true
                                
                                
                            }, label: {
                                Text("수정")
                                
                            })
                            
                        }
                        
                        
                    }// HStack 계약 이름
                    .padding(.horizontal)
                    .padding(.vertical, screenHeight / 81.5)
//                    .padding(.top, screenHeight / 81.5)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        
                        Text("한줄 설명")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailEmployeeContract{
                            TextField("", text: $contractOneLineExplanFor_DetailEmployee)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                                
                            
                            
//                            RightAlignedTextField(text: $contractOneLineExplanFor_DetailEmployee)
//                                .keyboardType(.numberPad)
                            
                            
                            
                        }else {
                            
                            Text("\(contractOneLineExplanFor_DetailEmployee)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("계약기간")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailEmployeeContract{
//                            RightAlignedTextField(text: $typeOfBusinessForDetailBrand)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
                            
                            Button {
                                
                                startDate = nil
                                endDate = nil
                                
                                self.showingDatePicker = true
                                
                                toastText = "시작 날짜 선택 후 확인을 눌러주세요"
                                showToast = true
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                    self.showToast = false
                                    toastText = ""
                                }
                                
                            } label: {
                                
                                
                                Text("\(contractStartDateFor_DetailEmployeeContract) ~ \(contractEndDateFor_DetailEmployeeContract)")
                                    .foregroundColor(Color("color00000040"))

                            }

                            
                            
                        }else {
                            
                            Text("\(contractStartDateFor_DetailEmployeeContract) ~ \(contractEndDateFor_DetailEmployeeContract)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .sheet(isPresented: $showingDatePicker) {
                        VStack {
                            
                                DatePicker("날짜를 선택해주세요", selection: $dateSelection, displayedComponents: .date)
                                    .datePickerStyle(GraphicalDatePickerStyle())
                                    .padding()
                                    .overlay{
                                        ZStack{}
                                        .toast(isShowing: $showToast, text: Text(toastText))
                                    }
                            
                            
                            
                            Button("확인") {
                                if self.dateCount == 0 {
                                    self.startDate = dateSelection
                                    self.dateCount += 1
                                } else if self.dateCount == 1 {
                                    if dateSelection > self.startDate! {
                                        self.endDate = dateSelection
                                        self.dateCount = 0
                                    } else {
                                        self.startDate = nil
                                        self.endDate = nil
                                        self.dateCount = 0
                                    }
                                }
                                
                                
                                if (startDate != nil && endDate == nil){
                                    
                                    toastText = "종료 날짜 선택 후 확인을 주세요"
                                    showToast = true
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                        self.showToast = false
                                        toastText = ""
                                    }
                                    
                                    self.showingDatePicker = true
                                }
                                
                                
                                if (startDate != nil && endDate != nil){
                                    contractStartDateFor_DetailEmployeeContract = convertDateToString(date: startDate)
                                    contractEndDateFor_DetailEmployeeContract = convertDateToString(date: endDate)
                                    
                                    self.showingDatePicker = false
                                }else{
                                    contractStartDateFor_DetailEmployeeContract = ""
                                    contractEndDateFor_DetailEmployeeContract = ""
                                }
                            }
                        }
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{
                        Text("계약금액")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailEmployeeContract{
                            
                            
                            TextField("", text: $contractPriceFor_DetailEmployee)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                                .keyboardType(.numberPad)
                                
                            
                            
//                            RightAlignedTextField(text: $contractPriceFor_DetailEmployee)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
//
                            
                            
                        }else {
                            
                            Text("\(contractPriceFor_DetailEmployee)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack(spacing: 0){
                        Text("진행상태")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        
                        HStack(spacing: 0){
                            
                            if modificationModeFor_DetailEmployeeContract{
                                
                                
                                Button(action: {
                                    selecteContractStatusFor_DetailEmployee = true
                                }, label: {
                                    
                                    if selecteContractStatusFor_DetailEmployee{
                                        
                                        HStack(spacing: screenWidth/128.3){ // spacing 3
                                            Button(action: {
                                                selecteContractStatusFor_DetailEmployee = false
                                                
                                                contractStatusFor_DetailEmployee = "신규"
                                                
                                            }, label: {
                                                Text("신규")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailEmployee = false
                                                
                                                contractStatusFor_DetailEmployee = "갱신"
                                                
                                            }, label: {
                                                Text("갱신")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailEmployee = false
                                                
                                                contractStatusFor_DetailEmployee = "변경"
                                                
                                            }, label: {
                                                Text("변경")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailEmployee = false
                                                
                                                contractStatusFor_DetailEmployee = "완료"
                                                
                                            }, label: {
                                                Text("완료")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailEmployee = false
                                                
                                                contractStatusFor_DetailEmployee = "퇴사"
                                                
                                            }, label: {
                                                Text("퇴사")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                        } //HStack
                                        
                                        
                                    }else{
                                        
                                        Button(action: {
                                            selecteContractStatusFor_DetailEmployee = true
                                            
                                        }, label: {
                                            Text("\(contractStatusFor_DetailEmployee)")
                                                .foregroundColor(.white)
                                                .bold()
                                                .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                .background(Color("mainColor"))
                                                .cornerRadius(16.0)
                                        })
                                    }
                                    
                                })
                            } else{
                                
                                Text("\(contractStatusFor_DetailEmployee)")
                                    .foregroundColor(.white)
                                    .bold()
                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                    .background(Color("mainColor"))
                                    .cornerRadius(16.0)
                            }
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    Divider()
                        .padding(.leading)
                    
                   
                    
                    
                    
                    HStack(spacing: 0){
                        
                        
                        VStack(alignment: .leading){// 사업자등록증 이미지
                            
                            Text("계약서 이미지")
                                .padding(.top, screenHeight/30.75)
                            
                            ZStack{
                                
                                
                                if modificationModeFor_DetailEmployeeContract{
                                    
                                    Button(action: {
                                        showingBusinessLicenseImagePickerFor_DetailEmployee = true
                                    }, label: {
                                        
                                        ZStack {
                                            if let image = selectedBusniessLicenseImageFor_DetailEmployee {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                    .cornerRadius(12.0)
                                                
                                            } else {
                                                AsyncImage(url: URL(string: imageS3Url + "/" + employeeContractImageString)) { image in
                                                    image.resizable()
                                                         .aspectRatio(contentMode: .fill)
                                                         .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                         .cornerRadius(12.0)
                                                } placeholder: {
                                                    ProgressView()
                                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                }
                                                
                                            }
                                        }
                                    })
                                    
                                }else{
                                    
                                    ZStack {
                                        if let image = selectedBusniessLicenseImageFor_DetailEmployee {
                                            Image(uiImage: image)
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                .cornerRadius(12.0)
                                            
                                        } else {
                                            AsyncImage(url: URL(string: imageS3Url + "/" + employeeContractImageString)) { image in
                                                image.resizable()
                                                     .aspectRatio(contentMode: .fill)
                                                     .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                     .cornerRadius(12.0)
                                            } placeholder: {
                                                ProgressView()
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                            }
                                            
                                        }
                                    }
                                    
                                }
                            } //ZStack 계약 이미지
                            .sheet(isPresented: $showingBusinessLicenseImagePickerFor_DetailEmployee, onDismiss: loadBusniessLicenseImageFor_DetailEmployee) {
                                ImagePicker(selectedImage: $selectedBusniessLicenseImageFor_DetailEmployee)
                            }
                            
                            
                            
                        }//VStack 계약 이미지
                        
                        
                        
                        Spacer()
                        
                        
                      
                    }//HStack
                    .padding(.leading)
                    
                    Spacer()
                    
                } //VStack
                
            }//ScrollView
        }//ZStack
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            
            self.presentationMode.wrappedValue.dismiss()
            
            detailEmployeeView.contractInquiry(accessToken: loginData.token)
            
        }) {
            HStack(spacing: 0){
                Image(systemName: "chevron.backward")
            }
        })
        .navigationBarTitle("\(selectedDetailEmployeeName)", displayMode: .inline)
        .onAppear{
            contractInquiry()
        }
        
      
        
        
        
    }//body

    
  
    private func loadBusniessLicenseImageFor_DetailEmployee() {
        guard let selectedBusniessLicenseImageFor_DetailEmployee = selectedBusniessLicenseImageFor_DetailEmployee else { return }
        
        isApiLoading = true
        
        uploadImage(image: selectedBusniessLicenseImageFor_DetailEmployee, imageType: "human_resource_contract")
    }
    
    //Api 를통해 필요한 값 호출 (url, key 값)
    private func getApiCommonEmployeeContractImageURL(imageType: String, completion: @escaping (String?, String?) -> Void) {
        
        let headers: HTTPHeaders = [
            "accept": "application/json",
            "Authorization": "Bearer \(loginData.token)"
        ]
        
        let parameters: Parameters = [
            "user_idx": selectedEmployeeContractIdx,
            "mimetype": "image/png",
            "type": "human_resource_contract",
            "extension": "png"
        ]
        
        AF.request("\(defaultUrl)/api/common/image/url", method: .get, parameters: parameters, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    let url = JSON["data"] as? String
                    let key = JSON["key"] as? String
                    completion(url, key)
                }
            case .failure(let error):
                print(error)
                completion(nil, nil)
            }
        }
    }
    
    //사진 선택시 uploadImag 함수를 호출
    private func uploadImage(image: UIImage, imageType: String) {
        getApiCommonEmployeeContractImageURL(imageType: imageType) { (url, key) in
            
            guard let preSignedUrl = url else {
                print("Failed to get pre-signed URL.")
                return
            }
            
            guard let imageData = image.pngData() else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: url!, method: .put).response { response in
                switch response.result {
                case .success:
                    print("Image uploaded successfully.")
                    
                    isApiLoading = false
                case .failure(let error):
                    print("Failed to upload image: \(error)")
                }
            }
            
            
            guard let imageData = image.jpegData(compressionQuality: 1.0) else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: preSignedUrl).response { response in
                if let error = response.error {
                    print("Image upload failed with error: \(error)")
                } else {
                    print("Image uploaded successfully.")
                    switch imageType {
                    case "human_resource_contract":
                        employeeContractImageString = key!
                        
                        print("\(employeeContractImageString)")
                        
                    default:
                        print("Unknown image type: \(imageType)")
                    }
                }
            }
        }
    }
    
    
    
    
    
    private func dateFormatter(getDate: String) -> String {
        
        let isoDateFormatter = ISO8601DateFormatter()
        isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

        let getDate = getDate
        if let date = isoDateFormatter.date(from: getDate) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateString = dateFormatter.string(from: date)
//            print(dateString) //ex "2024.01.05" 출력
            return dateString
        } else {
            print("날짜 변환에 실패했습니다.")
            return getDate
        }

    }
    
    
    
    //계약 가져오는 api
    private func contractInquiry() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        AF.request("\(defaultUrl)/admin/api/human-resource-contract?human_resource_idx=\(selectedEmployeeContractIdx)&contract_name=\(selectedEmployeeContractName)&page=1&limit=10", method: .get, headers: headers).responseDecodable(of: EmployeeCreatorContractDataResponse.self) { response in
            switch response.result {
    
            case .success(let response):
                // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                
                for employeeContractData in response.data {
                    contractNameFor_DetailEmployee = employeeContractData.contractName
                    contractOneLineExplanFor_DetailEmployee = employeeContractData.oneLineExplain
                    contractStartDateFor_DetailEmployeeContract = dateFormatter(getDate: employeeContractData.contractStartDt)
                    contractEndDateFor_DetailEmployeeContract = dateFormatter(getDate: employeeContractData.contractEndDt)
                    contractStatusFor_DetailEmployee = employeeContractData.contractStatus
                    contractPriceFor_DetailEmployee = String(employeeContractData.contractAmount)
                    
                    employeeContractImageString = employeeContractData.contractImage
                    
                    humanResourceContractIdx = employeeContractData.humanResourceContractIdx
                    
                }
                
                
//
//                DispatchQueue.main.async {
////                    contractDataList = response.data
////
////                    contractCount = response.pagination.total
//
////                    print("\(selectedCustomerBrandContractIdx) 계약 리스트 호출 성공 \(response.data)")
////
//                }
                
                
                
                
            case .failure(let error):
                // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                print(error)
            }
        }
    }
    
    //최종적으로 계약 수정
    private func patchEmployeeContract(accessToken: String) {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any] = [
            "human_resource_idx": humanResourceContractIdx,
            "contract_name": contractNameFor_DetailEmployee,
              "one_line_explain": contractOneLineExplanFor_DetailEmployee,
              "contract_start_dt": contractStartDateFor_DetailEmployeeContract,
              "contract_end_dt": contractEndDateFor_DetailEmployeeContract,
              "contract_status": contractStatusFor_DetailEmployee,
              "contract_image": employeeContractImageString,
              "contract_amount": Int(contractPriceFor_DetailEmployee)
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/human-resource-contract/\(humanResourceContractIdx)",
                   method: .patch,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("크리에이터 계약 수정 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("크리에이터 계약 수정 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("크리에이터 계약 수정 실패: \(error)")
                print(humanResourceContractIdx)
                
            }
        }
    }
    
    
    
    func convertDateToString(date: Date?) -> String {
        guard let date = date else {
            return ""
        }

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
}
//
//#Preview {
//    ContractDetailOfEmployeeView()
//}
