//
//  RegistrationCreatorView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/27/23.
//

import SwiftUI
import Alamofire

private enum RegistrationCreatorWorkArea: CaseIterable {
    case advertisingMarketing
    case mcn
    case anotherExample
}

struct RegistrationCreatorView: View {
    
    let mainEmployeeAndCreatorManagementView = MainEmployeeAndCreatorManagementView()
    //Api
    @EnvironmentObject var searchViewModel: SearchViewModel
    @EnvironmentObject var loginData: LoginData
    
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var creatorImageString: String = ""
    
    @State private var isApiLoading: Bool = false
    
    //View
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var userOneLineExplanFor_CreatorManagement: String = ""
    
    @State private var showingUserImagePickerFor_CreatorRegistration = false
    @State private var selectedUserImageFor_CreatorRegistration: UIImage? = nil
    
    
    @State private var selectedRegistrationCreatorWorkArea: RegistrationCreatorWorkArea = .advertisingMarketing
    @State private var selectedRegistrationCreatorWorkAreaString: String = "marketing"
    
    @State private var navigate: Bool = false
    
    
    
    var body: some View {
        
        
        ZStack{
            
            if (isApiLoading){
                
                ProgressView()
                    .zIndex(3)
                
            }
            
            ScrollView{
                
                //                HStack(spacing: 0) {
                //                    ForEach(UserTypeInRegistrationCreatorView.allCases, id: \.self) { flavor in
                //                        Button(action: {
                //                            selectedUserType = flavor
                //                            userTypeText = "\(flavor)"
                //                            print("\(flavor)")
                //                        }) {
                //                            Text(flavor == .employee ? "소속직원" : "크리에이터")
                //                                .frame(width: screenWidth/2.272, height: screenHeight/23.97)
                //                                .font(.system(size: 16))
                //
                //
                //
                //                        }
                //                        .background(selectedUserType == flavor ? Color.blue : Color.clear)
                //                        .foregroundColor(selectedUserType == flavor ? .white : .blue)
                //                        .cornerRadius(4)
                //                        .overlay(RoundedRectangle(cornerRadius: 4)
                //                            .stroke(Color("mainColor"), lineWidth: 1)
                //                        )
                //
                //
                //
                //
                //                    }
                //                }
                //                .overlay(RoundedRectangle(cornerRadius: 4)
                //                    .stroke(Color("mainColor"), lineWidth: 1)
                //                )
                //                .padding(.top, screenHeight / 25.46875) //32
                
                
                VStack(spacing: 0){
                    
                    
                    VStack(){
                        
                        HStack(spacing: 0){
                            Text("이름")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        HStack{
                            HStack(spacing: 0){
                                Text("\(searchViewModel.userName)")
                                    .foregroundColor(Color("hintTextColor"))
                                    .font(.system(size: 14))
                                    .padding(.leading, 14)
                                
                                Spacer()
                                
                            }
                            .frame(width: screenWidth / 1.358, height: screenHeight / 18.45)
                            .background(Color("colorF8F8F8"))
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                            )
                            
                            Spacer()
                            
                            Button(action: {
                                withAnimation(.none){
                                    searchViewModel.navigateECUserSearchView = true
                                    searchViewModel.userType = "creator"
                                }
                                
                            }, label: {
                                Circle()
                                    .frame(width: screenWidth/10.135)
                                    .foregroundColor(Color("mainColor"))
                                    .overlay{
                                        Image(systemName: "magnifyingglass")
                                            .foregroundColor(.white)
                                    }
                            })
                            
                            Spacer()
                            
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                    }//VStack 이름
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    
                    
                    
                    
                    VStack{ // 한줄 설명
                        
                        HStack(spacing: 0){
                            Text("한줄 설명")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $userOneLineExplanFor_CreatorManagement, prompt: Text("한줄 설명을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 담당부서
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("전화번호")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            Text("\(searchViewModel.userPhoneNum)")
                                .foregroundColor(Color("hintTextColor"))
                                .keyboardType(.phonePad)
                                .font(.system(size: 14))
                                .padding(.leading, 14)
                            
                            Spacer()
                            
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .background(Color("colorF8F8F8"))
                        .cornerRadius(4)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 전화번호
                    .padding(.top, screenHeight / 54.133) //15
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("이메일")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            Text("\(searchViewModel.userEmail)")
                                .foregroundColor(Color("hintTextColor"))
                                .font(.system(size: 14))
                                .padding(.leading, 14)
                            
                            Spacer()
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .background(Color("colorF8F8F8"))
                        .cornerRadius(4)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이메일
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    VStack{//업무영역
                        
                        HStack(spacing: 0){
                            Text("업무영역")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack(spacing: 0) {
                            
                            Button(action: {
                                
                                selectedRegistrationCreatorWorkArea = .advertisingMarketing
                                selectedRegistrationCreatorWorkAreaString = "marketing"
                                
                            }) {
                                Text("광고마케팅")
                                    .frame(width: screenWidth/3.26, height: screenHeight/23.97)
                                    .font(.system(size: 16))
                                
                                
                            }
                            .background(selectedRegistrationCreatorWorkArea == .advertisingMarketing ? Color.blue : Color.clear)
                            .foregroundColor(selectedRegistrationCreatorWorkArea == .advertisingMarketing ? .white : .blue)
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("mainColor"), lineWidth: 1)
                            )
                            
                            Button(action: {
                                
                                selectedRegistrationCreatorWorkArea = .mcn
                                selectedRegistrationCreatorWorkAreaString = "mcn"
                            }) {
                                Text("MCN")
                                    .frame(width: screenWidth/3.26, height: screenHeight/23.97)
                                    .font(.system(size: 16))
                                
                                
                            }
                            .background(selectedRegistrationCreatorWorkArea == .mcn ? Color.blue : Color.clear)
                            .foregroundColor(selectedRegistrationCreatorWorkArea == .mcn ? .white : .blue)
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("mainColor"), lineWidth: 1)
                            )
                            
                            Button(action: {
                                
                                selectedRegistrationCreatorWorkArea = .anotherExample
                                selectedRegistrationCreatorWorkAreaString = "etc"
                                
                            }) {
                                Text("기타")
                                    .frame(width: screenWidth/3.26, height: screenHeight/23.97)
                                    .font(.system(size: 16))
                                
                                
                            }
                            .background(selectedRegistrationCreatorWorkArea == .anotherExample ? Color.blue : Color.clear)
                            .foregroundColor(selectedRegistrationCreatorWorkArea == .anotherExample ? .white : .blue)
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("mainColor"), lineWidth: 1)
                            )
                            
                            
                            
                        }//HStack
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("mainColor"), lineWidth: 1)
                        )
                        
                        
                        
                    } //VStack 업무 영역
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    
                    
                    Spacer()
                    
                    
                    VStack(alignment: .leading){//대표 이미지
                        
                        HStack(spacing: 0){
                            Text("대표 이미지")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        
                        Button(action: {
                            
                                showingUserImagePickerFor_CreatorRegistration = true
                            
                            
                        }, label: {
                            
                            ZStack {
                                if let image = selectedUserImageFor_CreatorRegistration {
                                    Image(uiImage: image)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                } else {
                                    Rectangle()
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                        .foregroundColor(.colorE5E5E5)
                                    
                                    Circle()
                                        .frame(width: screenWidth/10.416)
                                        .foregroundColor(.colorEFEFF4)
                                        .overlay{
                                            Image(systemName: "plus")
                                                .imageScale(.medium)
                                        }
                                }
                            }
                            
                        })
                        .padding(.leading, screenWidth / 18.75)
                        
                    }//VStack 대표 이미지
                    .padding(.top, screenHeight / 54.133) //20    54.133 = 15
                    .sheet(isPresented: $showingUserImagePickerFor_CreatorRegistration, onDismiss: loadUserImageForCreatorRegistration) {
                        ImagePicker(selectedImage: $selectedUserImageFor_CreatorRegistration)
                    }
                    
                    HStack{
                        
                        Button(action: {
                            
                            if (!isApiLoading){
                                
                                if(searchViewModel.userName == "" || userOneLineExplanFor_CreatorManagement == "" || creatorImageString == ""){
                                    
                                    showAlert = true
                                    
                                    alertTitle = "크리에이터 등록 실패"
                                    alertMessage = "칸을 비울 수 없습니다."
                                    
                                }else{
                                    addCreator()
                                    
                                    mainEmployeeAndCreatorManagementView.creatorInquiry(accessToken: loginData.token)
                                    
                                    self.presentationMode.wrappedValue.dismiss()
                                }
                                
                            }
                            
                            
                            
                        }, label: {
                            
                            Text("크리에이터 등록")
                                .foregroundColor(.white)
                                .font(.system(size: 16))
                                .bold()
                                .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/67.9166) // 37
                        
                        
                        
                    }
                    
                    //하단 네비게이션을 위한 패딩
                    HStack{
                        Rectangle()
                            .frame(width: 0, height: 0)
                            .foregroundColor(.white)
                    }
                    .padding(.bottom, screenHeight/8.826)
                    
                    
                    Spacer()
                    
                    
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    
                    
                    mainEmployeeAndCreatorManagementView.creatorInquiry(accessToken: loginData.token)
                    
                    self.presentationMode.wrappedValue.dismiss()
                    
                    
                }) {
                    
                    HStack(spacing: 0){
                        
                        Image(systemName: "chevron.backward")
                        
                    }
                    
                    
                    
                })
                .navigationBarTitle("크리에이터 등록", displayMode: .inline)
                
                
            }//ScrollView
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
        }//ZStack
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
    }
    
    private func loadUserImageForCreatorRegistration() {
        guard let selectedUserImageFor_CreatorRegistration = selectedUserImageFor_CreatorRegistration else { return }
        
        isApiLoading = true
        
        uploadImage(image: selectedUserImageFor_CreatorRegistration, imageType: "user")
    }
    
    
    //Api 를통해 필요한 값 호출 (url, key 값)
    private func getApiCommonCreatorImageURL(imageType: String, completion: @escaping (String?, String?) -> Void) {
        
        let headers: HTTPHeaders = [
            "accept": "application/json",
            "Authorization": "Bearer \(loginData.token)"
        ]
        
        let parameters: Parameters = [
            "user_idx": searchViewModel.userIdx,
            "mimetype": "image/png",
            "type": "user",
            "extension": "png"
        ]
        
        AF.request("\(defaultUrl)/api/common/image/url", method: .get, parameters: parameters, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    let url = JSON["data"] as? String
                    let key = JSON["key"] as? String
                    completion(url, key)
                }
            case .failure(let error):
                print(error)
                completion(nil, nil)
            }
        }
    }
    
    //사진 선택시 uploadImag 함수를 호출
    private func uploadImage(image: UIImage, imageType: String) {
        getApiCommonCreatorImageURL(imageType: imageType) { (url, key) in
            
            guard let preSignedUrl = url else {
                print("Failed to get pre-signed URL.")
                return
            }
            
            guard let imageData = image.pngData() else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: url!, method: .put).response { response in
                switch response.result {
                case .success:
                    print("Image uploaded successfully.")
                    
                    isApiLoading = false
                case .failure(let error):
                    print("Failed to upload image: \(error)")
                }
            }
            
            
            guard let imageData = image.jpegData(compressionQuality: 1.0) else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: preSignedUrl).response { response in
                if let error = response.error {
                    print("Image upload failed with error: \(error)")
                } else {
                    print("Image uploaded successfully.")
                    switch imageType {
                    case "user":
                        creatorImageString = key!
                        
                        print("\(creatorImageString)")
                        
                    default:
                        print("Unknown image type: \(imageType)")
                    }
                }
            }
        }
    }
    
    
    //최종적으로 등록
    private func addCreator() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any?] = [
            
            "user_idx": searchViewModel.userIdx,
              "position": "",
              "department": "",
            "user_name": searchViewModel.userName,
            "user_email": searchViewModel.userEmail,
              "user_image": creatorImageString,
            "user_phone": searchViewModel.userPhoneNum,
              "manage_type": selectedRegistrationCreatorWorkAreaString,
            "one_line_explain": userOneLineExplanFor_CreatorManagement
            
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/human-resource",
                   method: .post,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("크리에이터 등록 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("크리에이터 등록 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    self.navigate = true
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("크리에이터 등록 실패: \(error)")
                
            }
        }
    }
}
//
//#Preview {
//    RegistrationCreatorView().environmentObject(SearchViewModel())
//}
