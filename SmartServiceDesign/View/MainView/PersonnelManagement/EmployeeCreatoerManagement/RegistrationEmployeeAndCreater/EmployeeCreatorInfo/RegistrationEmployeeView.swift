//
//  RegistrationEmployeeView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/26/23.
//

import SwiftUI
import Alamofire

struct RegistrationEmployeeView: View {
    
    
    let mainEmployeeAndCreatorManagementView = MainEmployeeAndCreatorManagementView()
    
    
    @EnvironmentObject private var loginData: LoginData
    @EnvironmentObject private var searchViewModel: SearchViewModel
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var employeeImageString: String = ""
    
    @State private var isApiLoading: Bool = false
    //View
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var userTypeText: String = ""
    
    
    @State private var userPositionFor_EmployeeAndCreatorManagement: String = ""
    
    @State private var userDepartmentFor_EmployeeAndCreatorManagement: String = ""
    
    
    @State private var showingUserImagePickerFor_EmployeeAndCreatorRegistration = false
    @State private var selectedUserImageFor_EmployeeAndCreatorRegistration: UIImage? = nil
    
    private let registrationEmpCreatorContainer = RegistrationEmpCreatorContainer()
    
    @State private var navigate: Bool = false
    
    var body: some View {
        
        
        ZStack{
            
            if (isApiLoading){
                
                ProgressView()
                    .zIndex(3)
                
            }
            
            ScrollView{
                
                VStack(spacing: 0){
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("이름")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        HStack{
                            HStack(spacing: 0){
                                Text("\(searchViewModel.userName)")
                                    .foregroundColor(Color("hintTextColor"))
                                    .font(.system(size: 14))
                                    .padding(.leading, 14)
                                
                                Spacer()
                                
                            }
                            .frame(width: screenWidth / 1.358, height: screenHeight / 18.45)
                            .background(Color("colorF8F8F8"))
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                            )
                            
                            Spacer()
                            
                            Button(action: {
                                searchViewModel.navigateECUserSearchView = true
                                searchViewModel.userType = "employee"
                            }, label: {
                                Circle()
                                    .frame(width: screenWidth/10.135)
                                    .foregroundColor(Color("mainColor"))
                                    .overlay{
                                        Image(systemName: "magnifyingglass")
                                            .foregroundColor(.white)
                                    }
                            })
                            
                            Spacer()
                            
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                    }//VStack 이름
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("직위")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $userPositionFor_EmployeeAndCreatorManagement, prompt: Text("직위를 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 직위
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("담당부서")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $userDepartmentFor_EmployeeAndCreatorManagement, prompt: Text("담당부서를 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 담당부서
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("전화번호")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            Text("\(searchViewModel.userPhoneNum)")
                                .foregroundColor(Color("hintTextColor"))
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            
                            Spacer()
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .background(Color("colorF8F8F8"))
                        .cornerRadius(4)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 전화번호
                    .padding(.top, screenHeight / 54.133) //15
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("이메일")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            Text("\(searchViewModel.userEmail)")
                                .foregroundColor(Color("hintTextColor"))
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            
                            Spacer()
                            
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .background(Color("colorF8F8F8"))
                        .cornerRadius(4)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이메일
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    Spacer()
                    
                    
                    VStack(alignment: .leading){//계약서 등록
                        
                        HStack(spacing: 0){
                            Text("대표 이미지")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        
                        Button(action: {
                            showingUserImagePickerFor_EmployeeAndCreatorRegistration = true
                        }, label: {
                            
                            ZStack {
                                if let image = selectedUserImageFor_EmployeeAndCreatorRegistration {
                                    Image(uiImage: image)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                } else {
                                    Rectangle()
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                        .foregroundColor(.colorE5E5E5)
                                    
                                    Circle()
                                        .frame(width: screenWidth/10.416)
                                        .foregroundColor(.colorEFEFF4)
                                        .overlay{
                                            Image(systemName: "plus")
                                                .imageScale(.medium)
                                        }
                                }
                            }
                            
                        })
                        .padding(.leading, screenWidth / 18.75)
                        
                    }//VStack 대표 이미지
                    .padding(.top, screenHeight / 54.133) //20    54.133 = 15
                    .sheet(isPresented: $showingUserImagePickerFor_EmployeeAndCreatorRegistration, onDismiss: loadUserImageForEmployeeAndCreatorRegistration) {
                        ImagePicker(selectedImage: $selectedUserImageFor_EmployeeAndCreatorRegistration)
                    }
                    
                    HStack{
                        
                        Button(action: {
                            
                            if (!isApiLoading) {
                                
                                if(searchViewModel.userName == "" || userPositionFor_EmployeeAndCreatorManagement == "" || userDepartmentFor_EmployeeAndCreatorManagement == ""){
                                    
                                    showAlert = true
                                    
                                    alertTitle = "직원 등록 실패"
                                    alertMessage = "칸을 비울 수 없습니다."
                                    
                                }else{
                                    
                                    
                                    addEmployee()
                                    
                                    mainEmployeeAndCreatorManagementView.empInquiry(accessToken: loginData.token)
                                    self.presentationMode.wrappedValue.dismiss()
                                }
                            }
                            
                             
                            
                            
                        }, label: {
                            
                                Text("직원 등록")
                                    .foregroundColor(.white)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/67.9166) // 37
                        
                        
                        
                    }
                    
                    
                    //하단 네비게이션을 위한 패딩
                    HStack{
                        Rectangle()
                            .frame(width: 0, height: 0)
                            .foregroundColor(.white)
                    }
                    .padding(.bottom, screenHeight/8.826)
                    
                    
                    
                    Spacer()
                    
                    
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    
                    mainEmployeeAndCreatorManagementView.empInquiry(accessToken: loginData.token)
                    self.presentationMode.wrappedValue.dismiss()
                    
                    
                }) {
                    
                    HStack(spacing: 0){
                        
                        Image(systemName: "chevron.backward")
                        
                    }
                    
                    
                    
                })
                .navigationBarTitle("직원 등록", displayMode: .inline)
                
                
            }//ScrollView
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
        }//ZStack
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        
    }
    
    private func loadUserImageForEmployeeAndCreatorRegistration() {
        guard let selectedUserImageFor_EmployeeAndCreatorRegistration = selectedUserImageFor_EmployeeAndCreatorRegistration else { return }
        // You can do something with the selected image here
        isApiLoading = true
        
        uploadImage(image: selectedUserImageFor_EmployeeAndCreatorRegistration, imageType: "user")
    }
    
    
    
    
    
    //Api 를통해 필요한 값 호출 (url, key 값)
    private func getApiCommonEmployeeImageURL(imageType: String, completion: @escaping (String?, String?) -> Void) {
        
        let headers: HTTPHeaders = [
            "accept": "application/json",
            "Authorization": "Bearer \(loginData.token)"
        ]
        
        let parameters: Parameters = [
            "user_idx": searchViewModel.userIdx,
            "mimetype": "image/png",
            "type": "user",
            "extension": "png"
        ]
        
        AF.request("\(defaultUrl)/api/common/image/url", method: .get, parameters: parameters, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    let url = JSON["data"] as? String
                    let key = JSON["key"] as? String
                    completion(url, key)
                }
            case .failure(let error):
                print(error)
                completion(nil, nil)
            }
        }
    }
    
    //사진 선택시 uploadImag 함수를 호출
    private func uploadImage(image: UIImage, imageType: String) {
        getApiCommonEmployeeImageURL(imageType: imageType) { (url, key) in
            
            guard let preSignedUrl = url else {
                print("Failed to get pre-signed URL.")
                return
            }
            
            guard let imageData = image.pngData() else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: url!, method: .put).response { response in
                switch response.result {
                case .success:
                    print("Image uploaded successfully.")
                    
                    isApiLoading = false
                case .failure(let error):
                    print("Failed to upload image: \(error)")
                }
            }
            
            
            guard let imageData = image.jpegData(compressionQuality: 1.0) else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: preSignedUrl).response { response in
                if let error = response.error {
                    print("Image upload failed with error: \(error)")
                } else {
                    print("Image uploaded successfully.")
                    switch imageType {
                    case "user":
                        employeeImageString = key!
                        
                        print("\(employeeImageString)")
                        
                    default:
                        print("Unknown image type: \(imageType)")
                    }
                }
            }
        }
    }
    
    
    //최종적으로 등록
    private func addEmployee() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any?] = [
            
            "user_idx": searchViewModel.userIdx,
              "position": userPositionFor_EmployeeAndCreatorManagement,
              "department": userDepartmentFor_EmployeeAndCreatorManagement,
            "user_name": searchViewModel.userName,
            "user_email": searchViewModel.userEmail,
              "user_image": employeeImageString,
            "user_phone": searchViewModel.userPhoneNum,
              "manage_type": nil,
            "one_line_explain": ""
            
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/human-resource",
                   method: .post,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("소속 직원 등록 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("소속 직원 등록 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    self.navigate = true
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("소속 직원 등록 실패: \(error)")
                
            }
        }
    }
    
}

#Preview {
    RegistrationEmployeeView()
}
