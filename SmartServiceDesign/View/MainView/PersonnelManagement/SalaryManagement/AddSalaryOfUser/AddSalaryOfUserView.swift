//
//  AddSalaryOfUserView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/21/24.
//

import SwiftUI
import Alamofire

struct AddSalaryOfUserView: View {
    
    let salaryManagementMainView = SalaryManagementMainView()
    
    //Api
    @State private var userImageString: String = ""
    @State private var userNameFor_DetailEmployeeInfo: String = ""
    @State private var userPositionFor_DetailEmployeeInfo: String = ""
    
    var selectedUserIdx: Int
    var selectedUserType: String
    
    // 이 init 메소드를 통해 외부에서 selectedCustomerBrandIdx 값을 설정할 수 있습니다.
    init(selectedUserIdx: Int, selectedUserType: String) {
        self.selectedUserIdx = selectedUserIdx
        self.selectedUserType = selectedUserType
    }
    
    
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var userContractImageString: String = ""
    
    @State private var isApiLoading: Bool = false
    
    //View
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var showToast: Bool = false
    @State private var toastText: String = "시작 날짜 선택 후 확인을 눌러주세요"
    
    @State private var salaryPriceForEmployee: String = ""
    @State private var salaryDayForEmployee: String = ""
    @State private var bankNumber: String = ""
    @State private var userSalaryStatusString: String = "신규"
    
    @State private var selectedBank: Int? = nil
    @State private var bankNameList: [String] = []
//    let myPets = ["개", "고양이", "토끼", "코끼리", "기린", "돌고래", "소", "쥐", "호랑이", "용", "뱀", "말", "양", "원숭이", "돼지"]
    @State private var showingActionSheet = false
    
    @State private var showingImagePickerContractImageFor_AddSalary = false
    @State private var selectedContractImageFor_AddSalary: UIImage? = nil
    
    //    //날짜 관련
    //    @State private var showingDatePicker = false
    //    @State private var dateSelection = Date()
    //    @State private var startDate: Date? = nil
    //    @State private var endDate: Date? = nil
    //    @State private var dateCount = 0
    let years = Array(2000...2099)
    let months = Array(1...12)
    
    @State private var selectedYear = Calendar.current.component(.year, from: Date())
    @State private var selectedMonth = Calendar.current.component(.month, from: Date())
    
    @State private var showingImagePickerCopyBankImageFor_AddUserSalary = false
    @State private var selectedCopyBankImage: UIImage? = nil
    @State private var userCopyBankImageString: String = ""
    
    @State private var navigate: Bool = false
    
    var body: some View {
        
        
        ZStack{
            
            if (isApiLoading){
                
                ProgressView()
                    .zIndex(3)
                
            }
            
            ScrollView{
                
                VStack(spacing: 0){
                    
                        
                        
                        HStack(){
                            
                            
                            VStack(alignment: .leading){// 브랜드 이미지
                                ZStack{
                                    ZStack {
                                       
                                            AsyncImage(url: URL(string: imageS3Url + "/" + userImageString)) { image in
                                                image.resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                    .cornerRadius(12.0)
                                            }placeholder: {
                                                ProgressView()
                                                    .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                            }
                                        
                                    }
                                    
                                } //ZStack BrandImage
                                
                            }//VStack 브랜드 이미지
                            
                            HStack{
                                
                                //직원이름
                                
                                Text("\(userNameFor_DetailEmployeeInfo)")
                                    .font(.title2)
                            }
                            HStack{
                                
                                Text("\(userPositionFor_DetailEmployeeInfo)")
                                    .font(.title2)
                                    .frame(width: screenWidth/7.8125)//48
                                
                            }
                            
                            Spacer()
                            
                            if (selectedUserType == "employee"){
                                
                                Text("소속직원")
                                    .foregroundColor(.white)
                                    .bold()
                                    .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                    .background(Color("mainColor"))
                                    .cornerRadius(16.0)
                            }else{
                                
                                Text("크리에이터")
                                    .foregroundColor(.white)
                                    .bold()
                                    .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                    .background(Color("mainColor"))
                                    .cornerRadius(16.0)
                            }
                            
                            
                        }
                        .padding()
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("급여액")
                                .font(.system(size: 14))
                            
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $salaryPriceForEmployee, prompt: Text("급여액을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.phonePad)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 급여액
                    .padding(.top, screenHeight / 50.75) //16
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("급여일")
                                .font(.system(size: 14))
                            
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        HStack{
                            
                            Text("매달")
                                .font(.system(size: 14))
                            
                            HStack{
                                
                                TextField("", text: $salaryDayForEmployee, prompt: Text("급여일을 입력해주세요.")
                                    .foregroundColor(Color("hintTextColor")))
                                .keyboardType(.phonePad)
                                .font(.system(size: 14))
                                .padding(.leading, 14)
                            }
                            .frame(width: screenWidth / 1.3586, height: screenHeight / 18.45)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                            )
                        }
                        
                        
                    }//VStack 급여일
                    .padding(.top, screenHeight / 50.75) //16
                    
                    
                    
                    
                    
                    VStack{ // 은행명
                        
                        
                        
                        HStack(spacing: 0){
                            Text("은행명")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        
                        HStack{
                            
                            Button(action: {
                                withAnimation {
                                    showingActionSheet = true
                                }
                            }) {
                                
                                HStack{
                                    Text("\(selectedBank != nil ? bankNameList[selectedBank!] : "--선택--")")
                                        .foregroundColor(Color("hintTextColor"))
                                        .font(.system(size: 14))
                                        .padding(.leading, 14)
                                    
                                    Spacer()
                                }
                            }
                            .actionSheet(isPresented: $showingActionSheet) {
                                ActionSheet(title: Text("은행 선택"), buttons: bankNameList.indices.map { i in
                                        .default(Text(bankNameList[i])) { selectedBank = i }
                                })
                            }
                            .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                            )
                            
                            
                            
                            
                        }
                        
                        
                    }//VStack
                    .padding(.top, screenHeight / 50.75) //16
                    
                    
                    VStack{//계약 금액
                        
                        HStack(spacing: 0){
                            Text("통장번호")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $bankNumber, prompt: Text("통장번호를 입력해주세요")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.phonePad)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 계약 금액
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    
                    VStack(alignment: .leading){//계약서 등록
                        
                        HStack(spacing: 0){
                            Text("통장 사본")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        
                        Button(action: {
                            showingImagePickerCopyBankImageFor_AddUserSalary = true
                        }, label: {
                            
                            ZStack {
                                if let image = selectedCopyBankImage {
                                    Image(uiImage: image)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                } else {
                                    Rectangle()
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                        .foregroundColor(.colorE5E5E5)
                                    
                                    Circle()
                                        .frame(width: screenWidth/10.416)
                                        .foregroundColor(.colorEFEFF4)
                                        .overlay{
                                            Image(systemName: "plus")
                                                .imageScale(.medium)
                                        }
                                }
                            }
                            
                        })
                        .padding(.leading, screenWidth / 18.75)
                        
                    }//VStack 계약서 등록 이미지
                    .padding(.top, screenHeight / 54.133) //20    54.133 = 15
                    .sheet(isPresented: $showingImagePickerCopyBankImageFor_AddUserSalary, onDismiss: loadCopyBankImageForAddUserSalary) {
                        ImagePicker(selectedImage: $selectedCopyBankImage)
                    }
                    
                    
                    
                    
                   
                   HStack{
                       
                       Button(action: {
                           
                           if (!isApiLoading){
                               
                               if(salaryPriceForEmployee == "" || salaryDayForEmployee == "" || selectedBank == nil || bankNumber == "" || userCopyBankImageString == "" ){
                                   
                                   showAlert = true
                                   
                                   alertTitle = "급여 등록 실패"
                                   alertMessage = "칸을 비울 수 없습니다."
                                   
                               }else{
                                   if( 1 <= Int(salaryDayForEmployee)! && Int(salaryDayForEmployee)! <= 31){
                                       
                                       addSalary()
                                       
                                       self.presentationMode.wrappedValue.dismiss()
                                       
                                       salaryManagementMainView.creatorInquiry(accessToken: loginData.token)
                                       salaryManagementMainView.empInquiry(accessToken: loginData.token)
                                       
                                   }else{
                                       showAlert = true
                                       
                                       alertTitle = "급여 등록 실패"
                                       alertMessage = "급여일을 확인해주세요."
                                   }
                               }
                           }
                           
                       }, label: {
                           
                           Text("급여 등록")
                               .foregroundColor(.white)
                               .font(.system(size: 16))
                               .bold()
                               .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                           
                       })
                       .background(Color("mainColor"))
                       .cornerRadius(4)
                       .padding(.top, screenHeight/25.375) //32
                       
                       
                       
                   }
                    
                    //하단 네비게이션을 위한 패딩
                    HStack{
                        Rectangle()
                            .frame(width: 0, height: 0)
                            .foregroundColor(.white)
                    }
                    .padding(.bottom, screenHeight/8.826)
                    
                    
                    
                    
                    Spacer()
                    
                }//VStack
                
                
            }//ScrollView
            
            
            
            
        }//ZStack
        .onAppear{
            bankList()
            
            if (selectedUserType == "employee"){
                employeeInquiry(accessToken: loginData.token)
            }else{
                creatorInquiry(accessToken: loginData.token)
            }
            
            payload(accessToken: loginData.token, userIdx: selectedUserIdx)
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            
            self.presentationMode.wrappedValue.dismiss()
            
        }) {
            
            HStack(spacing: 0){
                
                Image(systemName: "chevron.backward")
                
            }
            
        })
        .navigationBarTitle("\(userNameFor_DetailEmployeeInfo) 급여 등록", displayMode: .inline)
        .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
            UIApplication.shared.endEditing()
            
        }
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
    }
    
    
    func loadCopyBankImageForAddUserSalary() {
        guard let selectedCopyBankImage = selectedCopyBankImage else { return }
        // You can do something with the selected brand image here
        isApiLoading = true
        
        uploadImage(image: selectedCopyBankImage, imageType: "bank_book")
    }
    
    //
    //Api 를통해 필요한 값 호출 (url, key 값)
    private func getApiCommonSendSalaryImageURL(imageType: String, completion: @escaping (String?, String?) -> Void) {

        let headers: HTTPHeaders = [
            "accept": "application/json",
            "Authorization": "Bearer \(loginData.token)"
        ]

        let parameters: Parameters = [
            "mimetype": "image/png",
            "type": "bank_book",
            "extension": "png"
        ]

        AF.request("\(defaultUrl)/api/common/image/url", method: .get, parameters: parameters, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    let url = JSON["data"] as? String
                    let key = JSON["key"] as? String
                    completion(url, key)
                }
            case .failure(let error):
                print(error)
                completion(nil, nil)
            }
        }
    }
    
      
    
    //사진 선택시 uploadImag 함수를 호출
    private func uploadImage(image: UIImage, imageType: String) {
        getApiCommonSendSalaryImageURL(imageType: imageType) { (url, key) in

            guard let preSignedUrl = url else {
                print("Failed to get pre-signed URL.")
                return
            }

            guard let imageData = image.pngData() else {
                print("Could not convert image to data.")
                return
            }

            AF.upload(imageData, to: url!, method: .put).response { response in
                switch response.result {
                case .success:
                    print("Image uploaded successfully.")

                    isApiLoading = false
                case .failure(let error):
                    print("Failed to upload image: \(error)")
                }
            }


            guard let imageData = image.jpegData(compressionQuality: 1.0) else {
                print("Could not convert image to data.")
                return
            }

            AF.upload(imageData, to: preSignedUrl).response { response in
                if let error = response.error {
                    print("Image upload failed with error: \(error)")
                } else {
                    print("Image uploaded successfully.")
                    switch imageType {
                    case "bank_book":
                        userCopyBankImageString = key!

                        print("\(userCopyBankImageString)")

                    default:
                        print("Unknown image type: \(imageType)")
                    }
                }
            }
        }
    }
    
    private func bankList() {
        
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        AF.request("\(defaultUrl)/admin/api/common/bank", method: .get, headers: headers).responseDecodable(of: BankListDataResponse.self) { response in
            switch response.result {
            case .success(let response):
                // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                bankNameList = response.data.map { $0.name }
                
                print(response.data)
                
                
            case .failure(let error):
                // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                print(error)
            }
        }
    }
    
    private func employeeInquiry(accessToken: String) {
        
        
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json"
        ]
        
        AF.request("\(defaultUrl)/admin/api/human-resource?user_idx=\(selectedUserIdx)&user_type=employee&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: EmployeeDataResponse.self) { response in
            switch response.result {
            case .success(let response):
                // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                for employeeData in response.data {
                    
                    userImageString = employeeData.userImage
                    userNameFor_DetailEmployeeInfo = employeeData.userName
                    userPositionFor_DetailEmployeeInfo = employeeData.position!
                    
                }
                
                print(response.data)
                
                
            case .failure(let error):
                // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                print(error)
            }
        }
    }
    
    func creatorInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/human-resource?user_idx=\(selectedUserIdx)&user_type=creator&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: CreatorDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
               
                    for creatorData in response.data {
                        userImageString = creatorData.userImage
                        userNameFor_DetailEmployeeInfo = creatorData.userName
                    }
                    
                    print(response.data)
                    

                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
        })
        
        
    }
    
    private func payload(accessToken: String, userIdx: Int) {
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json"
        ]
        
        AF.request("\(defaultUrl)/admin/api/human-resource-contract/latest?user_idx=\(userIdx)", method: .get, headers: headers).response { response in
            guard let responseData = response.data else {
                print("No data received.")
                return
            }
            
            let jsonDecoder = JSONDecoder()
            if let successResponse = try? jsonDecoder.decode(PayloadSuccessResponse.self, from: responseData) {
                print("Contract amount: \(successResponse.contract_amount)")
                
                salaryPriceForEmployee = String(successResponse.contract_amount)
                
            } else if let errorResponse = try? jsonDecoder.decode(PayloadErrorResponse.self, from: responseData) {
                print("Error message: \(errorResponse.err.errorMessage), Status: \(errorResponse.err.status)")
            } else {
                print("Unexpected response received.")
            }
        }
    }
    
    //최종적으로 등록
    private func addSalary() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any] = [
            
            "user_idx": selectedUserIdx,
            "amount": Int(salaryPriceForEmployee)!,
            "per_month": Int(salaryDayForEmployee)!,
            "bank_name": "\(bankNameList[selectedBank!])",
            "account_number": "\(bankNumber)",
            "account_copy": "\(userCopyBankImageString)"
        ]
        
        print(selectedUserIdx)
        print(Int(salaryPriceForEmployee)!)
        print(Int(salaryDayForEmployee)!)
        print(bankNameList[selectedBank!])
        print(bankNumber)
        print(userCopyBankImageString)
        
        
        AF.request("\(defaultUrl)/admin/api/salary",
                   method: .post,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                isApiLoading = true
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("급여 등록 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    
                    
                    print("급여 등록 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    isApiLoading = false
                    
                    salaryManagementMainView.creatorInquiry(accessToken: loginData.token)
                    salaryManagementMainView.empInquiry(accessToken: loginData.token)
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("급여 등록 실패: \(error)")
                
            }
        }
    }

}

protocol PayloadBaseResponse: Codable {
    var result: Bool { get }
}

struct PayloadSuccessResponse: PayloadBaseResponse {
    let result: Bool
    let contract_amount: Int
}

struct PayloadErrorDetails: Codable {
    let errorMessage: String
    let status: Int
}

struct PayloadErrorResponse: PayloadBaseResponse {
    let result: Bool
    let err: PayloadErrorDetails
}
