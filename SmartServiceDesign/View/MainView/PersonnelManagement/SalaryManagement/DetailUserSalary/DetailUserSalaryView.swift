//
//  DetailUserSalaryView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/21/24.
//

import SwiftUI
import Alamofire

struct DetailUserSalaryView: View {
    
    let salaryManagementMainView = SalaryManagementMainView()
    //Api
    var selectedUserIdx: Int
    var selectedUserType: String
    
    // 이 init 메소드를 통해 외부에서 selectedCustomerBrandIdx 값을 설정할 수 있습니다.
    init(selectedUserIdx: Int, selectedUserType: String) {
        self.selectedUserIdx = selectedUserIdx
        self.selectedUserType = selectedUserType
    }
    
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var userImageString: String = ""
    @State private var userAccountCopy: String = ""
    
    @State private var isApiLoading: Bool = false
    //View
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    
    
    @State private var navigate: Bool = false
    @State private var navigateContractDetailForDetailCustomerBrandView: Bool = false
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var modificationMode: Bool = false
    
    @State private var showingImagePickerFor_DetailEmployeeInfo = false
    @State private var selectedDetailEmployeeInfo: UIImage? = nil
    
    
    @State private var userNameFor_DetailSalaryInfo: String = ""
    
    @State private var userPositionFor_DetailEmployeeInfo: String = ""
    
    @State private var userAmount: Int = 0 //급여액
    @State private var userAmountString: String = "" //급여액 문자
    
    @State private var perMonth: Int = 0 //급여일
    @State private var perMonthString: String = "" //급여일 문자
    
    @State private var bankName: String = "" //은행 이름
    @State private var accountNumber: String = "" //통장 번호
    @State private var accountCopy: String = "" //통장 이미지
    
    @State private var salaryIdx: Int = 0 //급여 번호
    
    
    @State private var contractImageForDetailEmployeeInfoContractList: UIImage? = nil
    
    
    @State private var sendSalaryHistoryDataList = [SendSalaryHistoryData]()
    @State private var sendSalaryHistoryDataListCount: Int = 0
    
    @State private var showAllContractList: Bool = false
    @State private var showShowAllButton: Bool = false
    
    @State private var showingActionSheet = false
    @State private var selectedBank: Int? = nil
    @State private var bankNameList: [String] = []
    
    @State private var isShowingMemo: Bool = false
    
    var body: some View {
        
        ZStack {
            
            if (isApiLoading){
                
                ProgressView()
                    .zIndex(3)
                
            }
            
            ScrollView(.vertical){
                VStack(spacing: 0){
                    
                    
                    HStack{
                        
                        
                        VStack(alignment: .leading){// 직원 이미지
//                            ZStack{
//                                if modificationMode{
//                                    
//                                    Button(action: {
//                                        showingImagePickerFor_DetailEmployeeInfo = true
//                                    }, label: {
//                                        
//                                        ZStack {
//                                            if let image = selectedDetailEmployeeInfo {
//                                                Image(uiImage: image)
//                                                    .resizable()
//                                                    .aspectRatio(contentMode: .fill)
//                                                    .frame(width: screenWidth/11.363, height: screenHeight/24.696)
//                                                    .cornerRadius(12.0)
//                                                
//                                            } else {
//                                                AsyncImage(url: URL(string: imageS3Url + "/" + userImageString)) { image in
//                                                    image.resizable()
//                                                        .aspectRatio(contentMode: .fill)
//                                                        .frame(width: screenWidth/11.363, height: screenHeight/24.696)
//                                                        .cornerRadius(12.0)
//                                                }placeholder: {
//                                                    ProgressView()
//                                                        .frame(width: screenWidth/11.363, height: screenHeight/24.696)
//                                                }
//                                            }
//                                        }
//                                    })
//                                    
//                                }else{
//                                    
//                                    
//                                    
//                                }
//                            } //ZStack BrandImage
//                            .sheet(isPresented: $showingImagePickerFor_DetailEmployeeInfo, onDismiss: loadDetailEmployeeInfoImage) {
//                                ImagePicker(selectedImage: $selectedDetailEmployeeInfo)
//                            }
                            
                            ZStack {
                                if let image = selectedDetailEmployeeInfo {
                                    Image(uiImage: image)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                        .cornerRadius(12.0)
                                    
                                } else {
                                    AsyncImage(url: URL(string: imageS3Url + "/" + userImageString)) { image in
                                        image.resizable()
                                            .aspectRatio(contentMode: .fill)
                                            .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                            .cornerRadius(12.0)
                                    }placeholder: {
                                        ProgressView()
                                            .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                    }
                                    
                                }
                            }
                            
                        }//VStack 브랜드 이미지
                        
                        HStack(){
                            
                            HStack{
                                
                                //직원이름
                                Text("\(userNameFor_DetailSalaryInfo)")
                                    .font(.title2)
                                
//                                if modificationMode{
//                                    
//                                    TextField(".", text: $userNameFor_DetailSalaryInfo, prompt: Text("\(userNameFor_DetailSalaryInfo)")
//                                        .foregroundColor(Color("hintTextColor")))
//                                    .keyboardType(.default)
//                                    .font(.title2)
//                                    .frame(width: screenWidth/6.3)
//    //                                .padding(.leading, -screenWidth/150)
//                                    
//                                    
//                                }else {
//                                    
//                                   
//                                        .frame(width: screenWidth/6.3)
//                                }
                            }
                            HStack{
                                
                                
                                //직원 포지션
                                Text("\(userPositionFor_DetailEmployeeInfo)")
                                    .font(.title2)
                                    .frame(width: screenWidth/7.8125)//48
//                                if modificationMode{
//                                    
//                                    TextField(".", text: $userPositionFor_DetailEmployeeInfo, prompt: Text("\(userPositionFor_DetailEmployeeInfo)")
//                                        .foregroundColor(Color("hintTextColor")))
//                                    .keyboardType(.default)
//                                    .font(.title2)
//                                    .frame(width: screenWidth/7.8125)//48
//                                }else {
//
//                                }
                            }
                            
                            Spacer()
                            
                            if(modificationMode){
                                Button(action: {
                                    
                                    if (!isApiLoading){
                                        
                                        
                                        
                                        if (userAmountString == "" ||  perMonthString == "" || bankName == "" || accountNumber == "" || accountCopy == ""){
                                            
                                            showAlert = true
                                            
                                            alertTitle = "급여정보 수정 실패"
                                            alertMessage = "칸을 비울 수 없습니다."
                                            
                                        } else {
                                            
                                            if(selectedBank != nil){
                                                bankName = bankNameList[selectedBank!]
                                            }
                                            
                                            patchSalaryInfo(accessToken: loginData.token)
                                            
                                            salaryManagementMainView.creatorInquiry(accessToken: loginData.token)
                                            salaryManagementMainView.empInquiry(accessToken: loginData.token)
                                            
                                            modificationMode = false
                                        }
                                        
                                        
                                    }
                                    
                                    
                                    
                                }, label: {
                                    Text("저장")
                                    
                                })
                            } else {
                                Button(action: {
                                    
                                    modificationMode = true
                                    
                                    
                                }, label: {
                                    Text("수정")
                                    
                                })
                            }
                            
                        }
                        
                        
                    }
                    .padding(.horizontal)
                    .padding(.vertical, screenHeight / 81.5)
                    .padding(.top, screenHeight / 81.5)
                    
                    Divider()
                        .padding(.leading)
                   

                    HStack{
                        Text("분류")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if (selectedUserType == "employee"){
                            
                            Text("소속직원")
                                .foregroundColor(.white)
                                .bold()
                                .frame(width: screenWidth/4.6875, height: screenHeight/32.48)
                                .background(Color("mainColor"))
                                .cornerRadius(16.0)
                        }else{
                            
                            Text("크리에이터")
                                .foregroundColor(.white)
                                .bold()
                                .frame(width: screenWidth/4.6875, height: screenHeight/32.48)
                                .background(Color("mainColor"))
                                .cornerRadius(16.0)
                        }
                        
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    
                    HStack{ //급여액
                        Text("급여액")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            
                            TextField("", text: $userAmountString)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                            
                            Text("원")
                                .foregroundColor(Color("color00000040"))

                            
                            
//                            RightAlignedTextField(text: $departmentFor_DetailEmployeeInfo)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(userAmountString)")
                                .foregroundColor(Color("color00000040"))
                            
                            Text("원")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{ //급여일
                        Text("급여일")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                           
                            
                            TextField("", text: $perMonthString)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)

//                            RightAlignedTextField(text: $departmentFor_DetailEmployeeInfo)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(perMonthString)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        Text("일")
                            .foregroundColor(Color("color00000040"))
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{ //은행명
                        Text("은행명")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
//                            
//                            TextField("", text: $bankName)
//                                .multilineTextAlignment(.trailing)
//                                .foregroundColor(Color("color00000040"))
//                                .frame(height: screenHeight/40.5)
//                                .autocapitalization(.none)
//                            
                            
                            
                            Button(action: {
                                withAnimation {
                                    showingActionSheet = true
                                }
                            }) {
                                
                                Text("\(selectedBank != nil ? bankNameList[selectedBank!] : bankName)")
                                    .foregroundColor(Color("color00000040"))
                                
                            }
                            .actionSheet(isPresented: $showingActionSheet) {
                                ActionSheet(title: Text("은행 선택"), buttons: bankNameList.indices.map { i in
                                        .default(Text(bankNameList[i])) { selectedBank = i }
                                })
                            }

                            
                        }else {
                            
                            Text("\(bankName)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{ //통장번호
                        Text("통장번호")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            
                            TextField("", text: $accountNumber)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)

                            
                            
//                            RightAlignedTextField(text: $departmentFor_DetailEmployeeInfo)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(accountNumber)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{//통장사본 이미지 보기 버튼
                        Text("통장사본")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                    
                        Button {
                            print("통장사본 버튼 클릭")
                            
                            self.isShowingMemo = true
                            
                        } label: {
                            
                            Text("보기")
                                .foregroundColor(.white)
                                .bold()
                                .frame(width: screenWidth/6.1475, height: screenHeight/32.48)
                                .background(Color("mainColor"))
                                .cornerRadius(16.0)
                        
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    VStack(spacing: 0) {
                        HStack{
                            
                            Text("급여 이체 리스트")
                                .font(.title2)
                                .bold()
                                .padding()
                            //                                .padding(.top, -screenHeight/13.5)
                            Spacer()
                        }
                        .padding(0)
                        
                        Button(action: {
                            print("급여 추가 버튼 클릭됨")
                            
                            self.navigate = true
                            
                        }, label: {
                            
                            NavigationLink(destination: AddSendSalaryView(selectedSalaryIdx: salaryIdx, selectedUserName: userNameFor_DetailSalaryInfo, selectedUserIdx: selectedUserIdx, selectedUserType: selectedUserType), isActive: $navigate) {
                                EmptyView()
                            }
                            
                            Text("+  이체 내역 추가")
                                .foregroundColor(.white)
                                .font(.system(size: 16))
                                .bold()
                                .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        
                        
                        VStack{//계약 리스트 터치시
                            
                            ForEach(sendSalaryHistoryDataList.prefix(showAllContractList ? sendSalaryHistoryDataList.count : 3), id: \.salaryTransferIdx) { sendSalaryHistoryData in
                                
                                NavigationLink(destination: DetailSendSalaryHistory(selectedSalaryTransferIdx: sendSalaryHistoryData.salaryTransferIdx, selectedUserName: userNameFor_DetailSalaryInfo, selectedUserIdx: selectedUserIdx, selectedUserType: selectedUserType)) {
                                    
                                    VStack{
                                        HStack{ //리스트항목의 모델
                                            
                                            if (sendSalaryHistoryData.proofImage != "") {
                                                
                                                AsyncImage(url: URL(string: imageS3Url + "/" + sendSalaryHistoryData.proofImage)) { image in
                                                    image.resizable()
                                                         .aspectRatio(contentMode: .fill)
                                                         .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                         .cornerRadius(12.0)
                                                } placeholder: {
                                                    ProgressView()
                                                        .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                }
                                                
                                            } else {
                                                
                                                Rectangle()
                                                    .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                    .cornerRadius(12.0)
                                                    .foregroundColor(.colorE5E5E5)
                                            }
                                            
                                            VStack(alignment: .leading){
                                                Text("\(sendSalaryHistoryData.salaryTransferName)")
                                                    .foregroundColor(.black)
                                                
                                                Text("이체금액: \(sendSalaryHistoryData.transferAmount)")
                                                    .foregroundColor(Color("color00000040"))
                                                    .font(.caption)
                                            }
                                            
                                            Spacer()
                                            
                                            
                                            Text("\(sendSalaryHistoryData.transferStatus)")
                                                .foregroundColor(.white)
                                                .bold()
                                                .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                                .background(Color("mainColor"))
                                                .cornerRadius(16.0)
                                            
                                            
                                        }
                                        
                                        Divider()
                                            .padding(.leading, screenWidth/8)
                                    }
                                    
                                }//NavigationLink
                            }
                            
                            
                            
                            
                        } //VStack 계약 리스트
                        .listStyle(PlainListStyle())
                        .padding(.horizontal)
                        .padding(.top, screenHeight/50.9375)
                        
                        
                        if showShowAllButton{
                            
                            Button(action: {
                                
                                showShowAllButton = false
                                showAllContractList = true
                                
                            }, label: {
                                
                                Text("더 보기")
                                    .foregroundColor(.black)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                            })
                            .background(.white)
                            .cornerRadius(4)
                            .padding()
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color(.black), lineWidth: 1)
                                .padding()
                            )
                            
                        }
                        
                        Spacer()
                        
                        
                        
                        
                    }//VStack
                    .frame(width: screenWidth/1.0932)
                    .background(Color.white)
                    .cornerRadius(20)
                    .shadow(color: Color("color00000040"), radius: 20, x: 0, y: 30)
                    .padding(.top, screenHeight/31.346)
                    
                    //                    .sheet(isPresented: $showingBankRecordCopyImagePickerForDetailBrand, onDismiss: loadContractImageForDetailBrandContractList) {
                    //                        ImagePicker(selectedImage: $selectedBankRecordCopyImageForDetailBrand)
                    //                    }
                    
                    
                    
                    
                } //VStack
            }//ScrollView
            
            
            
                if isShowingMemo {
                    
                    ZStack{//메모 뷰
                            
                        VStack{
                            
                            ZStack {
                                
                                AsyncImage(url: URL(string: imageS3Url + "/" + userAccountCopy)) { image in
                                    image.resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: screenWidth/1.3, height: screenHeight/1.8)
                                        .cornerRadius(12.0)
                                }placeholder: {
                                    ProgressView()
                                        .frame(width: screenWidth/1.3, height: screenHeight/1.8)
                                }
                                
                                
                            }
                            
                            
                            
                            
                            Button(action: {
                                print("사진 닫기 버튼 클릭")
                                
                                self.isShowingMemo = false
                                
                            }, label: {
                                
                                Text("닫기")
                                    .foregroundColor(.white)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.3, height: screenHeight/20.375)
                            })
                            .background(Color("mainColor"))
                            .cornerRadius(4)
                            
                            
                        }
                        .frame(width: screenWidth/1.2, height: screenHeight/1.5)
                        .background(.white)
                        .cornerRadius(8.0)
                        
                        
                        //하단 네비게이션을 위한 패딩
                        HStack{
                            Rectangle()
                                .frame(width: 0, height: 0)
                                .foregroundColor(.white)
                        }
                        .padding(.bottom, screenHeight/28) //29
                        
                        
                    }//ZStack 메모
                        .transition(.move(edge: .leading))
                        .zIndex(2)
                    
                    // 메뉴 외부를 클릭하면 메뉴를 닫습니다.
                    Button(action: {
    //                    withAnimation {
                            self.isShowingMemo = false
    //                    }
                    }) {
                        Color.gray
                            .edgesIgnoringSafeArea(.all)
                            .opacity(0.9)
                    }
                    .zIndex(1)
                    
                }
            
            
        }//ZStack
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            
            self.presentationMode.wrappedValue.dismiss()
            
            salaryManagementMainView.empInquiry(accessToken: loginData.token)
            
        }) {
            HStack(spacing: 0){
                Image(systemName: "chevron.backward")
            }
        })
        .navigationBarTitle("\(userNameFor_DetailSalaryInfo) \(userPositionFor_DetailEmployeeInfo)", displayMode: .inline)
        .onAppear {
            
            bankList()
            
            if (selectedUserType == "employee"){
                employeeInquiry(accessToken: loginData.token)
            }else{
                creatorInquiry(accessToken: loginData.token)
            }
            
            userSalaryDeatil(accessToken: loginData.token)
            sendSalaryHistoryInquiry(accessToken: loginData.token)
            
        }
        
        
        
        
        
    }//body
    
    private func loadDetailEmployeeInfoImage() {
        guard let selectedDetailEmployeeInfo = selectedDetailEmployeeInfo else { return }
        // You can do something with the selected brand image here
        isApiLoading = true
        
        uploadImage(image: selectedDetailEmployeeInfo, imageType: "user")
    }
    
    private func userSalaryDeatil(accessToken: String) {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json"
        ]
        
        AF.request("\(defaultUrl)/admin/api/salary?user_idx=\(selectedUserIdx)&page=1&limit=10", method: .get, headers: headers).responseDecodable(of: UserSalaryDetailDataResponse.self) { response in
            switch response.result {
            case .success(let response):
                // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                for salaryDeatilData in response.data {
                    
                    
                    userAmountString = String(salaryDeatilData.amount)
                    perMonthString = String(salaryDeatilData.per_month)
                    bankName = salaryDeatilData.bank_name
                    accountNumber = salaryDeatilData.account_number
                    accountCopy = salaryDeatilData.account_copy
                    
                    salaryIdx = salaryDeatilData.salary_idx //수정시 필요한 급여 번호
                    //급여액
                    //급여일
                    //은행명
                    //통장번호
                    //통장사본
                    
                    userAccountCopy = salaryDeatilData.account_copy
                 
                }
                
                print(response.data)
                
                
            case .failure(let error):
                // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                print(error)
            }
        }
    }
    
    private func employeeInquiry(accessToken: String) {
        
        
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json"
        ]
        
        AF.request("\(defaultUrl)/admin/api/human-resource?user_idx=\(selectedUserIdx)&user_type=employee&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: EmployeeDataResponse.self) { response in
            switch response.result {
            case .success(let response):
                // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                for employeeData in response.data {
                    
                    userImageString = employeeData.userImage
                    userNameFor_DetailSalaryInfo = employeeData.userName
                    userPositionFor_DetailEmployeeInfo = employeeData.position!
                    
                }
                
                print(response.data)
                
                
            case .failure(let error):
                // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                print(error)
            }
        }
    }
    
    func creatorInquiry(accessToken: String) {
        
        
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json"
        ]
        
        AF.request("\(defaultUrl)/admin/api/human-resource?user_idx=\(selectedUserIdx)&user_type=creator&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: CreatorDataResponse.self) { response in
            switch response.result {
            case .success(let response):
                // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                
                for creatorData in response.data {
                    userImageString = creatorData.userImage
                    userNameFor_DetailSalaryInfo = creatorData.userName
                }
                
                print(response.data)
                
                
            case .failure(let error):
                // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                print(error)
            }
        }
        
        
        
    }
    
    //Api 를통해 필요한 값 호출 (url, key 값)
    private func getApiCommonEmployeeImageURL(imageType: String, completion: @escaping (String?, String?) -> Void) {
        
        let headers: HTTPHeaders = [
            "accept": "application/json",
            "Authorization": "Bearer \(loginData.token)"
        ]
        
        let parameters: Parameters = [
            "user_idx": selectedUserIdx,
            "mimetype": "image/png",
            "type": "user",
            "extension": "png"
        ]
        
        AF.request("\(defaultUrl)/api/common/image/url", method: .get, parameters: parameters, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    let url = JSON["data"] as? String
                    let key = JSON["key"] as? String
                    completion(url, key)
                }
            case .failure(let error):
                print(error)
                completion(nil, nil)
            }
        }
    }
    
    
    
    //사진 선택시 uploadImag 함수를 호출
    private func uploadImage(image: UIImage, imageType: String) {
        getApiCommonEmployeeImageURL(imageType: imageType) { (url, key) in
            
            guard let preSignedUrl = url else {
                print("Failed to get pre-signed URL.")
                return
            }
            
            guard let imageData = image.pngData() else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: url!, method: .put).response { response in
                switch response.result {
                case .success:
                    print("Image uploaded successfully.")
                    
                    isApiLoading = false
                case .failure(let error):
                    print("Failed to upload image: \(error)")
                }
            }
            
            
            guard let imageData = image.jpegData(compressionQuality: 1.0) else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: preSignedUrl).response { response in
                if let error = response.error {
                    print("Image upload failed with error: \(error)")
                } else {
                    print("Image uploaded successfully.")
                    switch imageType {
                    case "user":
                        userImageString = key!
                        
                        print("\(userImageString)")
                        
                    default:
                        print("Unknown image type: \(imageType)")
                    }
                }
            }
        }
    }
    //최종적으로 수정
    private func patchSalaryInfo(accessToken: String) {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any?] = [
            "amount": Int(userAmountString),
              "per_month": Int(perMonthString),
              "bank_name": "\(bankName)",
              "account_number": "\(accountNumber)",
              "account_copy": "\(accountCopy)"
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/salary/\(salaryIdx)",
                   method: .patch,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("급여정보 수정 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("급여정보 수정 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("급여정보 수정 실패: \(error)")
                
            }
        }
    }
    
    
    private func dateFormatter(getDate: String) -> String {
        
        let isoDateFormatter = ISO8601DateFormatter()
        isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

        let getDate = getDate
        if let date = isoDateFormatter.date(from: getDate) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy.MM.dd"
            let dateString = dateFormatter.string(from: date)
            print(dateString) // "2024.01.03" 출력
            return dateString
        } else {
            print("날짜 변환에 실패했습니다.")
            return ""
        }

    }
    
    private func bankList() {
        
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        AF.request("\(defaultUrl)/admin/api/common/bank", method: .get, headers: headers).responseDecodable(of: BankListDataResponse.self) { response in
            switch response.result {
            case .success(let response):
                // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                bankNameList = response.data.map { $0.name }
                
                print(response.data)
                
                
            case .failure(let error):
                // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                print(error)
            }
        }
    }
    
    //계약 가져오는 api
    func sendSalaryHistoryInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/salary-transfer?salary_idx=\(salaryIdx)&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: SendSalaryHistoryDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    
                    DispatchQueue.main.async {
                        sendSalaryHistoryDataList = response.data
                        
                        sendSalaryHistoryDataListCount = response.pagination.total
                        
                        print("계약 리스트 호출 성공 \(response.data)")
                        
                        if (sendSalaryHistoryDataListCount <= 3) { //
                            showShowAllButton = false
                            showAllContractList = false
                        }else{
                            showShowAllButton = true //더 보기 버튼 보임
                            showAllContractList = false
                        }
                    }
                    
                    
                    
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
            
        })
        
    }
    
}
