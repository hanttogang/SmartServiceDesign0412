//
//  AddSendSalaryView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 3/5/24.
//

import SwiftUI
import Alamofire

enum SendSalaryStatus: CaseIterable {
    case stand
    case complete
}

struct AddSendSalaryView: View {
    
    //Api
    var selectedSalaryIdx: Int
    var selectedUserName: String
    
    var selectedUserIdx: Int
    var selectedUserType: String
    var detailUserSalaryView: DetailUserSalaryView
    
    // 이 init 메소드를 통해 외부에서 selectedCustomerBrandIdx 값을 설정할 수 있습니다.
    init(selectedSalaryIdx: Int, selectedUserName: String, selectedUserIdx: Int, selectedUserType: String) {
        self.selectedSalaryIdx = selectedSalaryIdx
        self.selectedUserName = selectedUserName
        
        self.selectedUserIdx = selectedUserIdx
        self.selectedUserType = selectedUserType
        self.detailUserSalaryView = DetailUserSalaryView(selectedUserIdx: selectedUserIdx, selectedUserType: selectedUserType)
    }
    
    
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var isApiLoading: Bool = false
    
    //View
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var showToast: Bool = false
    @State private var toastText: String = "날짜 선택 후 확인을 눌러주세요"
    
    @State private var sendSalaryName: String = ""
    @State private var sendSalaryDate: String = ""
    @State private var sendSalaryPrice: String = ""
    @State private var sendSalaryMemo: String = ""
    
    @State private var sendSalaryStatusString: String = "대기"
    
    @State private var showingImagePickerEvidenceImage = false
    @State private var selectedEvidenceImage: UIImage? = nil
    @State private var selectedEvidenceImageString: String = ""
    
    @State private var sendSalaryStatus: SendSalaryStatus = SendSalaryStatus.stand
    
    //날짜 관련
    @State private var showingDatePicker = false
    @State private var dateSelection = Date()
//    @State private var selectDate: Date? = nil
//    @State private var endDate: Date? = nil
//    @State private var dateCount = 0
    
    
    @State private var navigate: Bool = false
    
    var body: some View {
        
        
        ZStack{
            
            if (isApiLoading){
                
                ProgressView()
                    .zIndex(3)
                
            }
            
            ScrollView{
                
                VStack(spacing: 0){
                    
                    
                    
                    
                    VStack{//이체명
                        
                        HStack(spacing: 0){
                            Text("이체명")
                                .font(.system(size: 14))
                            
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $sendSalaryName, prompt: Text("이체명을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 계약명
                    .padding(.top, screenHeight / 25.46875) //32
                    
                    VStack{ // 이체날짜
                        
                        HStack(spacing: 0){
                            Text("이체날짜")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        Button(action: {
                            
                            self.showingDatePicker = true
                            
                            toastText = "날짜 선택 후 확인을 눌러주세요"
                            showToast = true
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                self.showToast = false
                                toastText = ""
                            }
                            
                        }, label: {
                            HStack{
                                Text("\(sendSalaryDate)")
                                    .foregroundColor(Color("hintTextColor"))
                                .keyboardType(.emailAddress)
                                .font(.system(size: 14))
                                .padding(.leading, 14)
                                
                                Spacer()
                                
                            }
                            .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                            )
                        })
                        
                    }//VStack 이체날짜
                    .sheet(isPresented: $showingDatePicker) {
                        VStack {
                            
                                DatePicker("날짜를 선택해주세요", selection: $dateSelection, displayedComponents: .date)
                                    .datePickerStyle(GraphicalDatePickerStyle())
                                    .padding()
                                    .overlay{
                                        ZStack{}
                                        .toast(isShowing: $showToast, text: Text(toastText))
                                    }
                            
                            
                            
                            Button("확인") {
                                if (dateSelection != nil){
                                    sendSalaryDate = convertDateToString(date: dateSelection)
                                    
                                    self.showingDatePicker = false
                                }else{
                                    sendSalaryDate = ""
                                }
                            }
                        }
                    }
                    .padding(.top, screenHeight / 54.133) //15
                    
                    VStack{//이체 금액
                        
                        HStack(spacing: 0){
                            Text("이체금액")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $sendSalaryPrice, prompt: Text("이체금액을 입력해주세요")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.phonePad)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이체금액
                    .padding(.top, screenHeight / 54.133) //15
                   
                    
                    VStack{ // 메모
                        
                        HStack(spacing: 0){
                            Text("메모")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $sendSalaryMemo, prompt: Text("")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 메모
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    
                     
                    
                    VStack{ //이체상태
                        
                        HStack(spacing: 0){
                            Text("이체상태")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        
                        HStack(spacing: screenWidth / 125 ){ //SelectedBtn
                            
                            Button(action: {
                                
                                sendSalaryStatus = .stand
                                sendSalaryStatusString = "대기"
                                print("\(sendSalaryStatus)")
                                
                            }, label: {
                                Text("대기")
                                    .foregroundColor(sendSalaryStatus == .stand ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(sendSalaryStatus == .stand ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            Button(action: {
                                
                                sendSalaryStatus = .complete
                                sendSalaryStatusString = "완료"
                                print("\(sendSalaryStatus)")
                                
                            }, label: {
                                Text("완료")
                                    .foregroundColor(sendSalaryStatus == .complete ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(sendSalaryStatus == .complete ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            
                            Spacer()
                            
                            
                        }//HStack (SelectedBtn 을 위한 HStack)
                        .padding(.leading, screenWidth / 15.625)
                        
                    } //VStack 이체상태
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    
                    VStack(alignment: .leading){//증빙 이미지 등록
                        
                        HStack(spacing: 0){
                            Text("증빙 이미지 등록")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        
                        Button(action: {
                            showingImagePickerEvidenceImage = true
                        }, label: {
                            
                            ZStack {
                                if let image = selectedEvidenceImage {
                                    Image(uiImage: image)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                } else {
                                    Rectangle()
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                        .foregroundColor(.colorE5E5E5)
                                    
                                    Circle()
                                        .frame(width: screenWidth/10.416)
                                        .foregroundColor(.colorEFEFF4)
                                        .overlay{
                                            Image(systemName: "plus")
                                                .imageScale(.medium)
                                        }
                                }
                            }
                            
                        })
                        .padding(.leading, screenWidth / 18.75)
                        
                    }//VStack 계약서 등록 이미지
                    .padding(.top, screenHeight / 54.133) //20    54.133 = 15
                    .sheet(isPresented: $showingImagePickerEvidenceImage, onDismiss: loadContractImageForDetailEmployee) {
                        ImagePicker(selectedImage: $selectedEvidenceImage)
                    }
                    
                    HStack{
                        
                        Button(action: {
                            
                            if (!isApiLoading){
                                
                                if(selectedEvidenceImage == nil || sendSalaryName == "" || sendSalaryDate == "" || sendSalaryPrice == "" || sendSalaryStatusString == ""){
                                    
                                    showAlert = true
                                    
                                    alertTitle = "급여이체 내역 추가 실패"
                                    alertMessage = "칸을 비울 수 없습니다."
                                    
                                }else{
                                    
                                    addSendSalaryHistory()
                                    
                                    self.presentationMode.wrappedValue.dismiss()
                                    
//                                    detailEmployeeView.contractInquiry(accessToken: loginData.token)
                                }
                            }
                            
                            
                            
                        }, label: {
                            
                                Text("이체 내역 추가")
                                    .foregroundColor(.white)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/67.9166) // 37
                        
                        
                        
                    }
                    
                    //하단 네비게이션을 위한 패딩
                    HStack{
                        Rectangle()
                            .frame(width: 0, height: 0)
                            .foregroundColor(.white)
                    }
                    .padding(.bottom, screenHeight/8.826)
                    
                    
                    
                    
                    Spacer()
                    
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    
                    self.presentationMode.wrappedValue.dismiss()
                    
//                    detailEmployeeView.contractInquiry(accessToken: loginData.token)
                    
                }) {
                    
                    HStack(spacing: 0){
                        
                        Image(systemName: "chevron.backward")
                        
                    }
                    
                    
                    
                })
                .navigationBarTitle("\(selectedUserName)", displayMode: .inline)
                
                
            }//ScrollView
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
        }//ZStack
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        
    }//body
    
    func loadContractImageForDetailEmployee() {
        guard let selectedEvidenceImage = selectedEvidenceImage else { return }
        // You can do something with the selected brand image here
        isApiLoading = true
        
        uploadImage(image: selectedEvidenceImage, imageType: "bank_book")
    }
    
    
    //Api 를통해 필요한 값 호출 (url, key 값)
    private func getApiCommonSendSalaryImageURL(imageType: String, completion: @escaping (String?, String?) -> Void) {

        let headers: HTTPHeaders = [
            "accept": "application/json",
            "Authorization": "Bearer \(loginData.token)"
        ]

        let parameters: Parameters = [
            "mimetype": "image/png",
            "type": "bank_book",
            "extension": "png"
        ]

        AF.request("\(defaultUrl)/api/common/image/url", method: .get, parameters: parameters, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    let url = JSON["data"] as? String
                    let key = JSON["key"] as? String
                    completion(url, key)
                }
            case .failure(let error):
                print(error)
                completion(nil, nil)
            }
        }
    }
    
    //사진 선택시 uploadImag 함수를 호출
    private func uploadImage(image: UIImage, imageType: String) {
        getApiCommonSendSalaryImageURL(imageType: imageType) { (url, key) in
            
            guard let preSignedUrl = url else {
                print("Failed to get pre-signed URL.")
                return
            }
            
            guard let imageData = image.pngData() else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: url!, method: .put).response { response in
                switch response.result {
                case .success:
                    print("Image uploaded successfully.")
                    
                    isApiLoading = false
                case .failure(let error):
                    print("Failed to upload image: \(error)")
                }
            }
            
            
            guard let imageData = image.jpegData(compressionQuality: 1.0) else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: preSignedUrl).response { response in
                if let error = response.error {
                    print("Image upload failed with error: \(error)")
                } else {
                    print("Image uploaded successfully.")
                    switch imageType {
                    case "bank_book":
                        selectedEvidenceImageString = key!
                        
                        print("\(selectedEvidenceImageString)")
                        
                    default:
                        print("Unknown image type: \(imageType)")
                    }
                }
            }
        }
    }
    
    
//    최종적으로 등록
    private func addSendSalaryHistory() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any] = [
            
            "salary_idx": selectedSalaryIdx,
            "salary_transfer_name": "\(sendSalaryName)",
            "transfer_date": "\(sendSalaryDate)",
            "transfer_amount": Int(sendSalaryPrice)!,
            "memo": "\(sendSalaryMemo)",
            "transfer_status": "\(sendSalaryStatusString)",
            "proof_image": "\(selectedEvidenceImageString)"
            
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/salary-transfer",
                   method: .post,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("급여이체 내역 등록 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("급여이체 내역 등록 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    self.navigate = true
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("급여이체 내역 등록 실패: \(error)")
                
            }
        }
    }
    
    func convertDateToString(date: Date?) -> String {
        guard let date = date else {
            return ""
        }

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
    
}
//
//#Preview {
//    AddContractOfDetailEmployeeView()
//}
