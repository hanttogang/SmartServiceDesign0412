//
//  DetailSendSalaryHistory.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 3/5/24.
//

import SwiftUI
import Alamofire

struct DetailSendSalaryHistory: View {
    
    //Api
    var selectedSalaryTransferIdx: Int
    var selectedUserName: String
    
    var selectedUserIdx: Int
    var selectedUserType: String
    var detailUserSalaryView: DetailUserSalaryView
    
    // 이 init 메소드를 통해 외부에서 selectedCreatorContractIdx 값을 설정할 수 있습니다.
    init(selectedSalaryTransferIdx: Int, selectedUserName: String, selectedUserIdx: Int, selectedUserType: String) {
        self.selectedSalaryTransferIdx = selectedSalaryTransferIdx
        self.selectedUserName = selectedUserName
        
        self.selectedUserIdx = selectedUserIdx
        self.selectedUserType = selectedUserType
        self.detailUserSalaryView = DetailUserSalaryView(selectedUserIdx: selectedUserIdx, selectedUserType: selectedUserType)
        
    }
    
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var detailSendSalaryImageString = ""
    
    @State private var isApiLoading: Bool = false
    
    //View
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State private var navigate: Bool = false
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var showToast: Bool = false
    @State private var toastText: String = "시작 날짜 선택 후 확인을 눌러주세요"
    
    @State private var modificationMode: Bool = false
    
    
    @State private var detailSendSalaryName: String = ""
    @State private var detailSendSalaryDate: String = ""
    
    
    @State private var contractStartDateFor_DetailEmployeeContract: String = ""
    @State private var contractEndDateFor_DetailEmployeeContract: String = ""
    @State private var detailSendSalaryPrice: String = ""
    
    @State private var detailSendSalaryMemo: String = ""
    
    @State private var detailSendSalaryStatus: String = ""
    
    @State private var selectDetailSendSalaryStatus: Bool = false
    
    @State private var showingEvidenceImagePicker = false
    @State private var selectedEvidenceImage: UIImage? = nil
    
    //날짜 관련
    @State private var showingDatePicker = false
    @State private var dateSelection = Date()
//    @State private var startDate: Date? = nil
//    @State private var endDate: Date? = nil
    @State private var dateCount = 0
  
    
    var body: some View {
        
        ZStack {
            
            if (isApiLoading){
                
                ProgressView()
                    .zIndex(3)
                
            }
            
            ScrollView(.vertical){
                
                VStack(spacing: 0){
                    
                    HStack{
                        
                    
                        
                        if modificationMode{
                            //급여이체 내역 이름
                            TextField(".", text: $detailSendSalaryName, prompt: Text("\(detailSendSalaryName)")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.title2)
                            .frame(height: screenHeight/29) //28
                            
                            
                        }else {
                            
                            Text("\(detailSendSalaryName)")
                                .font(.title2)
                                .frame(height: screenHeight/29) //28
                        }
                        
                        
                        
                        
                        
                        Spacer()
                        
                        if(modificationMode){
                            
                            Button(action: {
                                
                                if (!isApiLoading){
                                    
                                    if (detailSendSalaryName == "" || detailSendSalaryDate == "" || detailSendSalaryPrice == "" || detailSendSalaryStatus == "" || detailSendSalaryImageString == ""){
                                        
                                        showAlert = true
                                        
                                        alertTitle = "소속직원 계약 수정 실패"
                                        alertMessage = "칸을 비울 수 없습니다."
                                        
                                    } else {
                                        
                                        if(detailSendSalaryMemo == "없음"){
                                            detailSendSalaryMemo = ""
                                        }
                                        
                                        patchEmployeeContract(accessToken: loginData.token)
                                        
                                        detailUserSalaryView.sendSalaryHistoryInquiry(accessToken: loginData.token)
                                        
                                        modificationMode = false
                                    }
                                }
                                
                            }, label: {
                                Text("저장")
                                
                            })
                        } else {
                            Button(action: {
                                
                                modificationMode = true
                                
                                
                            }, label: {
                                Text("수정")
                                
                            })
                            
                        }
                        
                        
                    }// HStack 급여 이체명 및 버튼
                    .padding(.horizontal)
                    .padding(.vertical, screenHeight / 81.5)
//                    .padding(.top, screenHeight / 81.5)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    
                    
                    
                    HStack{ //이체날짜
                        Text("이체날짜")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
//                            RightAlignedTextField(text: $typeOfBusinessForDetailBrand)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
                            
                            Button(action: {
                                
                                self.showingDatePicker = true
                                
                                toastText = "날짜 선택 후 확인을 눌러주세요"
                                showToast = true
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                    self.showToast = false
                                    toastText = ""
                                }
                                
                            }, label: {
                                HStack{
                                    Text("\(detailSendSalaryDate)")
                                        .foregroundColor(Color("color00000040"))
                                    
                                }
                            })

                            
                            
                        }else {
                            
                            Text("\(detailSendSalaryDate)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .sheet(isPresented: $showingDatePicker) {
                        VStack {
                            
                                DatePicker("날짜를 선택해주세요", selection: $dateSelection, displayedComponents: .date)
                                    .datePickerStyle(GraphicalDatePickerStyle())
                                    .padding()
                                    .overlay{
                                        ZStack{}
                                        .toast(isShowing: $showToast, text: Text(toastText))
                                    }
                            
                            
                            
                            Button("확인") {
                                if (dateSelection != nil){
                                    detailSendSalaryDate = convertDateToString(date: dateSelection)
                                    
                                    self.showingDatePicker = false
                                }else{
                                    detailSendSalaryDate = ""
                                }
                            }
                        }
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{//이체금액
                        Text("이체금액")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            
                            
                            TextField("", text: $detailSendSalaryPrice)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                                .keyboardType(.numberPad)
                                
                            
                            
//                            RightAlignedTextField(text: $detailSendSalaryPrice)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
//
                            
                            
                        }else {
                            
                            Text("\(detailSendSalaryPrice)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{ //메모
                        
                        Text("메모")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            TextField("", text: $detailSendSalaryMemo)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                                
                            
                            
//                            RightAlignedTextField(text: $detailSendSalaryMemo)
//                                .keyboardType(.numberPad)
                            
                            
                            
                        }else {
                            
                            Text("\(detailSendSalaryMemo)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack(spacing: 0){//진행상태
                        Text("진행상태")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        
                        HStack(spacing: 0){
                            
                            if modificationMode{
                                
                                
                                Button(action: {
                                    selectDetailSendSalaryStatus = true
                                }, label: {
                                    
                                    if selectDetailSendSalaryStatus{
                                        
                                        HStack(spacing: screenWidth/128.3){ // spacing 3
                                            Button(action: {
                                                selectDetailSendSalaryStatus = false
                                                
                                                detailSendSalaryStatus = "대기"
                                                
                                            }, label: {
                                                Text("대기")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selectDetailSendSalaryStatus = false
                                                
                                                detailSendSalaryStatus = "완료"
                                                
                                            }, label: {
                                                Text("완료")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                        } //HStack
                                        
                                        
                                    }else{
                                        
                                        Button(action: {
                                            selectDetailSendSalaryStatus = true
                                            
                                        }, label: {
                                            Text("\(detailSendSalaryStatus)")
                                                .foregroundColor(.white)
                                                .bold()
                                                .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                .background(Color("mainColor"))
                                                .cornerRadius(16.0)
                                        })
                                    }
                                    
                                })
                            } else{
                                
                                Text("\(detailSendSalaryStatus)")
                                    .foregroundColor(.white)
                                    .bold()
                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                    .background(Color("mainColor"))
                                    .cornerRadius(16.0)
                            }
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    Divider()
                        .padding(.leading)
                    
                   
                    
                    
                    
                    HStack(spacing: 0){
                        
                        
                        VStack(alignment: .leading){// 사업자등록증 이미지
                            
                            Text("증빙 이미지")
                                .padding(.top, screenHeight/30.75)
                            
                            ZStack{
                                
                                
                                if modificationMode{
                                    
                                    Button(action: {
                                        showingEvidenceImagePicker = true
                                    }, label: {
                                        
                                        ZStack {
                                            if let image = selectedEvidenceImage {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                    .cornerRadius(12.0)
                                                
                                            } else {
                                                AsyncImage(url: URL(string: imageS3Url + "/" + detailSendSalaryImageString)) { image in
                                                    image.resizable()
                                                         .aspectRatio(contentMode: .fill)
                                                         .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                         .cornerRadius(12.0)
                                                } placeholder: {
                                                    ProgressView()
                                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                }
                                                
                                            }
                                        }
                                    })
                                    
                                }else{
                                    
                                    ZStack {
                                        if let image = selectedEvidenceImage {
                                            Image(uiImage: image)
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                .cornerRadius(12.0)
                                            
                                        } else {
                                            AsyncImage(url: URL(string: imageS3Url + "/" + detailSendSalaryImageString)) { image in
                                                image.resizable()
                                                     .aspectRatio(contentMode: .fill)
                                                     .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                     .cornerRadius(12.0)
                                            } placeholder: {
                                                ProgressView()
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                            }
                                            
                                        }
                                    }
                                    
                                }
                            } //ZStack 증빙 이미지
                            .sheet(isPresented: $showingEvidenceImagePicker, onDismiss: loadEvidenceImage) {
                                ImagePicker(selectedImage: $selectedEvidenceImage)
                            }
                            
                            
                            
                        }//VStack 계약 이미지
                        
                        
                        //하단 네비게이션을 위한 패딩
                        HStack{
                            Rectangle()
                                .frame(width: 0, height: 0)
                                .foregroundColor(.white)
                        }
                        .padding(.bottom, screenHeight/8.826)
                        
                        
                        Spacer()
                        
                        
                      
                    }//HStack
                    .padding(.leading)
                    
                    Spacer()
                    
                } //VStack
                
            }//ScrollView
        }//ZStack
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            
            self.presentationMode.wrappedValue.dismiss()
            
            detailUserSalaryView.sendSalaryHistoryInquiry(accessToken: loginData.token)
            
        }) {
            HStack(spacing: 0){
                Image(systemName: "chevron.backward")
            }
        })
        .navigationBarTitle("\(selectedUserName)", displayMode: .inline)
        .onAppear{
            sendSalaryHistoryInquiry(accessToken: loginData.token)
        }
        
      
        
        
        
    }//body

    
  
    private func loadEvidenceImage() {
        guard let selectedEvidenceImage = selectedEvidenceImage else { return }
        
        isApiLoading = true
        
        uploadImage(image: selectedEvidenceImage, imageType: "human_resource_contract")
    }
    
    //Api 를통해 필요한 값 호출 (url, key 값)
    private func getApiCommonSendSalaryImageURL(imageType: String, completion: @escaping (String?, String?) -> Void) {
        
        let headers: HTTPHeaders = [
            "accept": "application/json",
            "Authorization": "Bearer \(loginData.token)"
        ]
        
        let parameters: Parameters = [
            "user_idx": selectedUserIdx,
            "mimetype": "image/png",
            "type": "user",
            "extension": "png"
        ]
        
        AF.request("\(defaultUrl)/api/common/image/url", method: .get, parameters: parameters, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    let url = JSON["data"] as? String
                    let key = JSON["key"] as? String
                    completion(url, key)
                }
            case .failure(let error):
                print(error)
                completion(nil, nil)
            }
        }
    }
    
    
    //사진 선택시 uploadImag 함수를 호출
    private func uploadImage(image: UIImage, imageType: String) {
        getApiCommonSendSalaryImageURL(imageType: imageType) { (url, key) in
            
            guard let preSignedUrl = url else {
                print("Failed to get pre-signed URL.")
                return
            }
            
            guard let imageData = image.pngData() else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: url!, method: .put).response { response in
                switch response.result {
                case .success:
                    print("Image uploaded successfully.")
                    
                    isApiLoading = false
                case .failure(let error):
                    print("Failed to upload image: \(error)")
                }
            }
            
            
            guard let imageData = image.jpegData(compressionQuality: 1.0) else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: preSignedUrl).response { response in
                if let error = response.error {
                    print("Image upload failed with error: \(error)")
                } else {
                    print("Image uploaded successfully.")
                    switch imageType {
                    case "human_resource_contract":
                        detailSendSalaryImageString = key!
                        
                        print("\(detailSendSalaryImageString)")
                        
                    default:
                        print("Unknown image type: \(imageType)")
                    }
                }
            }
        }
    }
    
    
    
    
    
    private func dateFormatter(getDate: String) -> String {
        
        let isoDateFormatter = ISO8601DateFormatter()
        isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

        let getDate = getDate
        if let date = isoDateFormatter.date(from: getDate) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateString = dateFormatter.string(from: date)
//            print(dateString) //ex "2024.01.05" 출력
            return dateString
        } else {
            print("날짜 변환에 실패했습니다.")
            return getDate
        }

    }
    
    
    
    //계약 가져오는 api
    func sendSalaryHistoryInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/salary-transfer?salary_transfer_idx=\(selectedSalaryTransferIdx)&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: SendSalaryHistoryDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    
                    for salaryHistoryData in response.data {
                        
                        detailSendSalaryName = salaryHistoryData.salaryTransferName
                        
                        detailSendSalaryDate = convertDateStringToStringFormat(dateString: salaryHistoryData.transferDate)

                        detailSendSalaryPrice = String(salaryHistoryData.transferAmount)
                        
                        if(salaryHistoryData.memo == ""){
                            detailSendSalaryMemo = "없음"
                        }else{
                            detailSendSalaryMemo = salaryHistoryData.memo
                        }
                        
                        detailSendSalaryStatus = salaryHistoryData.transferStatus
                        detailSendSalaryImageString = salaryHistoryData.proofImage
                        
                        
                        
                    }
                    
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
            
        })
        
    }
    
    //최종적으로 급여이체 기록 수정
    private func patchEmployeeContract(accessToken: String) {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any] = [
            "salary_transfer_name": "\(detailSendSalaryName)",
            "transfer_date": "\(detailSendSalaryDate)",
            "transfer_amount": Int(detailSendSalaryPrice)!,
            "memo": "\(detailSendSalaryMemo)",
            "transfer_status": "\(detailSendSalaryStatus)",
            "proof_image": "\(detailSendSalaryImageString)"
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/salary-transfer/\(selectedSalaryTransferIdx)",
                   method: .patch,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("급여이체 기록 수정 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("급여이체 기록 수정 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("급여이체 기록 수정 실패: \(error)")
                
                
            }
        }
    }
    
    
    
    func convertDateToString(date: Date?) -> String {
        guard let date = date else {
            return ""
        }

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
    
    func convertDateStringToStringFormat(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        guard let date = dateFormatter.date(from: dateString) else {
            print("Invalid date string")
            return ""
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    
}
