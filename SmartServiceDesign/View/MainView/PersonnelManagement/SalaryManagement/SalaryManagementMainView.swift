//
//  SalaryManagementMainView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/21/24.
//

import SwiftUI
import Alamofire

enum UserTypeInSalaryManagementMainView: String, CaseIterable, Identifiable {
    case employee = "employee"
    case creator = "creator"
    
    var id: Self { self }
}


struct SalaryManagementMainView: View {
    
    
    //Api 관련
    @State private var employeeList = [EmployeeData]()
    @State private var creatorList = [CreatorData]()
    @EnvironmentObject var loginData: LoginData
    @EnvironmentObject var menuViewModel: MenuViewModel
    
    @State var isShowingMenu: Bool = false
    
    
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    init() {
        let appearance = UINavigationBarAppearance()
        //                appearance.configureWithOpaqueBackground()
        //                appearance.backgroundColor = UIColor(.white) // 원하는 색상으로 변경하세요.
        appearance.shadowColor = nil // 이 부분을 추가하면 네비게이션 바 하단 선이 사라집니다.
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        
    }
    
    
    @State var isShowingImage: Bool = false
    
    @State private var searchEmployeeAndCreatorText: String = ""
    @State private var editEmployeeAndCreatorText: Bool = false
    @State private var selectedCreatorIdx: Int = 0
    
    @State private var userTypeText: String = "employee"
    
    @State private var selectedUserType: UserTypeInSalaryManagementMainView = UserTypeInSalaryManagementMainView.employee
    
    
    @State private var salaryManagementEmployeeIdx: Int? = nil
    @State private var salaryManagementCreatorIdx: Int? = nil
    
    @State private var existEmployeeSalaryInfo: Bool = false
    @State private var existCreatorSalaryInfo: Bool = false
    
    @State private var navigate: Bool = false
    
    var body: some View {
        
        ZStack{
            
            NavigationView{
                
                
                VStack{
                    
                    HStack(spacing: 0){
                        
                        Button(action: {
                            withAnimation {
                                self.isShowingImage = true
                            }
                        }) {
                            
                            HStack(spacing: 0){
                                Image("img_menu")
                            }
                            .padding(.leading)
                            
                        }
                        
                        Spacer()
                    }
                    
                    
                    HStack{
                        
                        Text("급여 관리")
                            .font(.title)
                            .bold()
                        
                        
                        Spacer()
                        
                    }
                    .padding(.leading, 16)
                    
                    
                    
                    HStack{
                        
                        HStack{
                            
                        }
                        .frame(width: screenWidth, height: screenHeight/14.5)
                        .overlay{
                            //검색창을 받을수있는 택스트필드
                            TextField("Search" , text : self.$searchEmployeeAndCreatorText)
                                .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                .foregroundColor(Color("hintTextColor"))
                            //hint와 태두리에 간격을 띄우기위해 15정도의 간격을주고
                                .padding(10)
                            //양옆은 추가로 15를 더줌
                                .padding(.leading, screenWidth/12.5) //30
                            //배경색상은 자유롭게선택
                                .background(Color(.white))
                            //검색창이 너무각지면 딱딱해보이기때문에 모서리를 둥글게
                            //숫자는 취향것
                                .cornerRadius(15)
                            //내가만든 검색창 상단에
                            //돋보기를 넣어주기위해
                            //오버레이를 선언
                                .overlay(
                                    //HStack을 선언하여
                                    //가로로 view를 쌓을수있도록 하나 만들고
                                    HStack{
                                        //맨오른쪽으로 밀기위해 Spacer()로 밀어준다.
                                        
                                        //xcode에서 지원해주는 이미지
                                        //                                            if !self.editText {
                                        //magnifyingglass 를 사용
                                        //색상은 자유롭게 변경가능
                                        Image(systemName: "magnifyingglass")
                                            .foregroundColor(Color("hintTextColor"))
                                            .padding()
                                        //                                            }
                                        
                                        
                                        
                                        Spacer()
                                        
                                        if self.editEmployeeAndCreatorText{
                                            //x버튼이미지를 클릭하게되면 입력되어있던값들을 취소하고
                                            //키입력 이벤트를 종료해야한다.
                                            Button(action : {
                                                self.editEmployeeAndCreatorText = false
                                                self.searchEmployeeAndCreatorText = ""
                                                //키보드에서 입력을 끝내게하는 코드
                                                UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                                            }){
                                                Image(systemName: "multiply.circle.fill")
                                                    .foregroundColor(Color("hintTextColor"))
                                                    .padding()
                                            }
                                        }
                                        
                                    }
                                ).onTapGesture {
                                    self.editEmployeeAndCreatorText = true
                                }
                                .padding(.horizontal, 21.945) //37
                            
                            
                            
                        }//overlay
                        
                        
                        
                        
                        
                    }//HStack
                    .background(Color("colorF8F8F892"))
                    
                    
                    HStack{

                        Button(action: {
                            if (salaryManagementEmployeeIdx != nil || salaryManagementCreatorIdx != nil){
                                    
                                self.navigate = true
                                
                            }else{
                                print("직원 또는 크리에이터를 선택하세요")
                            }

                        }, label: {
                            
                            NavigationLink(destination: getDestinationView(), isActive: $navigate) {
                                
                                Text("급여 등록 / 급여 상세")
                                    .foregroundColor(.white)
                                    .bold()
                                    .font(.system(size: 16))
                                    .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                                
                            }
                            
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/50.75)



                    }
                    
                    HStack(spacing: 0) {
                        ForEach(UserTypeInSalaryManagementMainView.allCases, id: \.self) { flavor in
                            Button(action: {
                                selectedUserType = flavor
                                userTypeText = "\(flavor)"
                                print("\(flavor)")
                            }) {
                                Text(flavor == .employee ? "소속직원" : "크리에이터")
                                    .frame(width: screenWidth/2.272, height: screenHeight/23.97)
                                    .font(.system(size: 16))
                                
                                    
                                
                            }
                            .background(selectedUserType == flavor ? Color.blue : Color.clear)
                            .foregroundColor(selectedUserType == flavor ? .white : .blue)
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("mainColor"), lineWidth: 1)
                            )
                            
                            
                            
                            
                        }
                    }
                    .overlay(RoundedRectangle(cornerRadius: 4)
                        .stroke(Color("mainColor"), lineWidth: 1)
                    )
                    .padding(.top)
                    

                    //                    NavigationLink(destination: DetailEmployeeSalaryView(selectedEmployeeUserIdx: employeeData.userIdx, selectedEmployeeHumanResourceIdx: employeeData.humanResourceIdx)){}
                    if (userTypeText == "employee"){
                        
                        ScrollView {
                            VStack(spacing: 0) {
                                
                                ForEach(
                                    
                                    employeeList.filter {
                                        searchEmployeeAndCreatorText.isEmpty || $0.userName.contains(searchEmployeeAndCreatorText) || $0.department.contains(searchEmployeeAndCreatorText)
                                        
                                    }, id: \.humanResourceIdx) { employeeData in
                                        
                                        Button {
                                            
                                            salaryManagementEmployeeIdx = employeeData.user.userIdx
                                            salaryManagementCreatorIdx = nil
                                            
                                            userSalaryInquiry(accessToken: loginData.token, userIdx: salaryManagementEmployeeIdx!)
                                            
                                        } label: {
                                            VStack {
                                                HStack {
                                                    AsyncImage(url: URL(string: imageS3Url + "/" + employeeData.userImage)) { image in
                                                        image.resizable()
                                                            .aspectRatio(contentMode: .fill)
                                                            .frame(width: screenWidth/6.25, height: screenHeight/13.583)
                                                            .cornerRadius(12.0)
                                                    } placeholder: {
                                                        ProgressView()
                                                            .frame(width: screenWidth/6.25, height: screenHeight/13.583)
                                                    }
                                                    
                                                    VStack(alignment: .leading){
                                                        Text(employeeData.userName)
                                                            .font(.title2)
                                                            .bold()
                                                            .foregroundColor(.black)
                                                        
                                                        Text(employeeData.department)
                                                            .foregroundColor(.black)
                                                        
                                                        Text("등록일: \(dateFormatter(getDate: employeeData.firstCreateDt))")
                                                            .foregroundColor(.gray)
                                                            .font(.caption)
                                                    }
                                                    
                                                    Spacer()
                                                    
                                                    Image(systemName: "chevron.right")
                                                        .padding()
                                                        .foregroundColor(.gray)
                                                }
                                                .padding(.bottom, screenWidth/17.857)
                                                .padding(.top, screenWidth/15.625)
                                                
                                                Divider()
                                            }//VStack
                                            .padding(.leading)
                                        }//Button
                                        .background(salaryManagementEmployeeIdx == employeeData.user.userIdx ? Color.blue.opacity(0.05) : Color.white) // 선택한 항목의 배경색 변경
                                        
                                        
                                    }
                            }//VStack
                        }//ScrollView
                        .onAppear{
                            empInquiry(accessToken: loginData.token)
                        }
                        
                    }else if( userTypeText == "creator"){
                        
                        
//                        NavigationLink(destination: DetailCreatorView(selectedCreatorUserIdx: creatorData.userIdx, selectedCreatorHumanResourceIdx: creatorData.humanResourceIdx)){}
                        ScrollView {
                            VStack(spacing: 0) {
                                
                                ForEach(
                                   
                                    creatorList.filter {
                                            searchEmployeeAndCreatorText.isEmpty || $0.userName.contains(searchEmployeeAndCreatorText) || $0.manageType!.contains(searchEmployeeAndCreatorText)
                                        
                                    }, id: \.humanResourceIdx) { creatorData in
                                        
                                        
                                        Button {
                                            
                                            salaryManagementCreatorIdx = creatorData.user.userIdx
                                            salaryManagementEmployeeIdx = nil
                                            
                                            userSalaryInquiry(accessToken: loginData.token, userIdx: salaryManagementCreatorIdx!)
                                        } label: {
                                            
                                            VStack {
                                                HStack {
                                                    AsyncImage(url: URL(string: imageS3Url + "/" + creatorData.userImage)) { image in
                                                        image.resizable()
                                                            .aspectRatio(contentMode: .fill)
                                                            .frame(width: screenWidth/6.25, height: screenHeight/13.583)
                                                            .cornerRadius(12.0)
                                                    } placeholder: {
                                                        ProgressView()
                                                            .frame(width: screenWidth/6.25, height: screenHeight/13.583)
                                                    }
                                                    
                                                    VStack(alignment: .leading){
                                                        Text(creatorData.userName)
                                                            .font(.title2)
                                                            .bold()
                                                            .foregroundColor(.black)
                                                        
                                                        Text("\(returnManageType(manageType: creatorData.manageType!))")
                                                            .foregroundColor(.black)
                                                        
                                                        Text("등록일: \(dateFormatter(getDate: creatorData.firstCreateDt))")
                                                            .foregroundColor(.gray)
                                                            .font(.caption)
                                                    }
                                                    
                                                    Spacer()
                                                    
                                                    Image(systemName: "chevron.right")
                                                        .padding()
                                                        .foregroundColor(.gray)
                                                }
                                                .padding(.bottom, screenWidth/17.857)
                                                .padding(.top, screenWidth/15.625)
                                                
                                                Divider()
                                                
                                                
                                                
                                                
                                            }//VStack
                                            .padding(.leading)
                                        }//Button
                                        .background(salaryManagementCreatorIdx == creatorData.user.userIdx ? Color.blue.opacity(0.05) : Color.white) // 선택한 항목의 배경색 변경
                                        
                                    }
                            }//VStack
                        }//ScrollView
                        .onAppear{
                            creatorInquiry(accessToken: loginData.token)
                        }
                        
                    }//else if userTypeText = creator
                    
                    
                    
                    //하단 네비게이션을 위한 패딩
                    HStack{
                        Rectangle()
                            .frame(width: 0, height: 0)
                            .foregroundColor(.white)
                    }
                    .padding(.bottom, screenHeight/28) //29
                    
                    
                    Spacer()
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                //                    .navigationBarItems(leading: Button(action: {
                //
                //                        withAnimation {
                //                            self.isShowingImage = true
                //                        }
                //
                //                    }) {
                //
                //                        HStack(spacing: 0){
                //
                //                            Image("img_menu")
                //
                //                        }
                //
                //
                //
                //
                //                    })
                
                
                
                
                
                
                
                
                
                
            }//NavigationView
            //    .toast(isShowing: $showToast, text: Text(toastText))
            .navigationBarHidden(true)
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
            
            //하단 네비게이션 및 채팅 웹뷰
            CustomBottomNavigationBar()

            // isShowingImage 상태에 따라 메뉴 여닫기 zIndex 으로 조정
            if isShowingImage {
                
                MenuView()
                    .transition(.move(edge: .leading))
                    .zIndex(2)
                
                // 메뉴 외부를 클릭하면 메뉴를 닫습니다.
                Button(action: {
                    withAnimation {
                        self.isShowingImage = false
                    }
                }) {
                    Color.gray
                        .edgesIgnoringSafeArea(.all)
                        .opacity(0.5)
                }
                .zIndex(1)
                
            }
            
        }//ZStack
        
    } //body
    
    
    
    
    
    
    func empInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/human-resource?user_type=employee&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: EmployeeDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
               
                    print(response.data)
                    

                    DispatchQueue.main.async {
                        employeeList = response.data
                    }
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
            
        })
        
       
    }
    
    func creatorInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/human-resource?user_type=creator&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: CreatorDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
               
                    print(response.data)
                    

                    DispatchQueue.main.async {
                        creatorList = response.data
                    }
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
        })
        
        
    }
    
    func userSalaryInquiry(accessToken: String, userIdx: Int) {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/salary?user_idx=\(userIdx)&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: UserSalaryDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
               
                    print(response.data)
                    
                    if !response.data.isEmpty{
                        existEmployeeSalaryInfo = true
                        existCreatorSalaryInfo = true
                        print("userSalaryInquiry is not Empty")
                    }else{
                        print("userSalaryInquiry is Empty")
                        existEmployeeSalaryInfo = false
                        existCreatorSalaryInfo = false
                    }
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
        
    }
    
    private func dateFormatter(getDate: String) -> String {
        
        let isoDateFormatter = ISO8601DateFormatter()
        isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

        let getDate = getDate
        if let date = isoDateFormatter.date(from: getDate) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy.MM.dd"
            let dateString = dateFormatter.string(from: date)
            return dateString
        } else {
            print("날짜 변환에 실패했습니다.")
            return ""
        }

    }
    
    func returnManageType(manageType: String) -> String {
        switch manageType {
        case "marketing":
            return "광고마케팅"
        case "mcn":
            return "MCN"
        case "etc":
            return "기타"
        default:
            return ""
        }
    }
    
    func getDestinationView() -> AnyView {
        if salaryManagementEmployeeIdx != nil {
            if existEmployeeSalaryInfo {
                //사원 선택 및 급여 정보 있는 경우
                return AnyView(DetailUserSalaryView(selectedUserIdx: salaryManagementEmployeeIdx!, selectedUserType: userTypeText))
            } else {
                //사원 선택 및 급여 정보 없는 경우
                return AnyView(AddSalaryOfUserView(selectedUserIdx: salaryManagementEmployeeIdx!, selectedUserType: userTypeText))
            }
        } else if salaryManagementCreatorIdx != nil {
            if existCreatorSalaryInfo {
                //크리에이터 선택 및 급여 정보 있는 경우
                return AnyView(DetailUserSalaryView(selectedUserIdx: salaryManagementCreatorIdx!, selectedUserType: userTypeText))
            } else {
                //크리에이터 선택 및 급여 정보 없는 경우
                return AnyView(AddSalaryOfUserView(selectedUserIdx: salaryManagementCreatorIdx!, selectedUserType: userTypeText))
            }
        }
        return AnyView(EmptyView()) // 기본 반환 값
    }
    
}
