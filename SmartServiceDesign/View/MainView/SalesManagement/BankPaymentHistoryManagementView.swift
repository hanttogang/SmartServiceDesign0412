//
//  BankPaymentHistoryManagementView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/6/24.
//

import SwiftUI
import Alamofire

struct BankPaymentHistoryManagementView: View {
    
    
    //API 관련
    @State private var bankDataList = [BankData]()
    @EnvironmentObject var loginData: LoginData
    @EnvironmentObject var menuViewModel: MenuViewModel
    
    @State var isShowingMenu: Bool = false
    
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    //View 관련
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    init() {
        let appearance = UINavigationBarAppearance()
        //                appearance.configureWithOpaqueBackground()
        //                appearance.backgroundColor = UIColor(.white) // 원하는 색상으로 변경하세요.
        appearance.shadowColor = nil // 이 부분을 추가하면 네비게이션 바 하단 선이 사라집니다.
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        
    }
    
    //날짜 관련
    let today = Date()
    let calendar = Calendar.current
    
    @State private var showingDatePicker = false
    @State private var dateSelection = Date()
    @State private var startDate: Date? = nil
    @State private var endDate: Date? = nil
    @State private var dateCount = 0
    
    @State private var startDateFor_BankPaymentHistoryManagement: String = ""
    @State private var endDateFor_BankPaymentHistoryManagement: String = ""
    
    @State private var showToast: Bool = false
    @State private var toastText: String = "시작 날짜 선택 후 확인을 눌러주세요"
    
    
    
    var body: some View {
        
        ZStack{
            
            NavigationView{
                
                
                VStack{
                    
                    HStack(spacing: 0){
                        
                        Button(action: {
                            withAnimation {
                                isShowingMenu = true
                            }
                        }) {
                            
                            HStack(spacing: 0){
                                Image("img_menu")
                            }
                            .padding(.leading)
                            
                        }
                        
                        Spacer()
                    }
                    
                    
                    HStack{
                        
                        Text("은행 거래 내역 관리")
                            .font(.title)
                            .bold()
                        
                        
                        Spacer()
                        
                    }
                    .padding(.leading, 16)
                    
                    
                    
                    HStack{
                        
                        VStack{ // 선택 날짜
                            
                            Button(action: {
                                
                                startDate = nil
                                endDate = nil
                                
                                self.showingDatePicker = true
                                
                                toastText = "시작 날짜 선택 후 확인을 눌러주세요"
                                
                                showToast = true
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                    self.showToast = false
                                    toastText = ""
                                }
                                
                            }, label: {
                                HStack{
                                    if (startDateFor_BankPaymentHistoryManagement != "" && endDateFor_BankPaymentHistoryManagement != ""){
                                        
                                        Text("\(transformDateString(startDateFor_BankPaymentHistoryManagement)) ~ \(transformDateString(endDateFor_BankPaymentHistoryManagement))")
                                            .foregroundColor(Color("hintTextColor"))
                                            .font(.system(size: 14))
                                            .padding(.leading, 14)
                                    }else{
                                        
                                        let startDate = calendar.date(byAdding: .month, value: -1, to: today)!
                                        let endDate = today
                                        
                                        Text("\(initConvertDateToString(date: startDate)) ~ \(initConvertDateToString(date: endDate))")
                                            .foregroundColor(Color("hintTextColor"))
                                        .font(.system(size: 14))
                                        .padding(.leading, 14)
                                    }
                                    
                                    
                                    Spacer()
                                    
                                    Image(systemName: "calendar")
                                        .padding(.trailing)
                                    
                                }
                                .frame(height: screenHeight / 18.45)
                                .overlay(RoundedRectangle(cornerRadius: 4)
                                    .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                                )
                                
                                
                            })
                            .padding()
                            
                        }//VStack 계약기간
                        .sheet(isPresented: $showingDatePicker) {
                            VStack {
                                
                                DatePicker("날짜를 선택해주세요", selection: $dateSelection, displayedComponents: .date)
                                    .datePickerStyle(GraphicalDatePickerStyle())
                                    .padding()
                                    .overlay{
                                        ZStack{}
                                            .toast(isShowing: $showToast, text: Text(toastText))
                                    }
                                
                                
                                
                                Button("확인") {
                                    if self.dateCount == 0 {
                                        self.startDate = dateSelection
                                        self.dateCount += 1
                                    } else if self.dateCount == 1 {
                                        if dateSelection > self.startDate! {
                                            self.endDate = dateSelection
                                            self.dateCount = 0
                                        } else {
                                            self.startDate = nil
                                            self.endDate = nil
                                            self.dateCount = 0
                                        }
                                    }
                                    
                                    
                                    if (startDate != nil && endDate == nil){
                                        
                                        toastText = "종료 날짜 선택 후 확인을 주세요"
                                        showToast = true
                                        
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                            self.showToast = false
                                            toastText = ""
                                        }
                                        
                                        self.showingDatePicker = true
                                    }
                                    
                                    
                                    if (startDate != nil && endDate != nil){
                                        startDateFor_BankPaymentHistoryManagement = convertDateToString(date: startDate)
                                        endDateFor_BankPaymentHistoryManagement = convertDateToString(date: endDate)
                                        
                                        self.showingDatePicker = false
                                    }else{
                                        startDateFor_BankPaymentHistoryManagement = ""
                                        endDateFor_BankPaymentHistoryManagement = ""
                                    }
                                }
                            }
                        }
                        
                        
                        
                        
                        
                    }//HStack
                    .background(Color("colorF8F8F892"))
                    
                    
                    
                    
                    ScrollView {
                        VStack(spacing: 0) {
                            
                            
                            if (startDateFor_BankPaymentHistoryManagement == "" || endDateFor_BankPaymentHistoryManagement == ""){
                                let startDate = calendar.date(byAdding: .month, value: -1, to: today)!
                                let endDate = today
                                
                                let filteredBankDataList = bankDataList.filter { bankData in
                                    if let approvalDate = dateFromString(bankData.transactionDate) {
                                        return startDate <= approvalDate && approvalDate <= endDate
                                    }
                                    return false
                                }
                                
                                ForEach(filteredBankDataList, id: \.self) { bankData in
                                    
                                    VStack(alignment: .leading){
                                        HStack(spacing: 0){
                                            VStack(alignment: .leading, spacing: 8){
                                                
                                                HStack{
                                                    Text("계좌번호")
                                                 
                                                }
                                                
                                                HStack{
                                                    Text("금액")
                                                    
                                                }
                                                HStack{
                                                    Text("발행일자")
                                                    
                                                }
                                               
                                                
                                            }
                                            .padding(.leading)
                                            .padding(.vertical)
                                            
                                            
                                            
                                            VStack(spacing: 8){
                                                
                                              
                                                
                                                
                                                HStack{
                                                    
                                                    Text("\(bankData.accountNumber)")
                                                        .bold()
                                                    
                                                    Spacer()
                                                }
                                                
                                                
                                                HStack{
                                                    
                                                    if(bankData.transactionType == "입금"){
                                                        
                                                        Text("\(formatMoney(bankData.deposit))" + "원")
                                                            .bold()
                                                        Text("(입금)")
                                                            .foregroundColor(.red)
                                                    }else{
                                                        
                                                        Text("\(formatMoney(bankData.withdraw))" + "원")
                                                            .bold()
                                                        Text("(출금)")
                                                            .foregroundColor(.red)
                                                    }
                                                    Spacer()
                                                }
                                                
                                                
                                                HStack{
                                                    
                                                    Text("\(transformDateString(bankData.transactionDate))")
                                                        .bold()
                                                    
                                                    Text("\(formatTime(time: bankData.transactionTime))")
                                                    
                                                    Spacer()
                                                }
                                                
                                                
                                            }
                                            .padding()
                                           
                                        }
                                        
                                        
                                        
                                    }
                                    .overlay(RoundedRectangle(cornerRadius: 4)
                                        .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                                    )
                                    .padding()
                                    
                                }
                            }else{
                                
                                let startDate = dateFromString(startDateFor_BankPaymentHistoryManagement)!
                                let endDate = dateFromString(endDateFor_BankPaymentHistoryManagement)!
                                
                                let filteredBankDataList = bankDataList.filter { bankData in
                                    if let approvalDate = dateFromString(bankData.transactionDate) {
                                        return startDate <= approvalDate && approvalDate <= endDate
                                    }
                                    return false
                                }
                                
                                ForEach(filteredBankDataList, id: \.self) { bankData in
                                    
                                    VStack(alignment: .leading){
                                        HStack(spacing: 0){
                                            VStack(alignment: .leading, spacing: 8){
                                               
                                                
                                                HStack{
                                                    Text("계좌번호")
                                                 
                                                }
                                                
                                                HStack{
                                                    Text("금액")
                                                    
                                                }
                                                HStack{
                                                    Text("발행일자")
                                                    
                                                }
                                               
                                                
                                            }
                                            .padding(.leading)
                                            .padding(.vertical)
                                            
                                            
                                            
                                            VStack(spacing: 8){
                                                
                                               
                                                
                                                
                                                
                                                HStack{
                                                    
                                                    Text("\(bankData.accountNumber)")
                                                        .bold()
                                                    
                                                    Spacer()
                                                }
                                                
                                                
                                                HStack{
                                                    
                                                    if(bankData.transactionType == "입금"){
                                                        
                                                        Text("\(formatMoney(bankData.deposit))" + "원")
                                                            .bold()
                                                        Text("(입금)")
                                                            .foregroundColor(.red)
                                                    }else{
                                                        
                                                        Text("\(formatMoney(bankData.withdraw))" + "원")
                                                            .bold()
                                                        Text("(출금)")
                                                            .foregroundColor(.red)
                                                    }
                                                    Spacer()
                                                }
                                                
                                                
                                                HStack{
                                                    
                                                    Text("\(transformDateString(bankData.transactionDate))")
                                                        .bold()
                                                    
                                                    Text("\(formatTime(time: bankData.transactionTime))")
                                                    
                                                    Spacer()
                                                }
                                                
                                                
                                            }
                                            .padding()
                                           
                                        }
                                        
                                        
                                        
                                    }
                                    .overlay(RoundedRectangle(cornerRadius: 4)
                                        .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                                    )
                                    .padding()
                                    
                                }
                            }
                            
                            
                            
                            
                            
                        }
                    }
                    .onAppear {
                        bankPaymentHistoryInquiry(accessToken: loginData.token)
                    }
                    
                    //하단 네비게이션을 위한 패딩
                    HStack{
                        Rectangle()
                            .frame(width: 0, height: 0)
                            .foregroundColor(.white)
                    }
                    .padding(.bottom, screenHeight/28) //29
                    
                    
                    
                    Spacer()
                    
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                
            }//NavigationView
            //    .toast(isShowing: $showToast, text: Text(toastText))
            .navigationBarHidden(true)
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
            
            
            //하단 네비게이션 및 채팅 웹뷰
            CustomBottomNavigationBar()
            
            // menuViewModel.sideMenuView 상태에 따라 메뉴 여닫기 zIndex 으로 조정
            if isShowingMenu {
                
                MenuView()
                    .transition(.move(edge: .leading))
                    .zIndex(2)
                
                // 메뉴 외부를 클릭하면 메뉴를 닫습니다.
                Button(action: {
                    withAnimation {
                        isShowingMenu = false
                    }
                }) {
                    Color.gray
                        .edgesIgnoringSafeArea(.all)
                        .opacity(0.5)
                }
                .zIndex(1)
                
            }
            
        }//ZStack
        
    }
    
    
    
    
    func bankPaymentHistoryInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/sales/account?start_date=20000101&end_date=20991231", method: .get, headers: headers).responseDecodable(of: BankPaymentHistoryDataResponse.self) { response in
                
                switch response.result {
                    
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    
                    print(response.data)
                    
                    //                brandIdx = brandData.brandIdx
                    DispatchQueue.main.async {
                        bankDataList = response.data
                    }
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
            
        })
        
        
    }
    
    private func dateFormatter(getDate: String) -> String {
        
        let isoDateFormatter = ISO8601DateFormatter()
        isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
        
        let getDate = getDate
        if let date = isoDateFormatter.date(from: getDate) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy.MM.dd"
            let dateString = dateFormatter.string(from: date)
            print(dateString) // "2024.01.03" 출력
            return dateString
        } else {
            print("날짜 변환에 실패했습니다.")
            return ""
        }
        
    }
    
    
    
    
    func convertDateToString(date: Date?) -> String {
        guard let date = date else {
            return ""
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        return formatter.string(from: date)
    }
    
    func dateFromString(_ dateString: String, format: String = "yyyyMMdd") -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: dateString)
    }
    
    func transformDateString(_ date: String) -> String {
        let year = date.prefix(4)
        let month = date.dropFirst(4).prefix(2)
        let day = date.suffix(2)
        
        return "\(year)-\(month)-\(day)"
    }
    
    func formatMoney(_ number: Int) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        return formatter.string(from: NSNumber(value: number)) ?? ""
    }
    
    func formatBusinessNumber(_ number: String) -> String {
        var number = number
        number.insert("-", at: number.index(number.startIndex, offsetBy: 3))
        number.insert("-", at: number.index(number.startIndex, offsetBy: 6))
        return number
    }
    
    func formatTime(time: String) -> String {
        let hours = String(time.prefix(2))
        let minutes = String(time.dropFirst(2).prefix(2))
        let seconds = String(time.suffix(2))
        
        return "\(hours):\(minutes):\(seconds)"
    }
    func initConvertDateToString(date: Date?) -> String {
        guard let date = date else {
            return ""
        }

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
}
