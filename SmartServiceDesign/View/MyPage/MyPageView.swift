//
//  MyPageView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/7/24.
//

import SwiftUI
import Alamofire

struct MyPageView: View {
    
    
    //Api
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var contractImageStringFor_DetailContractBrand = ""
    
    @State private var isApiLoading: Bool = false
    
    @State private var password: String = ""
    @State private var checkPassword: String = ""
    
    
    //날짜 완련
    @State private var showingDatePicker = false
    @State private var dateSelection = Date()
    @State private var startDate: Date? = nil
    @State private var endDate: Date? = nil
    @State private var dateCount = 0
    
    
    
    
    
    //View
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    var selectedDetailCustomerName: String = ""
    
    @State private var navigate: Bool = false
    
    @State private var showToast: Bool = false
    @State private var toastText: String = ""
    
    @State private var modificationMode: Bool = false
    @State private var passwordModificationMode: Bool = false
    
    
    @State private var userName: String = ""
    @State private var userEmail: String = ""
    @State private var userPhoneNumber: String = ""
    @State private var userType: String = ""
    
    
    @State private var showAlert: Bool = false
    
    @State private var myPageAlertTitle: String = ""
    @State private var myPageAlertMessage: String = ""
    
    @EnvironmentObject var menuViewModel: MenuViewModel
    @State var isShowingMenu: Bool = false
    
    var body: some View {
        
        ZStack {
            
            VStack(spacing: 0){
                
                HStack(spacing: 0){
                    
                    if loginData.userType != "master"{
                        
                        HStack(spacing: 0){
                            Image("img_menu")
                        }
                        .padding(.leading)
                    }else{
                        Button(action: {
                            withAnimation {
                                self.isShowingMenu = true
                            }
                        }) {
                            
                            HStack(spacing: 0){
                                Image("img_menu")
                            }
                            .padding(.leading)
                            
                        }
                    }
                    
                    Spacer()
                }
                
                HStack{
                    Text("마이페이지")
                        .font(.title)
                    
                    Spacer()
                }
                .padding()
                
                HStack{
                    
                    
                    
                    if modificationMode{
                        
                        TextField(".", text: $userName, prompt: Text("\(userName)")
                            .foregroundColor(Color("hintTextColor")))
                        .keyboardType(.default)
                        .font(.title2)
                        
                        
                    }else {
                        
                        Text("\(userName)")
                            .font(.title2)
                    }
                    
                    
                    
                    
                    
                    Spacer()
                    
                    if(modificationMode){
                        Button(action: {
                            
                            
                            
                            if (userName == "" || userPhoneNumber == ""){
                                
                                print("저장 버튼 클릭되었으나 수정 x")
                                
                                myPageAlertTitle = "회원 정보 수정 실패"
                                myPageAlertMessage = "칸을 비울 수 없습니다."
                                
                                self.showAlert = true
                                
                                
                                
                            } else {
                                
                                modificationMode = false
                                
                                patchUserInfo(accesToken: loginData.token)
                                
                            }
                            
                            
                        }, label: {
                            Text("저장")
                            
                        })
                    } else {
                        Button(action: {
                            
                            modificationMode = true
                            
                            
                        }, label: {
                            Text("수정")
                            
                        })
                        
                    }
                    
                    
                }// HStack 계약 이름
                .padding(.horizontal)
                .padding(.vertical, screenHeight / 81.5)
                //                    .padding(.top, screenHeight / 81.5)
                
                
                Divider()
                    .padding(.leading)
                
                
                HStack{
                    
                    Text("이메일")
                        .padding(.vertical, screenHeight / 81.5)
                    
                    Spacer()
                    
                    Text("\(userEmail)")
                        .multilineTextAlignment(.trailing)
                        .foregroundColor(Color("color00000040"))
                        .frame(height: screenHeight/40.5)
                        .autocapitalization(.none)
                    
                }
                .padding(.horizontal)
                
                
                Divider()
                    .padding(.leading)
                
                HStack{
                    Text("전화번호")
                        .padding(.vertical, screenHeight / 81.5)
                    
                    Spacer()
                    
                    if modificationMode{
                        
                        
                        TextField("", text: $userPhoneNumber)
                            .multilineTextAlignment(.trailing)
                            .foregroundColor(Color("color00000040"))
                            .frame(height: screenHeight/40.5)
                            .autocapitalization(.none)
                            .keyboardType(.numberPad)
                        
                    }else {
                        
                        Text("\(userPhoneNumber)")
                            .foregroundColor(Color("color00000040"))
                    }
                    
                    
                    
                }
                .padding(.horizontal)
                
                Divider()
                    .padding(.leading)
                
                HStack{
                    
                    Text("회원분류")
                        .padding(.vertical, screenHeight / 81.5)
                    
                    Spacer()
                    
                    Text("\(userType)")
                        .multilineTextAlignment(.trailing)
                        .bold()
                        .foregroundColor(.white)
                        .padding(.horizontal)
                        .padding(.vertical, 2)
                        .background(.blue)
                        .cornerRadius(16.0)
                    
                }
                .padding(.horizontal)
                
                
                Divider()
                    .padding(.leading)
                
                
                VStack(spacing: 0){
                    HStack{
                        Text("비밀번호")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if passwordModificationMode{
                            
                            Button(action: {
                                
                                
                                if(password == "" || checkPassword == ""){
                                    
                                    showAlert = true
                                    
                                    print("칸을 비울 수 없습니다.")
                                    
                                    myPageAlertTitle = "비밀번호 변경 실패"
                                    myPageAlertMessage = "칸을 비울 수 없습니다."
                                    
                                }else if !validatePassword(password: password){
                                    
                                    showAlert = true
                                    print("비밀번호 형식을 확인해주세요.")
                                    
                                    myPageAlertTitle = "비밀번호 변경 실패"
                                    myPageAlertMessage = "비밀번호 형식을 확인해주세요."
                                    
                                }else if(password != checkPassword){
                                    
                                    showAlert = true
                                    print("비밀번호를 확인해주세요")
                                    
                                    myPageAlertTitle = "비밀번호 변경 실패"
                                    myPageAlertMessage = "비밀번호를 확인해주세요"
                                    
                                }else{
                                    
                                    passwordModificationMode = false
                                    
                                    //patchPassword
                                    patchPassword(accessToken: loginData.token, password: password)
                                }
                                
                            }, label: {
                                Text("저장")
                                
                            })
                            
                        }else {
                            
                            Button(action: {
                                
                                passwordModificationMode = true
                                
                                
                            }, label: {
                                Text("변경")
                                
                            })
                            
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                }
                
                
                
                if passwordModificationMode{
                    
                    
                    VStack{
                        HStack(spacing: 0){
                            Text("비밀번호")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading)
                        
                        HStack{
                            SecureField(".", text: $password, prompt: Text("비밀번호를 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.09, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        .padding(0)
                        
                        
                        
                        HStack(spacing: 0){
                            Text("비밀번호 재입력")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading)
                        
                        HStack{
                            SecureField(".", text: $checkPassword, prompt: Text("비밀번호를 다시 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.09, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }
                    
                    
                    
                    
                }else{
                    Divider()
                        .padding(.leading)
                }
                
                //하단 네비게이션을 위한 패딩
                HStack{
                    Rectangle()
                        .frame(width: 0, height: 0)
                        .foregroundColor(.white)
                }
                .padding(.bottom, screenHeight/28) //29
                
                Spacer()
                
                
            } //VStack
//
            
//            .toast(isShowing: $showToast, text: Text(toastText))
            
            CustomBottomNavigationBar()
            
            
            // isShowingMenu 상태에 따라 메뉴 여닫기 zIndex 으로 조정
            if isShowingMenu {
                
                MenuView()
                    .transition(.move(edge: .leading))
                    .zIndex(2)
                
                // 메뉴 외부를 클릭하면 메뉴를 닫습니다.
                Button(action: {
                    withAnimation {
                        self.isShowingMenu = false
                    }
                }) {
                    Color.gray
                        .edgesIgnoringSafeArea(.all)
                        .opacity(0.5)
                }
                .zIndex(1)
                
            }
            
            
            
            
        }//ZStack
//        .alert(isPresented: $showAlert) {
//                    Alert(title: Text("Important message"),
//                          message: Text("Wear sunscreen"),
//                          dismissButton: .default(Text("Got it!")))
//                }
//
//        .alert(isPresented: $showAlert, content: ) {
//            Alert(title: Text(myPageAlertTitle), message: Text(myPageAlertMessage), dismissButton: .default(Text("확인"), action: {
//                print("알림창 확인 버튼이 클릭되었다.")
//                showAlert = false
//                
//            }))
//        }
//        
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(myPageAlertTitle), message: Text(myPageAlertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                myPageAlertTitle = ""
                myPageAlertMessage = ""
            }))
        })
       
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            
            self.presentationMode.wrappedValue.dismiss()
            
        }) {
            
            HStack(spacing: 0){
                
                Image(systemName: "chevron.backward")
                
            }
            
            
            
        })
        .navigationBarTitle("\(selectedDetailCustomerName) 계약 상세", displayMode: .inline)
        //        .onAppear {
        //            contractInquiry()
        //        }
        .background(.white)
        .onAppear{
            userInfoSet()
        }
        .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
            UIApplication.shared.endEditing()
            
        }
        .onAppear{
            menuViewModel.selectView.send("myPage")
        }
        
        
    }//body
    
    func validatePassword(password: String) -> Bool {
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*\\d)(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: password)
    }
 
    
    
    //최종적으로 수정
    private func patchUserInfo(accesToken: String) {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accesToken)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any?] = [
            "user_name": userName,
             "user_phone": userPhoneNumber,
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/user",
                   method: .patch,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("회원정보 수정 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("회원정보 수정 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("스케줄 수정 실패: \(error)")
                
            }
        }
    }
    
    private func userInfoSet(){
        userName = loginData.userName
        userEmail = loginData.userEmail
        userPhoneNumber = loginData.userPhoneNumber
        userType = loginData.userType
    }
    
    
    //최종적으로 과업 수정
    private func patchPassword(accessToken: String, password: String) {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json"
        ]
        
        
        let parameters: [String: Any] = [
            "user_pwd": password
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/user/password",
                   method: .patch,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("비밀번호 수정 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("비밀번호 수정 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("비밀번호 수정 실패: \(error)")
                
            }
        }
    }
    
}

//extension MyPageView {
//    
//    private func createAlert(title: String, message: String){
//        Alert(title: Text(title), message: Text(message), dismissButton: .default(Text("확인"), action: {
//            print("알림창 확인 버튼이 클릭되었다.")
//        }))
//    }
//    
//}


//#Preview {
//    MyPageView()
//}
