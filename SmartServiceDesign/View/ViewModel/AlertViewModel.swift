//
//  AlertViewModel.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/13/24.
//

import Foundation

class AlertViewModel: ObservableObject {
    @Published var isPresented: Bool = false
    @Published var title: String = ""
    @Published var message: String = ""

    func show(title: String, message: String) {
        self.title = title
        self.message = message
        isPresented = true
    }

    func hide() {
        isPresented = false
    }
}
