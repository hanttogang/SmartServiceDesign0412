//
//  MenuViewModel.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 2/26/24.
//

import Foundation
import Combine


class MenuViewModel: ObservableObject {
    
//    var sideMenuView = PassthroughSubject<Bool, Never>()
    
    @Published var sideMenuBool: Bool = false
//
//    var myPageView = PassthroughSubject<Bool, Never>()
//    
//    var chatView = PassthroughSubject<Bool, Never>()
    
    var selectView = PassthroughSubject<String, Never>()

}
