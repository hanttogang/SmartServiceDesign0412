//
//  SaveApiInfo.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/30/24.
//

import Foundation


class SaveApiInfo: ObservableObject {
    
    @Published var currentIdx: Int = 0
    @Published var currentTitleName: String = ""
    
}
