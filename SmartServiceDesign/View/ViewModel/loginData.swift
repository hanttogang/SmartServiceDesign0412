//
//  loginData.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/11/23.
//

import Foundation

class LoginData: ObservableObject {
    
    
    @Published var isLogin: Bool = false
    @Published var token: String = ""
    @Published var userIdx: Int = 0
    
    @Published var userName: String = ""
    @Published var userEmail: String = ""
    @Published var userType: String = ""
    @Published var userPhoneNumber: String = ""
    
    @Published var userData: UserData?
    
    
}
